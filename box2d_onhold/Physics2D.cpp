#include "stdafx.h"
#include "Physics2D.h"
#include "components/Transform.h"
#include "components/PhysicsBody2D.h"
#include <World.h>

namespace Tremor
{
    Physics2D::Physics2D()
        : Tecs::System(Tecs::Aspect::createAspectForAll<PhysicsBody2D, Transform>()),
        m_b2world(b2Vec2(0.f, 0.f)),
        m_velocityIterations(5),
        m_positionIterations(5),
        m_physicsRatio(1.f),
        m_plane(PhysicsPlane::XY),
        m_intPositions(false),
        m_circlecastInitialized(false)
    {
        m_b2world.SetContactListener(this);
    }

    Physics2D::Physics2D(const glm::vec2& gravity)
        : Tecs::System(Tecs::Aspect::createAspectForAll<PhysicsBody2D, Transform>()),
        m_b2world(b2Vec2(gravity.x, gravity.y)),
        m_velocityIterations(5),
        m_positionIterations(5),
        m_physicsRatio(1.f),
        m_plane(PhysicsPlane::XY),
        m_intPositions(false),
        m_circlecastInitialized(false)
    {
        m_b2world.SetContactListener(this);
    }

    Physics2D::~Physics2D()
    {
        
    }

    void Physics2D::removed(unsigned int entity)
    {
        PhysicsBody2D& body = m_world->getComponentManager().getComponent<PhysicsBody2D>(entity);
        body.removeFromPhysics(*this);
    }

    void Physics2D::inserted(unsigned int entity)
    {
        PhysicsBody2D& body = m_world->getComponentManager().getComponent<PhysicsBody2D>(entity);
        body.addToPhysics(*this);
    }

    void Physics2D::begin()
    {
        if (!m_circlecastInitialized)
        {
            createCirclecastEntities();
            m_circlecastInitialized = true;
        }
    }

    void Physics2D::end()
    {

    }

    void Physics2D::setToXY()
    {
        m_plane = PhysicsPlane::XY;
    }

    void Physics2D::setToXZ()
    {
        m_plane = PhysicsPlane::XZ;
    }

    void Physics2D::setToYX()
    {
        m_plane = PhysicsPlane::YX;
    }

    void Physics2D::setToYZ()
    {
        m_plane = PhysicsPlane::YZ;
    }

    void Physics2D::setToZX()
    {
        m_plane = PhysicsPlane::ZX;
    }

    void Physics2D::setToZY()
    {
        m_plane = PhysicsPlane::ZY;
    }

    bool Physics2D::isXY()
    {
        return m_plane == PhysicsPlane::XY;
    }

    bool Physics2D::isXZ()
    {
        return m_plane == PhysicsPlane::XZ;
    }

    bool Physics2D::isYX()
    {
        return m_plane == PhysicsPlane::YX;
    }

    bool Physics2D::isYZ()
    {
        return m_plane == PhysicsPlane::YZ;
    }

    bool Physics2D::isZX()
    {
        return m_plane == PhysicsPlane::ZX;
    }

    bool Physics2D::isZY()
    {
        return m_plane == PhysicsPlane::ZY;
    }

    PhysicsPlane Physics2D::getPlane()
    {
        return m_plane;
    }

    void Physics2D::setPlane(PhysicsPlane plane)
    {
        m_plane = plane;
    }

    void Physics2D::processEntities(const std::vector<unsigned int>& entities, float delta)
    {
        // Step the Box2D world
        m_b2world.Step(delta, m_velocityIterations, m_positionIterations);

        // Clear circle casts
        for (unsigned int i = 0; i < m_circlecastBodies.size(); ++i)
        {
            m_circlecastBodies[i]->removeFromPhysics(*this);
            for (auto itr = m_collisionPairs.begin(); itr != m_collisionPairs.end(); ++itr)
            {
                if (&(*itr)->m_body1 == m_circlecastBodies[i] ||
                    &(*itr)->m_body2 == m_circlecastBodies[i])
                {
                    m_collisionPairs.erase(itr);
                    break;
                }
            }
        }
        m_circleCasts.clear();

        // Call listeners' onCollision()
        for (auto itr = m_collisionPairs.begin(); itr != m_collisionPairs.end(); ++itr)
        {
            PhysicsBody2D::Listener* l1 = (*itr)->m_body1.getListener();
            if (l1 != nullptr)
            {
                l1->onCollision((*itr)->m_body2);
            }
            PhysicsBody2D::Listener* l2 = (*itr)->m_body2.getListener();
            if (l2 != nullptr)
            {
                l2->onCollision((*itr)->m_body1);
            }
        }
        

        // Update the transforms
        for (unsigned int i = 0; i < entities.size(); ++i)
        {
            Transform& transform = m_world->getComponentManager().getComponent<Transform>(entities[i]);
            PhysicsBody2D& body = m_world->getComponentManager().getComponent<PhysicsBody2D>(entities[i]);
            
            glm::vec3 position = transform.getPosition();
            glm::quat rotation = transform.getRotation();
            glm::vec3 euler = glm::eulerAngles(rotation);
            float px = body.getX() * m_physicsRatio;
            float py = body.getY() * m_physicsRatio;
            
            float angle = body.getb2Body().GetAngle();

            switch (m_plane)
            {
            case XY:
                position.x = px;
                position.y = py;
                euler.z = angle;
                break;
            case XZ:
                position.x = px;
                position.z = py;
                euler.y = angle;
                break;
            case YX:
                position.y = px;
                position.x = py;
                euler.z = angle;
                break;
            case YZ:
                position.y = px;
                position.z = py;
                euler.x = angle;
                break;
            case ZX:
                position.z = px;
                position.x = py;
                euler.y = angle;
                break;
            case ZY:
                position.z = px;
                position.y = py;
                euler.x = angle;
                break;
            }

            transform.setPosition(position);
            transform.setRotation(glm::quat(euler));
        }
    }

    int Physics2D::getVelocityIterationCount()
    {
        return m_velocityIterations;
    }

    void Physics2D::setVelocityIterationCount(int velocityIterations)
    {
        m_velocityIterations = velocityIterations;
    }

    int Physics2D::getPositionIterationCount()
    {
        return m_positionIterations;
    }

    void Physics2D::setPositionIterationCount(int positionIterations)
    {
        m_positionIterations = positionIterations;
    }

    b2World& Physics2D::getb2World()
    {
        return m_b2world;
    }

    float Physics2D::getPhysicsRatio()
    {
        return m_physicsRatio;
    }

    void Physics2D::setPhysicsRatio(float ratio)
    {
        m_physicsRatio = ratio;
    }

    void Physics2D::useIntegerPositions(bool use)
    {
        m_intPositions = use;
    }

    void Physics2D::BeginContact(b2Contact* contact)
    {
        PhysicsBody2D* body1 = (PhysicsBody2D*) contact->GetFixtureA()->GetBody()->GetUserData();
        PhysicsBody2D* body2 = (PhysicsBody2D*) contact->GetFixtureB()->GetBody()->GetUserData();

        if (body1 != nullptr && body2 != nullptr)
        {
            if (body1->getb2Body().GetType() == b2BodyType::b2_dynamicBody)
            {
                PhysicsBody2D::Listener* l1 = body1->getListener();
                if (l1 != nullptr) l1->onCollisionStart(*body2);
            }

            if (body2->getb2Body().GetType() == b2BodyType::b2_dynamicBody)
            {
                PhysicsBody2D::Listener* l2 = body2->getListener();
                if (l2 != nullptr) l2->onCollisionStart(*body1);
            }

            m_collisionPairs.insert(std::unique_ptr<CollisionPair>(new CollisionPair(*body1, *body2)));
        }
    }

    void Physics2D::EndContact(b2Contact* contact)
    {
        PhysicsBody2D* body1 = (PhysicsBody2D*) contact->GetFixtureA()->GetBody()->GetUserData();
        PhysicsBody2D* body2 = (PhysicsBody2D*) contact->GetFixtureB()->GetBody()->GetUserData();
        
        if (body1 != nullptr && body2 != nullptr)
        {
            if (body1->getb2Body().GetType() == b2BodyType::b2_dynamicBody)
            {
                PhysicsBody2D::Listener* l1 = body1->getListener();
                if (l1 != nullptr) l1->onCollisionEnd(*body2);
            }

            if (body2->getb2Body().GetType() == b2BodyType::b2_dynamicBody)
            {
                PhysicsBody2D::Listener* l2 = body2->getListener();
                if (l2 != nullptr) l2->onCollisionEnd(*body1);
            }

            for (auto itr = m_collisionPairs.begin(); itr != m_collisionPairs.end(); ++itr)
            {
                if ((&(*itr)->m_body1 == body1 && &(*itr)->m_body2 && body2) ||
                    (&(*itr)->m_body1 == body2 && &(*itr)->m_body2 && body1))
                {
                    m_collisionPairs.erase(itr);
                    break;
                }
            }
        }
    }

    void Physics2D::circleCast(float x, float y, float radius, std::function<void(const PhysicsBody2D&)>& callback)
    {
        m_circleCasts.emplace_back(new CircleCast(*this, *m_circlecastBodies[m_nextCirclecastBody], x, y, radius, callback));

        m_nextCirclecastBody++;
        m_nextCirclecastBody %= m_circlecastBodies.size();
    }

    void Physics2D::circleCast(float x, float y, float radius, const std::string& luaCallbackName)
    {
        m_circleCasts.emplace_back(new CircleCast(*this, *m_circlecastBodies[m_nextCirclecastBody], x, y, radius, luaCallbackName));

        m_nextCirclecastBody++;
        m_nextCirclecastBody %= m_circlecastBodies.size();
    }

    void Physics2D::createCirclecastEntities()
    {
        for (unsigned int i = 0; i < 10; ++i)
        {
            // Tecs::Entity& entity = m_world->createEntity();
            unsigned int entity = m_world->createEntity().getId();
            PhysicsBody2D& body = m_world->getComponentManager().addComponent<PhysicsBody2D>(entity);
            m_circlecastBodies.push_back(&body);
        }
        m_nextCirclecastBody = 0;
    }
}