#pragma once

#include <Component.h>
#include <box2d/Box2D.h>
#include <algorithm>
#include <vector>

namespace Tremor
{
    class Physics2D;

    class PhysicsBody2D : public Tecs::Component
    {
    public:
        struct FixtureInfo
        {
        public:
            b2Shape* shape;
            b2FixtureDef def;

            FixtureInfo(float x, float y, float radius, bool sensor)
            {
                shape = new b2CircleShape();
                shape->m_radius = radius;
                def.shape = shape;
                def.isSensor = sensor;
            }

            FixtureInfo(float x, float y, float width, float height, bool sensor)
            {
                shape = new b2PolygonShape();
                ((b2PolygonShape*) shape)->SetAsBox(width / 2.f, height / 2.f, b2Vec2(x, y), 0.f);
                def.shape = shape;
                def.isSensor = sensor;
            }
        };

        class Listener
        {
        public:
            virtual void onCollisionStart(const PhysicsBody2D& other) {}
            virtual void onCollision(const PhysicsBody2D& other) {}
            virtual void onCollisionEnd(const PhysicsBody2D& other) {}
        };

        PhysicsBody2D();

        ~PhysicsBody2D();

        // void setAsBox(Physics2D& physics, float width, float height, bool dynamic);
        // void setAsCircle(Physics2D& physics, float radius, bool dynamic);

        void addCircle(float x, float y, float radius, bool sensor);
        void addRectangle(float x, float y, float width, float height, bool sensor);

        b2Body& getb2Body();
        b2BodyDef& getb2BodyDef();

        float getX();
        float getY();

        void setEnabled(bool enabled);
        bool isEnabled();


        void applyForce(float x, float y);
        void setVelocity(float x, float y);
        void setPosition(float x, float y);
        glm::vec2 getPosition();

        PhysicsBody2D::Listener* getListener();
        void setListener(PhysicsBody2D::Listener* listener);

        void setUserData(void* userData);
        void* getUserData();

        void setDynamic(bool dynamic);
        bool isDynamic();

        void setSensor(bool sensor);
        bool isSensor();

        void removeFromPhysics(Physics2D& physics);
        void addToPhysics(Physics2D& physics);

        const std::vector<FixtureInfo>& getFixtureInfos();
    private:
        std::vector<FixtureInfo> m_fixtureInfos;
        bool m_dynamic;
        bool m_addedToWorld;
        bool m_enabled;
        b2BodyDef m_bodyDef;
        b2Shape* m_shape;
        b2FixtureDef m_fixtureDef;
        b2Body* m_body;
        PhysicsBody2D::Listener* m_listener;
        void* m_userData;


    };
}