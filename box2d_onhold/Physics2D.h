#pragma once

#include <System.h>
#include <box2d/Box2D.h>
#include "components/PhysicsBody2D.h"
#include <unordered_set>

namespace Tremor
{
    enum PhysicsPlane
    {
        XY,
        XZ,
        YX,
        YZ,
        ZX,
        ZY
    };

    struct CircleCast : public PhysicsBody2D::Listener
    {
    public:
        
        std::function<void(const PhysicsBody2D&)> m_callback;

        CircleCast(Physics2D& physics, PhysicsBody2D& body, float x, float y, float radius, std::function<void(const PhysicsBody2D&)>& callback)
            : m_callback(callback)
        {
            // body.setAsCircle(physics, radius, true);
            body.addCircle(0.f, 0.f, radius, true);
            body.setDynamic(true);
            body.setPosition(x, y);
            body.setListener(this);
        }

        CircleCast(Physics2D& physics, PhysicsBody2D& body, float x, float y, float radius, const std::string& luaCallbackName)
        {
            // body.setAsCircle(physics, radius, true);
            body.addCircle(0.f, 0.f, radius, true);
            body.setDynamic(true);
            body.setPosition(x, y);
            body.setListener(this);
            m_callback = [](const PhysicsBody2D& other)
            {
                std::cout << "Should call the Lua callback now." << std::endl;
            };
        }

        virtual void onCollisionStart(const PhysicsBody2D& other)
        {
            if (!m_callback._Empty())
            {
                m_callback(other);
            }
        }
    };

    class Physics2D : public Tecs::System, b2ContactListener
    {
    public:
        Physics2D();
        Physics2D(const glm::vec2& gravity);
        ~Physics2D();

        int getVelocityIterationCount();
        void setVelocityIterationCount(int velocityIterations);
        int getPositionIterationCount();
        void setPositionIterationCount(int positionIterations);

        b2World& getb2World();

        float getPhysicsRatio();
        void setPhysicsRatio(float ratio);

        void setToXY();
        void setToXZ();
        void setToYX();
        void setToYZ();
        void setToZX();
        void setToZY();

        bool isXY();
        bool isXZ();
        bool isYX();
        bool isYZ();
        bool isZX();
        bool isZY();

        PhysicsPlane getPlane();
        void setPlane(PhysicsPlane plane);

        void useIntegerPositions(bool use);

        void circleCast(float x, float y, float radius, std::function<void(const PhysicsBody2D&)>& callback);
        void circleCast(float x, float y, float radius, const std::string& luaCallbackName);
    protected:
        virtual void begin();
        virtual void end();
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);
        virtual bool shouldProcess() { return true; }

        virtual void removed(unsigned int entity);
        virtual void inserted(unsigned int entity);
    private:
        void BeginContact(b2Contact* contact);
        void EndContact(b2Contact* contact);

        bool m_circlecastInitialized;
        PhysicsPlane m_plane;
        b2World m_b2world;
        
        int m_velocityIterations;
        int m_positionIterations;
        float m_physicsRatio;

        bool m_intPositions;

        struct CollisionPair
        {
        public:
            CollisionPair(PhysicsBody2D& body1, PhysicsBody2D& body2)
                : m_body1(body1),
                m_body2(body2)
            {

            }
            PhysicsBody2D& m_body1;
            PhysicsBody2D& m_body2;
        };

        std::unordered_set<std::unique_ptr<CollisionPair>> m_collisionPairs;

        void createCirclecastEntities();
        std::vector<PhysicsBody2D*> m_circlecastBodies;
        unsigned int m_nextCirclecastBody;
        std::vector<std::unique_ptr<CircleCast>> m_circleCasts;
    };
}