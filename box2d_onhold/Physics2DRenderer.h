#pragma once

#include <box2d/Box2D.h>
#include "components/PhysicsBody.h"
#include "systems/Physics.h"
#include "Camera.h"

namespace Tremor
{
    class Physics2DRenderer : public b2Draw
    {
    public:
        Physics2DRenderer();
        ~Physics2DRenderer();
        void render(Physics& physics, Camera& camera);

        /// Set the drawing flags.
        void SetFlags(uint32 flags);

        /// Get the drawing flags.
        uint32 GetFlags() const;

        /// Append flags to the current flags.
        void AppendFlags(uint32 flags);

        /// Clear flags from the current flags.
        void ClearFlags(uint32 flags);

        /// Draw a closed polygon provided in CCW order.
        virtual void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);

        /// Draw a solid closed polygon provided in CCW order.
        virtual void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);

        /// Draw a circle.
        virtual void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);

        /// Draw a solid circle.
        virtual void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);

        /// Draw a line segment.
        virtual void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);

        /// Draw a transform. Choose your own length scale.
        /// @param xf a transform.
        virtual void DrawTransform(const b2Transform& xf) ;
    private:
        // PhysicsPlane m_currentPlane;
        bool m_overlay;

        bool m_renderAABB;

        void renderAABB(const b2AABB& aabb);
        
        Physics* m_physics;
    };
}