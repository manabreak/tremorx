#include "stdafx.h"
#include "Physics2DRenderer.h"

namespace Tremor
{
    Physics2DRenderer::Physics2DRenderer()
        : m_overlay(true),
        m_renderAABB(true)
    {
        SetFlags(e_shapeBit);
    }

    Physics2DRenderer::~Physics2DRenderer()
    {

    }

    void Physics2DRenderer::SetFlags(uint32 flags)
    {
        m_drawFlags = flags;
    }

    uint32 Physics2DRenderer::GetFlags() const
    {
        return m_drawFlags;
    }

    void Physics2DRenderer::render(Physics2D& physics, Camera& camera)
    {
        m_currentPlane = physics.getPlane();

        if (m_overlay)
        {
            glClear(GL_DEPTH_BUFFER_BIT);
        }
        else
        {
            glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
        }

        glMatrixMode(GL_PROJECTION);
        glLoadMatrixf(&camera.getProjection()[0][0]);
        glMatrixMode(GL_MODELVIEW);
        glLoadMatrixf(&camera.getView()[0][0]);
        
        m_physics = &physics;
        b2World& world = physics.getb2World();
        world.SetDebugDraw(this);
        
        world.DrawDebugData();
    }

    void Physics2DRenderer::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
    {
        

        glColor3f(color.r, color.g, color.b);
        glBegin(GL_LINES);
        for (unsigned int i = 0; i < vertexCount; ++i)
        {
            float x0, y0, x1, y1;
            x0 = vertices[i].x;
            y0 = vertices[i].y;
            if (i < vertexCount - 1)
            {
                x1 = vertices[i + 1].x;
                y1 = vertices[i + 1].y;
            }
            else
            {
                x1 = vertices[0].x;
                y1 = vertices[0].y;
            }

            x0 *= m_physics->getPhysicsRatio();
            y0 *= m_physics->getPhysicsRatio();
            x1 *= m_physics->getPhysicsRatio();
            y1 *= m_physics->getPhysicsRatio();

            switch (m_currentPlane)
            {
            case XY:
                glVertex3f(x0, y0, 0.f);
                glVertex3f(x1, y1, 0.f);
                break;
            case XZ:
                glVertex3f(x0, 0.f, y0);
                glVertex3f(x1, 0.f, y1);
                break;
            }
        }
        glEnd();
    }

    void Physics2DRenderer::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
    {
        glColor3f(color.r, color.g, color.b);
        glBegin(GL_LINES);
        for (unsigned int i = 0; i < vertexCount; ++i)
        {
            float x0, y0, x1, y1;
            x0 = vertices[i].x;
            y0 = vertices[i].y;
            if (i < vertexCount - 1)
            {
                x1 = vertices[i + 1].x;
                y1 = vertices[i + 1].y;
            }
            else
            {
                x1 = vertices[0].x;
                y1 = vertices[0].y;
            }

            switch (m_currentPlane)
            {
            case XY:
                glVertex3f(x0, y0, 0.f);
                glVertex3f(x1, y1, 0.f);
                break;
            case XZ:
                glVertex3f(x0, 0.f, y0);
                glVertex3f(x1, 0.f, y1);
                break;
            }
        }
        glEnd();
    }

    void Physics2DRenderer::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
    {
        glColor3f(color.r, color.g, color.b);
        glBegin(GL_LINES);

        
        float adv = (glm::pi<float>() * 2.f) / 16.f;
        for (unsigned int i = 0; i < 16; ++i)
        {
            float x0 = 0.f, y0 = 0.f;
            float x1 = 0.f, y1 = 0.f;
            if (i < 15)
            {
                x0 = center.x + radius * glm::cos(adv * i);
                y0 = center.y + radius * glm::sin(adv * i);
                x1 = center.x + radius * glm::cos(adv * (i + 1));
                y1 = center.y + radius * glm::sin(adv * (i + 1));
            }
            else
            {
                x0 = center.x + radius * glm::cos(adv * i);
                y0 = center.y + radius * glm::sin(adv * i);
                x1 = center.x + radius;
                y1 = center.y;
            }

            switch (m_currentPlane)
            {
            case XY:
                glVertex3f(x0, y0, 0.f);
                glVertex3f(x1, y1, 0.f);
                break;
            case XZ:
                glVertex3f(x0, 0.f, y0);
                glVertex3f(x1, 0.f, y1);
                break;
            }
        }

        glEnd();
    }

    void Physics2DRenderer::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
    {
        glColor3f(color.r, color.g, color.b);
        glBegin(GL_LINES);


        float adv = (glm::pi<float>() * 2.f) / 16.f;
        for (unsigned int i = 0; i < 16; ++i)
        {
            float x0 = 0.f, y0 = 0.f;
            float x1 = 0.f, y1 = 0.f;
            if (i < 15)
            {
                x0 = center.x + radius * glm::cos(adv * i);
                y0 = center.y + radius * glm::sin(adv * i);
                x1 = center.x + radius * glm::cos(adv * (i + 1));
                y1 = center.y + radius * glm::sin(adv * (i + 1));
            }
            else
            {
                x0 = center.x + radius * glm::cos(adv * i);
                y0 = center.y + radius * glm::sin(adv * i);
                x1 = center.x + radius;
                y1 = center.y;
            }

            switch (m_currentPlane)
            {
            case XY:
                glVertex3f(x0, y0, 0.f);
                glVertex3f(x1, y1, 0.f);
                break;
            case XZ:
                glVertex3f(x0, 0.f, y0);
                glVertex3f(x1, 0.f, y1);
                break;
            }
        }

        glEnd();
    }

    void Physics2DRenderer::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
    {

    }

    void Physics2DRenderer::DrawTransform(const b2Transform& xf)
    {

    }
}