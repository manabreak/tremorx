#include "stdafx.h"
#include "PhysicsBody2D.h"
#include "systems/Physics2D.h"

namespace Tremor
{
    PhysicsBody2D::PhysicsBody2D()
        : m_listener(nullptr),
        m_body(nullptr),
        m_enabled(true),
        m_addedToWorld(false),
        m_shape(nullptr),
        m_dynamic(false)
    {
        m_bodyDef.type = b2_staticBody;
    }

    PhysicsBody2D::~PhysicsBody2D()
    {
        if (m_shape != nullptr)
        {
            delete m_shape;
            m_shape = nullptr;
        }
    }

    void PhysicsBody2D::addCircle(float x, float y, float radius, bool sensor)
    {
        m_fixtureInfos.emplace_back(x, y, radius, sensor);
    }

    void PhysicsBody2D::addRectangle(float x, float y, float width, float height, bool sensor)
    {
        m_fixtureInfos.emplace_back(x, y, width, height, sensor);
    }

    const std::vector<PhysicsBody2D::FixtureInfo>& PhysicsBody2D::getFixtureInfos()
    {
        return m_fixtureInfos;
    }

    /*
    void PhysicsBody2D::setAsBox(Physics2D& physics, float width, float height, bool dynamic)
    {
        m_physics = &physics;
        m_bodyDef.type = dynamic ? b2_dynamicBody : b2_staticBody;

        m_shape = new b2PolygonShape();

        b2PolygonShape* shape = (b2PolygonShape*) m_shape;
        shape->SetAsBox(width / 2.f, height / 2.f);

        m_fixtureDef.shape = shape;

        m_body = physics.getb2World().CreateBody(&m_bodyDef);
        m_body->SetUserData(this);
        m_body->CreateFixture(&m_fixtureDef);
        m_addedToWorld = true;
    }

    void PhysicsBody2D::setAsCircle(Physics2D& physics, float radius, bool dynamic)
    {
        m_physics = &physics;
        m_bodyDef.type = dynamic ? b2_dynamicBody : b2_staticBody;

        m_shape = new b2CircleShape();

        b2CircleShape* shape = (b2CircleShape*) m_shape;
        shape->m_radius = radius;

        m_fixtureDef.shape = shape;

        m_body = physics.getb2World().CreateBody(&m_bodyDef);
        m_body->SetUserData(this);
        m_body->CreateFixture(&m_fixtureDef);
        m_addedToWorld = true;
    }
    */

    void PhysicsBody2D::setEnabled(bool enabled)
    {
        if (enabled != m_enabled)
        {
            m_enabled = enabled;
            m_body->SetActive(m_enabled);
        }
    }

    bool PhysicsBody2D::isEnabled()
    {
        return m_enabled;
    }

    b2Body& PhysicsBody2D::getb2Body()
    {
        return *m_body;
    }

    b2BodyDef& PhysicsBody2D::getb2BodyDef()
    {
        return m_bodyDef;
    }

    float PhysicsBody2D::getX()
    {
        return m_body->GetPosition().x;
    }

    float PhysicsBody2D::getY()
    {
        return m_body->GetPosition().y;
    }

    void PhysicsBody2D::applyForce(float x, float y)
    {
        m_body->ApplyForce(b2Vec2(x, y), m_body->GetLocalCenter(), true);
    }

    void PhysicsBody2D::setVelocity(float x, float y)
    {
        m_body->SetLinearVelocity(b2Vec2(x, y));
    }

    void PhysicsBody2D::setPosition(float x, float y)
    {
        m_body->SetTransform(b2Vec2(x, y), 0.f);
    }

    PhysicsBody2D::Listener* PhysicsBody2D::getListener()
    {
        return m_listener;
    }

    void PhysicsBody2D::setListener(PhysicsBody2D::Listener* listener)
    {
        m_listener = listener;
    }

    void PhysicsBody2D::setDynamic(bool dynamic)
    {
        m_dynamic = dynamic;
        m_bodyDef.type = m_dynamic ? b2_dynamicBody : b2_staticBody;
        if (m_addedToWorld)
        {
            // TODO Should update the physics body.
        }
    }

    bool PhysicsBody2D::isDynamic()
    {
        return m_dynamic;
    }

    void PhysicsBody2D::setSensor(bool sensor)
    {
        m_body->GetFixtureList()->SetSensor(sensor);
    }

    bool PhysicsBody2D::isSensor()
    {
        return m_body->GetFixtureList()->IsSensor();
    }

    void PhysicsBody2D::removeFromPhysics(Physics2D& physics)
    {
        if (m_addedToWorld)
        {
            physics.getb2World().DestroyBody(m_body);
            m_addedToWorld = false;
        }
    }

    void PhysicsBody2D::addToPhysics(Physics2D& physics)
    {
        if (!m_addedToWorld)
        {
            m_body = physics.getb2World().CreateBody(&m_bodyDef);
            m_body->SetUserData(this);

            for (unsigned int i = 0; i < m_fixtureInfos.size(); ++i)
            {
                m_body->CreateFixture(&m_fixtureInfos[i].def);
            }

            m_addedToWorld = true;
        }
    }
}