#pragma once
#include "core/ApplicationListener.h"

namespace Tremor
{
    class Game : public ApplicationListener
    {
    public:
        Game();
        ~Game();
        virtual void create();
        virtual void update(float dt);
        virtual void draw();
        virtual void cleanUp();
    };
}