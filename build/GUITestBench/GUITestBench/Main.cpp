#include "core/Application.h"
#include "Game.h"

int main()
{
    Tremor::Game game;
    Tremor::LaunchConfig config;
    config.screenWidth = 1080;
    config.screenHeight = 720;
    Tremor::Application app(game, config);
    app.run();

    return 0;
}