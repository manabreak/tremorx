# TremorX #

TremorX is a cross-platform game engine. It's heavily in pre-alpha stage at the moment, however the following stuff is already working as intended:

* Runs on Windows, partial support for Android
* Context creation
* Forward and deferred rendering pipelines using OpenGL
* ECS system (Using Tecs)
* Asset management pipeline
* 2D and 3D graphics support (PNG, OBJ)
* Tiled map support
* Lua scripting
* 2D physics using Box2D

To be implemented:

* 3D physics (i.e. Bullet)
* Scene graph
* Reactive action system
* Single-file #include for the engine


### Dependencies

TremorX uses the following dependencies:

* GLM (For math)
* GLEW (For OpenGL extension support)
* GLFW3 (For context creation and low-level IO on Windows)
* Lua 5.1 (For Lua scripting support)
* Box2D (For 2D physics)
* Tecs (For ECS)

### How to build?

So far, only Visual Studio build files are included. The project work out-of-the-box as long as the dependencies are met.

### How to use?

The default way is to build a static library and link it to your actual game project.

Once linked, the engine can be instantiated like this:


```
#!c++

#include "core/Application.h"
#include "Game.h"

int main()
{
    // Create new game (which implements ApplicationListener
    Game game;

    // Create a launch config
    Tremor::LaunchConfig config;
    config.screenWidth = 1920; // Set screen width
    config.screenHeight = 1080; // Set screen height
    config.windowTitle = "Hello TremorX!"; // Set window title
    config.fullScreen = true; // Enable full-screen

    // Create a new application with the game and the config
    Tremor::Application app(game, config);

    // Run the game
    app.run();

    return 0;
}
```

The `Game` class would look something like this:

```
#!c++
#include "core/ApplicationListener.h"

class Game : public Tremor::ApplicationListener
{
public:
    Game();
    ~Game();
    virtual void create();
    virtual void update(float dt);
    virtual void draw();
    virtual void cleanUp();

private:
    // Any private members for your game class
};
```