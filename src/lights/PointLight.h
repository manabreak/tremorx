#pragma once

#include "Light.h"

namespace Tremor
{
    class PointLight : public Light
    {
    public:
        PointLight()
           : m_radius(100.f)
        {
        }

        ~PointLight()
        {

        }

        float getRadius()
        {
            return m_radius;
        }

        virtual void setPosition(float x, float y, float z)
        {
            Light::setPosition(x, y, z);
            // m_translation = glm::translate(m_position);
            m_transform = m_translation * glm::scale(glm::vec3(m_radius));
        }

        void setRadius(float radius)
        {
            m_radius = radius;
            m_transform = m_translation * glm::scale(glm::vec3(m_radius));
        }

        virtual void enableShadows()
        {
            Light::enableShadows();

        }

        virtual void disableShadows()
        {
            Light::disableShadows();
            m_views.clear();
        }
    private:
        float m_radius;

        // Projection matrix for shadows
        glm::mat4 m_projection;

        // View matrices for shadows
        std::vector<glm::mat4> m_views;
    };
}