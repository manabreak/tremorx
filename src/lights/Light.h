#pragma once

#include <Component.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>


namespace Tremor
{
    class Light : public Tecs::Component
    {
    public:
        Light()
            : m_intensity(1.f),
            m_color(1.f, 1.f, 1.f),
            m_shadows(false)
        {

        }

        float getIntensity()
        {
            return m_intensity;
        }

        void setIntensity(float intensity)
        {
            m_intensity = intensity;
        }

        const glm::vec3& getColor()
        {
            return m_color;
        }

        void setColor(float r, float g, float b)
        {
            m_color = glm::vec3(r, g, b);
        }

        const glm::vec3& getPosition()
        {
            return m_position;
        }

        virtual void setPosition(float x, float y, float z)
        {
            m_position = glm::vec3(x, y, z);
        }

        const glm::mat4& getTransformMatrix()
        {
            return m_transform;
        }

        virtual void enableShadows()
        {
            m_shadows = true;
        }

        virtual void disableShadows()
        {
            m_shadows = false;
        }

        bool isShadowsEnabled()
        {
            return m_shadows;
        }

        void setFinalPosition(float x, float y, float z)
        {
            m_finalPosition.x = x;
            m_finalPosition.y = y;
            m_finalPosition.z = z;

            m_translation = glm::translate(m_finalPosition);
        }

        const glm::vec3& getFinalPosition()
        {
            return m_finalPosition;
        }

        const glm::mat4& getTranslation()
        {
            return m_translation;
        }
    protected:
        float m_intensity;
        glm::vec3 m_color;
        glm::vec3 m_position;
        glm::vec3 m_finalPosition;
        glm::mat4 m_transform;
        glm::mat4 m_translation;
        bool m_shadows;
    };
}