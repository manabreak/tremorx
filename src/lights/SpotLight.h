#pragma once

#include "Light.h"

namespace Tremor
{
    class SpotLight : public Light
    {
    public:
        SpotLight()
            : m_direction(0.f, -1.f, 0.f),
            m_angle(60.f),
            m_range(100.f)
        {

        }

        ~SpotLight()
        {

        }

        float getAngle()
        {
            return m_angle;
        }

        float getRange()
        {
            return m_range;
        }

        void setRange(float range)
        {
            m_range = range;
            updateTransform();
        }

        const glm::vec3& getDirection()
        {
            return m_direction;
        }

        void setDirection(const glm::vec3& direction)
        {
            m_direction = direction;
            updateTransform();
        }

        virtual void setPosition(float x, float y, float z)
        {
            Light::setPosition(x, y, z);
            updateTransform();
        }

        void setAngle(float angle)
        {
            m_angle = angle;
            updateTransform();
        }
    protected:
        void updateTransform()
        {
            float scl = glm::tan(glm::radians(m_angle / 2.f));
            glm::quat rot = glm::rotation(glm::vec3(0.f, -1.f, 0.f), m_direction);
            m_transform = m_translation * glm::scale(glm::vec3(m_range)) * glm::scale(glm::vec3(scl, 1.f, scl)) * glm::mat4_cast(rot);
        }

        float m_angle;
        float m_range;
        glm::vec3 m_direction;
    };
}