#pragma once

#include "Camera.h"
#include "2d/Texture.h"
#include "3d/Mesh.h"

namespace Tremor
{
    class ForwardRenderer
    {
    public:
        ForwardRenderer();
        ~ForwardRenderer();

        void begin(Camera& cmaera);
        void render(const glm::mat4& world, Mesh& mesh);
        void render(const glm::mat4& world, Mesh& mesh, Texture& colorMap);
        void render(const glm::mat4& world, Mesh& mesh, Texture& colorMap, Texture& normalMap);
        void end();

        ShaderProgram& getShader();

    private:
        bool m_beginCalled;
        ShaderProgram m_shader;

        unsigned int m_worldLocation;
        unsigned int m_viewLocation;
        unsigned int m_projLocation;
        unsigned int m_colorMapLocation;
        unsigned int m_normalMapLocation;

        void cacheUniformLocations();

        static ShaderProgram& createDefaultShader();
    };
}