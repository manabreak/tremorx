#include "stdafx.h"
#include "DeferredRenderer.h"
#include "lights/PointLight.h"
#include "lights/SpotLight.h"

namespace Tremor
{
    DeferredRenderer::DeferredRenderer()
        : m_beginCalled(false),
        m_shaderC(createShaderC()),
        m_shaderCN(createShaderCN()),
        m_lightsController(*this)
    {
        cacheUniformLocations();
    }

    DeferredRenderer::DeferredRenderer(int width, int height)
        : m_gbuffer(width, height),
        m_beginCalled(false),
        m_shaderC(createShaderC()),
        m_shaderCN(createShaderCN()),
        m_lightsController(*this, width, height)
    {
        cacheUniformLocations();
    }

    DeferredRenderer::~DeferredRenderer()
    {

    }

    void DeferredRenderer::cacheUniformLocations()
    {
        m_SLC.m_worldLocation = m_shaderC.getUniformLocation("u_world");
        m_SLC.m_viewLocation = m_shaderC.getUniformLocation("u_view");
        m_SLC.m_projLocation = m_shaderC.getUniformLocation("u_projection");
        m_SLC.m_colorMapLocation = m_shaderC.getUniformLocation("u_colormap");

        m_SLCN.m_worldLocation = m_shaderCN.getUniformLocation("u_world");
        m_SLCN.m_viewLocation = m_shaderCN.getUniformLocation("u_view");
        m_SLCN.m_projLocation = m_shaderCN.getUniformLocation("u_projection");
        m_SLCN.m_colorMapLocation = m_shaderCN.getUniformLocation("u_colormap");
        m_SLCN.m_normalMapLocation = m_shaderCN.getUniformLocation("u_normalmap");
    }

    void DeferredRenderer::begin(Camera& camera)
    {
        m_camera = &camera;
        m_beginCalled = true;
        m_rqeC.clear();
        m_rqeCN.clear();
        m_direct = false;
        /*
        m_gbuffer.begin();
        m_defaultShader.begin();

        m_defaultShader.setUniform(m_viewLocation, camera.getView());
        m_defaultShader.setUniform(m_projLocation, camera.getProjection());
        m_defaultShader.setUniform(m_colorMapLocation, 0);
        m_defaultShader.setUniform(m_normalMapLocation, 1);
        */
    }

    Camera& DeferredRenderer::getCamera()
    {
        return *m_camera;
    }

    void DeferredRenderer::beginDirect(Camera& camera)
    {
        m_camera = &camera;
        m_beginCalled = true;
        m_gbuffer.begin(false);
        m_shaderC.begin();
        m_shaderC.setUniform(m_SLC.m_viewLocation, camera.getView());
        m_shaderC.setUniform(m_SLC.m_projLocation, camera.getProjection());
        m_shaderC.setUniform(m_SLC.m_colorMapLocation, 0);
        m_shaderC.setUniform(m_SLC.m_normalMapLocation, 1);
    }

    void DeferredRenderer::endDirect()
    {
        m_beginCalled = false;
        m_shaderC.end();
        m_gbuffer.end();
    }

    void DeferredRenderer::render(const glm::mat4& world, Mesh& mesh)
    {
        // m_defaultShader.setUniform(m_worldLocation, world);
        // mesh.render();
    }

    void DeferredRenderer::render(const glm::mat4& world, Mesh& mesh, Texture& colorMap)
    {
        // colorMap.bind(GL_TEXTURE0);
        // render(world, mesh);
        m_rqeC.emplace_back(world, mesh, colorMap);
    }

    void DeferredRenderer::render(const glm::mat4& world, Mesh& mesh, Texture& colorMap, Texture& normalMap)
    {
        // normalMap.bind(GL_TEXTURE1);
        // render(world, mesh, colorMap);
        m_rqeCN.emplace_back(world, mesh, colorMap, normalMap);
    }

    void DeferredRenderer::end()
    {
        m_beginCalled = false;

        m_gbuffer.begin();

        // Render queue - Color
        m_shaderC.begin();
        m_shaderC.setUniform(m_SLC.m_projLocation, m_camera->getProjection());
        m_shaderC.setUniform(m_SLC.m_viewLocation, m_camera->getView());
        m_shaderC.setUniform(m_SLC.m_colorMapLocation, 0);
        for (unsigned int i = 0; i < m_rqeC.size(); ++i)
        {
            m_shaderC.setUniform(m_SLC.m_worldLocation, m_rqeC[i].m_world);
            m_rqeC[i].m_colormap.bind(GL_TEXTURE0);
            m_rqeC[i].m_mesh.render();
        }
        m_shaderC.end();

        // Render queue - Color + Normal
        m_shaderCN.begin();
        m_shaderCN.setUniform(m_SLCN.m_projLocation, m_camera->getProjection());
        m_shaderCN.setUniform(m_SLCN.m_viewLocation, m_camera->getView());
        m_shaderCN.setUniform(m_SLCN.m_colorMapLocation, 0);
        m_shaderCN.setUniform(m_SLCN.m_normalMapLocation, 1);
        for (unsigned int i = 0; i < m_rqeCN.size(); ++i)
        {
            m_shaderCN.setUniform(m_SLCN.m_worldLocation, m_rqeCN[i].m_world);
            m_rqeCN[i].m_colormap.bind(GL_TEXTURE0);
            m_rqeCN[i].m_normalmap.bind(GL_TEXTURE1);
            m_rqeCN[i].m_mesh.render();
        }
        m_shaderCN.end();

        m_gbuffer.end();
    }

    void DeferredRenderer::beginLights()
    {
        m_lightsController.begin(*m_camera);
    }

    void DeferredRenderer::renderLight(PointLight& light)
    {
        m_lightsController.renderPointLight(light);
    }

    void DeferredRenderer::renderLight(SpotLight& light)
    {
        m_lightsController.renderSpotLight(light);
    }

    void DeferredRenderer::endLights()
    {
        m_lightsController.end();
    }

    GBuffer& DeferredRenderer::getGBuffer()
    {
        return m_gbuffer;
    }

    ShaderProgram& DeferredRenderer::getShaderC()
    {
        return m_shaderC;
    }

    ShaderProgram& DeferredRenderer::getShaderCN()
    {
        return m_shaderCN;
    }

    void DeferredRenderer::outputColorResult()
    {
        m_quadRenderer.render(m_gbuffer.getColorResult());
    }

    void DeferredRenderer::outputNormalResult()
    {
        m_quadRenderer.render(m_gbuffer.getNormalResult());
    }

    void DeferredRenderer::outputDepthResult()
    {
        m_quadRenderer.render(m_gbuffer.getDepthResult());
    }

    void DeferredRenderer::outputLightResult()
    {
        m_quadRenderer.render(m_lightsController.getLightsResult());
    }

    void DeferredRenderer::outputShadowResult()
    {
        m_quadRenderer.render(m_lightsController.getShadowResult());
    }

    void DeferredRenderer::outputFinalResult()
    {
        m_quadRenderer.render(m_gbuffer.getColorResult(), m_lightsController.getLightsResult());
    }

    DeferredLightsController& DeferredRenderer::getLightsController()
    {
        return m_lightsController;
    }

    void DeferredRenderer::setAmbientLight(float r, float g, float b)
    {
        m_lightsController.setAmbientLight(r, g, b);
    }

    ShaderProgram& DeferredRenderer::createShaderC()
    {
        std::string v =
            "#version 330 core\n"

            "layout(location = 0) in vec3 in_pos;\n"
            "layout(location = 1) in vec2 in_uv;\n"
            "layout(location = 2) in vec3 in_col;\n"
            "layout(location = 3) in vec3 in_nor;\n"
            "layout(location = 4) in vec3 in_tan;\n"
            "layout(location = 5) in vec3 in_bin;\n"

            "uniform mat4 u_projection;\n"
            "uniform mat4 u_view;\n"
            "uniform mat4 u_world;\n"

            "out vec2 texcoord;\n"
            "out mat3 tbn;\n"
            "out vec3 color;\n"

            "void main() {\n"
            "  gl_Position = u_projection * u_view * u_world * vec4(in_pos, 1.0);\n"

            "  "

            "  texcoord = in_uv;\n"
            "  color = in_col;\n"
            "  tbn[0] = (u_world * vec4(in_tan, 0.0)).xyz;\n"
            "  tbn[1] = (u_world * vec4(in_bin, 0.0)).xyz;\n"
            "  tbn[2] = (u_world * vec4(in_nor, 0.0)).xyz;\n"
            "}\n"
            ;

        std::string f =
            "#version 330 core\n"

            "in vec2 texcoord;\n"
            "in mat3 tbn;\n"
            "in vec3 color;\n"

            "layout(location = 0) out vec4 out_color;\n" // RGB + Specular Intensity
            "layout(location = 1) out vec4 out_normal;\n" // Normals + Specular Power

            "uniform sampler2D u_colormap;\n"

            "void main() {\n"
            // COLOR
            "  vec4 texColor = texture2D(u_colormap, texcoord);\n"
            "  if(texColor.a < 0.5) discard; \n"
            "  out_color.xyz = texColor.xyz * color;\n"

            // NORMAL
            "  vec3 normal = vec3(0.5, 0.5, 1.0);\n"
            "  normal = 2.0 * normal - 1.0;\n"
            "  normal = (tbn * normal);\n"
            "  normal = normalize(normal);\n"
            "  out_normal.xyz = 0.5 * (normal + 1.0);\n"

            // SPECULAR
            "  float specIntensity = 0.8;\n"
            "  float specPower = 0.5;\n"
            "  out_color.a = specIntensity;\n"
            "  out_normal.a = specPower;\n"
            "}\n"
            ;

        return AssetManager::loadShader(v, f);
    }

    ShaderProgram& DeferredRenderer::createShaderCN()
    {
        std::string v =
            "#version 330 core\n"

            "layout(location = 0) in vec3 in_pos;\n"
            "layout(location = 1) in vec2 in_uv;\n"
            "layout(location = 2) in vec3 in_col;\n"
            "layout(location = 3) in vec3 in_nor;\n"
            "layout(location = 4) in vec3 in_tan;\n"
            "layout(location = 5) in vec3 in_bin;\n"

            "uniform mat4 u_projection;\n"
            "uniform mat4 u_view;\n"
            "uniform mat4 u_world;\n"

            "out vec2 texcoord;\n"
            "out mat3 tbn;\n"
            "out vec3 color;\n"

            "void main() {\n"
            "  gl_Position = u_projection * u_view * u_world * vec4(in_pos, 1.0);\n"

            "  "

            "  texcoord = in_uv;\n"
            "  color = in_col;\n"
            "  tbn[0] = (u_world * vec4(in_tan, 0.0)).xyz;\n"
            "  tbn[1] = (u_world * vec4(in_bin, 0.0)).xyz;\n"
            "  tbn[2] = (u_world * vec4(in_nor, 0.0)).xyz;\n"
            "}\n"
            ;

        std::string f =
            "#version 330 core\n"

            "in vec2 texcoord;\n"
            "in mat3 tbn;\n"
            "in vec3 color;\n"

            "layout(location = 0) out vec4 out_color;\n" // RGB + Specular Intensity
            "layout(location = 1) out vec4 out_normal;\n" // Normals + Specular Power

            "uniform sampler2D u_colormap;\n"
            "uniform sampler2D u_normalmap;\n"

            "void main() {\n"
            // COLOR
            "  vec4 texColor = texture2D(u_colormap, texcoord);\n"
            "  if(texColor.a < 0.5) discard; \n"
            "  out_color.xyz = texColor.xyz * color;\n"

            // NORMAL
            "  vec3 normal = texture2D(u_normalmap, texcoord).xyz;\n"
            "  normal = 2.0 * normal - 1.0;\n"
            "  normal = (tbn * normal);\n"
            "  normal = normalize(normal);\n"
            "  out_normal.xyz = 0.5 * (normal + 1.0);\n"

            // SPECULAR
            "  float specIntensity = 0.8;\n"
            "  float specPower = 0.5;\n"
            "  out_color.a = specIntensity;\n"
            "  out_normal.a = specPower;\n"
            "}\n"
            ;

        return AssetManager::loadShader(v, f);
    }
}