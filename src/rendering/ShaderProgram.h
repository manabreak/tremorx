#pragma once

#include <string>
#include <vector>
#include <map>

#ifdef __TREMOR_ANDROID__
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#else
#include <glew/include/GL/glew.h>
#endif

namespace Tremor
{
    class ShaderProgram
    {
    public:
        ShaderProgram(const std::string& srcVertex, const std::string& srcFragment);
        ~ShaderProgram();
        bool isCompiled();

        bool hasAttribute(const std::string& name);
        int getAttributeType(const std::string& name);
        int getAttributeLocation(const std::string& name);
        int getAttributeSize(const std::string& name);

        bool hasUniform(const std::string& name);

        int getUniformLocation(const std::string& name);

        void setUniform(const std::string& name, int value);
        void setUniform(const std::string& name, float value);
        void setUniform(const std::string& name, const glm::vec2& value);
        void setUniform(const std::string& name, const glm::vec3& value);
        void setUniform(const std::string& name, const glm::mat4& value);

        void setUniform(unsigned int location, int value);
        void setUniform(unsigned int location, float value);
        void setUniform(unsigned int location, const glm::vec2& value);
        void setUniform(unsigned int location, const glm::vec3& value);
        void setUniform(unsigned int location, const glm::mat4& value);

        void begin();
        void end();
    private:
        void compile(const std::string& srcVertex, const std::string& srcFragment);
        int loadShader(int type, const std::string& src);

        int linkProgram();

        void parseUniformLocations(const std::string& src);

        bool m_bound;
        bool m_compiled;
        GLuint m_program;
        GLuint m_vertexShader;
        GLuint m_fragmentShader;

        std::vector<std::string> m_uniforms;
    };
}