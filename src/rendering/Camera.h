#pragma once

#include <glm/glm.hpp>

namespace Tremor
{
    class Transform;

    class Camera
    {
    public:
        Camera();
        Camera(float width, float height);
        Camera(const Camera&) = delete;
        ~Camera();

        void lookAtXYZ(float x, float y, float z);
        void lookAt(const glm::vec3& target);

        const glm::mat4& getView();
        const glm::mat4& getProjection();
        const glm::mat4& getViewProjection();
        const glm::mat4& getInverseViewProjection();

        float getNear();
        float getFar();
        void setNear(float near);
        void setFar(float far);

        const glm::vec3& getDirection();
        const glm::vec3& getUp();

        void setSize(float width, float height);
        void setOrtho(float width, float height);
        void setPerspective(float fov, float width, float height);
        bool isOrtho();
        void setFov(float fov);
        float getFov();

        void setPositionXYZ(float x, float y, float z);
        void setPosition(const glm::vec3& position);
        const glm::vec3& getPosition();

        void translateXYZ(float x, float y, float z);
        void translate(const glm::vec3& amount);

        void setRotation(const glm::quat& rotation);
        const glm::quat& getRotation();

        void rotate(const glm::quat& amount);
        void rotateX(float radians);

        glm::vec2 worldPointToScreenPoint(const glm::vec3& worldPoint);
        glm::vec2 worldTransformToScreenPoint(Transform* transform);
        glm::vec3 screenToWorldPoint(int x, int y, float zPlane);
        glm::vec3 viewportToWorldPoint(float x, float y, float zPlane);

        // Converts a screen coordinate to a viewport coordinate.
        // (0, 0) is the top-left corner of the screen.
        glm::vec2 screenToViewport(int x, int y);
    private:
        void update();

        bool m_ortho;
        bool m_viewDirty;
        bool m_projDirty;

        float m_near;
        float m_far;
        float m_width;
        float m_height;
        float m_fov;

        glm::mat4 m_view;
        glm::mat4 m_projection;
        glm::mat4 m_combined;
        glm::mat4 m_invViewProj;

        glm::vec3 m_position;
        glm::quat m_rotation;

        glm::vec3 m_direction;
        glm::vec3 m_up;
    };
}