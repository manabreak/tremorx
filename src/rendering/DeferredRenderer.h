#pragma once

#include "gbuffer.h"
#include "Camera.h"
#include "3d/Mesh.h"
#include "2d/Texture.h"
#include "QuadRenderer.h"
#include "DeferredLightsController.h"

namespace Tremor
{
    struct RQE
    {
    public:
        const glm::mat4& m_world;
        Mesh& m_mesh;
        RQE(const glm::mat4& world, Mesh& mesh)
            : m_world(world),
            m_mesh(mesh)
        {

        }
    };

    struct RQE_C : public RQE
    {
    public:
        Texture& m_colormap;
        RQE_C(const glm::mat4& world, Mesh& mesh, Texture& colormap)
            : RQE(world, mesh),
            m_colormap(colormap)
        {

        }
    };

    struct RQE_CN : public RQE_C
    {
        Texture& m_normalmap;
        RQE_CN(const glm::mat4& world, Mesh& mesh, Texture& colormap, Texture& normalmap)
            : RQE_C(world, mesh, colormap),
            m_normalmap(normalmap)
        {

        }
    };

    struct ShaderLocations
    {
    public:
        unsigned int m_worldLocation;
        unsigned int m_viewLocation;
        unsigned int m_projLocation;
        unsigned int m_colorMapLocation;
        unsigned int m_normalMapLocation;
    };

    class DeferredRenderer
    {
    public:
        DeferredRenderer();
        DeferredRenderer(int width, int height);
        ~DeferredRenderer();

        void begin(Camera& camera);
        void render(const glm::mat4& world, Mesh& mesh);
        void render(const glm::mat4& world, Mesh& mesh, Texture& colorMap);
        void render(const glm::mat4& world, Mesh& mesh, Texture& colorMap, Texture& normalMap);
        void end();

        void beginDirect(Camera& camera);
        void endDirect();

        Camera& getCamera();

        void beginLights();
        void renderLight(PointLight& light);
        void renderLight(SpotLight& light);
        void endLights();

        GBuffer& getGBuffer();

        ShaderProgram& getShaderC();
        ShaderProgram& getShaderCN();

        void outputColorResult();
        void outputNormalResult();
        void outputDepthResult();
        void outputLightResult();
        void outputShadowResult();
        void outputFinalResult();

        DeferredLightsController& getLightsController();

        void setAmbientLight(float r, float g, float b);
    private:
        GBuffer m_gbuffer;
        bool m_beginCalled;
        bool m_direct;
        ShaderProgram& m_shaderC;
        ShaderProgram& m_shaderCN;
        QuadRenderer m_quadRenderer;
        Camera* m_camera;
        DeferredLightsController m_lightsController;

        ShaderLocations m_SLC;
        ShaderLocations m_SLCN;

        void cacheUniformLocations();

        std::vector<RQE_C> m_rqeC;
        std::vector<RQE_CN> m_rqeCN;

        static ShaderProgram& createShaderC(); // Colormap
        static ShaderProgram& createShaderCN(); // Color + Normal
    };
}