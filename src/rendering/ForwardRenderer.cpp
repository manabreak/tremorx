#include "stdafx.h"
#include "ForwardRenderer.h"

namespace Tremor
{
    ForwardRenderer::ForwardRenderer()
        : m_beginCalled(false),
        m_shader(createDefaultShader())
    {
        cacheUniformLocations();
    }

    ForwardRenderer::~ForwardRenderer()
    {

    }

    void ForwardRenderer::begin(Camera& camera)
    {
        m_beginCalled = true;
        m_shader.begin();

        m_shader.setUniform(m_viewLocation, camera.getView());
        m_shader.setUniform(m_projLocation, camera.getProjection());
        m_shader.setUniform(m_colorMapLocation, 0);
        m_shader.setUniform(m_normalMapLocation, 1);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void ForwardRenderer::render(const glm::mat4& world, Mesh& mesh)
    {
        m_shader.setUniform(m_worldLocation, world);
        mesh.render();
    }

    void ForwardRenderer::render(const glm::mat4& world, Mesh& mesh, Texture& colorMap)
    {
        colorMap.bind(GL_TEXTURE0);
        render(world, mesh);
    }

    void ForwardRenderer::render(const glm::mat4& world, Mesh& mesh, Texture& colorMap, Texture& normalMap)
    {
        normalMap.bind(GL_TEXTURE1);
        render(world, mesh, colorMap);
    }

    void ForwardRenderer::end()
    {
        m_shader.end();
        m_beginCalled = false;
    }

    ShaderProgram& ForwardRenderer::getShader()
    {
        return m_shader;
    }

    void ForwardRenderer::cacheUniformLocations()
    {
        m_worldLocation = m_shader.getUniformLocation("u_world");
        m_viewLocation = m_shader.getUniformLocation("u_view");
        m_projLocation = m_shader.getUniformLocation("u_projection");
        m_colorMapLocation = m_shader.getUniformLocation("u_colormap");
        m_normalMapLocation = m_shader.getUniformLocation("u_normalmap");
    }

    ShaderProgram& ForwardRenderer::createDefaultShader()
    {
        std::string v =
            "#version 330 core\n"

            "layout(location = 0) in vec3 in_pos;\n"
            "layout(location = 1) in vec2 in_uv;\n"
            "layout(location = 2) in vec3 in_col;\n"

            "uniform mat4 u_projection;\n"
            "uniform mat4 u_view;\n"
            "uniform mat4 u_world;\n"

            "out vec2 texcoord;\n"
            "out vec4 color;\n"

            "void main() {\n"
            "  gl_Position = u_projection * u_view * u_world * vec4(in_pos, 1.0);\n"
            "  texcoord = in_uv;\n"
            "  color = in_col;\n"
            "}\n"
            ;

        std::string f =
            "#version 330 core\n"

            "in vec2 texcoord;\n"
            "in vec4 color;\n"

            "layout(location = 0) out vec4 out_color;\n"

            "uniform sampler2D u_colormap;\n"

            "void main() {\n"
            "  vec4 texColor = texture2D(u_colormap, texcoord);\n"
            "  out_color = texColor * color;\n"
            "}\n"
            ;

        return AssetManager::loadShader(v, f);
    }
}