#include "stdafx.h"
#include "SpriteBatch.h"
#include "ForwardRenderer.h"
#include "DeferredRenderer.h"
#include <glm/glm.hpp>
#include "core/Tremor.h"
#include "2d/Text2D.h"
#include "2d/BitmapFont.h"

namespace Tremor
{
    SpriteBatch::SpriteBatch()
        : m_drawing(false),
        m_projectionMatrix(1.f),
        m_viewMatrix(1.f),
        m_modelMatrix(1.f),
        m_lastTexture(nullptr),
        m_invTexWidth(0.f),
        m_invTexHeight(0.f),
        m_idx(0),
        m_ownsShader(false),
        m_shader(&createDefaultShader()),
        m_useDeferredRenderer(false),
        m_useForwardRenderer(false),
        m_deferredRenderer(nullptr),
        m_forwardRenderer(nullptr),
        m_mesh(4 * 5460, 6 * 5460),
        m_roundPositions(false)
    {
        initialize();
    }

    SpriteBatch::SpriteBatch(int width, int height)
        : m_drawing(false),
        m_projectionMatrix(1.f),
        m_viewMatrix(1.f),
        m_modelMatrix(1.f),
        m_lastTexture(nullptr),
        m_invTexWidth(0.f),
        m_invTexHeight(0.f),
        m_idx(0),
        m_ownsShader(false),
        m_shader(&createDefaultShader()),
        m_useDeferredRenderer(false),
        m_useForwardRenderer(false),
        m_deferredRenderer(nullptr),
        m_forwardRenderer(nullptr),
        m_mesh(4 * 5460, 6 * 5460),
        m_roundPositions(false)
    {
        initialize();
    }

    SpriteBatch::SpriteBatch(DeferredRenderer& deferredRenderer)
        : m_drawing(false),
        m_projectionMatrix(1.f),
        m_viewMatrix(1.f),
        m_modelMatrix(1.f),
        m_lastTexture(nullptr),
        m_invTexWidth(0.f),
        m_invTexHeight(0.f),
        m_idx(0),
        m_ownsShader(false),
        m_shader(&createDefaultShader()),
        m_useDeferredRenderer(true),
        m_useForwardRenderer(false),
        m_deferredRenderer(&deferredRenderer),
        m_forwardRenderer(nullptr),
        m_mesh(4 * 5460, 6 * 5460),
        m_roundPositions(false)
    {
        initialize();
    }

    SpriteBatch::SpriteBatch(ForwardRenderer& forwardRenderer)
        : m_drawing(false),
        m_projectionMatrix(1.f),
        m_viewMatrix(1.f),
        m_modelMatrix(1.f),
        m_lastTexture(nullptr),
        m_invTexWidth(0.f),
        m_invTexHeight(0.f),
        m_idx(0),
        m_ownsShader(false),
        m_shader(&createDefaultShader()),
        m_useDeferredRenderer(false),
        m_useForwardRenderer(true),
        m_deferredRenderer(nullptr),
        m_forwardRenderer(&forwardRenderer),
        m_mesh(4 * 5460, 6 * 5460),
        m_roundPositions(false)
    {
        initialize();
    }

    SpriteBatch::~SpriteBatch()
    {
    }

    void SpriteBatch::setFromCamera(Camera& camera)
    {
        m_projectionMatrix = camera.getProjection();
        m_viewMatrix = camera.getView();
        m_modelMatrix = glm::mat4(1.f);
    }

    void SpriteBatch::setScreen(float x, float y, float width, float height)
    {
        m_projectionMatrix = glm::ortho(x, width, y, height, -10.f, 10.f);
        m_viewMatrix = glm::mat4(1.f);
        m_modelMatrix = glm::mat4(1.f);
    }

    void SpriteBatch::initialize()
    {
        m_mesh.setDynamic();

        m_projectionMatrix = glm::ortho(0.f, (float) Tremor::graphics->getWidth(), 0.f, (float) Tremor::graphics->getHeight(), -10.f, 10.f);
        m_viewMatrix = glm::mat4(1.f);
        m_modelMatrix = glm::mat4(1.f);

        int len = 5460 * 6;
        std::vector<unsigned short> indices(len);
        for (int i = 0, j = 0; i < len; i += 6, j += 4)
        {
            indices[i] = j;
            indices[i + 1] = j + 1;
            indices[i + 2] = j + 2;
            indices[i + 3] = j + 2;
            indices[i + 4] = j + 3;
            indices[i + 5] = j;
        }
        m_mesh.setIndices(indices);
    }

    void SpriteBatch::begin()
    {
        if (m_drawing) return;
        m_renderCalls = 0;
        m_drawing = true;
        

        if (m_useDeferredRenderer)
        {
            glUniformMatrix4fv(m_deferredRenderer->getShaderC().getUniformLocation("u_world"), 1, GL_FALSE, &m_modelMatrix[0][0]);
            glUniform1i(m_deferredRenderer->getShaderC().getUniformLocation("u_colormap"), 0);
            if (m_lastTexture != nullptr)
            {
                m_lastTexture->bind(GL_TEXTURE0);
            }
        }
        else if (m_useForwardRenderer)
        {
            glUniformMatrix4fv(m_forwardRenderer->getShader().getUniformLocation("u_world"), 1, GL_FALSE, &m_modelMatrix[0][0]);
            glUniform1i(m_forwardRenderer->getShader().getUniformLocation("u_colormap"), 0);
            if (m_lastTexture != nullptr)
            {
                m_lastTexture->bind(GL_TEXTURE0);
            }
        }
        else
        {
            m_shader->begin();
            m_combinedMatrix = m_projectionMatrix * m_viewMatrix * m_modelMatrix;
            glUniformMatrix4fv(m_shader->getUniformLocation("u_world"), 1, GL_FALSE, &m_combinedMatrix[0][0]);
            glUniform1i(m_shader->getUniformLocation("u_colormap"), 0);
        }
    }

    void SpriteBatch::drawPtr(Sprite* sprite)
    {
        draw(*sprite);
    }

    void SpriteBatch::draw(Sprite& sprite)
    {
        Texture* texture = &sprite.getTexture();
        const std::vector<Vertex>& spriteVertices = sprite.getTransformedVertices();

        int verticesLength = m_mesh.getVertices().size();
        int remainingVertices = verticesLength;
        if (m_lastTexture == nullptr || texture != m_lastTexture) switchTexture(*texture);
        else
        {
            remainingVertices -= m_idx;
            if (remainingVertices == 0)
            {
                flush();
                remainingVertices = verticesLength;
            }
        }

        for (unsigned int i = 0; i < 4; ++i)
        {
            m_mesh.setVertex(m_idx++, spriteVertices[i]);
        }
    }

    void SpriteBatch::drawTextPtr(Text2D* text)
    {
        drawText(*text);
    }

    void SpriteBatch::drawText(Text2D& text)
    {
        Texture* texture = &text.getFont().getTexture();
        const std::vector<Vertex>& vertices = text.getVertices();

        int verticesLength = m_mesh.getVertices().size();
        int remainingVertices = verticesLength;
        if (m_lastTexture == nullptr || texture != m_lastTexture) switchTexture(*texture);
        else
        {
            remainingVertices -= m_idx;
            if (remainingVertices == 0)
            {
                flush();
                remainingVertices = verticesLength;
            }
        }

        for (unsigned int i = 0; i < vertices.size(); ++i)
        {
            m_mesh.setVertex(m_idx++, vertices[i]);
        }
    }

    void SpriteBatch::end()
    {
        if (!m_drawing) return;
        if (m_idx > 0) flush();

        m_drawing = false;

        if (!m_useDeferredRenderer)
        {
            m_shader->end();
        }

        m_lastTexture = nullptr;
        glDepthMask(GL_TRUE);
    }

    void SpriteBatch::flush()
    {
        if (m_idx == 0) return;

        m_renderCalls++;
        m_totalRenderCalls++;
        int spritesInBatch = m_idx / 4;
        if (spritesInBatch > m_maxSpritesInBatch) m_maxSpritesInBatch = spritesInBatch;
        int count = spritesInBatch * 6;

        m_mesh.update(m_idx, count);
        // glDepthMask(GL_FALSE);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        m_mesh.render(*m_shader, GL_TRIANGLES, 0, count);
        // glDepthMask(GL_TRUE);
        m_idx = 0;
    }

    void SpriteBatch::switchTexture(Texture& texture)
    {
        flush();
        m_lastTexture = &texture;
        m_invTexWidth = 1.f / texture.getWidth();
        m_invTexHeight = 1.f / texture.getHeight();
        m_lastTexture->bind(GL_TEXTURE0);

        if (m_useDeferredRenderer)
        {
            // glUniform1i(m_deferredRenderer->getShaderC().getUniformLocation("u_colormap"), 0);
        }
        else if (m_useForwardRenderer)
        {
            glUniform1i(m_forwardRenderer->getShader().getUniformLocation("u_colormap"), 0);
        }
        else
        {
            glUniform1i(m_shader->getUniformLocation("u_colormap"), 0);
        }
    }

    ShaderProgram& SpriteBatch::createDefaultShader()
    {
        std::string vertexShader =
            "#version 330 core\n"

            "layout(location = 0) in vec3 a_position;\n"
            "layout(location = 1) in vec2 a_uv;\n"
            "layout(location = 2) in vec4 a_color;\n"
            "layout(location = 3) in vec3 a_normal;\n"

            "uniform mat4 u_world;\n"

            "out vec2 uv;\n"
            "out vec4 color;\n"

            "void main() {\n"
            "  uv = a_uv;\n"
            "  color = a_color;\n"
            "  gl_Position = u_world * vec4(a_position, 1.0);\n"
            "}\n";

        std::string fragmentShader =
            "#version 330 core\n"

            "in vec4 color;\n"
            "in vec2 uv;\n"
            "out vec4 out_color;\n"
            "uniform sampler2D u_colormap;\n"

            "void main() {\n"
            "  out_color = texture2D(u_colormap, uv);\n"
            "  out_color *= color;\n"
            "  // out_color = vec4(1, 0, 1, 1);\n"
            "}\n";

        return AssetManager::loadShader(vertexShader, fragmentShader);
    }
}