#pragma once

// Interface for rendering systems that provide rendering support for
// shadow maps.
namespace Tremor
{
    class ShaderProgram;
 
    class ShadowCasterSystem
    {
    public:
        virtual void renderForShadows(ShaderProgram& shader) = 0;
    };
}