#pragma once


#include "components/Sprite.h"

namespace Tremor
{
    class Camera;
    class ForwardRenderer;
    class DeferredRenderer;
    class Text2D;

    class SpriteBatch
    {
    public:
        // Constructs a new sprite batch with its own renderer
        SpriteBatch();
        SpriteBatch(int width, int height);
        SpriteBatch(DeferredRenderer& renderer);
        SpriteBatch(ForwardRenderer& renderer);

        ~SpriteBatch();

        void setFromCamera(Camera& camera);
        void setScreen(float x, float y, float width, float height);

        void begin();
        void draw(Sprite& sprite);
        void drawPtr(Sprite* sprite);
        void drawText(Text2D& text);
        void drawTextPtr(Text2D* text);
        void end();

    private:
        void initialize();

        bool m_drawing;

        bool m_useDeferredRenderer;
        bool m_useForwardRenderer;

        void flush();

        Mesh m_mesh;
        Texture* m_lastTexture;
        ShaderProgram* m_shader;

        DeferredRenderer* m_deferredRenderer;
        ForwardRenderer* m_forwardRenderer;

        float m_invTexWidth;
        float m_invTexHeight;

        glm::mat4 m_modelMatrix;
        glm::mat4 m_viewMatrix;
        glm::mat4 m_projectionMatrix;
        glm::mat4 m_combinedMatrix;

        int m_idx;
        bool m_ownsShader;

        bool m_blendingDisabled;

        int m_renderCalls;
        int m_totalRenderCalls;
        int m_maxSpritesInBatch;

        void switchTexture(Texture& texture);

        static ShaderProgram& createDefaultShader();

        bool m_roundPositions;

        int m_width;
        int m_height;
    };
}