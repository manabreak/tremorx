#include "stdafx.h"
#include "ShaderProgram.h"
#include <sstream>

namespace Tremor
{

    ShaderProgram::ShaderProgram(const std::string& srcVertex, const std::string& srcFragment)
        : m_bound(false),
        m_compiled(false),
        m_program(0),
        m_vertexShader(0),
        m_fragmentShader(0)
    {
        compile(srcVertex, srcFragment);
        parseUniformLocations(srcVertex);
        parseUniformLocations(srcFragment);
    }

    ShaderProgram::~ShaderProgram()
    {
        if (m_compiled && m_program)
        {
            glDeleteProgram(m_program);
        }
    }

    void ShaderProgram::parseUniformLocations(const std::string& src)
    {
        std::string line;
        std::string first;
        std::stringstream srcStream(src);
        while (!srcStream.eof())
        {
            std::stringstream lineStream;
            std::getline(srcStream, line);
            lineStream << line;
            
            lineStream >> first;

            if (first == "uniform")
            {
                std::string type;
                lineStream >> type;
                std::string name;
                lineStream >> name;
                name = name.substr(0, name.size() - 1);

                GLuint location = glGetUniformLocation(m_program, name.c_str());
                if (location > 1000) continue;

                if (location == m_uniforms.size()) m_uniforms.push_back(name);
                else if (location >= m_uniforms.size())
                {
                    m_uniforms.resize(location);
                    m_uniforms.push_back(name);
                }
            }
        }
    }

    bool ShaderProgram::isCompiled()
    {
        return m_compiled;
    }


    int ShaderProgram::getUniformLocation(const std::string& name)
    {
        for (unsigned int i = 0; i < m_uniforms.size(); ++i)
        {
            if (m_uniforms[i] == name) return i;
        }

        int loc = glGetUniformLocation(m_program, &name[0]);
        return loc;
    }

    void ShaderProgram::begin()
    {
        if (!m_bound)
        {
            glUseProgram(m_program);
            m_bound = true;
        }
    }

    void ShaderProgram::end()
    {
        if (m_bound)
        {
            glUseProgram(0);
            m_bound = false;
        }
    }

    void ShaderProgram::compile(const std::string& srcVertex, const std::string& srcFragment)
    {
        m_vertexShader = loadShader(GL_VERTEX_SHADER, srcVertex);
        m_fragmentShader = loadShader(GL_FRAGMENT_SHADER, srcFragment);
        m_program = linkProgram();

        glDeleteShader(m_vertexShader);
        glDeleteShader(m_fragmentShader);

        m_compiled = true;
        m_bound = false;
    }

    int ShaderProgram::loadShader(int type, const std::string& src)
    {
        int shader = glCreateShader(type);
        if (shader == 0) return -1;

        char const* srcPointer = src.c_str();
        glShaderSource(shader, 1, &srcPointer, NULL);
        glCompileShader(shader);

        GLint result = GL_FALSE;
        int infoLogLength;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
        if (result == GL_FALSE)
        {
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

            std::vector<char> errorMsg(infoLogLength);
            int len;
            glGetShaderInfoLog(shader, infoLogLength, &len, &errorMsg[0]);
            std::string msgString = std::string(&errorMsg[0]);
            Log::log(msgString);
        }

        return shader;
    }

    int ShaderProgram::linkProgram()
    {
        int program = glCreateProgram();
        glAttachShader(program, m_vertexShader);
        glAttachShader(program, m_fragmentShader);
        glLinkProgram(program);

        GLint result = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &result);

        return program;
    }

    void ShaderProgram::setUniform(const std::string& name, int value)
    {
        glUniform1i(getUniformLocation(name), value);
    }

    void ShaderProgram::setUniform(const std::string& name, float value)
    {
        glUniform1f(getUniformLocation(name), value);
    }

    void ShaderProgram::setUniform(const std::string& name, const glm::vec2& value)
    {
        glUniform2fv(getUniformLocation(name), 1, &value[0]);
    }

    void ShaderProgram::setUniform(const std::string& name, const glm::vec3& value)
    {
        glUniform3fv(getUniformLocation(name), 1, &value[0]);
    }

    void ShaderProgram::setUniform(const std::string& name, const glm::mat4& value)
    {
        glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, &value[0][0]);
    }

    void ShaderProgram::setUniform(unsigned int location, int value)
    {
        glUniform1i(location, value);
    }

    void ShaderProgram::setUniform(unsigned int location, float value)
    {
        glUniform1f(location, value);
    }

    void ShaderProgram::setUniform(unsigned int location, const glm::vec2& value)
    {
        glUniform2fv(location, 1, &value[0]);
    }

    void ShaderProgram::setUniform(unsigned int location, const glm::vec3& value)
    {
        glUniform3fv(location, 1, &value[0]);
    }

    void ShaderProgram::setUniform(unsigned int location, const glm::mat4& value)
    {
        glUniformMatrix4fv(location, 1, GL_FALSE, &value[0][0]);
    }
}