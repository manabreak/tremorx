#include "stdafx.h"
#include "DeferredLightsController.h"
#include "core/Tremor.h"
#include "core/Graphics.h"
#include "assets/AssetManager.h"
#include "utils/MeshFactory.h"
#include "Camera.h"
#include "DeferredRenderer.h"
#include "lights/PointLight.h"
#include "lights/SpotLight.h"

#include <chrono>

#define SHADOW_MAP_SIZE 2048

namespace Tremor
{
    DeferredLightsController::DeferredLightsController(DeferredRenderer& renderer)
        : m_renderer(renderer),
        m_shadowMapSize(SHADOW_MAP_SIZE),
        m_width(Tremor::graphics->getWidth()),
        m_height(Tremor::graphics->getHeight()),
        m_paraShader(createParaShader()),
        m_depthShader(createDepthShader()),
        m_spotShader(createSpotShader()),
        m_spotShadowShader(createSpotShadowShader()),
        m_pointShader(createPointShader()),
        m_pointShadowShader(createPointShadowShader()),
        m_spotMesh(MeshFactory::createSpotLightCone()),
        m_pointMesh(MeshFactory::createUnitSphere(0)),
        m_lightTexture(Tremor::graphics->getWidth(), Tremor::graphics->getHeight(), TextureFormat::RGBA8888),
        m_shadowTexture(m_shadowMapSize, m_shadowMapSize, TextureFormat::DEPTH),
        m_camera(nullptr)
    {
        init();
    }

    DeferredLightsController::DeferredLightsController(DeferredRenderer& renderer, int width, int height)
        : m_renderer(renderer),
        m_shadowMapSize(SHADOW_MAP_SIZE),
        m_width(width),
        m_height(height),
        m_paraShader(createParaShader()),
        m_depthShader(createDepthShader()),
        m_spotShader(createSpotShader()),
        m_spotShadowShader(createSpotShadowShader()),
        m_pointShader(createPointShader()),
        m_pointShadowShader(createPointShadowShader()),
        m_spotMesh(MeshFactory::createSpotLightCone()),
        m_pointMesh(MeshFactory::createUnitSphere(0)),
        m_lightTexture(width, height, TextureFormat::RGBA8888),
        m_shadowTexture(m_shadowMapSize, m_shadowMapSize, TextureFormat::DEPTH),
        m_camera(nullptr)
    {
        init();
    }

    void DeferredLightsController::init()
    {
        glGenFramebuffers(1, &m_lightBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, m_lightBuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_lightTexture.getHandle(), 0);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Yaaarrrggh 1!\n");
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        glGenFramebuffers(1, &m_shadowBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, m_shadowBuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_shadowTexture.getHandle(), 0);
        glDrawBuffer(GL_NONE);
        status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Yaaarrrggh 2!\n");
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        for (int i = 0; i < 6; ++i)
        {
            m_pyramidMeshes.emplace_back(MeshFactory::createUnitPyramid(i));
        }
    }

    void DeferredLightsController::addRenderSystem(ShadowCasterSystem* system)
    {
        m_renderSystems.push_back(system);
    }

    void DeferredLightsController::begin(Camera& camera)
    {
        m_camera = &camera;
        m_pointLights.clear();
        m_pointLightsShadow.clear();
        m_spotLights.clear();
        m_spotLightsShadow.clear();
        m_rendering = true;

        /*
        glBindFramebuffer(GL_FRAMEBUFFER, m_lightBuffer);
        glViewport(0, 0, m_width, m_height);
        // glClearColor(1.0f, 0.1f, 0.01f, 0.f);
        // glClearColor(0.f, 0.f, 0.f, 0.f);
        glClearColor(m_ambientColor.r, m_ambientColor.g, m_ambientColor.b, 0.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDepthMask(GL_FALSE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);

        m_renderer.getGBuffer().getNormalResult().bind(GL_TEXTURE0);
        m_renderer.getGBuffer().getDepthResult().bind(GL_TEXTURE1);
        */
        
    }

    void DeferredLightsController::renderPointLight(PointLight& light)
    {
        assert(m_rendering);
        if (light.isShadowsEnabled())
        {
            m_pointLightsShadow.push_back(&light);
        }
        else
        {
            m_pointLights.push_back(&light);
        }
    }

    void DeferredLightsController::renderSpotLight(SpotLight& light)
    {
        assert(m_rendering);
        if (light.isShadowsEnabled())
        {
            m_spotLightsShadow.push_back(&light);
        }
        else
        {
            m_spotLights.push_back(&light);
        }
    }

    void DeferredLightsController::bindLightBuffer(bool clear)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, m_lightBuffer);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        glDepthMask(GL_FALSE);
        glCullFace(GL_BACK);
        
        if (clear)
        {
            glClearColor(m_ambientColor.r, m_ambientColor.g, m_ambientColor.b, 0.f);
            // glClearColor(0.51f, 0.51f, 0.51f, 1.f);
            glClear(GL_COLOR_BUFFER_BIT);
        }
        glViewport(0, 0, m_lightTexture.getWidth(), m_lightTexture.getHeight());
        m_renderer.getGBuffer().getNormalResult().bind(GL_TEXTURE0);
        m_renderer.getGBuffer().getDepthResult().bind(GL_TEXTURE1);
    }

    void DeferredLightsController::bindShadowBuffer(bool clear)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, m_shadowBuffer);
        glDisable(GL_BLEND);
        glDepthMask(GL_TRUE);
        glEnable(GL_DEPTH_TEST);

        if (clear)
        {
            glClear(GL_DEPTH_BUFFER_BIT);
        }
        glViewport(0, 0, m_shadowTexture.getWidth(), m_shadowTexture.getHeight());
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
    }

    void DeferredLightsController::end()
    {
        float camNear = m_camera->getNear();
        float camFar = m_camera->getFar();

        bindLightBuffer(true);

        // Normal point lights
        renderPointLights();

        // Normal spot lights
        renderSpotLights();

        // Shadow-casting point lights
        renderShadowPointLights();

        // Shadow-casting spot lights
        renderShadowSpotLights();

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        m_camera->setNear(camNear);
        m_camera->setFar(camFar);
    }

    void DeferredLightsController::renderPointLights()
    {
        m_pointShader.begin();
        for (unsigned int i = 0; i < m_pointLights.size(); ++i)
        {
            PointLight& light = *m_pointLights[i];

            m_pointShader.setUniform("u_camproj", m_camera->getProjection());
            m_pointShader.setUniform("u_camview", m_camera->getView());
            m_pointShader.setUniform("u_invviewproj", m_camera->getInverseViewProjection());
            m_pointShader.setUniform("u_normal", 0);
            m_pointShader.setUniform("u_depth", 1);
            m_pointShader.setUniform("u_lightColor", light.getColor());
            m_pointShader.setUniform("u_lightIntensity", light.getIntensity());
            m_pointShader.setUniform("u_lightPosition", light.getFinalPosition());
            m_pointShader.setUniform("u_lightRadius", light.getRadius());

            glm::mat4 transform = glm::translate(light.getFinalPosition()) * glm::scale(glm::vec3(light.getRadius()));
            m_pointShader.setUniform("u_world", transform);
            m_pointMesh->render();
        }
        m_pointShader.end();
    }

    void DeferredLightsController::renderShadowPointLights()
    {
        using namespace std::chrono;

        auto shadowsStart = high_resolution_clock::now();
        for (unsigned int i = 0; i < m_pointLightsShadow.size(); ++i)
        {
            PointLight& light = *m_pointLightsShadow[i];
            glm::mat4 perspective = glm::perspective(90.f, 1.f, 0.1f, light.getRadius());

            for (unsigned int v = 0; v < 6; ++v)
            {
                glm::mat4 lightWVP;
                switch (v)
                {
                case 0: lightWVP = glm::lookAt(light.getFinalPosition(), light.getFinalPosition() + glm::vec3(0.f, 0.f, -1.f), glm::vec3(1.f, 0.f, 0.f));
                    break;
                case 1: lightWVP = glm::lookAt(light.getFinalPosition(), light.getFinalPosition() + glm::vec3(0.f, 0.f, 1.f), glm::vec3(1.f, 0.f, 0.f));
                    break;
                case 2: lightWVP = glm::lookAt(light.getFinalPosition(), light.getFinalPosition() + glm::vec3(1.f, 0.f, 0.f), glm::vec3(0.f, 0.f, 1.f));
                    break;
                case 3: lightWVP = glm::lookAt(light.getFinalPosition(), light.getFinalPosition() + glm::vec3(-1.f, 0.f, 0.f), glm::vec3(0.f, 0.f, 1.f));
                    break;
                case 4: lightWVP = glm::lookAt(light.getFinalPosition(), light.getFinalPosition() + glm::vec3(0.f, 1.f, 0.f), glm::vec3(0.f, 0.f, 1.f));
                    break;
                case 5: lightWVP = glm::lookAt(light.getFinalPosition(), light.getFinalPosition() + glm::vec3(0.f, -1.f, 0.f), glm::vec3(0.f, 0.f, 1.f));
                    break;
                }

                bindShadowBuffer(true);
                m_depthShader.begin();
                m_depthShader.setUniform("u_proj", perspective);
                m_depthShader.setUniform("u_view", lightWVP);
                for (unsigned int j = 0; j < m_renderSystems.size(); ++j)
                {
                    m_renderSystems[j]->renderForShadows(m_depthShader);
                }
                m_depthShader.end();

                m_shadowTexture.bind(GL_TEXTURE2);

                bindLightBuffer(false);
                m_pointShadowShader.begin();

                m_pointShadowShader.setUniform("u_camproj", m_camera->getProjection());
                m_pointShadowShader.setUniform("u_camview", m_camera->getView());
                m_pointShadowShader.setUniform("u_cameraPosition", m_camera->getPosition());
                m_pointShadowShader.setUniform("u_invviewproj", m_camera->getInverseViewProjection());
                m_pointShadowShader.setUniform("u_normal", 0);
                m_pointShadowShader.setUniform("u_depth", 1);
                m_pointShadowShader.setUniform("u_shadow", 2);

                m_pointShadowShader.setUniform("u_lightColor", light.getColor());
                m_pointShadowShader.setUniform("u_lightIntensity", light.getIntensity());
                m_pointShadowShader.setUniform("u_lightPosition", light.getFinalPosition());
                m_pointShadowShader.setUniform("u_lightRadius", light.getRadius());
                m_pointShadowShader.setUniform("u_lightWVP", perspective * lightWVP);

                glm::mat4 transform = glm::translate(light.getFinalPosition()) * glm::scale(glm::vec3(light.getRadius()));
                m_pointShadowShader.setUniform("u_world", transform);
                m_pyramidMeshes[v]->render();

                m_pointShadowShader.end();
            }
        }
        auto shadowsEnd = high_resolution_clock::now();
        auto shadowsDuration = shadowsEnd - shadowsStart;
        auto shadowsMs = duration_cast<milliseconds>(shadowsDuration);
        // std::cout << "Shadows took: " << shadowsMs.count() << " ms." << std::endl;
    }

    void DeferredLightsController::renderSpotLights()
    {
        m_spotShader.begin();

        for (unsigned int i = 0; i < m_spotLights.size(); ++i)
        {
            SpotLight& light = *m_spotLights[i];
            m_spotShader.setUniform("u_camproj", m_camera->getProjection());
            m_spotShader.setUniform("u_camview", m_camera->getView());
            m_spotShader.setUniform("u_invviewproj", m_camera->getInverseViewProjection());
            m_spotShader.setUniform("u_normal", 0);
            m_spotShader.setUniform("u_depth", 1);

            m_spotShader.setUniform("u_lightPosition", light.getFinalPosition());
            m_spotShader.setUniform("u_lightDirection", light.getDirection());
            m_spotShader.setUniform("u_lightColor", light.getColor());
            m_spotShader.setUniform("u_lightRange", light.getRange());
            m_spotShader.setUniform("u_lightIntensity", light.getIntensity());
            m_spotShader.setUniform("u_lightAngle", glm::radians(light.getAngle() / 2.f));

            float scl = glm::tan(glm::radians(light.getAngle() / 2.f));
            glm::mat4 transform = glm::translate(light.getFinalPosition()) * glm::scale(glm::vec3(light.getRange())) * glm::scale(glm::vec3(scl, 1.f, scl));
            m_spotShader.setUniform("u_world", transform);
            m_spotMesh->render();
        }
        m_spotShader.end();
    }

    void DeferredLightsController::renderShadowSpotLights()
    {
        for (unsigned int i = 0; i < m_spotLightsShadow.size(); ++i)
        {
            SpotLight& light = *m_spotLightsShadow[i];

            glm::mat4 proj = glm::perspective(light.getAngle(), 1.f, 30.f, light.getRange());
            glm::mat4 view = glm::lookAt(light.getFinalPosition(), light.getFinalPosition() + glm::vec3(0.f, -1.f, 0.f), glm::vec3(0.f, 0.f, -1.f));

            bindShadowBuffer(true);
            m_depthShader.begin();

            m_depthShader.setUniform("u_proj", proj);
            m_depthShader.setUniform("u_view", view);
            for (unsigned int j = 0; j < m_renderSystems.size(); ++j)
            {
                m_renderSystems[j]->renderForShadows(m_depthShader);
            }
            m_depthShader.end();

            m_shadowTexture.bind(GL_TEXTURE2);

            bindLightBuffer(false);
            m_spotShadowShader.begin();
            m_spotShadowShader.setUniform("u_camproj", m_camera->getProjection());
            m_spotShadowShader.setUniform("u_camview", m_camera->getView());
            m_spotShadowShader.setUniform("u_invviewproj", m_camera->getInverseViewProjection());
            m_spotShadowShader.setUniform("u_normal", 0);
            m_spotShadowShader.setUniform("u_depth", 1);
            m_spotShadowShader.setUniform("u_shadow", 2);

            m_spotShadowShader.setUniform("u_lightWVP", proj * view);
            m_spotShadowShader.setUniform("u_lightPosition", light.getFinalPosition());
            m_spotShadowShader.setUniform("u_lightDirection", light.getDirection());
            m_spotShadowShader.setUniform("u_lightColor", light.getColor());
            m_spotShadowShader.setUniform("u_lightRange", light.getRange());
            m_spotShadowShader.setUniform("u_lightIntensity", light.getIntensity());
            m_spotShadowShader.setUniform("u_lightAngle", glm::radians(light.getAngle() / 2.f));

            float scl = glm::tan(glm::radians(light.getAngle() / 2.f));
            glm::mat4 transform = glm::translate(light.getFinalPosition()) * glm::scale(glm::vec3(light.getRange())) * glm::scale(glm::vec3(scl, 1.f, scl));
            m_spotShadowShader.setUniform("u_world", transform);
            m_spotMesh->render();

            m_spotShadowShader.end();
        }
    }

    Texture& DeferredLightsController::getLightsResult()
    {
        return m_lightTexture;
    }

    Texture& DeferredLightsController::getShadowResult()
    {
        return m_shadowTexture;
    }

    void DeferredLightsController::setAmbientLight(float r, float g, float b)
    {
        m_ambientColor.r = r;
        m_ambientColor.g = g;
        m_ambientColor.b = b;
    }

    ShaderProgram& DeferredLightsController::createPointShader()
    {
        std::string vs =
            "#version 330 core\n"
            "layout(location = 0) in vec3 a_position;\n"

            "uniform mat4 u_camproj;\n"
            "uniform mat4 u_camview;\n"
            "uniform mat4 u_world;\n"

            "out vec2 uv;\n"
            "out vec4 screenPosition;\n"

            "void main() {\n"
            "  gl_Position = u_camproj * u_camview * u_world * vec4(a_position, 1.0);\n"
            "  screenPosition = gl_Position;\n"
            "}\n"
            ;

        std::string fs =
            "#version 330 core\n"
            "in vec4 screenPosition;\n"
            "layout(location = 0) out vec4 result;\n"

            "uniform mat4 u_invviewproj;\n"

            "uniform sampler2D u_normal;\n"
            "uniform sampler2D u_depth;\n"

            "in vec2 uv;\n"

            "uniform vec3 u_lightPosition;\n"
            "uniform float u_lightRadius;\n"
            "uniform float u_lightIntensity;\n"
            "uniform vec3 u_lightColor;\n"

            "const float ringFactor = 7.0;\n"

            "void main() {\n"
            "  result = vec4(0);\n"
            "  vec4 scrPos = screenPosition;\n"
            "  scrPos.xy /= scrPos.w;\n"
            "  vec2 texcoord = 0.5 * (vec2(scrPos.xy) + 1.0);\n"
            "  vec3 normal = normalize(texture2D(u_normal, texcoord).xyz * 2.0 - 1.0);\n"
            "  vec4 position = vec4(texcoord.x, texcoord.y, texture2D(u_depth, texcoord).x, 1.0);\n"

            "  position = u_invviewproj * (position * 2.0 - 1.0);\n"
            "  position /= position.w;\n"

            "  vec3 pixelToLight = u_lightPosition - position.xyz;\n"
            "  vec3 normPTL = normalize(pixelToLight);\n"
            "  float ndl = max(0.0, dot(normal, normPTL));\n"

            "  float d = length(pixelToLight);\n"
            "  float dist = d / u_lightRadius;\n"
            "  dist = clamp(dist, 0.0, 1.0);\n"

            "  float a = clamp(1.0 - dist, 0.0, 1.0);\n"
            "  a *= a;\n"

            "  result = vec4(ndl * vec3(round(a * ringFactor) / ringFactor) * a * u_lightColor * u_lightIntensity, 0.0);\n"
            "}\n"
            ;

        return AssetManager::loadShader(vs, fs);
    }

    ShaderProgram& DeferredLightsController::createPointShadowShader()
    {
        std::string vs =
            "#version 330 core\n"
            "layout(location = 0) in vec3 a_position;\n"

            "uniform mat4 u_camproj;\n"
            "uniform mat4 u_camview;\n"
            "uniform mat4 u_world;\n"

            "out vec2 uv;\n"
            "out vec4 screenPosition;\n"

            "void main() {\n"
            "  gl_Position = u_camproj * u_camview * u_world * vec4(a_position, 1.0);\n"
            "  screenPosition = gl_Position;\n"
            "}\n"
            ;

        std::string fs =
            "#version 330 core\n"

            "const vec2 poissonDisk[4] = vec2 []("
            "    vec2(-0.94201624, -0.39906216),"
            "    vec2(0.94558609, -0.76890725),"
            "    vec2(-0.094184101, -0.92938870),"
            "    vec2(0.34495938, 0.29387760)"
            ");\n"

            "const mat4 biasMatrix = mat4(vec4(0.5, 0.0, 0.0, 0.0),"
            "                             vec4(0.0, 0.5, 0.0, 0.0),"
            "                             vec4(0.0, 0.0, 0.5, 0.0),"
            "                             vec4(0.5, 0.5, 0.5, 1.0));\n"

            "in vec4 screenPosition;\n"
            "layout(location = 0) out vec4 result;\n"

            "uniform mat4 u_invviewproj;\n"

            "uniform sampler2D u_normal;\n"
            "uniform sampler2D u_depth;\n"

            "uniform sampler2D u_shadow;\n"

            "in vec2 uv;\n"

            "uniform mat4 u_lightWVP;\n"
            "uniform vec3 u_lightPosition;\n"
            "uniform float u_lightRadius;\n"
            "uniform float u_lightIntensity;\n"
            "uniform vec3 u_lightColor;\n"

            "uniform vec3 u_cameraPosition;\n"

            "const float ringFactor = 7.0;\n"

            "void main() {\n"
            "  result = vec4(0);\n"

            "  vec4 scrPos = screenPosition;\n"
            "  scrPos.xy /= scrPos.w;\n"
            "  vec2 texcoord = 0.5 * (vec2(scrPos.xy) + 1.0);\n"
            "  vec4 normal = texture2D(u_normal, texcoord) * 2.0 - 1.0;\n"
            "  vec4 position = vec4(texcoord.x, texcoord.y, texture2D(u_depth, texcoord).x, 1.0);\n"

            "      position = u_invviewproj * (position * 2.0 - 1.0);\n"
            "      position /= position.w;\n"


            "      vec3 pixelToLight = u_lightPosition - position.xyz;\n"
            "      vec3 normPTL = normalize(pixelToLight);\n"
            "      float ndl = max(0.0, dot(normal.xyz, normPTL));\n"

            "          vec4 shadowCoord = biasMatrix * u_lightWVP * position;\n"
            "          shadowCoord /= shadowCoord.w;\n"
            "          if(shadowCoord.x >= 0.0 && shadowCoord.x <= 1.0 && shadowCoord.y >= 0.0 && shadowCoord.y <= 1.0) {\n"

            "              float shadow = texture2D(u_shadow, shadowCoord.xy).x;\n"
            "              // float shadow = texture2D(u_shadow, shadowCoord.xy + vec2(-normPTL.x / 180.0, normPTL.z / 120.0)).x; \n"

            "              float d = length(pixelToLight);\n"
            "              float rad2 = u_lightRadius;\n"
            "              float dist = d / u_lightRadius;\n"
            "              dist = clamp(dist, 0.0, 1.0);\n"

            "              float a = clamp(1.0 - d / rad2, 0.0, 1.0);\n"
            "              a *= a;\n"

            "              float resultAmount = 1.0;\n"

            "              for(int i = 0; i < 4; i++) {\n"
            "                  if(texture2D(u_shadow, shadowCoord.xy + poissonDisk[i] / 550.0).x < shadowCoord.z) {\n"
            "                      resultAmount -= 0.25;\n"
            "                  }\n"
            "              }\n"
            "              result = vec4(ndl * vec3(round(a * ringFactor) / ringFactor) * a * u_lightColor, 0.0);\n"
            "              float gamma = 1.0 / 2.2;\n"
            "              result.xyz = vec3(pow(result.x, gamma), pow(result.y, gamma), pow(result.z, gamma));\n"
            "              result *= resultAmount;\n"
            /*
            "              if(shadow >= (shadowCoord.z)) {\n"
            //                 Specular
            "                  vec3 reflectionVector = reflect(-normPTL, normal.xyz);\n"
            "                  vec3 dirToCam = normalize(u_cameraPosition - position.xyz);\n"
            "                  float cosAlpha = clamp(dot(dirToCam, reflectionVector), 0.0, 1.0);\n"
            "                  float specularLight = 0.7 * pow(cosAlpha, 512.0) / (a * a);\n"

            "                  specularLight *= clamp(dot(normal.xyz, vec3(0, 0, 1)), 0, 1) * pow(ndl, 256.0);\n"
            "                  result = vec4(ndl * vec3(round(a * ringFactor) / ringFactor) * a * u_lightColor, specularLight);\n"

            "                  float gamma = 1.0 / 2.2;\n"
            "                  result.xyz = vec3(pow(result.x, gamma), pow(result.y, gamma), pow(result.z, gamma));\n"
            "              }\n"
            */
            
            "          }\n"
            "}\n"
            ;

        return AssetManager::loadShader(vs, fs);
    }

    ShaderProgram& DeferredLightsController::createSpotShader()
    {
        std::string vs =
            "#version 330 core\n"
            "layout(location = 0) in vec3 a_position;\n"

            "uniform mat4 u_camproj;\n"
            "uniform mat4 u_camview;\n"
            "uniform mat4 u_world;\n"

            "out vec2 uv;\n"
            "out vec4 screenPosition;\n"

            "void main() {\n"
            "  gl_Position = u_camproj * u_camview * u_world * vec4(a_position, 1.0);\n"
            "  screenPosition = gl_Position;\n"
            "}\n";

        std::string fs =
            "#version 330 core\n"
            "in vec4 screenPosition;\n"
            "layout(location = 0) out vec3 result;\n"

            "uniform mat4 u_invviewproj;\n"
            "uniform sampler2D u_depth;\n"
            "uniform sampler2D u_normal;\n"
            "in vec2 uv;\n"

            "uniform vec3 u_lightPosition;\n"
            "uniform vec3 u_lightDirection;\n"
            "uniform float u_lightAngle;\n"
            "uniform vec3 u_lightColor;\n"
            "uniform float u_lightRange;\n"
            "uniform float u_lightIntensity;\n"

            "const float ringFactor = 4.0;\n"

            "void main() {\n"
            "  result = vec3(0);\n"
            "  vec4 scrPos = screenPosition;\n"
            "  scrPos.xy /= scrPos.w;\n"
            "  vec2 texcoord = 0.5 * (vec2(scrPos.xy) + 1.0);\n"
            "  vec3 normal = normalize(texture2D(u_normal, texcoord).xyz * 2.0 - 1.0);\n"
            "  vec4 position = vec4(texcoord.x, texcoord.y, texture2D(u_depth, texcoord).x, 1.0);\n"
            "  position = u_invviewproj * (position * 2.0 - 1.0);\n"
            "  position /= position.w;\n"

            "  vec3 pixelToLight = u_lightPosition - position.xyz;\n"
            "  vec3 normPTL = normalize(pixelToLight);\n"
            "  float ndl = max(0.0, dot(normal, normPTL));\n"
            "  // ndl = clamp(ndl * 2, 0.0, 1.0);\n"
            "  vec3 diffuseLight = ndl * u_lightColor;\n"

            "  float rho = dot(normPTL, -u_lightDirection);\n"
            "  float rhoAngle = acos(rho);\n"
            "  if(rhoAngle < u_lightAngle) {\n"
            "    // result += 0.1;\n"
            "    float cosLightAngle = cos(u_lightAngle);\n"
            "    float factor = (rho - cosLightAngle) / (1.2 - (cosLightAngle));\n"
            "    factor = pow(factor, 1.4);\n"
            "    // factor = ceil(factor * 4.0) / 4.0; \n"
            "    result = diffuseLight * factor * u_lightIntensity;\n"
            "    // result = round(result * ringFactor) / ringFactor;\n"
            "  }\n"
            "}\n";

        return AssetManager::loadShader(vs, fs);
    }

    ShaderProgram& DeferredLightsController::createSpotShadowShader()
    {
        std::string vs =
            "#version 330 core\n"
            "layout(location = 0) in vec3 a_position;\n"

            "uniform mat4 u_camproj;\n"
            "uniform mat4 u_camview;\n"
            "uniform mat4 u_world;\n"

            "out vec4 screenPosition;\n"

            "void main() {\n"
            "  gl_Position = u_camproj * u_camview * u_world * vec4(a_position, 1.0);\n"
            "  screenPosition = gl_Position;\n"
            "}\n";

        std::string fs =
            "#version 330 core\n"
            "in vec4 screenPosition;\n"
            "layout(location = 0) out vec3 result;\n"

            "const mat4 biasMatrix = mat4(vec4(0.5, 0.0, 0.0, 0.0),\n"
            "                             vec4(0.0, 0.5, 0.0, 0.0),\n"
            "                             vec4(0.0, 0.0, 0.5, 0.0),\n"
            "                             vec4(0.5, 0.5, 0.5, 1.0));\n"

            "uniform mat4 u_invviewproj;\n"
            "uniform sampler2D u_depth;\n"
            "uniform sampler2D u_normal;\n"
            "uniform sampler2D u_shadow;\n"

            "uniform mat4 u_lightWVP;\n"
            "uniform vec3 u_lightPosition;\n"
            "uniform vec3 u_lightDirection;\n"
            "uniform float u_lightAngle;\n"
            "uniform vec3 u_lightColor;\n"
            "uniform float u_lightRange;\n"
            "uniform float u_lightIntensity;\n"

            "void main() {\n"
            "  result = vec3(0);\n"
            "  vec4 scrPos = screenPosition;\n"
            "  scrPos.xy /= scrPos.w;\n"
            "  vec2 texcoord = 0.5 * (vec2(scrPos.xy) + 1.0);\n"
            "  vec3 normal = normalize(texture2D(u_normal, texcoord).xyz * 2.0 - 1.0);\n"
            "  vec4 position = vec4(texcoord.x, texcoord.y, texture2D(u_depth, texcoord).x, 1.0);\n"

            "    position = u_invviewproj * (position * 2.0 - 1.0);\n"
            "    position /= position.w;\n"

            "    vec3 pixelToLight = u_lightPosition - position.xyz;\n"
            "    vec3 normPTL = normalize(pixelToLight);\n"
            "    float ndl = max(0.0, dot(normal, normPTL));\n"

            "    vec4 lightClipPos = biasMatrix * u_lightWVP * position;\n"
            "    lightClipPos /= lightClipPos.w;\n"
            "    float shadow = texture2D(u_shadow, lightClipPos.xy + vec2(-normPTL.x / 90.0, normPTL.z / 60.0)).x;\n" // T�RKE� OFFSET!
            "    float bias = 0.005;\n"
            "    if(shadow > (lightClipPos.z - bias)) {\n"

            "      vec3 diffuseLight = ndl * u_lightColor;\n"
            "      float rho = dot(normPTL, -u_lightDirection);\n"
            "      float rhoAngle = acos(rho);\n"

            "      if(rhoAngle < u_lightAngle) {\n"

            "        float cosLightAngle = cos(u_lightAngle);\n"
            "        float factor = (rho - cosLightAngle) / (1.2 - (cosLightAngle));\n"
            "        // factor = pow(factor, 1.4);\n"

            "        float d = length(pixelToLight);\n"
            "        float dist = d / u_lightRange;\n"
            "        dist = clamp(dist, 0.0, 1.0);\n"
            "        float a = clamp(1.0 - dist, 0.0, 1.0);\n"
            "        a *= a;\n"

            "        result = diffuseLight * factor * u_lightIntensity;\n"
            "      }\n"
            "      // result = ndl * vec3(1);\n"

            "    }\n"
            "}\n";

        return AssetManager::loadShader(vs, fs);
    }

    ShaderProgram& DeferredLightsController::createDepthShader()
    {
        std::string vs =
            "#version 330 core\n"
            "layout(location = 0) in vec3 a_position;\n"
            "uniform mat4 u_proj;\n"
            "uniform mat4 u_view;\n"
            "uniform mat4 u_world;\n"
            "void main() {\n"
            "  gl_Position = u_proj * u_view * u_world * vec4(a_position, 1.0);\n"
            "}\n"
            ;

        std::string fs =
            "#version 330 core\n"
            "void main() {\n"
            "  // gl_FragColor = vec4(1.0);\n"
            "}\n"
            ;

        return AssetManager::loadShader(vs, fs);
    }

    ShaderProgram& DeferredLightsController::createParaShader()
    {
        std::string vs =
            "#version 330 core\n"

            "layout(location = 0) in vec3 a_position;\n"

            "uniform mat4 u_proj;\n"
            "uniform mat4 u_view;\n"
            "uniform mat4 u_world;\n"

            "void main() {\n"

            "  gl_Position = u_proj * u_view * u_world * vec4(a_position, 1.0); \n"
            "  "

            "}\n"
            ;

        std::string fs =
            "#version 330 core\n"
            "void main() {\n"
            "}\n"
            ;

        return AssetManager::loadShader(vs, fs);
    }
}