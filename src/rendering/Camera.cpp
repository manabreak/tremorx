#include "stdafx.h"
#include "Camera.h"
#include "core/Tremor.h"
#include "components/Transform.h"

namespace Tremor
{
    Camera::Camera()
        : m_width(Tremor::graphics->getWidth()),
        m_height(Tremor::graphics->getHeight()),
        m_ortho(true),
        m_viewDirty(true),
        m_projDirty(true),
        m_near(1.f),
        m_far(100.f),
        m_direction(0.f, 0.f, -1.f),
        m_up(0.f, 1.f, 0.f)
    {

    }

    Camera::Camera(float width, float height)
        : m_width(width),
        m_height(height),
        m_ortho(true),
        m_viewDirty(true),
        m_projDirty(true),
        m_near(1.f),
        m_far(100.f),
        m_direction(0.f, 0.f, -1.f),
        m_up(0.f, 1.f, 0.f)
    {

    }

    Camera::~Camera()
    {

    }

    void Camera::lookAtXYZ(float x, float y, float z)
    {
        lookAt(glm::vec3(x, y, z));
    }

    void Camera::lookAt(const glm::vec3& target)
    {
        m_direction = target - m_position;
        m_direction = glm::normalize(m_direction);
        m_rotation = glm::rotation(glm::vec3(0.f, 0.f, -1.f), m_direction);
        m_up = glm::vec3(0.f, 1.f, 0.f) * m_rotation;
        m_viewDirty = true;
    }

    const glm::mat4& Camera::getView()
    {
        update();
        return m_view;
    }

    const glm::mat4& Camera::getProjection()
    {
        update();
        return m_projection;
    }

    const glm::mat4& Camera::getViewProjection()
    {
        update();
        return m_combined;
    }

    const glm::mat4& Camera::getInverseViewProjection()
    {
        update();
        return m_invViewProj;
    }

    const glm::vec3& Camera::getDirection()
    {
        return m_direction;
    }

    const glm::vec3& Camera::getUp()
    {
        return m_up;
    }

    float Camera::getNear()
    {
        return m_near;
    }

    float Camera::getFar()
    {
        return m_far;
    }

    void Camera::setNear(float near)
    {
        m_near = near;
        m_projDirty = true;
    }

    void Camera::setFar(float far)
    {
        m_far = far;
        m_projDirty = true;
    }

    void Camera::setSize(float width, float height)
    {
        m_width = width;
        m_height = height;
        m_projDirty = true;
    }

    void Camera::setFov(float fov)
    {
        m_fov = fov;
        m_projDirty = true;
    }

    bool Camera::isOrtho()
    {
        return m_ortho;
    }

    void Camera::setOrtho(float width, float height)
    {
        m_ortho = true;
        m_width = width;
        m_height = height;
        m_projDirty = true;
    }

    void Camera::setPerspective(float fov, float width, float height)
    {
        m_ortho = false;
        m_fov = fov;
        m_width = width;
        m_height = height;
        m_projDirty = true;
    }

    void Camera::update()
    {
        if (!m_projDirty && !m_viewDirty) return;

        if (m_projDirty)
        {
            float halfWidth = m_width / 2.f;
            float halfHeight = m_height / 2.f;

            if (m_ortho)
            {
                m_projection = glm::ortho(-halfWidth, halfWidth, -halfHeight, halfHeight, m_near, m_far);
            }
            else
            {
                m_projection = glm::perspectiveFov(m_fov, halfWidth, halfHeight, m_near, m_far);
            }
            m_projDirty = false;
        }

        if (m_viewDirty)
        {
            m_view = glm::lookAt(m_position, m_position + m_direction, m_up);
            m_viewDirty = false;
        }

        m_combined = m_projection * m_view;
        m_invViewProj = glm::inverse(m_combined);
    }

    void Camera::setPositionXYZ(float x, float y, float z)
    {
        m_position.x = x;
        m_position.y = y;
        m_position.z = z;
        m_viewDirty = true;
    }

    void Camera::setPosition(const glm::vec3& position)
    {
        m_position = position;
        m_viewDirty = true;
    }

    const glm::vec3& Camera::getPosition()
    {
        return m_position;
    }

    void Camera::translateXYZ(float x, float y, float z)
    {
        m_position.x += x;
        m_position.y += y;
        m_position.z += z;
        m_viewDirty = true;
    }

    void Camera::translate(const glm::vec3& amount)
    {
        m_position += amount;
        m_viewDirty = true;
    }

    void Camera::setRotation(const glm::quat& rotation)
    {
        m_rotation = rotation;
        m_direction = glm::vec3(0.f, 0.f, -1.f) * m_rotation;
        m_up = glm::vec3(0.f, 1.f, 0.f) * m_rotation;
        m_viewDirty = true;
    }

    const glm::quat& Camera::getRotation()
    {
        return m_rotation;
    }

    void Camera::rotate(const glm::quat& amount)
    {
        m_rotation *= amount;
        m_direction = glm::vec3(0.f, 0.f, -1.f) * m_rotation;
        m_up = glm::vec3(0.f, 1.f, 0.f) * m_rotation;
        m_viewDirty = true;
    }

    void Camera::rotateX(float radians)
    {
        rotate(glm::quat(glm::vec3(radians, 0.f, 0.f)));
    }

    glm::vec2 Camera::worldPointToScreenPoint(const glm::vec3& worldPoint)
    {
        glm::vec4 pos(worldPoint, 1.f);
        glm::vec4 result = m_view * pos;
        return glm::vec2(result.x, result.y);
    }

    glm::vec2 Camera::worldTransformToScreenPoint(Transform* transform)
    {
        glm::vec4 pos(transform->getWorldPosition(), 1.f);

        glm::vec4 result = m_view * pos;
        return glm::vec2(result.x, result.y);
    }

    glm::vec3 Camera::screenToWorldPoint(int x, int y, float zPlane)
    {
        glm::vec2 vp = screenToViewport(x, y);
        return viewportToWorldPoint(vp.x, vp.y, zPlane);
    }

    glm::vec3 Camera::viewportToWorldPoint(float x, float y, float zPlane)
    {
        return glm::unProject(glm::vec3(x, y, zPlane), m_view, m_projection, glm::vec4(0, 0, m_width, m_height));
    }
    
    glm::vec2 Camera::screenToViewport(int x, int y)
    {
        float scrWidth = (float) Tremor::graphics->getWidth();
        float scrHeight = (float) Tremor::graphics->getHeight();

        y = scrHeight - y;

        float ratioX = m_width / scrWidth;
        float ratioY = m_height / scrHeight;

        return glm::vec2(ratioX * (float)x, ratioY * (float)y);
    }
}