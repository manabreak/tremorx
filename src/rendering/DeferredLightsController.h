#pragma once

#include "ShadowCasterSystem.h"

namespace Tremor
{
    class DeferredRenderer;
    class PointLight;
    class SpotLight;
    class Camera;

    class DeferredLightsController
    {
    public:
        DeferredLightsController(DeferredRenderer& renderer);
        DeferredLightsController(DeferredRenderer& renderer, int width, int height);

        void begin(Camera& camera);
        void renderPointLight(PointLight& light);
        void renderSpotLight(SpotLight& light);
        void end();

        Texture& getLightsResult();
        Texture& getShadowResult();

        void setAmbientLight(float r, float g, float b);

        void addRenderSystem(ShadowCasterSystem* system);
    private:
        int m_shadowMapSize;

        void init();

        void renderPointLights();
        void renderShadowPointLights();
        void renderSpotLights();
        void renderShadowSpotLights();

        void bindLightBuffer(bool clear);
        void bindShadowBuffer(bool clear);

        std::vector<ShadowCasterSystem*> m_renderSystems;

        DeferredRenderer& m_renderer;
        Texture m_lightTexture;
        Texture m_shadowTexture;

        GLuint m_lightBuffer;
        GLuint m_shadowBuffer;

        int m_width;
        int m_height;

        

        glm::vec3 m_ambientColor;

        bool m_rendering;

        std::unique_ptr<Mesh> m_spotMesh;
        std::unique_ptr<Mesh> m_pointMesh;
        std::vector<std::unique_ptr<Mesh>> m_pyramidMeshes;

        ShaderProgram& m_paraShader;
        ShaderProgram& m_depthShader;
        ShaderProgram& m_spotShader;
        ShaderProgram& m_spotShadowShader;
        ShaderProgram& m_pointShader;
        ShaderProgram& m_pointShadowShader;

        Camera* m_camera;

        std::vector<PointLight*> m_pointLights;
        std::vector<PointLight*> m_pointLightsShadow;
        std::vector<SpotLight*> m_spotLights;
        std::vector<SpotLight*> m_spotLightsShadow;

        static ShaderProgram& createParaShader();
        static ShaderProgram& createDepthShader();
        static ShaderProgram& createSpotShader();
        static ShaderProgram& createSpotShadowShader();
        static ShaderProgram& createPointShader();
        static ShaderProgram& createPointShadowShader();

    };
}