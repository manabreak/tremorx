#include "stdafx.h"
#include "QuadRenderer.h"
#include "utils/MeshFactory.h"
#include <core/Tremor.h>

namespace Tremor
{
    QuadRenderer::QuadRenderer()
        : m_mesh(std::unique_ptr<Mesh>(MeshFactory::createQuadXY(2.f, 2.f))),
        m_defaultShader(&createDefaultShader()),
        m_defaultCombineShader(&createDefaultCombineShader())
    {

    }

    QuadRenderer::~QuadRenderer()
    {
        
    }

    void QuadRenderer::render(Texture& texture)
    {
        glClearColor(0.f, 0.f, 0.f, 0.f);
        glDisable(GL_BLEND);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glViewport(0, 0, Tremor::graphics->getWidth(), Tremor::graphics->getHeight());
        m_defaultShader->begin();
        texture.bind(GL_TEXTURE0);
        glUniform1i(m_defaultShader->getUniformLocation("u_colormap"), 0);
        m_mesh->render();
        m_defaultShader->end();
    }

    void QuadRenderer::render(Texture& base, Texture& overlay)
    {
        glClearColor(0.f, 0.f, 0.f, 0.f);
        glDisable(GL_BLEND);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glViewport(0, 0, Tremor::graphics->getWidth(), Tremor::graphics->getHeight());
        m_defaultCombineShader->begin();
        base.bind(GL_TEXTURE0);
        overlay.bind(GL_TEXTURE1);
        // glUniform1i(m_defaultShader->getUniformLocation("u_base"), 0);
        // glUniform1i(m_defaultShader->getUniformLocation("u_overlay"), 1);
        m_defaultCombineShader->setUniform("u_base", 0);
        m_defaultCombineShader->setUniform("u_overlay", 1);
        m_mesh->render();
        m_defaultCombineShader->end();
    }

    ShaderProgram& QuadRenderer::createDefaultShader()
    {
        std::string vs =
            "#version 330 core\n"
            "layout(location = 0) in vec3 a_position;\n"
            "layout(location = 1) in vec2 a_uv;\n"
            "out vec2 uv;\n"
            "void main() {\n"
            "  uv = a_uv;\n"
            "  gl_Position = vec4(a_position, 1.0);\n"
            "}\n";

        std::string fs =
            "#version 330 core\n"
            "in vec2 uv;\n"
            "layout(location = 0) out vec3 result;\n"
            "uniform sampler2D u_colormap;\n"
            "void main() {\n"
            "  // result = vec3(texture2D(u_colormap, uv).x);\n"
            "  // result = vec4(1, 0, 1, 1);\n"
            "  result = texture2D(u_colormap, uv).xyz;\n"
            "  int fx = int(floor(uv.x * 1080.0));\n"
            "  int fy = int(floor(uv.y * 720.0));\n"
            "  // if(fx % 3 == 0 ||fy % 3 == 0) result *= 0.9;\n"

            "  vec2 d = abs(vec2(0.5, 0.5) - uv);\n"
            "  // result *= 1.0 - length(d) * 0.4;\n"
            "}\n";

        return AssetManager::loadShader(vs, fs);
    }

    ShaderProgram& QuadRenderer::createDefaultCombineShader()
    {
        std::string vs =
            "#version 330 core\n"
            "layout(location = 0) in vec3 a_position;\n"
            "layout(location = 1) in vec2 a_uv;\n"
            "out vec2 uv;\n"
            "void main() {\n"
            "  uv = a_uv;\n"
            "  gl_Position = vec4(a_position, 1.0);\n"
            "}\n";

        std::string fs =
            "#version 330 core\n"
            "in vec2 uv;\n"
            "layout(location = 0) out vec3 result;\n"
            "uniform sampler2D u_base;\n"
            "uniform sampler2D u_overlay;\n"
            "void main() {\n"
            "  vec2 tc = uv/* - vec2(0.5 / 1080, 0.5 / 720)*/;\n"
            "  vec3 base = texture2D(u_base, tc).xyz;\n"
            "  vec4 overlay = texture2D(u_overlay, tc);\n"

            "  // result = texture2D(u_base, tc).xyz * overlay;\n"
            "  result = base * overlay.xyz;\n"



            // GAMEBOY FOR FUN!
            /*
            "  float value = result.x * 30 + result.y * 40 + result.z * 30;\n"
            "  value /= 100;\n"
            "  int val = int(round(value * 6));\n"
            
            "  if(val == 0) result = vec3(0.0588, 0.22, 0.0588);\n"
            "  else if(val == 1) result = vec3(0.188, 0.384, 0.188);\n"
            "  else if(val == 2) result = vec3(0.545, 0.675, 0.059);\n"
            "  else result = vec3(0.6078, 0.7373, 0.0588);\n"
            */
            /*
            "  if(val == 0) result = vec3(1.0, 0.0, 0.0);\n"
            "  else if(val == 1) result = vec3(0.0, 1.0, 0.0);\n"
            "  else if(val == 2) result = vec3(0.0, 0.0, 1.0);\n"
            "  else result = vec3(1.0, 1.0, 1.0);\n"
            */
            "}\n";

        return AssetManager::loadShader(vs, fs);
    }
}