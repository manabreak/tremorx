#pragma once

#include "2d/Texture.h"
#include "3d/Mesh.h"
#include "rendering/ShaderProgram.h"

namespace Tremor
{
    class QuadRenderer
    {
    public:
        QuadRenderer();
        ~QuadRenderer();

        void render(Texture& texture);
        void render(Texture& base, Texture& overlay);
        /*
        void render(ShaderProgram& shader);
        void render(ShaderProgram& shader, Texture& texture);
        void render(ShaderProgram& shader, Texture& texture0, Texture& texture1);
        */
    private:
        std::unique_ptr<Mesh> m_mesh;
        ShaderProgram* m_defaultShader;
        ShaderProgram* m_defaultCombineShader;

        static ShaderProgram& createDefaultShader();
        static ShaderProgram& createDefaultCombineShader();
    };
}