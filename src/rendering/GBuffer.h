#pragma once

#include "2d/Texture.h"

namespace Tremor
{
    class GBuffer
    {
    public:
        GBuffer();
        GBuffer(int width, int height);
        GBuffer(const GBuffer&) = delete;

        ~GBuffer();

        void begin(bool clear = true);
        void end();

        Texture& getColorResult();
        Texture& getNormalResult();
        Texture& getDepthResult();

        float getDepthAt(int x, int y, bool flipY = true);
        glm::vec3 getNormalAt(int x, int y, bool flipY = true);
        glm::vec3 getColorAt(int x, int y, bool flipY = true);
    private:
        void init();

        Texture m_color;
        Texture m_normal;
        Texture m_depth;

        int m_width;
        int m_height;
        GLuint m_fbo;
    };
}