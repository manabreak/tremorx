#include "stdafx.h"
#include "GBuffer.h"
#include "core/Tremor.h"

namespace Tremor
{
    GBuffer::GBuffer()
        : m_width(Tremor::graphics->getWidth()),
        m_height(Tremor::graphics->getHeight()),
        m_color(Texture(Tremor::graphics->getWidth(), Tremor::graphics->getHeight(), TextureFormat::RGB888)),
        m_normal(Texture(Tremor::graphics->getWidth(), Tremor::graphics->getHeight(), TextureFormat::RGB888)),
        m_depth(Texture(Tremor::graphics->getWidth(), Tremor::graphics->getHeight(), TextureFormat::DEPTH)),
        m_fbo(0)
    {
        init();
    }

    GBuffer::GBuffer(int width, int height)
        : m_width(width),
        m_height(height),
        m_color(Texture(width, height, TextureFormat::RGBA8888)),
        m_normal(Texture(width, height, TextureFormat::RGBA8888)),
        m_depth(Texture(width, height, TextureFormat::DEPTH)),
        m_fbo(0)
    {
        init();
    }

    GBuffer::~GBuffer()
    {

    }

    void GBuffer::init()
    {
        glGenFramebuffers(1, &m_fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

        // Color texture
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_color.getHandle(), 0);

        // Normal texture
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_normal.getHandle(), 0);

        // Depth texture
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depth.getHandle(), 0);

        GLenum drawBuffers[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
        glDrawBuffers(2, drawBuffers);

        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE)
        {
            std::cout << "Framebuffer not created correctly!" << std::endl;
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    void GBuffer::begin(bool clear)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
        glViewport(0, 0, m_width, m_height);
        glDepthMask(GL_TRUE);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glDisable(GL_BLEND);
        if (clear)
        {
            glClearColor(0.f, 0.f, 0.f, 0.f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
    }

    void GBuffer::end()
    {
        glDepthMask(GL_FALSE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    Texture& GBuffer::getColorResult()
    {
        return m_color;
    }

    Texture& GBuffer::getNormalResult()
    {
        return m_normal;
    }

    Texture& GBuffer::getDepthResult()
    {
        return m_depth;
    }

    float GBuffer::getDepthAt(int x, int y, bool flipY)
    {
        glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
        glReadBuffer(GL_NONE);
        float result = 0.f;
        if (flipY) y = m_height - y;
        glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &result);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
        return result;
    }

    glm::vec3 GBuffer::getNormalAt(int x, int y, bool flipY)
    {
        glm::vec3 result;
        glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
        glReadBuffer(GL_COLOR_ATTACHMENT1);
        if (flipY) y = m_height - y;
        glReadPixels(x, y, 1, 1, GL_RGB, GL_FLOAT, &result);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
        return result;
    }

    glm::vec3 GBuffer::getColorAt(int x, int y, bool flipY)
    {
        glm::vec3 result;
        glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
        glReadBuffer(GL_COLOR_ATTACHMENT0);
        if (flipY) y = m_height - y;
        glReadPixels(x, y, 1, 1, GL_RGB, GL_FLOAT, &result);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
        return result;
    }
}