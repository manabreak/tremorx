#pragma once
#include <glm/glm.hpp>

namespace Tremor
{
    struct Vertex
    {
        glm::vec3 m_pos;
        glm::vec2 m_uv;
        glm::vec4 m_col;
        glm::vec3 m_nor;
        glm::vec3 m_tan;
        glm::vec3 m_bin;

        Vertex()
        {
            m_col = glm::vec4(1.f, 1.f, 1.f, 1.f);
        }

        Vertex(glm::vec3 pos)
        {
            m_pos = pos;
            m_col = glm::vec4(1.f, 1.f, 1.f, 1.f);
        }

        Vertex(float x, float y, float z)
        {
            m_pos = glm::vec3(x, y, z);
            m_col = glm::vec4(1.f, 1.f, 1.f, 1.f);
        }

        Vertex(glm::vec3 pos, glm::vec2 uv)
        {
            m_pos = pos;
            m_uv = uv;
            m_col = glm::vec4(1.f, 1.f, 1.f, 1.f);
        }

        Vertex(float x, float y, float z, float u, float v)
        {
            m_pos = glm::vec3(x, y, z);
            m_uv = glm::vec2(u, v);
            m_col = glm::vec4(1.f, 1.f, 1.f, 1.f);
        }

        Vertex(glm::vec3 pos, glm::vec2 uv, glm::vec3 nor)
        {
            m_pos = pos;
            m_uv = uv;
            m_nor = nor;
            m_col = glm::vec4(1.f, 1.f, 1.f, 1.f);
        }

        Vertex(float x, float y, float z, float u, float v, float norX, float norY, float norZ)
        {
            m_pos = glm::vec3(x, y, z);
            m_uv = glm::vec2(u, v);
            m_nor = glm::vec3(norX, norY, norZ);
            m_col = glm::vec4(1.f, 1.f, 1.f, 1.f);
        }

        Vertex(glm::vec3 pos, glm::vec2 uv, glm::vec3 nor, glm::vec3 tan, glm::vec3 bin)
        {
            m_pos = pos;
            m_uv = uv;
            m_nor = nor;
            m_tan = tan;
            m_bin = bin;
            m_col = glm::vec4(1.f, 1.f, 1.f, 1.f);
        }
    };
}