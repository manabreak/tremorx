#include "stdafx.h"
#include "Mesh.h"

namespace Tremor
{

    void Mesh::setVertices(const std::vector<Vertex>& vertices)
    {
        m_vertices = vertices;
    }

    void Mesh::setIndices(const std::vector<unsigned short>& indices)
    {
        m_indices.clear();
        m_indices = indices;
    }

    void Mesh::recalculateBounds()
    {
        glm::vec3 min(10000.f);
        glm::vec3 max(-10000.f);
        for each(Vertex v in m_vertices)
        {
            min.x = glm::min(min.x, v.m_pos.x);
            min.y = glm::min(min.y, v.m_pos.y);
            min.z = glm::min(min.z, v.m_pos.z);

            max.x = glm::max(max.x, v.m_pos.x);
            max.y = glm::max(max.y, v.m_pos.y);
            max.z = glm::max(max.z, v.m_pos.z);
        }

        // m_bounds.set(min, max);
    }

    void Mesh::setStatic()
    {
        m_static = true;
    }

    void Mesh::setDynamic()
    {
        m_static = false;
    }

    void Mesh::update()
    {
        updateIndices();
        updateVertices();
    }

    void Mesh::updateIndices()
    {
        if (m_indices.size() == 0) return;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(m_indices[0]), &m_indices[0], m_static ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);
    }

    void Mesh::updateIndices(int offset, int count)
    {
        if (m_indices.size() == 0 || count == 0) return;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, count * sizeof(m_indices[0]), &m_indices[offset]);
    }

    void Mesh::updateVertices()
    {
        if (m_vertices.size() == 0) return;

        glBindVertexArray(m_vao);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(Vertex), &m_vertices[0], m_static ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);
    }

    void Mesh::updateVertices(int offset, int count)
    {
        if (m_vertices.size() == 0 || count == 0) return;
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBufferSubData(GL_ARRAY_BUFFER, offset, count * sizeof(Vertex), &m_vertices[offset]);
    }

    void Mesh::initFor(int vertexCount, int indexCount)
    {
        for (int i = 0; i < vertexCount; ++i)
        {
            m_vertices.push_back(Vertex());
        }
        for (int i = 0; i < indexCount; ++i)
        {
            m_indices.push_back(0);
        }
        initMesh();
    }

    void Mesh::initMesh()
    {
        glEnableVertexAttribArray(5);
        glEnableVertexAttribArray(4);
        glEnableVertexAttribArray(3);
        glEnableVertexAttribArray(2);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(0);

        glGenBuffers(1, &m_ibo);
        updateIndices();

        glGenBuffers(1, &m_vbo);
        updateVertices();
    }

    void Mesh::deleteMesh()
    {
        glDeleteBuffers(1, &m_ibo);
        glDeleteBuffers(1, &m_vbo);
    }

    void Mesh::renderDebug()
    {
        glBegin(GL_LINES);

        glColor3f(1.f, 1.f, 1.f);

        for (unsigned int i = 0; i < m_indices.size(); i += 3)
        {
            glm::vec3 v0 = m_vertices[m_indices[i]].m_pos;
            glm::vec3 v1 = m_vertices[m_indices[i + 1]].m_pos;
            glm::vec3 v2 = m_vertices[m_indices[i + 2]].m_pos;
            glVertex3f(v0.x, v0.y, v0.z); glVertex3f(v1.x, v1.y, v1.z);
            glVertex3f(v1.x, v1.y, v1.z); glVertex3f(v2.x, v2.y, v2.z);
            glVertex3f(v2.x, v2.y, v2.z); glVertex3f(v0.x, v0.y, v0.z);
        }

        glEnd();
    }

    void Mesh::renderMesh()
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

        // Binormal
        glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(glm::vec3) * 3 + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Tangent
        glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(glm::vec3) * 2 + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Normal
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(glm::vec3) + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Color
        glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(glm::vec3) + sizeof(glm::vec2)));
        // UV
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(glm::vec3)));
        // Position
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) 0);

        glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_SHORT, 0);
    }

    void Mesh::renderMesh(int type, int offset, int count)
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

        glEnableVertexAttribArray(5);
        glEnableVertexAttribArray(4);
        glEnableVertexAttribArray(3);
        glEnableVertexAttribArray(2);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(0);

        // Binormal
        glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(glm::vec3) * 3 + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Tangent
        glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(glm::vec3) * 2 + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Normal
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(glm::vec3) + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Color
        glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(glm::vec3) + sizeof(glm::vec2)));
        // UV
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(glm::vec3)));
        // Position
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) 0);

        glDrawRangeElements(type, offset, count, count, GL_UNSIGNED_SHORT, 0);

        // glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_SHORT, (void*) offset);
    }

    Mesh::Mesh()
    {
        initMesh();
    }

    Mesh::Mesh(int vertexCount, int indexCount)
        : m_vertices(vertexCount, Vertex()),
        m_indices(indexCount, 0)
    {
        initMesh();
    }

    Mesh::Mesh(const std::vector<Vertex>& vertices)
        : m_vertices(vertices)
    {
        for (unsigned short i = 0; i < vertices.size(); ++i)
        {
            m_indices.push_back(i);
        }
        initMesh();
    }

    Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<unsigned short>& indices)
        : m_vertices(vertices),
        m_indices(indices)
    {
        initMesh();
    }

    Mesh::~Mesh()
    {
        deleteMesh();
    }

    void Mesh::render()
    {
        renderMesh();
    }

    void Mesh::render(ShaderProgram& shader, int type, int offset, int count)
    {
        renderMesh(type, offset, count);
    }

    std::vector<Vertex>& Mesh::getVertices()
    {
        return m_vertices;
    }

    std::vector<unsigned short>& Mesh::getIndices()
    {
        return m_indices;
    }

    void Mesh::update(int vertexCount, int indexCount)
    {
        updateIndices(0, indexCount);
        updateVertices(0, vertexCount);
    }

    void Mesh::setIndex(unsigned short i, unsigned short j)
    {
        m_indices[i] = j;
    }

    void Mesh::setVertex(int index, float x, float y)
    {
        m_vertices[index].m_pos.x = x;
        m_vertices[index].m_pos.y = y;
    }

    void Mesh::update(int vertexCount, int vertexOffset, int indexCount, int indexOffset)
    {
        updateVertices(vertexOffset, vertexCount);
        updateIndices(indexOffset, indexCount);
    }

    void Mesh::setVertex(int index, float x, float y, float z)
    {
        m_vertices[index].m_pos.x = x;
        m_vertices[index].m_pos.y = y;
        m_vertices[index].m_pos.y = z;
    }

    void Mesh::setVertex(int index, float x, float y, float z, float u, float v)
    {
        m_vertices[index].m_pos.x = x;
        m_vertices[index].m_pos.y = y;
        m_vertices[index].m_pos.y = z;
        m_vertices[index].m_uv.x = u;
        m_vertices[index].m_uv.y = v;
    }

    void Mesh::setVertex(int index, float x, float y, float z, float u, float v, float nx, float ny, float nz)
    {
        m_vertices[index].m_pos.x = x;
        m_vertices[index].m_pos.y = y;
        m_vertices[index].m_pos.y = z;
        m_vertices[index].m_uv.x = u;
        m_vertices[index].m_uv.y = v;
        m_vertices[index].m_nor.x = nx;
        m_vertices[index].m_nor.y = ny;
        m_vertices[index].m_nor.z = nz;
    }

    void Mesh::setVertex(int index, const glm::vec3& pos)
    {
        m_vertices[index].m_pos = pos;
    }

    void Mesh::setVertex(int index, const glm::vec3& pos, const glm::vec2& uv)
    {
        m_vertices[index].m_pos = pos;
        m_vertices[index].m_uv = uv;
    }

    void Mesh::setVertex(int index, const glm::vec3& pos, const glm::vec2& uv, const glm::vec4& col, const glm::vec3& nor, const glm::vec3& tan, const glm::vec3& bin)
    {
        m_vertices[index].m_pos = pos;
        m_vertices[index].m_uv = uv;
        m_vertices[index].m_col = col;
        m_vertices[index].m_nor = nor;
        m_vertices[index].m_nor = bin;
        m_vertices[index].m_tan = tan;
    }

    void Mesh::setVertex(int index, const Vertex& vertex)
    {
        m_vertices[index] = vertex;
    }
}