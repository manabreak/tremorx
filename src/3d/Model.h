#pragma once

namespace Tremor
{
    class Model
    {
    public:
        Model(Mesh& mesh)
            : m_mesh(mesh),
            m_colormap(nullptr),
            m_normalmap(nullptr)
        {
        }

        Model(Mesh& mesh, Texture& colorMap)
            : m_mesh(mesh),
            m_colormap(&colorMap),
            m_normalmap(nullptr)
        {
        }

        Model(Mesh& mesh, Texture& colorMap, Texture& normalMap)
            : m_mesh(mesh),
            m_colormap(&colorMap),
            m_normalmap(&normalMap)
        {
        }

        Mesh& getMesh()
        {
            return m_mesh;
        }

        Texture& getColorMap()
        {
            return *m_colormap;
        }

        Texture& getNormalMap()
        {
            return *m_normalmap;
        }

        bool hasNormalMap()
        {
            return m_normalmap != nullptr;
        }

    private:
        Mesh& m_mesh;
        Texture* m_colormap;
        Texture* m_normalmap;
    };
}