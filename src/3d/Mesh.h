#pragma once

#include <vector>
#include "Vertex.h"
#include "../rendering/ShaderProgram.h"

namespace Tremor
{
    class Mesh
    {
    public:
        // Creates an empty mesh
        Mesh();

        // Creates a mesh and initializes it with empty vertices and indices
        Mesh(int vertexCount, int indexCount);

        // Creates a mesh with given vertices and running indices through the vertices (0, 1, 2, 3, 4, 5...)
        Mesh(const std::vector<Vertex>& vertices);

        // Creates a mesh with given vertices and indices
        Mesh(const std::vector<Vertex>& vertices, const std::vector<unsigned short>& indices);
        ~Mesh();
        void render();
        void render(ShaderProgram& shader, int type, int offset, int count);
        void renderDebug();
        void update();
        void update(int vertexCount, int indexCount);
        void update(int vertexCount, int vertexOffset, int indexCount, int indexOffset);
        void updateVertices(int count, int offset);
        void updateIndices(int count, int offset);

        void updateIndices();
        void updateVertices();

        void setDynamic();
        void setStatic();

        std::vector<Vertex>& getVertices();
        std::vector<unsigned short>& getIndices();
        void setVertices(const std::vector<Vertex>& vertices);
        void setVertices(const std::vector<Vertex>& vertices, bool update);
        void setIndices(const std::vector<unsigned short>& indices);
        void setIndices(const std::vector<unsigned short>& indices, bool update);

        void setIndex(unsigned short i, unsigned short j);

        void setVertex(int index, float x, float y);
        void setVertex(int index, float x, float y, float z);
        void setVertex(int index, float x, float y, float z, float u, float v);
        void setVertex(int index, float x, float y, float z, float u, float v, float nx, float ny, float nz);
        void setVertex(int index, const glm::vec3& pos);
        void setVertex(int index, const glm::vec3& pos, const glm::vec2& uv);
        void setVertex(int index, const glm::vec3& pos, const glm::vec2& uv, const glm::vec4& col, const glm::vec3& nor, const glm::vec3& tan, const glm::vec3& bin);
        void setVertex(int index, const Vertex& vertex);

        bool isManaged();

        void setName(const std::string& name) { m_name = name; }
        const std::string& getName() { return m_name; }
    private:
        bool m_static;
        GLuint m_vbo;
        GLuint m_ibo;
        GLuint m_vao;
        std::vector<Vertex> m_vertices;
        std::vector<unsigned short> m_indices;

        std::string m_name;

        void recalculateBounds();
        
        void initFor(int vertexCount, int indexCount);
        void initMesh();
        void deleteMesh();
        void renderMesh();
        void renderMesh(int type, int offset, int count);
    };
}