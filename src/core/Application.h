#pragma once

// #include <vld.h>
#include <memory>
#include <assert.h>
#include <thread>
#include "ApplicationListener.h"
#include "ApplicationContext.h"
#include "LaunchConfig.h"
#include "Tremor.h"

namespace Tremor
{
    class Application
    {
    public:
        // Constructs a new Tremor application with a TRApplicationListener.
        // The constructed TRApplication takes ownership of the application listener
        // and releases the memory when the program is closed.
        Application(ApplicationListener& appListener);

        // Constructs a new Tremor application with a TRApplicationListener,
        // plus a pre-defined application context. This is used on Android
        // to construct the application, essentially to avoid passing Android-specific
        // stuff around in the core code.
        Application(ApplicationListener& appListener, ApplicationContext& context);

        // Constructs a new Tremor application with a TRApplicationListener and
        // the given launch configuration.
        Application(ApplicationListener& appListener, LaunchConfig& launchConfig);

        ~Application();

        // Starts the execution of the application. Call this function to start
        // the main update and rendering loops. If the application was stop()'d
        // earlier, calling run() will continue executing the app.
        void run();

        // Stops the execution of the application. 
        void stop();

        void pause();

        void resume();

        void cleanUp();
    private:
        // The application listener registered to this application.
        ApplicationListener& m_appListener;

        // The application context (platform-specific)
        ApplicationContext m_appContext;

        bool m_appCreated;

        bool m_paused;
        bool m_isRunning;

        std::thread* m_renderThread;
    };
}