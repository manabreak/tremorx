#pragma once

#include <string>

namespace Tremor
{
    class ApplicationContext
    {
    private:
        struct ContextMembers;
        ContextMembers* m_contextMembers;
    public:
        ApplicationContext();
        ApplicationContext(int width, int height, const std::string& title, bool fullScreen = false);
        ~ApplicationContext();
        bool shouldClose();
        void clear();
        void swapBuffers();
        bool pollEvents();
        ContextMembers& getContextMembers();
    };
}