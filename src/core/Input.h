#pragma once

#include <vector>

namespace Tremor
{
    enum Button
    {
        BTN_LEFT = 0,
        BTN_RIGHT = 1,
        BTN_MIDDLE = 2,
    };

    enum Key
    {
        UNKNOWN = -1,
        SPACE = 32,
        APOSTROPHE = 39,
        COMMA = 44,
        MINUS = 45,
        PERIOD = 46,
        SLASH = 47,
        NUM_0 = 48,
        NUM_1,
        NUM_2,
        NUM_3,
        NUM_4,
        NUM_5,
        NUM_6,
        NUM_7,
        NUM_8,
        NUM_9,
        SEMICOLON = 59,
        EQUAL = 61,
        A = 65,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        Z,
        LEFT_BRACKET = 91,
        BACKSLASH = 92,
        RIGHT_BRACKET = 93,
        GRAVE_aCCENT = 96,
        WORLD_1 = 161,
        WORLD_2 = 162,
        ESCAPE = 256,
        ENTER = 257,
        TAB = 258,
        BACKSPACE = 259,
        INSERT = 260,
        DEL = 261,
        RIGHT = 262,
        LEFT = 263,
        DOWN = 264,
        UP = 265,
        PAGE_UP = 266,
        PAGE_DOWN = 267,
        HOME = 268,
        END = 269,
        CAPS_LOCK = 280,
        SCROLL_LOCK = 281,
        NUM_LOCK = 282,
        PRINT_SCREEN = 283,
        PAUSE = 284,
        F1 = 290,
        F2,
        F3,
        F4,
        F5,
        F6,
        F7,
        F8,
        F9,
        F10,
        F11,
        F12,
        NUMPAD_0 = 320,
        NUMPAD_1,
        NUMPAD_2,
        NUMPAD_3,
        NUMPAD_4,
        NUMPAD_5,
        NUMPAD_6,
        NUMPAD_7,
        NUMPAD_8,
        NUMPAD_9,
        NUMPAD_DECIMAL = 330,
        NUMPAD_DIVIDE = 331,
        NUMPAD_MULTIPLY = 332,
        NUMPAD_SUBTRACT = 333,
        NUMPAD_ADD = 334,
        NUMPAD_ENTER = 335,
        NUMPAD_EQUAL = 336,
        LEFT_SHIFT = 340,
        LEFT_CONTROL = 341,
        LEFT_ALT = 342,
        LEFT_SUPER = 343,
        RIGHT_SHIFT = 344,
        RIGHT_CONTROL = 345,
        RIGHT_ALT = 346,
        RIGHT_SUPER = 347,
        MENU = 348
    };

    struct InputListener
    {
    public:
        virtual void onKeyDown(Key key) {}
        virtual void onKeyUp(Key key) {}

        virtual void onButtonDown(Button button) {}
        virtual void onButtonUp(Button button) {}
    };

    class Input
    {
        friend class ApplicationContext;
        friend class Application;
    public:
        // Was the given key just pressed this frame?
        static bool isKeyDown(Key key);
        // static bool isKeyDown(int key);

        // Was the given key down this frame?
        static bool isKeyPressed(Key key);
        // static bool isKeyPressed(int key);

        // Was the given key just released this frame?
        static bool isKeyUp(Key key);
        // static bool isKeyUp(int key);

        // Was the given mouse button just pressed this frame?
        static bool isButtonDown(Button btn);
        // static bool isButtonDown(int btn);

        // Was the given mouse button down this frame?
        static bool isButtonPressed(Button btn);
        // static bool isButtonPressed(int btn);

        // Was the given mouse button released this frame?
        static bool isButtonUp(Button btn);
        // static bool isButtonUp(int btn);

        // Returns the mouse X coordinate relative to the window's left edge (Top-Left corner = 0,0)
        static int getMouseX();

        // Returns the mouse Y coordinate relative to the windows' top edge (Top-Left corner = 0,0)
        static int getMouseY();

        // Returns the mouse's delta movement since last poll along X axis (right = positive, left = negative)
        static int getMouseDeltaX();

        // Returns the mouse's delta movement since last poll along Y axis (down = positive, up = negative)
        static int getMouseDeltaY();

        static void registerInputListener(InputListener* inputListener);
        static void unregisterInputListener(InputListener* inputListener);
        static void clearInputListeners();
    private:
        static std::vector<int> m_keysdown;
        static std::vector<int> m_keyspressed;
        static std::vector<int> m_keysup;
        static std::vector<int> m_buttonsdown;
        static std::vector<int> m_buttonspressed;
        static std::vector<int> m_buttonsup;

        static void clear();
        static int m_mouseX;
        static int m_mouseY;
        static int m_mousedeltaX;
        static int m_mousedeltaY;

        static std::vector<InputListener*> m_inputListeners;
    };

}