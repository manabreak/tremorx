#pragma once

#include "stdafx.h"

namespace Tremor
{
    struct LaunchConfig
    {
        int screenWidth = 1080;
        int screenHeight = 720;
        int retroDivisor;
        bool fullScreen = false;
        std::string windowTitle;

        LaunchConfig()
        {
            screenWidth = 1280;
            screenHeight = 720;
            retroDivisor = 3;
            windowTitle = "Hello Tremor";
        }
    };
}