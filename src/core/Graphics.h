#pragma once

#include "ApplicationContext.h"
#include <glm/glm.hpp>

namespace Tremor
{
    class Graphics
    {
        friend class ApplicationContext;
    public:
        Graphics();
        ~Graphics();
        int getWidth();
        int getHeight();
        int getRetroWidth();
        int getRetroHeight();
        static glm::vec4 clearColor;
    private:
        int m_width;
        int m_height;
        int m_retroDivisor;
    };
}