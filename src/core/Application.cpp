#include "stdafx.h"
#include "Application.h"
#include "ApplicationContext.h"
#include "ApplicationListener.h"
#include "../assets/AssetManager.h"

namespace Tremor
{
    Application::Application(ApplicationListener& appListener)
        : m_paused(false),
        m_appCreated(false),
        m_appListener(appListener),
        m_appContext(ApplicationContext(640, 480, "Tremor!"))
    {

    }

    Application::Application(ApplicationListener& appListener, ApplicationContext& context)
        : m_paused(false),
        m_appCreated(false),
        m_appListener(appListener),
        m_appContext(context)
    {

    }

    Application::Application(ApplicationListener& appListener, LaunchConfig& launchConfig)
        : m_paused(false),
        m_appCreated(false),
        m_appListener(appListener),
        m_appContext(ApplicationContext(launchConfig.screenWidth, launchConfig.screenHeight, launchConfig.windowTitle, launchConfig.fullScreen))
    {

    }

    void Application::run()
    {
        typedef std::chrono::duration<int, std::ratio<1, 85>> frame_duration;
        typedef std::chrono::duration<int, std::ratio<1, 1>> second_duration;
        typedef std::chrono::duration<float> fsec;
        auto start_time = std::chrono::steady_clock::now();
        float delta = 0.f;

        if (!m_appCreated)
        {
            m_appListener.create();
            m_appCreated = true;
        }

        m_isRunning = true;

        std::vector<long long> times(60);
        int index = 0;

        // Main loop
        while (m_isRunning)
        {
            // auto measureStart = std::chrono::high_resolution_clock::now();

            // Get time delta since last frame
            fsec fs = std::chrono::steady_clock::now() - start_time;
            delta = fs.count();

            // Snapshot the time this frame started
            start_time = std::chrono::steady_clock::now();

            // Call the update method of the client
            m_appListener.update(delta);

            // Clear the screen
            m_appContext.clear();

            // Call the draw method of the client
            m_appListener.draw();

            // Clear per-frame input sets
            Input::clear();

            // Swap graphics buffers
            m_appContext.swapBuffers();

            /*
            auto measureEnd = std::chrono::high_resolution_clock::now();
            auto duration = measureEnd - measureStart;
            auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(duration);
            std::cout << "Loop duration: " << ms.count() << " ms." << std::endl;
            */

            // Poll events
            if (m_appContext.pollEvents())
            {
                m_isRunning = false;
            }
            else
            {
                // Sleep for the remainder, if needed
                auto end_time = start_time + frame_duration(1);
                std::this_thread::sleep_until(end_time);
            }
        }
    }

    void Application::cleanUp()
    {
        m_appListener.cleanUp();
    }

    void Application::stop()
    {
        m_isRunning = false;
    }

    void Application::pause()
    {
        m_paused = true;
    }

    void Application::resume()
    {
        m_paused = false;
        m_isRunning = true;

        // TODO resume graphics, input... ?
    }

    Application::~Application()
    {
        printf("Application() destruct\n");
        
    }
}