#pragma once

namespace Tremor
{
    class ApplicationListener
    {
    public:

        // Used to initialize the application
        virtual void create() = 0;

        /* Update function, called once every frame, is used to
           update the game logic */
        virtual void update(float dt) = 0;

        // All the rendering should be done in this method
        virtual void draw() = 0;

        virtual void cleanUp() = 0;
        // TODO Add other methods here

    };
}