#pragma once

#include <string>

namespace Tremor
{
    class Log
    {
    public:
        static void log(std::string& msg);
    };
}