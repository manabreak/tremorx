#pragma once

#include "Graphics.h"
#include "ApplicationContext.h"
#include "Input.h"

namespace Tremor
{
    // Container for static references to the main components of the engine.
    class Tremor
    {
    public:
        // Graphics component
        static Graphics* graphics;
        static ApplicationContext* context;

        static bool debugGeometry;
    };
}