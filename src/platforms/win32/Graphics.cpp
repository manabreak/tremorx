#include "stdafx.h"
#include "../../core/Graphics.h"
#include <GLFW/include/glfw3.h>

namespace Tremor
{
    glm::vec4 Graphics::clearColor(0.0f, 0.f, 0.f, 1.f);

    Graphics::Graphics()
    {
        m_retroDivisor = 3;
    }

    Graphics::~Graphics()
    {

    }

    int Graphics::getWidth()
    {
        return m_width;
    }

    int Graphics::getHeight()
    {
        return m_height;
    }

    int Graphics::getRetroWidth()
    {
        if (m_retroDivisor != 0) return m_width / m_retroDivisor;
        return m_width;
    }

    int Graphics::getRetroHeight()
    {
        if (m_retroDivisor != 0) return m_height / m_retroDivisor;
        return m_height;
    }
}