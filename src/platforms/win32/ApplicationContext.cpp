#include "stdafx.h"
#include "../../core/ApplicationContext.h"
#include <stdlib.h>
#include <glew/include/GL/glew.h>
#include <GLFW/include/glfw3.h>

#include "../../core/Graphics.h"
#include "../../core/Log.h"
#include "../../core/Tremor.h"

namespace Tremor
{
    struct ApplicationContext::ContextMembers
    {
        GLFWwindow* window;
    };

    ApplicationContext::ApplicationContext(int width, int height, const std::string& title, bool fullScreen)
        : m_contextMembers(new ContextMembers())
    {
        if (!glfwInit()) exit(EXIT_FAILURE);

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        // glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_ANY_PROFILE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
        glewExperimental = true;

        m_contextMembers->window = glfwCreateWindow(width, height, title.c_str(), fullScreen ? glfwGetPrimaryMonitor() : NULL, NULL);
        if (!m_contextMembers->window) exit(EXIT_FAILURE);

        glfwSetWindowPos(m_contextMembers->window, 800, 30);
        glfwMakeContextCurrent(m_contextMembers->window);

        GLenum err = glewInit();
        if (GLEW_OK != err)
        {
            fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
        }

        Tremor::graphics = new Graphics();
        Tremor::graphics->m_width = width;
        Tremor::graphics->m_height = height;

        std::cout << "Graphics size set to " << Tremor::graphics->getWidth() << ", " << Tremor::graphics->getHeight() << std::endl;

        auto keyCallback = [](GLFWwindow* window, int key, int scancode, int action, int mods)
        {
            if (action == GLFW_PRESS)
            {
                // fprintf(stdout, "Pressed %i\n", key);
                Input::m_keysdown.push_back(key);
                Input::m_keyspressed.push_back(key);
                for (unsigned int i = 0; i < Input::m_inputListeners.size(); ++i)
                {
                    Input::m_inputListeners[i]->onKeyDown((Key) key);
                }
            }
            else if (action == GLFW_RELEASE)
            {
                // fprintf(stdout, "Released %i\n", key);
                // Tremor::input->m_keyspressed.erase(key);
                std::vector<int>& pressed = Input::m_keyspressed;
                for (unsigned int i = 0; pressed.size(); ++i)
                {
                    if (pressed[i] == key)
                    {
                        std::swap(pressed[i], pressed.back());
                        pressed.pop_back();
                        break;
                    }
                }
                Input::m_keysup.push_back(key);
                for (unsigned int i = 0; i < Input::m_inputListeners.size(); ++i)
                {
                    Input::m_inputListeners[i]->onKeyUp((Key) key);
                }
            }
        };

        auto mouseCallback = [](GLFWwindow* window, int button, int action, int mods)
        {
            if (action == GLFW_PRESS)
            {
                Input::m_buttonsdown.push_back(button);
                Input::m_buttonspressed.push_back(button);
            }
            else if (action == GLFW_RELEASE)
            {
                // Tremor::input->m_buttonspressed.erase(button);
                std::vector<int>& pressed = Input::m_buttonspressed;
                for (unsigned int i = 0; pressed.size(); ++i)
                {
                    if (pressed[i] == button)
                    {
                        std::swap(pressed[i], pressed.back());
                        pressed.pop_back();
                        break;
                    }
                }
                Input::m_buttonsup.push_back(button);
            }
        };

        auto scrollCallback = [](GLFWwindow* window, double xoffset, double yoffset)
        {

        };

        auto cursorPosCallback = [](GLFWwindow* window, double xpos, double ypos)
        {
            Input::m_mousedeltaX = (int) xpos - Input::m_mouseX;
            Input::m_mouseX = (int) xpos;
            Input::m_mousedeltaY = (int) ypos - Input::m_mouseY;
            Input::m_mouseY = (int) ypos;
        };

        glfwSetKeyCallback(m_contextMembers->window, keyCallback);
        glfwSetMouseButtonCallback(m_contextMembers->window, mouseCallback);
        glfwSetCursorPosCallback(m_contextMembers->window, cursorPosCallback);
    }

    ApplicationContext::~ApplicationContext()
    {
        glfwDestroyWindow(m_contextMembers->window);
        glfwTerminate();

        delete m_contextMembers;
        delete Tremor::graphics;
    }

    bool ApplicationContext::shouldClose()
    {
        // return glfwWindowShouldClose(m_contextMembers->window);
        if (glfwWindowShouldClose(m_contextMembers->window)) return true;
        return false;
    }

    void ApplicationContext::clear()
    {
        glClearColor(Graphics::clearColor.r, Graphics::clearColor.g, Graphics::clearColor.b, Graphics::clearColor.a);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void ApplicationContext::swapBuffers()
    {
        glfwSwapBuffers(m_contextMembers->window);
    }

    bool ApplicationContext::pollEvents()
    {
        glfwPollEvents();
        return shouldClose();
    }
}