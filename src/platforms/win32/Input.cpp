#include "stdafx.h"

#include "core/Input.h"
#include <GLFW/include/glfw3.h>
#include "core/Tremor.h"

namespace Tremor
{
    std::vector<int> Input::m_keysdown;
    std::vector<int> Input::m_keyspressed;
    std::vector<int> Input::m_keysup;
    std::vector<int> Input::m_buttonsdown;
    std::vector<int> Input::m_buttonspressed;
    std::vector<int> Input::m_buttonsup;
    int Input::m_mouseX;
    int Input::m_mouseY;
    int Input::m_mousedeltaX;
    int Input::m_mousedeltaY;
    std::vector<InputListener*> Input::m_inputListeners;

    bool Input::isKeyDown(Key key)
    {
        // return m_keysdown.find(key) != m_keysdown.end();
        for (unsigned int i = 0; i < m_keysdown.size(); ++i)
        {
            if (m_keysdown[i] == (int) key) return true;
        }
        return false;
    }

    /*
    bool Input::isKeyDown(int key)
    {
        // return m_keysdown.find(key) != m_keysdown.end();
        for (unsigned int i = 0; i < m_keysdown.size(); ++i)
        {
            if (m_keysdown[i] == key) return true;
        }
        return false;
    }
    */

    bool Input::isKeyPressed(Key key)
    {
        // return m_keyspressed.find(key) != m_keyspressed.end();
        for (unsigned int i = 0; i < m_keyspressed.size(); ++i)
        {
            if (m_keyspressed[i] == (int) key) return true;
        }
        return false;
    }

    /*
    bool Input::isKeyPressed(int key)
    {
        // return m_keyspressed.find(key) != m_keyspressed.end();
        for (unsigned int i = 0; i < m_keyspressed.size(); ++i)
        {
            if (m_keyspressed[i] == key) return true;
        }
        return false;
    }
    */

    bool Input::isKeyUp(Key key)
    {
        // return m_keysup.find(key) != m_keysup.end();
        for (unsigned int i = 0; i < m_keysup.size(); ++i)
        {
            if (m_keysup[i] == (int) key) return true;
        }
        return false;
    }

    /*
    bool Input::isKeyUp(int key)
    {
        // return m_keysup.find(key) != m_keysup.end();
        for (unsigned int i = 0; i < m_keysup.size(); ++i)
        {
            if (m_keysup[i] == key) return true;
        }
        return false;
    }
    */

    void Input::clear()
    {
        m_keysdown.clear();
        m_keysup.clear();
        m_buttonsdown.clear();
        m_buttonsup.clear();
    }

    bool Input::isButtonDown(Button btn)
    {
        // return m_buttonsdown.find(btn) != m_buttonsdown.end();
        for (unsigned int i = 0; i < m_buttonsdown.size(); ++i)
        {
            if (m_buttonsdown[i] == (int) btn) return true;
        }
        return false;
    }

    /*
    bool Input::isButtonDown(int btn)
    {
        // return m_buttonsdown.find(btn) != m_buttonsdown.end();
        for (unsigned int i = 0; i < m_buttonsdown.size(); ++i)
        {
            if (m_buttonsdown[i] == btn) return true;
        }
        return false;
    }
    */

    bool Input::isButtonPressed(Button btn)
    {
        // return m_buttonspressed.find(btn) != m_buttonspressed.end();
        for (unsigned int i = 0; i < m_buttonspressed.size(); ++i)
        {
            if (m_buttonspressed[i] == (int) btn) return true;
        }
        return false;
    }
    /*
    bool Input::isButtonPressed(int btn)
    {
        // return m_buttonspressed.find(btn) != m_buttonspressed.end();
        for (unsigned int i = 0; i < m_buttonspressed.size(); ++i)
        {
            if (m_buttonspressed[i] == btn) return true;
        }
        return false;
    }
    */

    bool Input::isButtonUp(Button btn)
    {
        // return m_buttonsup.find(btn) != m_buttonsup.end();
        for (unsigned int i = 0; i < m_buttonsup.size(); ++i)
        {
            if (m_buttonsup[i] == (int) btn) return true;
        }
        return false;
    }

    /*
    bool Input::isButtonUp(int btn)
    {
        // return m_buttonsup.find(btn) != m_buttonsup.end();
        for (unsigned int i = 0; i < m_buttonsup.size(); ++i)
        {
            if (m_buttonsup[i] == btn) return true;
        }
        return false;
    }
    */

    int Input::getMouseX()
    {
        return m_mouseX;
    }

    int Input::getMouseY()
    {
        return m_mouseY;
    }

    int Input::getMouseDeltaX()
    {
        return m_mousedeltaX;
    }

    int Input::getMouseDeltaY()
    {
        return m_mousedeltaY;
    }

    void Input::registerInputListener(InputListener* inputListener)
    {
        m_inputListeners.push_back(inputListener);
    }

    void Input::unregisterInputListener(InputListener* inputListener)
    {
        for (unsigned int i = 0; i < m_inputListeners.size(); ++i)
        {
            if (m_inputListeners[i] == inputListener)
            {
                m_inputListeners.erase(m_inputListeners.begin() + i);
                break;
            }
        }
    }

    void Input::clearInputListeners()
    {
        m_inputListeners.clear();
    }
}