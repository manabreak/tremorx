#include "stdafx.h"
#include "core/Log.h"

namespace Tremor
{
    void Log::log(std::string& msg)
    {
        std::cout << msg << std::endl;
    }
}