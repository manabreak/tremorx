#include "../../TRLog.h"
#include <android/log.h>



void TRLog::log(std::string msg)
{
	__android_log_print(ANDROID_LOG_INFO, "TREMOR", msg.c_str());
}