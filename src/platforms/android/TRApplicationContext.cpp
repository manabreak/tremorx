#include "../../TRApplicationContext.h"
#include "../../TRLog.h"
// #include "GLES/gl.h"
#include <android/native_window.h>
#include <GLES2/gl2.h>
#include <EGL/egl.h>
#include <jni.h>
#include <sstream>
#include <android/log.h>
#include "TRAndroidInterface.h"
#include "../../Tremor.h"

#define LOG_INFO(...) __android_log_print(ANDROID_LOG_INFO, "TREMOR", __VA_ARGS__)

struct TRApplicationContext::ContextMembers
{
	const EGLint attribs[11] =
	{
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
        EGL_RED_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_BLUE_SIZE, 8,
		EGL_NONE
	};

    const EGLint contextAttribs[3] =
    {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
    };

	ANativeWindow* window;
	EGLDisplay display = 0;
    EGLConfig config;    
    EGLint numConfigs;
    EGLint format;
    EGLSurface surface = 0;
    EGLContext context = 0;
    EGLint width;
    EGLint height;
    GLfloat ratio;
	
    // Just for testing
    std::string vShaderStr =
        "attribute vec4 vPosition; \n"
        "void main() \n"
        "{ \n"
        " gl_Position = vPosition; \n"
        "} \n";

    std::string fShaderStr =
        "precision mediump float; \n"
        "void main() \n"
        "{ \n"
        " gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0); \n"
        "} \n";

    GLuint vertexShader, fragmentShader, programObject;

    GLuint loadShader(const char* shaderSrc, GLenum type)
    {
        GLuint shader;
        GLint compiled;
        shader = glCreateShader(type);
        if (!shader) return 0;
        glShaderSource(shader, 1, &shaderSrc, NULL);
        glCompileShader(shader);
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled)
        {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen > 1)
            {
                char* infoLog = (char*)malloc(sizeof(char) * infoLen);
                glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
                LOG_INFO("Error compiling shader:\n%s\n", infoLog);
                free(infoLog);
            }

            glDeleteShader(shader);
            return 0;
        }
        return shader;
    }

    void createTestShader()
    {
        vertexShader = loadShader(vShaderStr.c_str(), GL_VERTEX_SHADER);
        fragmentShader = loadShader(fShaderStr.c_str(), GL_FRAGMENT_SHADER);
        programObject = glCreateProgram();
        if (!programObject)
        {
            TRLog::log("ERROR - COULD NOT CREATE A SHADER PROGRAM!");
            return;
        }

        glAttachShader(programObject, vertexShader);
        glAttachShader(programObject, fragmentShader);
        glBindAttribLocation(programObject, 0, "vPosition");
        glLinkProgram(programObject);

        // Check the link status
        GLint linked;
        glGetProgramiv(programObject, GL_LINK_STATUS, &linked);
        if (!linked)
        {
            GLint infoLen = 0;
            glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

            if (infoLen > 1)
            {
                char* infoLog = (char*)malloc(sizeof(char) * infoLen);
                glGetProgramInfoLog(programObject, infoLen, NULL, infoLog);
                LOG_INFO("Error linking program:\n%s\n", infoLog);

                free(infoLog);
            }
            glDeleteProgram(programObject);
        }
    }

    void printErrors()
    {
        std::ostringstream vers;
        vers << eglGetError();
        TRLog::log(vers.str());
    }

	void init()
	{
        TRLog::log("TRApplicationContext / ContextMembers: init()");
		
        window = TRAndroidInterface::getWindow();
        if (!window) TRLog::log("Error: window");

		display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
        if (display == EGL_NO_DISPLAY)
        {
            TRLog::log("Tremor: eglGetDisplay() returned error " + eglGetError());
            return;
        }

        EGLint verMajor, verMinor;
        if (!eglInitialize(display, &verMajor, &verMinor))
        {
            TRLog::log("Tremor: eglInitialize() returned error: " + eglGetError());
        }

        std::ostringstream vers;
        vers << "Version: " << verMajor << "." << verMinor;
        TRLog::log(vers.str());
        vers.str(std::string());

        if (!eglChooseConfig(display, attribs, &config, 1, &numConfigs))
        {
            // TRLog::log("Tremor: eglChooseConfig() returned error: " + eglGetError());
            vers << "eglChooseConfig() error: " << eglGetError();
            TRLog::log(vers.str());
            vers.str(std::string());
        }

        if (numConfigs == 0)
        {
            TRLog::log("Tremor: zero EGL configs.");
        }

        if (!eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format))
        {
            // TRLog::log("Tremor: eglGetConfigAttrib() returned error: " + eglGetError());
            vers << "eglGetConfigAttrib() error: " << eglGetError();
            TRLog::log(vers.str());
            vers.str(std::string());
        }

		ANativeWindow_setBuffersGeometry(window, 0, 0, format);

        if (!(surface = eglCreateWindowSurface(display, config, window, 0)))
        {
            // TRLog::log("Tremor: eglCreateWindowSurface() returned error: " + eglGetError());
            vers << "eglCreateWindowSurface() error: " << eglGetError();
            TRLog::log(vers.str());
            vers.str(std::string());
        }

        if (!(context = eglCreateContext(display, config, 0, contextAttribs)))
        {
            // TRLog::log("Tremor: eglCreateContext() returned error: " + eglGetError());
            vers << "eglCreateContext() error: " << eglGetError();
            TRLog::log(vers.str());
            vers.str(std::string());
        }

        if (!eglMakeCurrent(display, surface, surface, context))
        {
            // TRLog::log("Tremor: eglMakeCurrent() returned error: " + eglGetError());
            vers << "eglMakeCurrent() error: " << eglGetError();
            TRLog::log(vers.str());
            vers.str(std::string());
        }

        if (!eglQuerySurface(display, surface, EGL_WIDTH, &width) || !eglQuerySurface(display, surface, EGL_HEIGHT, &height))
        {
            // TRLog::log("Tremor: eglQuerySurface() returned error: " + eglGetError());
            vers << "eglQuerySurface() error: " << eglGetError();
            TRLog::log(vers.str());
            vers.str(std::string());
        }

        TRLog::log("Android context initialized successfully!");
        vers << "Size: " << width << "," << height;
        TRLog::log(vers.str());
        vers.str(std::string());

        createTestShader();
        LOG_INFO("Shader program : %i\n", programObject);

        glViewport(0, 0, width, height);

        // Set input
        Tremor::input = new TRInput();
	}
};

TRApplicationContext::TRApplicationContext()
	: m_contextMembers(new ContextMembers())
{
    TRLog::log("TRApplicationContext: ctor()");
	m_contextMembers->init();
}

TRApplicationContext::TRApplicationContext(int width, int height, const std::string& title, bool fullScreen)
    : m_contextMembers(new ContextMembers())
{
    m_contextMembers->init();
}

TRApplicationContext::~TRApplicationContext()
{
	delete m_contextMembers;
	m_contextMembers = 0;
}

bool TRApplicationContext::shouldClose()
{
	return false;
}

void TRApplicationContext::clear()
{
	glClearColor(1.f, 0.f, 0.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(m_contextMembers->programObject);
}

void TRApplicationContext::swapBuffers()
{
    GLint err = glGetError();
    if (err != GL_NO_ERROR)
    {
        TRLog::log("Tremor: GLError " + err);
    }

    eglSwapBuffers(m_contextMembers->display, m_contextMembers->surface);
}

bool TRApplicationContext::pollEvents()
{
    return false;
}