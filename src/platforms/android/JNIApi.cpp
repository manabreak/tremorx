#include <jni.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>
#include "JNIApi.h"
#include "../../TRApplication.h"
#include "../../TRApplicationContext.h"
#include "../../TRLog.h"

#include <android/log.h>
#define LOG_INFO(...) __android_log_print(ANDROID_LOG_INFO, "TREMOR", __VA_ARGS__)

TRApplication* JNIApi::application;
ANativeWindow* JNIApi::window;
std::thread* JNIApi::mainThread;

void JNIApi::initAndRun()
{
    JNIApi::application = new TRApplication(NULL);
    JNIApi::application->run();
}

JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorOnStart(JNIEnv* jenv, jobject obj)
{
    TRLog::log("Tremor: onStart()");
}

JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorOnResume(JNIEnv* jenv, jobject obj)
{
    TRLog::log("Tremor: onResume()");
}

JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorOnPause(JNIEnv* jenv, jobject obj)
{
    TRLog::log("Tremor: onPause()");
}

JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorOnStop(JNIEnv* jenv, jobject obj)
{
    TRLog::log("Tremor: onStop()");
    JNIApi::application->stop();
}

JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorSurfaceCreated(JNIEnv* jenv, jobject obj, jobject surface)
{
    TRLog::log("Tremor: surfaceCreated()");
    if (surface != 0)
    {
        JNIApi::window = ANativeWindow_fromSurface(jenv, surface);
        LOG_INFO("Tremor: Got window %p", JNIApi::window);
        JNIApi::mainThread = new std::thread(&JNIApi::initAndRun);
    }
    else
    {
        TRLog::log("Tremor: Surface was null.");
        ANativeWindow_release(JNIApi::window);
    }
}

JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorSurfaceChanged(JNIEnv* jenv, jobject obj, jobject surface, int format, int width, int height)
{
    TRLog::log("Tremor: surfaceChanged()");
}
