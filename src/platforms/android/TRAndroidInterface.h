#pragma once

#include <android/native_window.h>

class TRAndroidInterface
{
public:
    static void setWindow(ANativeWindow* window);
    static ANativeWindow* getWindow();
private:
    static ANativeWindow* m_window;
};