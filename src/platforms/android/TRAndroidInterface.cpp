#include "TRAndroidInterface.h"

ANativeWindow* TRAndroidInterface::m_window;

void TRAndroidInterface::setWindow(ANativeWindow* window)
{
    TRAndroidInterface::m_window = window;
}

ANativeWindow* TRAndroidInterface::getWindow()
{
    return TRAndroidInterface::m_window;
}