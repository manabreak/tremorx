#pragma once

#include <jni.h>
#include <android\native_window.h>
#include <thread>
#include "../../TRApplication.h"

extern "C"
{
	JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorOnStart(JNIEnv* jenv, jobject obj);
    JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorOnResume(JNIEnv* jenv, jobject obj);
    JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorOnPause(JNIEnv* jenv, jobject obj);
    JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorOnStop(JNIEnv* jenv, jobject obj);
    JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorSurfaceCreated(JNIEnv* jenv, jobject obj, jobject surface);
    JNIEXPORT void JNICALL Java_com_manabreak_tremor_TremorLauncher_tremorSurfaceChanged(JNIEnv* jenv, jobject obj, jobject surface, int format, int width, int height);
}

class JNIApi
{
public:
    static TRApplication* application;
    static ANativeWindow* window;
    static std::thread* mainThread;
    static void initAndRun();
};
