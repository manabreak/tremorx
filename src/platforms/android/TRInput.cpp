#include "../../TRInput.h"
#include "../../Tremor.h"

TRInput::TRInput()
{

}

TRInput::~TRInput()
{

}


bool TRInput::isKeyDown(TRKey key)
{
    return m_keysdown.find(key) != m_keysdown.end();
}

bool TRInput::isKeyDown(int key)
{
    return m_keysdown.find(key) != m_keysdown.end();
}

bool TRInput::isKeyPressed(TRKey key)
{
    return m_keyspressed.find(key) != m_keyspressed.end();
}

bool TRInput::isKeyPressed(int key)
{
    return m_keyspressed.find(key) != m_keyspressed.end();
}

bool TRInput::isKeyUp(TRKey key)
{
    return m_keysup.find(key) != m_keysup.end();
}

bool TRInput::isKeyUp(int key)
{
    return m_keysup.find(key) != m_keysup.end();
}

void TRInput::clear()
{
    m_keysdown.clear();
    m_keysup.clear();
    m_buttonsdown.clear();
    m_buttonsup.clear();
}

bool TRInput::isButtonDown(TRButton btn)
{
    return false;
}

bool TRInput::isButtonDown(int btn)
{
    return false;
}

bool TRInput::isButtonPressed(TRButton btn)
{
    return false;
}

bool TRInput::isButtonPressed(int btn)
{
    return false;
}

bool TRInput::isButtonUp(TRButton btn)
{
    return false;
}

bool TRInput::isButtonUp(int btn)
{
    return false;
}

int TRInput::getMouseX()
{
    return 0;
}

int TRInput::getMouseY()
{
    return 0;
}

int TRInput::getMouseDeltaX()
{
    return 0;
}

int TRInput::getMouseDeltaY()
{
    return 0;
}
