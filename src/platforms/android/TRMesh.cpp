#include "../../TRMesh.h"
#include "../../TRLog.h"
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <GLES2/gl2platform.h>

int TRMesh::ctorCount = 0;
int TRMesh::dtorCount = 0;

struct TRMesh::MeshMembers
{
    bool m_managed = false;
    bool m_static = true;
    GLuint m_vbo;
    GLuint m_ibo;
    std::vector<TRVertex> m_vertices;
    std::vector<unsigned short> m_indices;
    TRBounds m_bounds;

    MeshMembers()
    {
        TRMesh::ctorCount++;
    }

    ~MeshMembers()
    {
        TRMesh::dtorCount++;
        m_vertices.clear();
        m_indices.clear();
        deleteMesh();
    }

    void setVertices(const std::vector<TRVertex>& vertices)
    {
        m_vertices.clear();
        for (std::vector<TRVertex>::iterator iter = m_vertices.begin(); iter != vertices.end(); ++iter)
        {
            m_vertices.push_back(*iter);
        }
    }

    void setIndices(const std::vector<unsigned short>& indices)
    {
        m_indices.clear();
        m_indices = indices;
    }

    void recalculateBounds()
    {
        glm::vec3 min(10000.f);
        glm::vec3 max(-10000.f);
        // for each(TRVertex v in m_vertices)
        for (std::vector<TRVertex>::iterator iter = m_vertices.begin(); iter != m_vertices.end(); ++iter)
        {
            TRVertex v = *iter;
            min.x = glm::min(min.x, v.m_pos.x);
            min.y = glm::min(min.y, v.m_pos.y);
            min.z = glm::min(min.z, v.m_pos.z);

            max.x = glm::max(max.x, v.m_pos.x);
            max.y = glm::max(max.y, v.m_pos.y);
            max.z = glm::max(max.z, v.m_pos.z);
        }

        m_bounds.set(min, max);
    }

    void setStatic()
    {
        m_static = true;
    }

    void setDynamic()
    {
        m_static = false;
    }

    void update()
    {
        updateIndices();
        updateVertices();
    }

    void updateIndices()
    {
        if (m_indices.size() == 0) return;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(m_indices[0]), &m_indices[0], m_static ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);
    }

    void updateIndices(int offset, int count)
    {
        if (m_indices.size() == 0 || count == 0) return;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, count * sizeof(m_indices[0]), &m_indices[offset]);
    }

    void updateVertices()
    {
        if (m_vertices.size() == 0) return;
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(TRVertex), &m_vertices[0], m_static ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);
    }

    void updateVertices(int offset, int count)
    {
        if (m_vertices.size() == 0 || count == 0) return;
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBufferSubData(GL_ARRAY_BUFFER, offset, count * sizeof(TRVertex), &m_vertices[offset]);
    }

    void initFor(int vertexCount, int indexCount)
    {
        for (int i = 0; i < vertexCount; ++i)
        {
            m_vertices.push_back(TRVertex());
        }
        for (int i = 0; i < indexCount; ++i)
        {
            m_indices.push_back(0);
        }
        initMesh();
    }

    void initMesh()
    {
        if (m_indices.size() == 0)
        {
            for (unsigned short i = 0; i < m_vertices.size(); ++i)
            {
                m_indices.push_back(i);
            }
        }

        glGenBuffers(1, &m_ibo);
        updateIndices();

        glGenBuffers(1, &m_vbo);
        updateVertices();
    }

    void deleteMesh()
    {
        glDeleteBuffers(1, &m_ibo);
        glDeleteBuffers(1, &m_vbo);
    }

    void renderMesh()
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

        glEnableVertexAttribArray(5);
        glEnableVertexAttribArray(4);
        glEnableVertexAttribArray(3);
        glEnableVertexAttribArray(2);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(0);

        // Binormal
        glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) (sizeof(glm::vec3) * 3 + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Tangent
        glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) (sizeof(glm::vec3) * 2 + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Normal
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) (sizeof(glm::vec3) + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Color
        glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) (sizeof(glm::vec3) + sizeof(glm::vec2)));
        // UV
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) (sizeof(glm::vec3)));
        // Position
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) 0);

        glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_SHORT, 0);
    }

    void renderMesh(int type, int offset, int count)
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

        glEnableVertexAttribArray(5);
        glEnableVertexAttribArray(4);
        glEnableVertexAttribArray(3);
        glEnableVertexAttribArray(2);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(0);

        // Binormal
        glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) (sizeof(glm::vec3) * 3 + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Tangent
        glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) (sizeof(glm::vec3) * 2 + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Normal
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) (sizeof(glm::vec3) + sizeof(glm::vec2) + sizeof(glm::vec4)));
        // Color
        glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) (sizeof(glm::vec3) + sizeof(glm::vec2)));
        // UV
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) (sizeof(glm::vec3)));
        // Position
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(TRVertex), (void*) 0);

        // glDrawRangeElements(type, offset, count, count, GL_UNSIGNED_SHORT, 0);

        glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_SHORT, (void*) offset);
    }
};

TRMesh::TRMesh()
    : m_meshMembers(new TRMesh::MeshMembers())
{
    m_meshMembers->initMesh();
}

TRMesh::TRMesh(int vertexCount, int indexCount)
    : m_meshMembers(new TRMesh::MeshMembers())
{
    m_meshMembers->initFor(vertexCount, indexCount);
}

TRMesh::TRMesh(const std::vector<TRVertex>& vertices)
    : m_meshMembers(new TRMesh::MeshMembers())
{
    m_meshMembers->setVertices(vertices);
    m_meshMembers->initMesh();
}

TRMesh::TRMesh(const std::vector<TRVertex>& vertices, const std::vector<unsigned short>& indices)
    : m_meshMembers(new TRMesh::MeshMembers())
{
    m_meshMembers->setVertices(vertices);
    m_meshMembers->setIndices(indices);
    m_meshMembers->initMesh();
}

TRMesh::~TRMesh()
{
    m_meshMembers->deleteMesh();
    delete m_meshMembers;
    m_meshMembers = 0;
}

void TRMesh::render()
{
    m_meshMembers->renderMesh();
}

void TRMesh::render(TRShaderProgram* shader, int type, int offset, int count)
{
    // shader->begin();
    m_meshMembers->renderMesh(type, offset, count);
}

void TRMesh::renderDebug()
{
    // Not yet implemented.
}

void TRMesh::setVertices(const std::vector<TRVertex>& vertices)
{
    m_meshMembers->setVertices(vertices);
    m_meshMembers->updateVertices();
}

void TRMesh::setVertices(const std::vector<TRVertex>& vertices, bool update)
{
    m_meshMembers->setVertices(vertices);
    if (update) m_meshMembers->updateVertices();
}

void TRMesh::setIndices(const std::vector<unsigned short>& indices)
{
    m_meshMembers->setIndices(indices);
    m_meshMembers->updateIndices();
}

void TRMesh::setIndices(const std::vector<unsigned short>& indices, bool update)
{
    m_meshMembers->setIndices(indices);
    if (update) m_meshMembers->updateIndices();
}

std::vector<TRVertex>& TRMesh::getVertices()
{
    return m_meshMembers->m_vertices;
}

std::vector<unsigned short>& TRMesh::getIndices()
{
    return m_meshMembers->m_indices;
}


void TRMesh::update()
{
    m_meshMembers->update();
}

void TRMesh::update(int vertexCount, int indexCount)
{
    m_meshMembers->updateIndices(0, indexCount);
    m_meshMembers->updateVertices(0, vertexCount);
}

void TRMesh::setVertex(int index, float x, float y)
{
    m_meshMembers->m_vertices[index].m_pos.x = x;
    m_meshMembers->m_vertices[index].m_pos.y = y;
}

void TRMesh::update(int vertexCount, int vertexOffset, int indexCount, int indexOffset)
{
    m_meshMembers->updateVertices(vertexOffset, vertexCount);
    m_meshMembers->updateIndices(indexOffset, indexCount);
}

void TRMesh::updateVertices(int count, int offset)
{
    m_meshMembers->updateVertices(offset, count);
}

void TRMesh::updateIndices(int count, int offset)
{
    m_meshMembers->updateIndices(offset, count);
}

void TRMesh::setVertex(int index, float x, float y, float z)
{
    m_meshMembers->m_vertices[index].m_pos.x = x;
    m_meshMembers->m_vertices[index].m_pos.y = y;
    m_meshMembers->m_vertices[index].m_pos.y = z;
}

void TRMesh::setVertex(int index, float x, float y, float z, float u, float v)
{
    m_meshMembers->m_vertices[index].m_pos.x = x;
    m_meshMembers->m_vertices[index].m_pos.y = y;
    m_meshMembers->m_vertices[index].m_pos.y = z;
    m_meshMembers->m_vertices[index].m_uv.x = u;
    m_meshMembers->m_vertices[index].m_uv.y = v;
}

void TRMesh::setVertex(int index, float x, float y, float z, float u, float v, float nx, float ny, float nz)
{
    m_meshMembers->m_vertices[index].m_pos.x = x;
    m_meshMembers->m_vertices[index].m_pos.y = y;
    m_meshMembers->m_vertices[index].m_pos.y = z;
    m_meshMembers->m_vertices[index].m_uv.x = u;
    m_meshMembers->m_vertices[index].m_uv.y = v;
    m_meshMembers->m_vertices[index].m_nor.x = nx;
    m_meshMembers->m_vertices[index].m_nor.y = ny;
    m_meshMembers->m_vertices[index].m_nor.z = nz;
}

void TRMesh::setVertex(int index, const glm::vec3& pos)
{
    m_meshMembers->m_vertices[index].m_pos = pos;
}

void TRMesh::setVertex(int index, const glm::vec3& pos, const glm::vec2& uv)
{
    m_meshMembers->m_vertices[index].m_pos = pos;
    m_meshMembers->m_vertices[index].m_uv = uv;
}

void TRMesh::setVertex(int index, const glm::vec3& pos, const glm::vec2& uv, const glm::vec4& col, const glm::vec3& nor, const glm::vec3& tan, const glm::vec3& bin)
{
    m_meshMembers->m_vertices[index].m_pos = pos;
    m_meshMembers->m_vertices[index].m_uv = uv;
    m_meshMembers->m_vertices[index].m_col = col;
    m_meshMembers->m_vertices[index].m_nor = nor;
    m_meshMembers->m_vertices[index].m_nor = bin;
    m_meshMembers->m_vertices[index].m_tan = tan;
}

void TRMesh::setVertex(int index, const TRVertex& vertex)
{
    m_meshMembers->m_vertices[index] = vertex;
}

void TRMesh::setDynamic()
{
    m_meshMembers->m_static = false;
}

void TRMesh::setStatic()
{
    m_meshMembers->m_static = true;
}

const TRBounds& TRMesh::getBounds()
{
    return m_meshMembers->m_bounds;
}

bool TRMesh::isManaged()
{
    return m_meshMembers->m_managed;
}