#include "../../TRLog.h"
#include <windows.h>
#include <sstream>

void TRLog::log(std::string msg)
{
    LPCSTR w = msg.c_str();
    OutputDebugStringA(w);
}