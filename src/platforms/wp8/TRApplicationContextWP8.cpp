#include "../../TRApplicationContext.h"
#include "../../TRLog.h"
#include "WP8Api.h"

struct TRApplicationContext::ContextMembers
{
   
};

TRApplicationContext::TRApplicationContext()
    : m_contextMembers(new ContextMembers())
{
    TRLog::log("Whee, constructed a WP8 context!");
}

TRApplicationContext::~TRApplicationContext()
{
    delete m_contextMembers;
    m_contextMembers = 0;
}

bool TRApplicationContext::shouldClose()
{
    return false;
}

void TRApplicationContext::clear()
{
    TRLog::log("clear()");
    WP8Api::dxContext->RSSetViewports(1, &WP8Api::dxViewport);
    ID3D11RenderTargetView *const targets[1] = { WP8Api::dxRenderTargetView };
    WP8Api::dxContext->OMSetRenderTargets(1, targets, WP8Api::dxDepthStencilView);

    WP8Api::dxContext->ClearRenderTargetView(WP8Api::dxRenderTargetView, DirectX::Colors::CornflowerBlue);
    WP8Api::dxContext->ClearDepthStencilView(WP8Api::dxDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

void TRApplicationContext::swapBuffers()
{
    TRLog::log("swapBuffers()");

    HRESULT hr = WP8Api::dxSwapChain->Present(1, 0);
    WP8Api::dxContext->DiscardView(WP8Api::dxRenderTargetView);
    WP8Api::dxContext->DiscardView(WP8Api::dxDepthStencilView);

    if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
    {
        
    }
}

TRApplicationContext::ContextMembers* TRApplicationContext::getContextMembers()
{
    return m_contextMembers;
}