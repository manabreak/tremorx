#pragma once
#include <d3d11_2.h>
#include <DirectXColors.h>

// Keeps references to the D3D-related stuff
class WP8Api
{
public:
    // D3D
    static ID3D11Device2* dxDevice;
    static ID3D11DeviceContext2* dxContext;
    static IDXGISwapChain1* dxSwapChain;
    static D3D_FEATURE_LEVEL dxFeatureLevel;
    static ID3D11RenderTargetView* dxRenderTargetView;
    static ID3D11DepthStencilView* dxDepthStencilView;
    static D3D11_VIEWPORT dxViewport;
    static DirectX::XMFLOAT4X4 dxOrientationTransform3D;

    // D2D
};

