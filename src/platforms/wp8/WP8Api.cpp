#include "WP8Api.h"

ID3D11Device2* WP8Api::dxDevice;
ID3D11DeviceContext2* WP8Api::dxContext;
IDXGISwapChain1* WP8Api::dxSwapChain;
D3D_FEATURE_LEVEL WP8Api::dxFeatureLevel;
ID3D11RenderTargetView* WP8Api::dxRenderTargetView;
ID3D11DepthStencilView* WP8Api::dxDepthStencilView;
D3D11_VIEWPORT WP8Api::dxViewport;
DirectX::XMFLOAT4X4 WP8Api::dxOrientationTransform3D;
