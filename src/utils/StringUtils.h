#pragma once

namespace Tremor
{
    class StringUtils
    {
    public:
        static std::string readFile(const std::string& filename);
        static std::string trimStart(const std::string& s);
        static std::string trimEnd(const std::string& s);
        static std::string trim(const std::string& s);
    };
}