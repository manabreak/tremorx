#include "stdafx.h"
#include "LuaExposer.h"
#include "systems/LuaScriptSystem.h"
#include <LuaBridge/LuaBridge.h>
#include "core/Input.h"
#include "LuaUtilities.h"
#include "components/PhysicsBody.h"
#include "components/ModelComponent.h"
#include "components/Sprite.h"
#include "components/SpriteAnimation.h"
#include "components/Transform.h"
#include "core/Application.h"
#include "assets/AssetManager.h"
#include "assets/TiledMapLoader.h"
#include <World.h>
#include <Entity.h>
#include <vector>
#include "rendering/Camera.h"
#include "glmhelper.h"
#include "rendering/SpriteBatch.h"
#include "2d/Text2D.h"
#include "2d/BitmapFont.h"
#include "lights/PointLight.h"
#include "lights/SpotLight.h"
#include "components/PizzaLight.h"
#include "systems/PizzaLightingSystem.h"
#include "components/AtlasSprite.h"

using namespace luabridge;

namespace Tremor
{
    LuaExposer::LuaExposer(lua_State* L)
    {
        this->L = L;
        exposeTECS();
        exposeAssetManager();
        exposeInput();
        exposeUtilities();
        exposePhysicsBody();
        exposeModel();
        exposeSprite();
        exposeBitmapFont();
        exposeText2D();
        exposeSpriteAnimation();
        exposeTransform();
        exposeGLM();
        exposeTiledMap();
        exposeCamera();
        exposeLights();
    }

    LuaExposer::LuaExposer(LuaScriptSystem& system)
    {
        L = system.getL();
        exposeInput();
        exposeUtilities();
    }

    LuaExposer::~LuaExposer()
    {

    }

    void LuaExposer::exposeBitmapFont()
    {
        getGlobalNamespace(L)
            .beginClass<BitmapFont>("Font")
            .addFunction("getHeight", &BitmapFont::getHeight)
            .addFunction("getStringWidth", &BitmapFont::getStringWidth)
            .endClass();
    }

    void LuaExposer::exposeText2D()
    {
        getGlobalNamespace(L)
            .beginClass<Text2D>("Text2D")
            .addFunction("setText", &Text2D::setText)
            .addFunction("getText", &Text2D::getText)
            .addFunction("getLength", &Text2D::getLength)
            .addFunction("getWidth", &Text2D::getWidth)
            .addFunction("getHeight", &Text2D::getHeight)
            .addFunction("getRotation", &Text2D::getRotation)
            .addFunction("setRotation", &Text2D::setRotation)
            .addFunction("setVisible", &Text2D::setVisible)
            .addFunction("isVisible", &Text2D::isVisible)
            .addFunction("setRGB", &Text2D::setRGB)
            .addFunction("setRGBA", &Text2D::setRGBA)
            .addFunction("setAlpha", &Text2D::setAlpha)
            .addFunction("getR", &Text2D::getR)
            .addFunction("getG", &Text2D::getG)
            .addFunction("getB", &Text2D::getB)
            .addFunction("getAlpha", &Text2D::getAlpha)
            .addFunction("setScale", &Text2D::setScale)
            .addFunction("setScaleX", &Text2D::setScaleX)
            .addFunction("setScaleY", &Text2D::setScaleY)
            .addFunction("getScaleX", &Text2D::getScaleX)
            .addFunction("getScaleY", &Text2D::getScaleY)
            .addFunction("setPosition", &Text2D::setPosition)
            .addFunction("setX", &Text2D::setX)
            .addFunction("setY", &Text2D::setY)
            .addFunction("getX", &Text2D::getX)
            .addFunction("getY", &Text2D::getY)
            .addFunction("isUniformScale", &Text2D::isUniformScale)
            .endClass();
    }

    void LuaExposer::exposeTiledMap()
    {
        getGlobalNamespace(L)
            // TiledMap
            .beginClass<TiledMap>("TiledMap")
            .addConstructor<void(*)(const std::string&, int, int, int, const std::string&, int, int)>()
            .addFunction("getFilename", &TiledMap::getFilename)
            .addFunction("getLayers", &TiledMap::getLayers)
            .addFunction("getTilesets", &TiledMap::getTilesets)
            .addFunction("getTileWidth", &TiledMap::getTileWidth)
            .addFunction("getTileHeight", &TiledMap::getTileHeight)
            .addFunction("getVersion", &TiledMap::getVersion)
            .addFunction("getWidth", &TiledMap::getWidth)
            .addFunction("getHeight", &TiledMap::getHeight)
            .addFunction("getOrientation", &TiledMap::getOrientation)
            .addFunction("getTileset", &TiledMap::getTileset)
            .addFunction("getLayerCount", &TiledMap::getLayerCount)
            .addFunction("getLayer", &TiledMap::getLayer)
            .endClass()
            // MapLayer
            .beginClass<MapLayer>("MapLayer")
            .addConstructor<void(*)()>()
            .addFunction("isVisible", &MapLayer::isVisible)
            .addFunction("isTileLayer", &MapLayer::isTileLayer)
            .addFunction("getWidth", &MapLayer::getWidth)
            .addFunction("getHeight", &MapLayer::getHeight)
            .addFunction("getX", &MapLayer::getX)
            .addFunction("getY", &MapLayer::getY)
            .addFunction("getData", &MapLayer::getData)
            .addFunction("getObjects", &MapLayer::getObjects)
            .addFunction("getName", &MapLayer::getName)
            .addFunction("getType", &MapLayer::getType)
            .addFunction("getProperties", &MapLayer::getProperties)
            .addFunction("getDataCount", &MapLayer::getDataCount)
            .addFunction("getObjectCount", &MapLayer::getObjectCount)
            .addFunction("getDataAt", &MapLayer::getDataAt)
            .addFunction("getMapObject", &MapLayer::getMapObject)
            .endClass()
            // MapObject
            .beginClass<MapObject>("MapObject")
            .addFunction("getX", &MapObject::getX)
            .addFunction("getY", &MapObject::getY)
            .addFunction("getWidth", &MapObject::getWidth)
            .addFunction("getHeight", &MapObject::getHeight)
            .addFunction("getName", &MapObject::getName)
            .addFunction("getType", &MapObject::getType)
            .addFunction("isVisible", &MapObject::isVisible)
            .addFunction("getProperties", &MapObject::getProperties)
            .endClass()
            // MapProperties
            .beginClass<MapProperties>("MapProperties")
            .addFunction("getFloat", &MapProperties::getFloat)
            .addFunction("getInt", &MapProperties::getInt)
            .addFunction("getString", &MapProperties::getString)
            .addFunction("getBool", &MapProperties::getBool)
            .addFunction("hasProperty", &MapProperties::hasProperty)
            .endClass()
            ;
    }

    void LuaExposer::exposeApplication()
    {
        /*
        getGlobalNamespace(L)
            .beginClass<Application>("Application")
            .addConstructor<void(*)(ApplicationListener&)>()
            .addConstructor<void(*)(ApplicationListener&, ApplicationContext&)>()
            .addConstructor<void(*)(ApplicationListener&, LaunchConfig&)>()
            .addFunction("run", &Application::run)
            .addFunction("stop", &Application::stop)
            .addFunction("pause", &Application::pause)
            .addFunction("resume", &Application::pause)
            .endClass();
            */
    }

    void LuaExposer::exposeAssetManager()
    {
        getGlobalNamespace(L)
            .beginClass<AssetManager>("Assets")
            .addStaticFunction("loadBitmapFont", &AssetManager::loadBitmapFont)
            .addStaticFunction("loadTexture", &AssetManager::loadTexture)
            .addStaticFunction("loadShader", &AssetManager::loadShader)
            .addStaticFunction("loadTiledMap", &AssetManager::loadTiledMap)
            .endClass();
    }

    void LuaExposer::exposeInput()
    {
        // TODO Implement.
        
        getGlobalNamespace(L)
            .beginClass<Input>("Input")
            .addStaticFunction("isKeyDown", &Input::isKeyDown)
            .endClass();
        
    }

    void LuaExposer::exposeGLM()
    {
        getGlobalNamespace(L)
            // glm statics
            .beginClass<GLMHelper>("glm")
            .addStaticFunction("addVec2", &GLMHelper::addVec2)
            .addStaticFunction("addVec3", &GLMHelper::addVec3)
            .addStaticFunction("subVec2", &GLMHelper::subVec2)
            .addStaticFunction("subVec3", &GLMHelper::subVec3)
            .addStaticFunction("lengthVec2", &GLMHelper::lengthVec2)
            .addStaticFunction("lengthVec3", &GLMHelper::lengthVec3)
            .addStaticFunction("directionVec2", &GLMHelper::directionVec2)
            .addStaticFunction("directionVec3", &GLMHelper::directionVec3)
            .addStaticFunction("getDirectionXZ", &GLMHelper::getDirectionXZ)
            .endClass()
            // vec3
            .beginClass<glm::vec3>("vec3")
            .addConstructor<void(*)(float)>()
            .addConstructor<void(*)(float, float, float)>()
            .addData("x", &glm::vec3::x)
            .addData("y", &glm::vec3::y)
            .addData("z", &glm::vec3::z)
            .endClass()
            // vec2
            .beginClass<glm::vec2>("vec2")
            .addConstructor<void(*)(float, float)>()
            .addData("x", &glm::vec2::x)
            .addData("y", &glm::vec2::y)
            .endClass();


    }

    void LuaExposer::exposeTransform()
    {
        getGlobalNamespace(L)
            .beginClass<Transform>("Transform")
            .addFunction("addChild", &Transform::addChild)
            .addFunction("remove", &Transform::remove)
            .addFunction("getParent", &Transform::getParent)
            .addFunction("hasParent", &Transform::hasParent)
            .addFunction("setPosition", &Transform::setPositionXYZ)
            .addFunction("getX", &Transform::getX)
            .addFunction("getY", &Transform::getY)
            .addFunction("getZ", &Transform::getZ)
            .addFunction("getPosition", &Transform::getPosition)
            .addFunction("setX", &Transform::setX)
            .addFunction("setY", &Transform::setY)
            .addFunction("setZ", &Transform::setZ)
            .addFunction("getWorldX", &Transform::getWorldX)
            .addFunction("getWorldY", &Transform::getWorldY)
            .addFunction("getWorldZ", &Transform::getWorldZ)
            .addFunction("translate", &Transform::translateXYZ)
            .addFunction("setEulerAngles", &Transform::setEulerAnglesXYZ)
            .addFunction("getEulerX", &Transform::getEulerX)
            .addFunction("getEulerY", &Transform::getEulerY)
            .addFunction("getEulerZ", &Transform::getEulerZ)
            .addFunction("rotateEuler", &Transform::rotateEulerXYZ)
            .addFunction("setUniformScale", &Transform::setUniformScale)
            .addFunction("setScaleXYZ", &Transform::setScaleXYZ)
            .addFunction("getScaleX", &Transform::getScaleX)
            .addFunction("getScaleY", &Transform::getScaleY)
            .addFunction("getScaleZ", &Transform::getScaleZ)
            .addFunction("scaleUniform", &Transform::scaleUniform)
            .addFunction("scaleXYZ", &Transform::scaleXYZ)
            .endClass();
    }

    void LuaExposer::exposeSpriteAnimation()
    {
        getGlobalNamespace(L)
            .beginClass<SpriteAnimation>("SpriteAnimation")
            .addFunction("addTake", &SpriteAnimation::addTake)
            .addFunction("removeTake", &SpriteAnimation::removeTake)
            .addFunction("getCurrentTake", &SpriteAnimation::getCurrentTake)
            .addFunction("isPlaying", &SpriteAnimation::isPlaying)
            .addFunction("isPaused", &SpriteAnimation::isPaused)
            .addFunction("play", &SpriteAnimation::play)
            .addFunction("pause", &SpriteAnimation::pause)
            .addFunction("stop", &SpriteAnimation::stop)
            .addFunction("isLooping", &SpriteAnimation::isLooping)
            .addFunction("setLooping", &SpriteAnimation::setLooping)
            .addFunction("getCurrentFrame", &SpriteAnimation::getCurrentFrame)
            .addFunction("setFrame", &SpriteAnimation::setFrame)
            .addFunction("nextFrame", &SpriteAnimation::nextFrame)
            .endClass();
    }

    void LuaExposer::exposeLights()
    {
        getGlobalNamespace(L)
            .beginClass<Light>("Light")
            .addFunction("setColor", &Light::setColor)
            .addFunction("setIntensity", &Light::setIntensity)
            .addFunction("setPosition", &Light::setPosition)
            .addFunction("enableShadows", &Light::enableShadows)
            .addFunction("disableShadows", &Light::disableShadows)
            .endClass()
            .deriveClass<PointLight, Light>("PointLight")
            .addFunction("setRadius", &PointLight::setRadius)
            .endClass()
            .deriveClass<SpotLight, Light>("SpotLight")
            .addFunction("setRange", &SpotLight::setRange)
            .addFunction("setAngle", &SpotLight::setAngle)
            .addFunction("setDirection", &SpotLight::setDirection)
            .endClass();

        getGlobalNamespace(L)
            .beginClass<PizzaLight>("PizzaLight")
            .endClass()
            .beginClass<PizzaHull>("PizzaHull")
            .endClass()
            .deriveClass<RectanglePizzaHull, PizzaHull>("RectanglePizzaHull")
            .endClass()
            .beginClass<PizzaLightingSystem>("PizzaLightingSystem")
            .endClass();

    }

    void LuaExposer::exposeSprite()
    {
        getGlobalNamespace(L)
            .beginClass<SpriteBatch>("SpriteBatch")
            .addConstructor<void(*)()>()
            .addFunction("Begin", &SpriteBatch::begin)
            .addFunction("End", &SpriteBatch::end)
            .addFunction("Draw", &SpriteBatch::drawPtr)
            .addFunction("DrawText", &SpriteBatch::drawTextPtr)
            .addFunction("setScreen", &SpriteBatch::setScreen)
            .endClass()

            .beginClass<Texture>("Texture")
            .endClass()

            .beginClass<Sprite>("Sprite")
            .addFunction("setPosition", &Sprite::setPosition)
            .addFunction("setX", &Sprite::setX)
            .addFunction("setY", &Sprite::setY)
            .addFunction("getX", &Sprite::getX)
            .addFunction("getY", &Sprite::getY)
            .addFunction("setSize", &Sprite::setSize)
            .addFunction("getWidth", &Sprite::getWidth)
            .addFunction("getHeight", &Sprite::getHeight)
            .addFunction("setRotationX", &Sprite::setRotationX)
            .addFunction("setAlpha", &Sprite::setAlpha)
            .addFunction("setColor", &Sprite::setColor)
            .addFunction("setColorRGB", &Sprite::setColorRGB)
            .addFunction("setColorRGBA", &Sprite::setColorRGBA)
            .addFunction("getR", &Sprite::getColorR)
            .addFunction("getG", &Sprite::getColorG)
            .addFunction("getB", &Sprite::getColorB)
            .addFunction("setOrigin", &Sprite::setOrigin)
            .addFunction("setOriginX", &Sprite::setOriginX)
            .addFunction("setOriginY", &Sprite::setOriginY)
            .addFunction("getOriginX", &Sprite::getOriginX)
            .addFunction("getOriginY", &Sprite::getOriginY)
            .addFunction("setRegion", &Sprite::setRegion)
            .addFunction("setU", &Sprite::setU)
            .addFunction("setV", &Sprite::setV)
            .addFunction("setU2", &Sprite::setU2)
            .addFunction("setV2", &Sprite::setV2)
            .addFunction("setFlip", &Sprite::setFlip)
            .addFunction("flip", &Sprite::flip)
            .addFunction("scroll", &Sprite::scroll)
            .addFunction("setSourceRectangle", &Sprite::setSourceRectangle)
            .endClass()
            .deriveClass<AtlasSprite, Sprite>("AtlasSprite")
            
            .endClass();
    }

    void LuaExposer::exposeUtilities()
    {
        getGlobalNamespace(L)
            .beginClass<LuaUtilities>("Utils")
            .addStaticFunction("print", &LuaUtilities::print)
            .endClass();
            
    }

    void LuaExposer::exposeModel()
    {
        getGlobalNamespace(L)
            .beginClass<ModelComponent>("ModelComponent")
            .endClass()
            .beginClass<Model>("Model")
            .endClass();
    }

    void LuaExposer::exposePhysicsBody()
    {
        getGlobalNamespace(L)
            .deriveClass<PhysicsBody, Tecs::Component>("PhysicsBody")
            // Box colliders and sensors
            .addFunction("addBoxCollider", &PhysicsBody::addBoxCollider)
            .addFunction("addOffsetBoxCollider", &PhysicsBody::addOffsetBoxCollider)
            .addFunction("addBoxSensor", &PhysicsBody::addBoxSensor)
            .addFunction("addOffsetBoxSensor", &PhysicsBody::addOffsetBoxSensor)
            // Circle colliders and sensors
            .addFunction("addCircleCollider", &PhysicsBody::addCircleCollider)
            .addFunction("addOffsetCircleCollider", &PhysicsBody::addOffsetCircleCollider)
            .addFunction("addCircleSensor", &PhysicsBody::addCircleSensor)
            .addFunction("addOffsetCircleSensor", &PhysicsBody::addOffsetCircleSensor)
            // Body type
            .addFunction("isDynamic", &PhysicsBody::isDynamic)
            .addFunction("isKinematic", &PhysicsBody::isKinematic)
            .addFunction("isStatic", &PhysicsBody::isStatic)
            .addFunction("setDynamic", &PhysicsBody::setDynamic)
            .addFunction("setStatic", &PhysicsBody::setStatic)
            .addFunction("setKinematic", &PhysicsBody::setKinematic)
            // Position and velocity
            .addFunction("setPosition", &PhysicsBody::setPosition)
            .addFunction("setLinearVelocity", &PhysicsBody::setVelocity)
            .addFunction("getX", &PhysicsBody::getX)
            .addFunction("getY", &PhysicsBody::getY)
            .addFunction("getPosition", &PhysicsBody::getPosition)
            .endClass()
            
            // CollisionListener - can be created in Lua
            .beginClass<CollisionListener>("CollisionListener")
            
            .endClass();
    }

    void LuaExposer::exposeTECS()
    {
        getGlobalNamespace(L)
            // Tecs::World
            .beginClass<Tecs::World>("World")
            .addFunction("createEntity", &Tecs::World::createEntity)
            .addFunction("deleteEntity", &Tecs::World::deleteEntity)
            .addFunction("changedEntity", &Tecs::World::changedEntity)
            .addFunction("enableEntity", &Tecs::World::enableEntity)
            .addFunction("disableEntity", &Tecs::World::disableEntity)
            .addFunction("getDelta", &Tecs::World::getDelta)
            .addFunction("getEntityCount", &Tecs::World::getEntityCount)
            .addFunction("clearEntities", &Tecs::World::clearEntities)

            .endClass()
            // Tecs::Entity
            .beginClass<Tecs::Entity>("Entity")
            .addFunction("getID", &Tecs::Entity::getId)
            .addFunction("enable", &Tecs::Entity::enable)
            .addFunction("disable", &Tecs::Entity::disable)
            .addFunction("refresh", &Tecs::Entity::refresh)
            .addFunction("remove", &Tecs::Entity::remove)
            .addFunction("isEnabled", &Tecs::Entity::isEnabled)

            .endClass()
            .beginClass<Tecs::Component>("Component")
            .addFunction("getEntityID", &Tecs::Component::getEntityID)
            .addFunction("enable", &Tecs::Component::enable)
            .addFunction("disable", &Tecs::Component::disable)
            .addFunction("isEnabled", &Tecs::Component::isEnabled)
            .endClass();
    }

    void LuaExposer::exposeCamera()
    {
        getGlobalNamespace(L)
            .beginClass<Camera>("Camera")
            .addFunction("lookAt", &Camera::lookAtXYZ)
            .addFunction("getNear", &Camera::getNear)
            .addFunction("getFar", &Camera::getFar)
            .addFunction("setNear", &Camera::setNear)
            .addFunction("setFar", &Camera::setFar)
            .addFunction("setPosition", &Camera::setPositionXYZ)
            .addFunction("rotateX", &Camera::rotateX)
            .addFunction("worldTransformToScreenPoint", &Camera::worldTransformToScreenPoint)
            .endClass();
    }
}