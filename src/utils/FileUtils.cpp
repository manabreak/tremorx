#include "stdafx.h"
#include "FileUtils.h"

#include <fstream>

namespace Tremor
{
    bool FileUtils::fileExists(const std::string& file)
    {
        std::ifstream f(file, std::ios::binary);
        if (!f) return false;
        return true;
    }

    std::string FileUtils::findFile(const std::string& filename, const std::string& basepath)
    {
        // Check if the file is in the working directory
        if (fileExists(filename)) return filename;

        

        return "";
    }
}