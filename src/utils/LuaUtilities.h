#pragma once

#include <string>
#include <iostream>

namespace Tremor
{
    class LuaUtilities
    {
    public:
        static void print(const std::string& string);
    };
}