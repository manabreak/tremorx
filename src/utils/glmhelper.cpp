#include "stdafx.h"
#include "glmhelper.h"

namespace Tremor
{
    glm::vec2 GLMHelper::addVec2(const glm::vec2& a, const glm::vec2& b)
    {
        return a + b;
    }

    glm::vec3 GLMHelper::addVec3(const glm::vec3& a, const glm::vec3& b)
    {
        return a + b;
    }

    glm::vec2 GLMHelper::subVec2(const glm::vec2& a, const glm::vec2& b)
    {
        return a - b;
    }

    glm::vec3 GLMHelper::subVec3(const glm::vec3& a, const glm::vec3& b)
    {
        return a - b;
    }

    float GLMHelper::lengthVec2(const glm::vec2& v)
    {
        return glm::length(v);
    }

    float GLMHelper::lengthVec3(const glm::vec3& v)
    {
        return glm::length(v);
    }

    glm::vec2 GLMHelper::directionVec2(const glm::vec2& a, const glm::vec2& b)
    {
        // std::cout << "a: " << a.x << ", " << a.y << " | b: " << b.x << ", " << b.y << std::endl;
        glm::vec2 dir = glm::normalize(b - a);
        // std::cout << "Dir: " << dir.x << ", " << dir.y << std::endl;
        return dir;
    }

    glm::vec3 GLMHelper::directionVec3(const glm::vec3& a, const glm::vec3& b)
    {
        return glm::normalize(b - a);
    }

    glm::vec2 GLMHelper::getDirectionXZ(const glm::vec3& a, const glm::vec3& b)
    {
        glm::vec3 dir = directionVec3(a, b);
        return glm::vec2(dir.x, dir.z);
    }
}