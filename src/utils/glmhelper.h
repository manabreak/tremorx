#pragma once
#include <glm/glm.hpp>

namespace Tremor
{
    class GLMHelper
    {
    public:
        static glm::vec2 addVec2(const glm::vec2& a, const glm::vec2& b);
        static glm::vec3 addVec3(const glm::vec3& a, const glm::vec3& b);

        static glm::vec2 subVec2(const glm::vec2& a, const glm::vec2& b);
        static glm::vec3 subVec3(const glm::vec3& a, const glm::vec3& b);

        static float lengthVec2(const glm::vec2& v);
        static float lengthVec3(const glm::vec3& v);
        static glm::vec2 directionVec2(const glm::vec2& a, const glm::vec2& b);
        static glm::vec3 directionVec3(const glm::vec3& a, const glm::vec3& b);
        static glm::vec2 getDirectionXZ(const glm::vec3& a, const glm::vec3& b);
    };
}