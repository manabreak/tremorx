#pragma once

#include <string>

namespace Tremor
{
    class FileUtils
    {
    public:
        // Checks if the given file exists.
        static bool fileExists(const std::string& file);

        // Finds a file called 'filename' in the folder 'basepath' and its subfolders.
        // Returns and empty string if file is not found.
        static std::string findFile(const std::string& filename, const std::string& basepath = "./");
    };
}