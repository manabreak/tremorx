#include "stdafx.h"

#include "StringUtils.h"
#include <fstream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

namespace Tremor
{
    std::string StringUtils::readFile(const std::string& filename)
    {
        std::ifstream ifs(filename);
        std::string content;
        content.assign((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
        return content;
    }

    std::string StringUtils::trimStart(const std::string& s)
    {
        std::string t = s;
        t.erase(t.begin(), std::find_if(t.begin(), t.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return t;
    }

    std::string StringUtils::trimEnd(const std::string& s)
    {
        std::string t = s;
        t.erase(std::find_if(t.rbegin(), t.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), t.end());
        return t;
    }

    std::string StringUtils::trim(const std::string& s)
    {
        return trimStart(trimEnd(s));
    }
}