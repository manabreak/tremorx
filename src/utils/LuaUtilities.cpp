#include "stdafx.h"
#include "LuaUtilities.h"

namespace Tremor
{
    void LuaUtilities::print(const std::string& string)
    {
        std::cout << "LuaUtilities::print() called with: " << string << std::endl;
    }
}