#include "stdafx.h"
#include "MeshFactory.h"

namespace Tremor
{
    Mesh* MeshFactory::createQuadXY(float width, float height)
    {
        float w = width / 2.f;
        float h = height / 2.f;

        std::vector<Vertex> vertices;
        std::vector<unsigned short> indices;

        Vertex v0(-w, h, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f);
        Vertex v1(-w, -h, 0.f, 0.f, 0.f, 0.f, 0.f, 1.f);
        Vertex v2(w, -h, 0.f, 1.f, 0.f, 0.f, 0.f, 1.f);
        Vertex v3(w, h, 0.f, 1.f, 1.f, 0.f, 0.f, 1.f);

        vertices.push_back(v0);
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.push_back(v3);

        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);
        indices.push_back(2);
        indices.push_back(3);
        indices.push_back(0);

        return new Mesh(vertices, indices);
    }

    Mesh* MeshFactory::createQuadXZ(float width, float height)
    {
        float w = width / 2.f;
        float h = height / 2.f;

        std::vector<Vertex> vertices;
        std::vector<unsigned short> indices;

        Vertex v0(-w, 0.f, -h, 0.f, 1.f, 0.f, 1.f, 0.f);
        Vertex v1(-w, 0.f, h, 0.f, 0.f, 0.f, 1.f, 0.f);
        Vertex v2(w, 0.f, h, 1.f, 0.f, 0.f, 1.f, 0.f);
        Vertex v3(w, 0.f, -h, 1.f, 1.f, 0.f, 1.f, 0.f);

        vertices.push_back(v0);
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.push_back(v3);

        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);
        indices.push_back(2);
        indices.push_back(3);
        indices.push_back(0);

        return new Mesh(vertices, indices);
    }

    Mesh* MeshFactory::createUnitPyramidBackward()
    {
        return createUnitPyramid(0);
    }

    Mesh* MeshFactory::createUnitPyramidForward()
    {
        return createUnitPyramid(1);
    }

    Mesh* MeshFactory::createUnitPyramidRight()
    {
        return createUnitPyramid(2);
    }

    Mesh* MeshFactory::createUnitPyramidLeft()
    {
        return createUnitPyramid(3);
    }

    Mesh* MeshFactory::createUnitPyramidUp()
    {
        return createUnitPyramid(4);
    }

    Mesh* MeshFactory::createUnitPyramidDown()
    {
        return createUnitPyramid(5);
    }


    Mesh* MeshFactory::createUnitPyramid(int direction)
    {
        std::vector<Vertex> verts;
        std::vector<unsigned short> indices;
        glm::vec3 top(0.f, 0.f, 0.f);

        verts.push_back(Vertex(top));

        switch (direction)
        {
        case 0: // BACKWARD (-Z)
            verts.push_back(Vertex(glm::vec3(1.f, 1.f, -1.f)));
            verts.push_back(Vertex(glm::vec3(1.f, -1.f, -1.f)));
            verts.push_back(Vertex(glm::vec3(-1.f, -1.f, -1.f)));
            verts.push_back(Vertex(glm::vec3(-1.f, 1.f, -1.f)));
            break;
        case 1: // FORWARD (+Z)
            verts.push_back(Vertex(glm::vec3(-1.f, 1.f, 1.f)));
            verts.push_back(Vertex(glm::vec3(-1.f, -1.f, 1.f)));
            verts.push_back(Vertex(glm::vec3(1.f, -1.f, 1.f)));
            verts.push_back(Vertex(glm::vec3(1.f, 1.f, 1.f)));
            break;
        case 2: // LEFT (+X)
            verts.push_back(Vertex(glm::vec3(1.f, 1.f, 1.f)));
            verts.push_back(Vertex(glm::vec3(1.f, -1.f, 1.f)));
            verts.push_back(Vertex(glm::vec3(1.f, -1.f, -1.f)));
            verts.push_back(Vertex(glm::vec3(1.f, 1.f, -1.f)));
            break;
        case 3: // RIGHT (-X)
            verts.push_back(Vertex(glm::vec3(-1.f, 1.f, -1.f)));
            verts.push_back(Vertex(glm::vec3(-1.f, -1.f, -1.f)));
            verts.push_back(Vertex(glm::vec3(-1.f, -1.f, 1.f)));
            verts.push_back(Vertex(glm::vec3(-1.f, 1.f, 1.f)));
            break;
        case 4: // UP (+Y)
            verts.push_back(Vertex(glm::vec3(1.f, 1.f, -1.f)));
            verts.push_back(Vertex(glm::vec3(1.f, 1.f, 1.f)));
            verts.push_back(Vertex(glm::vec3(-1.f, 1.f, 1.f)));
            verts.push_back(Vertex(glm::vec3(-1.f, 1.f, -1.f)));
            break;
        case 5: // DOWN (-Y)
            verts.push_back(Vertex(glm::vec3(-1.f, -1.f, -1.f)));
            verts.push_back(Vertex(glm::vec3(-1.f, -1.f, 1.f)));
            verts.push_back(Vertex(glm::vec3(1.f, -1.f, 1.f)));
            verts.push_back(Vertex(glm::vec3(1.f, -1.f, -1.f)));
            break;
        }
        

        // Sides
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);
        indices.push_back(0);
        indices.push_back(2);
        indices.push_back(3);
        indices.push_back(0);
        indices.push_back(3);
        indices.push_back(4);
        indices.push_back(0);
        indices.push_back(4);
        indices.push_back(1);

        // Cap
        indices.push_back(1);
        indices.push_back(3);
        indices.push_back(2);
        indices.push_back(3);
        indices.push_back(1);
        indices.push_back(4);

        return new Mesh(verts, indices);
    }

    Mesh* MeshFactory::createSpotLightCone()
    {
        std::vector<Vertex> verts;
        std::vector<unsigned short> indices;
        int sides = 20;
        float sect = 360.f / sides;

        glm::vec3 top(0.f, 0.f, 0.f);
        glm::vec3 bottom(0.f, -1.f, 0.f);

        std::vector<glm::vec3> base;
        float angle = 0.f;
        for (int i = 0; i < sides; ++i)
        {
            float rad = glm::radians(angle);
            float x = glm::cos(rad);
            float z = glm::sin(rad);
            base.push_back(glm::vec3(x, -1.f, z));
            angle += sect;
        }

        verts.push_back(Vertex(top));
        verts.push_back(Vertex(bottom));
        for (unsigned int i = 0; i < base.size(); ++i)
        {
            verts.push_back(Vertex(base[i]));
        }

        // Sides
        for (unsigned short i = 2; i < sides; ++i)
        {
            if (i < sides - 1)
            {
                indices.push_back(0);
                indices.push_back(i);
                indices.push_back(i + 1);
            }
            else
            {
                indices.push_back(0);
                indices.push_back(i);
                indices.push_back(2);
            }
        }

        // Bottom
        for (unsigned short i = 2; i < sides; ++i)
        {
            if (i < sides - 1)
            {
                indices.push_back(1);
                indices.push_back(i + 1);
                indices.push_back(i);
            }
            else
            {
                indices.push_back(1);
                indices.push_back(2);
                indices.push_back(i);
            }
        }

        return new Mesh(verts, indices);
    }

    Mesh* MeshFactory::createUnitSphere(int level)
    {
        assert(level >= 0);

        std::vector<Vertex> verts;
        std::vector<unsigned short> indices;
        std::map<int, unsigned short> middlePointIndexCache;
        unsigned short index = 0;

        auto addVertex = [&](glm::vec3 pos)
        {
            float len = glm::sqrt(pos.x * pos.x + pos.y * pos.y + pos.z * pos.z);
            glm::vec3 newPos(pos.x / len, pos.y / len, pos.z / len);
            verts.push_back(Vertex(newPos));
            return index++;
        };

        auto getMiddlePoint = [&](unsigned short p1, unsigned short p2)
        {
            unsigned short smallerIndex = p1 < p2 ? p1 : p2;
            unsigned short greaterIndex = p1 < p2 ? p2 : p1;
            int key = (smallerIndex << 31) + greaterIndex;

            if (middlePointIndexCache.find(key) != middlePointIndexCache.end())
            {
                return middlePointIndexCache[key];
            }

            glm::vec3 point1 = verts[p1].m_pos;
            glm::vec3 point2 = verts[p2].m_pos;
            glm::vec3 middle = (point1 + point2) / 2.f;

            unsigned short i = addVertex(middle);
            middlePointIndexCache[key] = i;
            return i;
        };

        float t = (1.f + glm::sqrt(5.f)) / 2.f;
        addVertex(glm::vec3(-1.f, t, 0.f));
        addVertex(glm::vec3(1.f, t, 0.f));
        addVertex(glm::vec3(-1.f, -t, 0.f));
        addVertex(glm::vec3(1.f, -t, 0.f));

        addVertex(glm::vec3(0.f, -1, t));
        addVertex(glm::vec3(0.f, 1, t));
        addVertex(glm::vec3(0.f, -1, -t));
        addVertex(glm::vec3(0.f, 1, -t));

        addVertex(glm::vec3(t, 0.f, -1.f));
        addVertex(glm::vec3(t, 0.f, 1.f));
        addVertex(glm::vec3(-t, 0.f, -1.f));
        addVertex(glm::vec3(-t, 0.f, 1.f));

        indices.push_back(0); indices.push_back(11); indices.push_back(5);
        indices.push_back(0); indices.push_back(5); indices.push_back(1);
        indices.push_back(0); indices.push_back(1); indices.push_back(7);
        indices.push_back(0); indices.push_back(7); indices.push_back(10);
        indices.push_back(0); indices.push_back(10); indices.push_back(11);

        indices.push_back(1); indices.push_back(5); indices.push_back(9);
        indices.push_back(5); indices.push_back(11); indices.push_back(4);
        indices.push_back(11); indices.push_back(10); indices.push_back(2);
        indices.push_back(10); indices.push_back(7); indices.push_back(6);
        indices.push_back(7); indices.push_back(1); indices.push_back(8);

        indices.push_back(3); indices.push_back(9); indices.push_back(4);
        indices.push_back(3); indices.push_back(4); indices.push_back(2);
        indices.push_back(3); indices.push_back(2); indices.push_back(6);
        indices.push_back(3); indices.push_back(6); indices.push_back(8);
        indices.push_back(3); indices.push_back(8); indices.push_back(9);

        indices.push_back(4); indices.push_back(9); indices.push_back(5);
        indices.push_back(2); indices.push_back(4); indices.push_back(11);
        indices.push_back(6); indices.push_back(2); indices.push_back(10);
        indices.push_back(8); indices.push_back(6); indices.push_back(7);
        indices.push_back(9); indices.push_back(8); indices.push_back(1);

        for (int i = 0; i < level; ++i)
        {
            std::vector<unsigned short> indices2;
            for (unsigned int index = 0; index < indices.size(); index += 3)
            {
                unsigned short v1 = indices[index];
                unsigned short v2 = indices[index + 1];
                unsigned short v3 = indices[index + 2];
                unsigned short a = getMiddlePoint(v1, v2);
                unsigned short b = getMiddlePoint(v2, v3);
                unsigned short c = getMiddlePoint(v3, v1);

                indices2.push_back(v1); indices2.push_back(a); indices2.push_back(c);
                indices2.push_back(v2); indices2.push_back(b); indices2.push_back(a);
                indices2.push_back(v3); indices2.push_back(c); indices2.push_back(b);
                indices2.push_back(a); indices2.push_back(b); indices2.push_back(c);
            }

            indices = indices2;
        }

        Mesh* mesh = new Mesh(verts, indices);
        return mesh;
    }

    Mesh* MeshFactory::createCube(float width, float height, float depth)
    {
        float w = width / 2.f;
        float h = height / 2.f;
        float d = depth / 2.f;

        std::vector<Vertex> vertices;
        std::vector<unsigned short> indices;

        // Top
        vertices.emplace_back(-w, h, -d, 0.f, 1.f, 0.f, 1.f, 0.f);
        vertices.emplace_back(-w, h,  d, 0.f, 0.f, 0.f, 1.f, 0.f);
        vertices.emplace_back( w, h,  d, 1.f, 0.f, 0.f, 1.f, 0.f);
        vertices.emplace_back( w, h, -d, 1.f, 1.f, 0.f, 1.f, 0.f);

        // Front
        vertices.emplace_back(-w,  h, d, 0.f, 1.f, 0.f, 0.f, 1.f);
        vertices.emplace_back(-w, -h, d, 0.f, 0.f, 0.f, 0.f, 1.f);
        vertices.emplace_back( w, -h, d, 1.f, 0.f, 0.f, 0.f, 1.f);
        vertices.emplace_back( w,  h, d, 1.f, 1.f, 0.f, 0.f, 1.f);

        // Bottom
        

        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);
        indices.push_back(2);
        indices.push_back(3);
        indices.push_back(0);

        indices.push_back(4);
        indices.push_back(5);
        indices.push_back(6);
        indices.push_back(6);
        indices.push_back(7);
        indices.push_back(4);

        return new Mesh(vertices, indices);
    }

    Mesh* MeshFactory::createSolidRectangleHull(float width, float height)
    {
        float w = width / 2.f;
        float h = height / 2.f;

        std::vector<Vertex> vertices;
        std::vector<unsigned short> indices;

        // Right
        vertices.emplace_back( w, 0.f, -h, 1.f, 1.f, 1.f, 0.f, 0.f);
        vertices.emplace_back( w, 0.f,  h, 1.f, 1.f, 1.f, 0.f, 0.f);

        // Down
        vertices.emplace_back( w, 0.f,  h, 1.f, 1.f, 0.f, 0.f, 1.f);
        vertices.emplace_back(-w, 0.f,  h, 1.f, 1.f, 0.f, 0.f, 1.f);

        // Left
        vertices.emplace_back(-w, 0.f,  h, 1.f, 1.f, -1.f, 0.f, 0.f);
        vertices.emplace_back(-w, 0.f, -h, 1.f, 1.f, -1.f, 0.f, 0.f);

        // Up
        vertices.emplace_back(-w, 0.f, -h, 1.f, 1.f, 0.f, 0.f, -1.f);
        vertices.emplace_back( w, 0.f, -h, 1.f, 1.f, 0.f, 0.f, -1.f);

        // Right inv.
        vertices.emplace_back(w, 0.f, -h, 1.f, 1.f, -1.f, 0.f, 0.f);
        vertices.emplace_back(w, 0.f, h, 1.f, 1.f, -1.f, 0.f, 0.f);

        // Down inv.
        vertices.emplace_back(w, 0.f, h, 1.f, 1.f, 0.f, 0.f, -1.f);
        vertices.emplace_back(-w, 0.f, h, 1.f, 1.f, 0.f, 0.f, -1.f);

        // Left inv.
        vertices.emplace_back(-w, 0.f, h, 1.f, 1.f, 1.f, 0.f, 0.f);
        vertices.emplace_back(-w, 0.f, -h, 1.f, 1.f, 1.f, 0.f, 0.f);

        // Up inv.
        vertices.emplace_back(-w, 0.f, -h, 1.f, 1.f, 0.f, 0.f, 1.f);
        vertices.emplace_back(w, 0.f, -h, 1.f, 1.f, 0.f, 0.f, 1.f);

        // Right face
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(8);
        indices.push_back(8);
        indices.push_back(1);
        indices.push_back(9);

        // Down face
        indices.push_back(2);
        indices.push_back(3);
        indices.push_back(10);
        indices.push_back(10);
        indices.push_back(3);
        indices.push_back(11);

        // Left face
        indices.push_back(4);
        indices.push_back(5);
        indices.push_back(12);
        indices.push_back(12);
        indices.push_back(5);
        indices.push_back(13);

        // Top face
        indices.push_back(6);
        indices.push_back(7);
        indices.push_back(14);
        indices.push_back(14);
        indices.push_back(7);
        indices.push_back(15);

        return new Mesh(vertices, indices);
    }

    Mesh* MeshFactory::createTransparentRectangleHull(float width, float height)
    {
        std::vector<Vertex> vertices;
        std::vector<unsigned short> indices;

        float w = width / 2.f;
        float h = height / 2.f;

        float y = 0.01f;
        float y2 = 16.f / glm::cos((glm::pi<float>() / 6.f));

        // *** "BOTTOM" face ***
        // Right
        vertices.emplace_back(w, y, -h, 0.f, 0.f, 1.f, 0.f, 0.f);
        vertices.emplace_back(w, y,  h, 0.f, 0.f, 1.f, 0.f, 0.f);

        // Bottom
        vertices.emplace_back(w, y,  h, 0.f, 0.f, 0.f, 0.f, 1.f);
        vertices.emplace_back(-w, y, h, 0.f, 0.f, 0.f, 0.f, 1.f);

        // Left
        vertices.emplace_back(-w, y, h, 0.f, 0.f, -1.f, 0.f, 0.f);
        vertices.emplace_back(-w, y, -h, 0.f, 0.f, -1.f, 0.f, 0.f);

        // Top
        vertices.emplace_back(-w, y, -h, 0.f, 0.f, 0.f, 0.f, -1.f);
        vertices.emplace_back(w, y, -h, 0.f, 0.f, 0.f, 0.f, -1.f);

        // *** "RIGHT FACE" ***
        // Back edge
        vertices.emplace_back(w, y2, -h, 0.f, 0.f, 0.f, 0.f, -1.f);
        vertices.emplace_back(w, y, -h, 0.f, 0.f, 0.f, 0.f, -1.f);
        // Down edge
        vertices.emplace_back(w, y, -h, 0.f, 0.f, 0.f, -1.f, 0.f);
        vertices.emplace_back(w, y,  h, 0.f, 0.f, 0.f, -1.f, 0.f);
        // Front edge
        vertices.emplace_back(w, y, h, 0.f, 0.f, 0.f, 0.f, 1.f);
        vertices.emplace_back(w, y2, h, 0.f, 0.f, 0.f, 0.f, 1.f);
        // Top edge
        vertices.emplace_back(w, y2, h, 0.f, 0.f, 0.f, 1.f, 0.f);
        vertices.emplace_back(w, y2, -h, 0.f, 0.f, 0.f, 1.f, 0.f);

        // *** "LEFT FACE" ***
        // Back edge
        vertices.emplace_back(-w, y2, -h, 0.f, 0.f, 0.f, 0.f, -1.f);
        vertices.emplace_back(-w, y, -h, 0.f, 0.f, 0.f, 0.f, -1.f);
        // Down edge
        vertices.emplace_back(-w, y, -h, 0.f, 0.f, 0.f, -1.f, 0.f);
        vertices.emplace_back(-w, y, h, 0.f, 0.f, 0.f, -1.f, 0.f);
        // Front edge
        vertices.emplace_back(-w, y, h, 0.f, 0.f, 0.f, 0.f, 1.f);
        vertices.emplace_back(-w, y2, h, 0.f, 0.f, 0.f, 0.f, 1.f);
        // Top edge
        vertices.emplace_back(-w, y2, h, 0.f, 0.f, 0.f, 1.f, 0.f);
        vertices.emplace_back(-w, y2, -h, 0.f, 0.f, 0.f, 1.f, 0.f);

        for (unsigned short i = 0; i < 6; ++i)
        {
            indices.push_back(i + 1);
            indices.push_back(0);
            indices.push_back(i + 2);
        }

        for (unsigned short i = 8; i < 14; ++i)
        {
            indices.push_back(8);
            indices.push_back(i + 1);
            indices.push_back(i + 2);
        }

        for (unsigned short i = 16; i < 22; ++i)
        {
            indices.push_back(i + 1);
            indices.push_back(8);
            indices.push_back(i + 2);
        }

        return new Mesh(vertices, indices);
    }


}