#pragma once

#include "3d/Mesh.h"

namespace Tremor
{
    class MeshFactory
    {
    public:
        static Mesh* createQuadXY(float width, float height);
        static Mesh* createQuadXZ(float width, float height);
        static Mesh* createSpotLightCone();
        static Mesh* createUnitSphere(int level);

        static Mesh* createCube(float width, float height, float depth);

        static Mesh* createSolidRectangleHull(float width, float height);
        static Mesh* createTransparentRectangleHull(float width, float height);

        static Mesh* createUnitPyramidBackward();
        static Mesh* createUnitPyramidForward();
        static Mesh* createUnitPyramidRight();
        static Mesh* createUnitPyramidLeft();
        static Mesh* createUnitPyramidUp();
        static Mesh* createUnitPyramidDown();
        static Mesh* createUnitPyramid(int direction);
    private:
    };
}