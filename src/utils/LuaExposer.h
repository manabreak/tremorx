#pragma once

namespace Tremor
{
    class LuaScriptSystem;

    class LuaExposer
    {
    public:
        LuaExposer(lua_State* L);
        LuaExposer(LuaScriptSystem& system);
        ~LuaExposer();

    private:
        lua_State* L;
        void exposeApplication();
        void exposeAssetManager();
        void exposeInput();
        void exposeUtilities();
        void exposePhysicsBody();
        void exposeModel();
        void exposeSprite();
        void exposeText2D();
        void exposeBitmapFont();
        void exposeSpriteAnimation();
        void exposeTransform();
        void exposeGLM();
        void exposeTECS();
        void exposeTiledMap();
        void exposeCamera();
        void exposeLights();
    };
}