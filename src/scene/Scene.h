#pragma once

#include <string>
#include <World.h>

namespace Tremor
{
    class Scene
    {
    public:
        Scene();
        ~Scene();

        const std::string& getName();
        void setName(const std::string& name);

        Tecs::World& getWorld();
    private:
        std::string m_name;
        Tecs::World m_world;
    };
}