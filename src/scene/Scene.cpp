#include "stdafx.h"
#include "Scene.h"

namespace Tremor
{
    Scene::Scene()
        : m_name(""),
        m_world()
    {

    }

    Scene::~Scene()
    {

    }

    const std::string& Scene::getName()
    {
        return m_name;
    }

    void Scene::setName(const std::string& name)
    {
        m_name = name;
    }

    Tecs::World& Scene::getWorld()
    {
        return m_world;
    }
}