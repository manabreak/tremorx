#pragma once
#include <Component.h>
#include <glm/glm.hpp>
#include <vector>
#include "utils/MeshFactory.h"

namespace Tremor
{
    struct PizzaHullPoint
    {
    public:
        PizzaHullPoint(const glm::vec2& position, const glm::vec2& normal)
            : m_position(position),
            m_normal(normal)
        {
        }

        PizzaHullPoint(float posX, float posY, float norX, float norY)
            : m_position(posX, posY),
            m_normal(norX, norY)
        {

        }

        glm::vec2 m_position;
        glm::vec2 m_normal;
    };

    class PizzaHull : public Tecs::Component
    {
    public:

        

        ~PizzaHull()
        {

        }

        void setPosition(float x, float y)
        {
            m_position.x = x;
            m_position.y = y;
        }

        const glm::vec2& getPosition()
        {
            return m_position;
        }

        Mesh& getMesh()
        {
            return *m_mesh;
        }

    protected:
        PizzaHull()
            : m_position(0.f, 0.f),
            m_angle(0.f),
            m_maxRadius(0.f),
            m_visible(true),
            m_scale(1.f, 1.f),
            m_opacity(1.f)
        {

        }

        glm::vec2 m_position;
        float m_angle;
        float m_maxRadius;
        std::unique_ptr<Mesh> m_mesh;
        bool m_visible;
        glm::vec2 m_scale;
        float m_opacity;
    };

    class RectanglePizzaHull : public PizzaHull
    {
    public:
        RectanglePizzaHull(float width, float height)
        {
            float d = 16.f;

            std::vector<Vertex> verts;
            std::vector<unsigned short> indices;

            m_mesh = std::unique_ptr<Mesh>(MeshFactory::createTransparentRectangleHull(width, height));

            /*
            // Left face
            verts.emplace_back(-w, -h,  d, 0.f, 0.f, -1.f, 0.f, 0.f);
            verts.emplace_back(-w, -h, -d, 0.f, 0.f, -1.f, 0.f, 0.f);
            verts.emplace_back(-w,  h, -d, 0.f, 0.f, -1.f, 0.f, 0.f);
            verts.emplace_back(-w,  h,  d, 0.f, 0.f, -1.f, 0.f, 0.f);

            // Right face
            verts.emplace_back(w,  h,  d, 0.f, 0.f, 1.f, 0.f, 0.f);
            verts.emplace_back(w,  h, -d, 0.f, 0.f, 1.f, 0.f, 0.f);
            verts.emplace_back(w, -h, -d, 0.f, 0.f, 1.f, 0.f, 0.f);
            verts.emplace_back(w, -h,  d, 0.f, 0.f, 1.f, 0.f, 0.f);

            // Back face
            verts.emplace_back(w, -h,  d, 0.f, 0.f, 0.f, 0.f, -1.f);
            verts.emplace_back(w, -h, -d, 0.f, 0.f, 0.f, 0.f, -1.f);
            verts.emplace_back(-w, -h, -d, 0.f, 0.f, 0.f, 0.f, -1.f);
            verts.emplace_back(-w, -h,  d, 0.f, 0.f, 0.f, 0.f, -1.f);

            // Front face
            verts.emplace_back(-w, h, d, 0.f, 0.f, 0.f, 0.f, 1.f);
            verts.emplace_back(-w, h, -d, 0.f, 0.f, 0.f, 0.f, 1.f);
            verts.emplace_back(w, h, -d, 0.f, 0.f, 0.f, 0.f, 1.f);
            verts.emplace_back(w, h, d, 0.f, 0.f, 0.f, 0.f, 1.f);

            // Top face
            verts.emplace_back(-w, -h, d, 0.f, 0.f, 0.f, 1.f, 0.f);
            verts.emplace_back(-w, h, d, 0.f, 0.f, 0.f, 1.f, 0.f);
            verts.emplace_back(w, h, d, 0.f, 0.f, 0.f, 1.f, 0.f);
            verts.emplace_back(w, -h, d, 0.f, 0.f, 0.f, 1.f, 0.f);

            // Bottom face
            verts.emplace_back(w, -h, -d, 0.f, 0.f, 0.f, -1.f, 0.f);
            verts.emplace_back(w, h, -d, 0.f, 0.f, 0.f, -1.f, 0.f);
            verts.emplace_back(-w, h, -d, 0.f, 0.f, 0.f, -1.f, 0.f);
            verts.emplace_back(-w, -h, -d, 0.f, 0.f, 0.f, -1.f, 0.f);

            indices.push_back(0);
            indices.push_back(1);
            indices.push_back(2);
            indices.push_back(2);
            indices.push_back(3);
            indices.push_back(0);

            indices.push_back(4);
            indices.push_back(5);
            indices.push_back(6);
            indices.push_back(6);
            indices.push_back(7);
            indices.push_back(4);

            indices.push_back(8);
            indices.push_back(9);
            indices.push_back(10);
            indices.push_back(10);
            indices.push_back(11);
            indices.push_back(8);

            indices.push_back(12);
            indices.push_back(13);
            indices.push_back(14);
            indices.push_back(14);
            indices.push_back(15);
            indices.push_back(12);

            indices.push_back(16);
            indices.push_back(17);
            indices.push_back(18);
            indices.push_back(18);
            indices.push_back(19);
            indices.push_back(16);

            indices.push_back(20);
            indices.push_back(21);
            indices.push_back(22);
            indices.push_back(22);
            indices.push_back(23);
            indices.push_back(20);
            */
            // m_mesh = Mesh(verts, indices);
        }
    };
}