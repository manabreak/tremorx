#include "stdafx.h"
#include "SpriteAnimation.h"

namespace Tremor
{
    SpriteAnimation::SpriteAnimation()
        : m_playing(false),
        m_paused(false),
        m_take(0),
        m_frame(0),
        m_timer(0.f),
        m_loop(false),
        m_dirty(true)
    {
        
    }

    SpriteAnimation::~SpriteAnimation()
    {

    }

    /*
    unsigned int SpriteAnimation::addTake(int startRow, int startCol, int frameWidth, int frameHeight, int frames, float frameSpeed)
    {
        unsigned int id = m_takes.size();
        m_takes.emplace_back(id, startRow, startCol, frameWidth, frameHeight, frames, frameSpeed);
        return id;
    }
    */

    unsigned int SpriteAnimation::addTake(int x, int y, int frameWidth, int frameHeight, int frames, float frameSpeed)
    {
        unsigned int id = m_takes.size();
        m_takes.emplace_back(id, x, y, frameWidth, frameHeight, frames, frameSpeed);
        return id;
    }

    void SpriteAnimation::removeTake(unsigned int id)
    {
        if (id < m_takes.size())
        {
            std::swap(m_takes[id], m_takes.back());
            m_takes.pop_back();
        }
    }

    std::vector<SpriteAnimation::Take>& SpriteAnimation::getTakes()
    {
        return m_takes;
    }

    unsigned int SpriteAnimation::getCurrentTake()
    {
        return m_take;
    }

    bool SpriteAnimation::isPlaying()
    {
        return m_playing;
    }

    bool SpriteAnimation::isPaused()
    {
        return m_paused;
    }

    void SpriteAnimation::play(unsigned int take, bool loop)
    {
        assert(take < m_takes.size(), "Tried to play a take that does not exist!");

        if (take == m_take && m_paused)
        {
            m_paused = false;
        }
        else
        {
            m_take = take;
            m_loop = loop;
            m_timer = 0.f;
            m_playing = true;
            m_paused = false;
            m_dirty = true;
            m_frame = 0;
        }
    }

    void SpriteAnimation::pause()
    {
        m_paused = true;
    }

    void SpriteAnimation::stop(unsigned int stopFrame)
    {
        m_playing = false;
        m_paused = false;
        m_dirty = true;
        m_frame = 0;
    }

    bool SpriteAnimation::isLooping()
    {
        return m_loop;
    }

    void SpriteAnimation::setLooping(bool loop)
    {
        m_loop = loop;
    }

    unsigned int SpriteAnimation::getCurrentFrame()
    {
        return m_frame;
    }

    void SpriteAnimation::setFrame(unsigned int take, unsigned int frame)
    {
        assert(take < m_takes.size(), "Tried to set a take that does not exist!");
        m_take = take;

        m_frame = frame;
        if (m_frame >= m_takes[m_take].getFrameCount())
        {
            m_frame = m_takes[m_take].getFrameCount() - 1;
        }

        m_dirty = true;
    }

    void SpriteAnimation::nextFrame()
    {
        m_frame++;
        if (m_frame >= m_takes[m_take].getFrameCount())
        {
            if (m_loop)
            {
                m_frame = 0;
            }
            else
            {
                m_frame--;
            }
        }
    }

    bool SpriteAnimation::update(float delta)
    {
        if (m_playing && !m_paused)
        {
            m_timer += delta;
            float t = m_takes[m_take].getFramespeed(m_frame);
            if (m_timer >= t)
            {
                m_timer -= t;
                m_frame++;
                if (m_frame >= m_takes[m_take].getFrameCount())
                {
                    if (m_loop)
                    {
                        m_frame = 0;
                    }
                    else
                    {
                        m_frame--;
                        m_playing = false;
                        m_paused = false;
                    }
                }
                
                return true;
            }
        }

        if (m_dirty)
        {
            m_dirty = false;
            return true;
        }

        return false;
    }
}