#pragma once

#include <Component.h>
#include <vector>

namespace Tremor
{
    class Transform : public Tecs::Component
    {
    public:
        Transform();
        ~Transform();

        void addChild(Transform* child);
        void remove();

        Transform* getParent();
        bool hasParent();

        const std::vector<Transform*>& getChildren();

        // Position
        void setPositionXYZ(float x, float y, float z);
        void setPosition(const glm::vec3& position);
        const glm::vec3& getPosition() const;
        float getX();
        float getY();
        float getZ();

        void setX(float x);
        void setY(float y);
        void setZ(float z);

        glm::vec3 getWorldPosition();
        float getWorldX();
        float getWorldY();
        float getWorldZ();
        glm::quat getWorldRotation();

        void translateXYZ(float x, float y, float z);
        void translate(const glm::vec3& amount);

        // Rotation
        void setRotation(float x, float y, float z, float w);
        void setRotation(const glm::quat& rotation);
        void setEulerAnglesXYZ(float x, float y, float z);
        void setEulerAngles(const glm::vec3& euler);
        const glm::quat& getRotation();
        const glm::vec3& getEulerAngles();
        float getEulerX();
        float getEulerY();
        float getEulerZ();

        void rotate(const glm::quat& rotation);
        void rotate(const glm::mat4& rotation);
        void rotateEulerXYZ(float x, float y, float z);
        void rotateEuler(const glm::vec3& euler);

        // Scale
        void setUniformScale(float xyz);
        void setScaleXYZ(float x, float y, float z);
        void setScale(const glm::vec3& scale);
        const glm::vec3& getScale();

        float getScaleX();
        float getScaleY();
        float getScaleZ();

        void scaleUniform(float amount);
        void scaleXYZ(float x, float y, float z);
        void scale(const glm::vec3& scale);

        // Transform
        const glm::mat4& getWorldTransform();
        const glm::mat4& getLocalTransform();

        bool isDirty();
        void setDirty();
    private:
        Transform* m_parent;
        std::vector<Transform*> m_children;

        bool m_dirty;
        glm::vec3 m_position;
        glm::quat m_rotation;
        glm::vec3 m_eulerAngles;
        glm::vec3 m_scale;
        glm::mat4 m_localTransform;
        glm::mat4 m_worldTransform;

        void calculateLocalTransform();
    };
}