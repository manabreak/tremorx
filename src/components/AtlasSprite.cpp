#include "stdafx.h"
#include "AtlasSprite.h"

namespace Tremor
{
    AtlasSprite::AtlasSprite(AtlasRegion& region)
        : Sprite(region),
        m_region(region),
        m_originalOffsetX(region.getOffsetX()),
        m_originalOffsetY(region.getOffsetY())
    {
        
    }

    AtlasSprite::~AtlasSprite()
    {

    }

    void AtlasSprite::setSize(float width, float height)
    {
        float rw = width / m_region.getOriginalWidth();
        float rh = height / m_region.getOriginalHeight();
        m_region.setOffsetX(m_originalOffsetX * rw);
        m_region.setOffsetY(m_originalOffsetY * rh);
        int pw = m_region.shouldRotate() ? m_region.getPackedHeight() : m_region.getPackedWidth();
        int ph = m_region.shouldRotate() ? m_region.getPackedWidth() : m_region.getPackedHeight();
        Sprite::setSize(pw * rw, ph * rh);
    }

    float AtlasSprite::getWidthRatio()
    {
        return Sprite::getWidth() / m_region.getRotatedPackedWidth();
    }

    float AtlasSprite::getHeightRatio()
    {
        return Sprite::getHeight() / m_region.getRotatedPackedHeight();
    }

    AtlasRegion& AtlasSprite::getAtlasRegion()
    {
        return m_region;
    }

    void AtlasSprite::flip(bool x, bool y)
    {
        Sprite::flip(x, y);

        
    }
}