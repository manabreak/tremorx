#pragma once

#include <Component.h>
#include "Transform.h"
#include "2d/TextureRegion.h"
#include "3d/Vertex.h"

namespace Tremor
{
    class Sprite : public Tecs::Component
    {
    public:
        Sprite(Texture& texture);
        Sprite(Texture& texture, int width, int height);
        Sprite(Texture& texture, int x, int y, int width, int height);
        Sprite(TextureRegion& region);
        Sprite(TextureRegion& region, int x, int y, int width, int height);
        // Sprite(const Sprite& other);
        ~Sprite();

        void set(const Sprite& other);

        // Position - ignored when used with SpriteSystem
        void setPosition(float x, float y);
        void setX(float x);
        void setY(float y);
        float getX();
        float getY();

        // Size
        void setSize(float width, float height);
        // void setSize(const glm::vec2& size);
        const glm::vec2& getSize();
        float getWidth();
        float getHeight();

        // Rotation
        void setRotationX(float degrees);

        // Color
        void setAlpha(float a);
        float getAlpha();
        void setColor(float rgba);
        void setColorRGB(float r, float g, float b);
        void setColorRGBA(float r, float g, float b, float a);
        float getColorR();
        float getColorG();
        float getColorB();
        glm::vec3 getColorRGB();
        const glm::vec4& getColorRGBA();

        // Origin
        void setOrigin(float x, float y);
        // void setOrigin(const glm::vec2& origin);
        const glm::vec2& getOrigin();
        void setOriginX(float x);
        float getOriginX();
        void setOriginY(float y);
        float getOriginY();

        // Vertices
        const std::vector<Vertex>& getVertices();
        const std::vector<Vertex>& getTransformedVertices();
        void applyTransform(Transform& transform, bool round);

        // Region
        void setRegion(float u, float v, float u2, float v2);
        void setU(float u);
        void setV(float v);
        void setU2(float u2);
        void setV2(float v2);
        void setFlip(bool x, bool y);
        void flip(bool x, bool y);
        void scroll(float xAmount, float yAmount);

        void setSourceRectangle(int x, int y, int width, int height);

        void rotate90(bool clockwise);

        Texture& getTexture();
    protected:
        glm::vec2 m_position;
        glm::vec2 m_origin;
        glm::vec2 m_size;
        glm::vec4 m_color;
        TextureRegion m_region;
    private:
        void init(TextureRegion& region, int x, int y, int width, int height);

        std::vector<Vertex> m_vertices;
        std::vector<Vertex> m_transformedVertices;

        float m_rotationX;
    };
}