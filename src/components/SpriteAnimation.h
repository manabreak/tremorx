#pragma once

#include <Component.h>
#include <vector>
#include <stack>

namespace Tremor
{
    class SpriteAnimation : public Tecs::Component
    {
    public:
        class Take
        {
        public:
            Take(unsigned int id, int x, int y, int frameWidth, int frameHeight, int frames, float frameSpeed)
                : m_id(id),
                m_startX(x),
                m_startY(y),
                m_frameWidth(frameWidth),
                m_frameHeight(frameHeight),
                m_frameCount(frames),
                m_frameSpeeds(frames, frameSpeed)
            {

            }

            ~Take() {}

            unsigned int getID() const { return m_id; }

            void setFrameWidth(int width) { m_frameWidth = width; }
            void setFrameHeight(int height) { m_frameHeight = height; }
            void setFrameSize(int width, int height) { m_frameWidth = width; m_frameHeight = height; }
            int getFrameWidth() const { return m_frameWidth; }
            int getFrameHeight() const { return m_frameHeight; }

            void setFrameCount(int count)
            {
                m_frameCount = count;
                while (m_frameSpeeds.size() < m_frameCount) m_frameSpeeds.push_back(0.1f);
                while (m_frameSpeeds.size() > m_frameCount) m_frameSpeeds.pop_back();
            }
            int getFrameCount() const { return m_frameCount; }

            // void setStart(int row, int column) { m_startRow = row; m_startCol = column; }
            void setStart(int x, int y) { m_startX = x; m_startY = y; }
            // void setStartRow(int row) { m_startRow = row; }
            void setStartX(int x) { m_startX = x; }
            // void setStartColumn(int column) { m_startCol = column; }
            void setStartY(int y) { m_startY = y; }
            // int getStartRow() const { return m_startRow; }
            int getStartX() { return m_startX; }
            // int getStartColumn() const { return m_startCol; }
            int getStartY() { return m_startY; }

            void setConstantFramespeed(float speed)
            {
                m_frameSpeeds.clear();
                for (unsigned int i = 0; i < m_frameCount; ++i)
                {
                    m_frameSpeeds.push_back(speed);
                }
            }

            void setFramespeed(unsigned int frame, float speed)
            {
                if (frame < m_frameCount) return;
                m_frameSpeeds[frame] = speed;
            }

            void setFrameSpeed(float frame0)
            {
                if (m_frameCount > 0) m_frameSpeeds[0] = frame0;
            }

            void setFrameSpeed(float frame0, float frame1)
            {
                if (m_frameCount > 1)
                {
                    m_frameSpeeds[0] = frame0;
                    m_frameSpeeds[1] = frame1;
                }
                else setFrameSpeed(frame0);
            }

            void setFrameSpeed(float frame0, float frame1, float frame2)
            {
                if (m_frameCount > 2)
                {
                    m_frameSpeeds[0] = frame0;
                    m_frameSpeeds[1] = frame1;
                    m_frameSpeeds[2] = frame2;
                }
                else setFrameSpeed(frame0, frame1);
            }

            void setFrameSpeed(float frame0, float frame1, float frame2, float frame3)
            {
                if (m_frameCount > 3)
                {
                    m_frameSpeeds[0] = frame0;
                    m_frameSpeeds[1] = frame1;
                    m_frameSpeeds[2] = frame2;
                    m_frameSpeeds[3] = frame3;
                }
                else setFrameSpeed(frame0, frame1, frame2);
            }

            void setFrameSpeed(float frame0, float frame1, float frame2, float frame3, float frame4)
            {
                if (m_frameCount > 4)
                {
                    m_frameSpeeds[0] = frame0;
                    m_frameSpeeds[1] = frame1;
                    m_frameSpeeds[2] = frame2;
                    m_frameSpeeds[3] = frame3;
                    m_frameSpeeds[4] = frame4;
                }
                else setFrameSpeed(frame0, frame1, frame2, frame3);
            }

            void setFrameSpeed(float frame0, float frame1, float frame2, float frame3, float frame4, float frame5)
            {
                if (m_frameCount > 5)
                {
                    m_frameSpeeds[0] = frame0;
                    m_frameSpeeds[1] = frame1;
                    m_frameSpeeds[2] = frame2;
                    m_frameSpeeds[3] = frame3;
                    m_frameSpeeds[4] = frame4;
                    m_frameSpeeds[5] = frame5;
                }
                else setFrameSpeed(frame0, frame1, frame2, frame3, frame4);
            }

            void setFrameSpeed(float frame0, float frame1, float frame2, float frame3, float frame4, float frame5, float frame6)
            {
                if (m_frameCount > 6)
                {
                    m_frameSpeeds[0] = frame0;
                    m_frameSpeeds[1] = frame1;
                    m_frameSpeeds[2] = frame2;
                    m_frameSpeeds[3] = frame3;
                    m_frameSpeeds[4] = frame4;
                    m_frameSpeeds[5] = frame5;
                    m_frameSpeeds[6] = frame6;
                }
                else setFrameSpeed(frame0, frame1, frame2, frame3, frame4, frame5);
            }

            void setFrameSpeed(float frame0, float frame1, float frame2, float frame3, float frame4, float frame5, float frame6, float frame7)
            {
                if (m_frameCount > 7)
                {
                    m_frameSpeeds[0] = frame0;
                    m_frameSpeeds[1] = frame1;
                    m_frameSpeeds[2] = frame2;
                    m_frameSpeeds[3] = frame3;
                    m_frameSpeeds[4] = frame4;
                    m_frameSpeeds[5] = frame5;
                    m_frameSpeeds[6] = frame6;
                    m_frameSpeeds[7] = frame7;
                }
                else setFrameSpeed(frame0, frame1, frame2, frame3, frame4, frame5, frame6);
            }

            float getFramespeed(unsigned int frame) const
            {
                if (frame < m_frameCount) return m_frameSpeeds[frame];
                return 0.f;
            }

            // Sets the total duration of the animation, stretching all
            // frame durations equally if needed
            void setTotalDuration(float totalDuration)
            {
                // TODO Not yet implemented.
            }

            // Sets the frame speeds to follow the sine curve, starting
            // slowly, accelerating to the max speed in the middle of the animtion
            // and then slowing again towards the end of the animation.
            void easeWithSine(float totalDuration)
            {
                // TODO Not yet implemented.
            }

        private:
            unsigned int m_id;
            int m_frameWidth;
            int m_frameHeight;
            unsigned int m_frameCount;
            // int m_startRow;
            // int m_startCol;
            int m_startX;
            int m_startY;
            std::vector<float> m_frameSpeeds;
        };

        SpriteAnimation();
        ~SpriteAnimation();

        // unsigned int addTake(int startRow, int startCol, int frameWidth, int frameHeight, int frames, float frameSpeed);
        unsigned int addTake(int x, int y, int frameWidth, int frameHeight, int frames, float frameSpeed);

        void removeTake(unsigned int id);

        std::vector<Take>& getTakes();
        unsigned int getCurrentTake();
        
        bool isPlaying();
        bool isPaused();
        
        void play(unsigned int take, bool loop = false);
        void pause();
        void stop(unsigned int stopframe = 0);

        bool isLooping();
        void setLooping(bool loop);

        unsigned int getCurrentFrame();
        void setFrame(unsigned int take, unsigned int frame);

        // Manually advance to the next frame
        void nextFrame();

        // Updates the animation.
        // Returns 'true' if the animation changed (frame change, animation completed etc.)
        bool update(float delta);
    private:
        unsigned int m_take;
        unsigned int m_frame;
        float m_timer;
        bool m_loop;

        std::vector<Take> m_takes;

        bool m_playing;
        bool m_paused;
        bool m_dirty;
    };
}