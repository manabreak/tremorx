#pragma once

#include "Sprite.h"
#include "2d/AtlasRegion.h"

namespace Tremor
{
    class AtlasSprite : public Sprite
    {
    public:
        AtlasSprite(AtlasRegion& region);
        ~AtlasSprite();
        void setSize(float width, float height);
        float getWidthRatio();
        float getHeightRatio();
        AtlasRegion& getAtlasRegion();
        void flip(bool x, bool y);
    private:
        AtlasRegion& m_region;
        float m_originalOffsetX;
        float m_originalOffsetY;
    };
}