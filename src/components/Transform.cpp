#include "stdafx.h"
#include "Transform.h"
#include <algorithm>

namespace Tremor
{

    Transform::Transform()
        : m_parent(nullptr),
        m_position(0.f, 0.f, 0.f),
        m_rotation(1.f, 0.f, 0.f, 0.f),
        m_scale(1.f, 1.f, 1.f),
        m_dirty(true)
    {

    }

    Transform::~Transform()
    {

    }

    void Transform::addChild(Transform* child)
    {
        child->remove();
        m_children.push_back(child);
        child->m_parent = this;
        child->setDirty();
    }

    void Transform::remove()
    {
        if (m_parent != nullptr)
        {
            m_parent->m_children.erase(std::remove(m_parent->m_children.begin(), m_parent->m_children.end(), this), m_parent->m_children.end());
            m_parent = nullptr;
            setDirty();
        }
    }

    Transform* Transform::getParent()
    {
        return m_parent;
    }

    bool Transform::hasParent()
    {
        return m_parent != nullptr;
    }

    const std::vector<Transform*>& Transform::getChildren()
    {
        return m_children;
    }

    void Transform::setPositionXYZ(float x, float y, float z)
    {
        m_position.x = x;
        m_position.y = y;
        m_position.z = z;
        setDirty();
    }

    void Transform::setPosition(const glm::vec3& position)
    {
        m_position = position;
        setDirty();
    }

    const glm::vec3& Transform::getPosition() const
    {
        return m_position;
    }

    float Transform::getX()
    {
        return m_position.x;
    }

    float Transform::getY()
    {
        return m_position.y;
    }

    float Transform::getZ()
    {
        return m_position.z;
    }

    void Transform::setX(float x)
    {
        m_position.x = x;
        setDirty();
    }

    void Transform::setY(float y)
    {
        m_position.y = y;
        setDirty();
    }

    void Transform::setZ(float z)
    {
        m_position.z = z;
        setDirty();
    }

    glm::vec3 Transform::getWorldPosition()
    {
        if (m_parent != nullptr)
        {
            return m_position + m_parent->getWorldPosition();
        }
        else
        {
            return m_position;
        }
    }

    float Transform::getWorldX()
    {
        return getWorldPosition().x;
    }

    float Transform::getWorldY()
    {
        return getWorldPosition().y;
    }

    float Transform::getWorldZ()
    {
        return getWorldPosition().z;
    }

    void Transform::translateXYZ(float x, float y, float z)
    {
        m_position.x += x;
        m_position.y += y;
        m_position.z += z;
        setDirty();
    }

    void Transform::translate(const glm::vec3& amount)
    {
        m_position += amount;
        setDirty();
    }

    void Transform::setRotation(float x, float y, float z, float w)
    {
        m_rotation.x = x;
        m_rotation.y = y;
        m_rotation.z = z;
        m_rotation.w = w;
        m_eulerAngles = glm::eulerAngles(m_rotation);
        setDirty();
    }

    void Transform::setRotation(const glm::quat& rotation)
    {
        m_rotation = rotation;
        m_eulerAngles = glm::eulerAngles(m_rotation);
        setDirty();
    }

    void Transform::setEulerAnglesXYZ(float x, float y, float z)
    {
        m_eulerAngles.x = glm::radians(x);
        m_eulerAngles.y = glm::radians(y);
        m_eulerAngles.z = glm::radians(z);
        m_rotation = glm::quat(m_eulerAngles);
        setDirty();
    }

    void Transform::setEulerAngles(const glm::vec3& euler)
    {
        m_eulerAngles = euler;
        m_rotation = glm::quat(m_eulerAngles);
        setDirty();
    }

    const glm::quat& Transform::getRotation()
    {
        return m_rotation;
    }

    glm::quat Transform::getWorldRotation()
    {
        if (m_parent)
        {
            return m_parent->getWorldRotation() * m_rotation;
        }
        else
        {
            return m_rotation;
        }
    }

    const glm::vec3& Transform::getEulerAngles()
    {
        return m_eulerAngles;
    }

    float Transform::getEulerX()
    {
        return m_eulerAngles.x;
    }

    float Transform::getEulerY()
    {
        return m_eulerAngles.y;
    }

    float Transform::getEulerZ()
    {
        return m_eulerAngles.z;
    }

    void Transform::rotate(const glm::quat& rotation)
    {
        m_rotation = rotation * m_rotation;
        setDirty();
    }

    void Transform::rotate(const glm::mat4& rotation)
    {
        m_rotation = glm::quat_cast(rotation) * m_rotation;
        m_eulerAngles = glm::eulerAngles(m_rotation);
        setDirty();
    }

    void Transform::rotateEulerXYZ(float x, float y, float z)
    {
        x = glm::radians(x);
        y = glm::radians(y);
        z = glm::radians(z);
        m_rotation = glm::quat(glm::vec3(x, y, z)) * m_rotation;
        m_eulerAngles = glm::eulerAngles(m_rotation);
        setDirty();
    }

    void Transform::rotateEuler(const glm::vec3& euler)
    {
        glm::vec3 degrees = glm::radians(euler);
        m_rotation = glm::quat(degrees) * m_rotation;
        m_eulerAngles = glm::eulerAngles(m_rotation);
        setDirty();
    }

    void Transform::setUniformScale(float xyz)
    {
        m_scale.x = xyz;
        m_scale.y = xyz;
        m_scale.z = xyz;
        setDirty();
    }

    void Transform::setScaleXYZ(float x, float y, float z)
    {
        m_scale.x = x;
        m_scale.y = y;
        m_scale.z = z;
        setDirty();
    }

    void Transform::setScale(const glm::vec3& scale)
    {
        m_scale = scale;
        setDirty();
    }

    const glm::vec3& Transform::getScale()
    {
        return m_scale;
    }

    float Transform::getScaleX()
    {
        return m_scale.x;
    }

    float Transform::getScaleY()
    {
        return m_scale.y;
    }

    float Transform::getScaleZ()
    {
        return m_scale.z;
    }

    void Transform::scaleUniform(float amount)
    {
        m_scale *= amount;
        setDirty();
    }

    void Transform::scaleXYZ(float x, float y, float z)
    {
        m_scale.x *= x;
        m_scale.y *= y;
        m_scale.z *= z;
        setDirty();
    }

    void Transform::scale(const glm::vec3& scale)
    {
        m_scale.x *= scale.x;
        m_scale.y *= scale.y;
        m_scale.z *= scale.z;
        setDirty();
    }

    const glm::mat4& Transform::getWorldTransform()
    {
        if (isDirty())
        {
            calculateLocalTransform();
            if (m_parent != nullptr)
            {
                m_worldTransform = m_parent->getWorldTransform() * m_localTransform;
            }
            else
            {
                m_worldTransform = m_localTransform;
            }
            m_dirty = false;
        }
        return m_worldTransform;
    }

    const glm::mat4& Transform::getLocalTransform()
    {
        if (m_dirty)
        {
            calculateLocalTransform();
            m_dirty = false;
        }
        return m_localTransform;
    }

    bool Transform::isDirty()
    {
        return m_dirty || (m_parent == nullptr ? false : m_parent->isDirty());
    }

    void Transform::setDirty()
    {
        m_dirty = true;
        for (unsigned int i = 0; i < m_children.size(); ++i)
        {
            m_children[i]->setDirty();
        }
    }

    void Transform::calculateLocalTransform()
    {
        m_localTransform = glm::translate(m_position) * glm::mat4_cast(m_rotation) * glm::scale(m_scale);
    }
}