#pragma once
#include <Component.h>
#include <functional>

namespace Tremor
{
    class Event : public Tecs::Component
    {
    public:
        Event(std::function<void()>& func);
        ~Event();

    private:
        std::function<void()>& m_func;
    };
}