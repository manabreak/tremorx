#pragma once
#include <Component.h>

extern "C"
{
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}
#include <LuaBridge/LuaBridge.h>

using namespace luabridge;

namespace Tremor
{
    class LuaUpdateScript : public Tecs::Component
    {
    public:
        LuaUpdateScript(LuaRef objRef, LuaRef updateFuncRef)
            : m_objRef(objRef),
            m_updateFuncRef(updateFuncRef)
        {

        }

        void update(float delta)
        {
            m_updateFuncRef(m_objRef, delta);
        }
    private:
        LuaRef m_objRef;
        LuaRef m_updateFuncRef;
    };
}