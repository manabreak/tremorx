#include "stdafx.h"
#include "Sprite.h"
#include <glm/gtx/rotate_vector.hpp>

namespace Tremor
{

    Sprite::Sprite(Texture& texture)
        : m_color(1.f, 1.f, 1.f, 1.f),
        m_vertices(4, Vertex()),
        m_transformedVertices(4, Vertex()),
        m_region(TextureRegion(texture)),
        m_rotationX(0.f)
    {
        init(m_region, 0, 0, texture.getWidth(), texture.getHeight());
    }

    Sprite::Sprite(Texture& texture, int width, int height)
        : m_color(1.f, 1.f, 1.f, 1.f),
        m_vertices(4, Vertex()),
        m_transformedVertices(4, Vertex()),
        m_region(TextureRegion(texture, width, height)),
        m_rotationX(0.f)
    {
        init(m_region, 0, 0, width, height);
    }

    Sprite::Sprite(Texture& texture, int x, int y, int width, int height)
        : m_color(1.f, 1.f, 1.f, 1.f),
        m_vertices(4, Vertex()),
        m_transformedVertices(4, Vertex()),
        m_region(TextureRegion(texture, x, y, width, height)),
        m_rotationX(0.f)
    {
        init(m_region, x, y, width, height);
    }

    Sprite::Sprite(TextureRegion& region)
        : m_color(1.f, 1.f, 1.f, 1.f),
        m_vertices(4, Vertex()),
        m_transformedVertices(4, Vertex()),
        m_region(region),
        m_rotationX(0.f)
    {
        init(m_region, 0, 0, m_region.getRegionWidth(), m_region.getRegionHeight());
    }

    Sprite::Sprite(TextureRegion& region, int x, int y, int width, int height)
        : m_color(1.f, 1.f, 1.f, 1.f),
        m_vertices(4, Vertex()),
        m_transformedVertices(4, Vertex()),
        m_region(region),
        m_rotationX(0.f)
    {
        init(m_region, x, y, width, height);
    }

    Sprite::~Sprite()
    {
        
    }

    void Sprite::setPosition(float x, float y)
    {
        m_position.x = x;
        m_position.y = y;

        m_vertices[0].m_pos.x = x - m_size.x / 2.f;
        m_vertices[0].m_pos.y = y + m_size.y / 2.f;

        m_vertices[1].m_pos.x = x - m_size.x / 2.f;
        m_vertices[1].m_pos.y = y - m_size.y / 2.f;

        m_vertices[2].m_pos.x = x + m_size.x / 2.f;
        m_vertices[2].m_pos.y = y - m_size.y / 2.f;

        m_vertices[3].m_pos.x = x + m_size.x / 2.f;
        m_vertices[3].m_pos.y = y + m_size.y / 2.f;

        m_transformedVertices[0].m_pos = m_vertices[0].m_pos;
        m_transformedVertices[1].m_pos = m_vertices[1].m_pos;
        m_transformedVertices[2].m_pos = m_vertices[2].m_pos;
        m_transformedVertices[3].m_pos = m_vertices[3].m_pos;
    }

    void Sprite::setX(float x)
    {
        m_position.x = x;
        m_vertices[0].m_pos.x = x - m_size.x / 2.f;
        m_vertices[1].m_pos.x = x - m_size.x / 2.f;
        m_vertices[2].m_pos.x = x + m_size.x / 2.f;
        m_vertices[3].m_pos.x = x + m_size.x / 2.f;
        m_transformedVertices[0].m_pos.x = m_vertices[0].m_pos.x;
        m_transformedVertices[1].m_pos.x = m_vertices[1].m_pos.x;
        m_transformedVertices[2].m_pos.x = m_vertices[2].m_pos.x;
        m_transformedVertices[3].m_pos.x = m_vertices[3].m_pos.x;
    }

    void Sprite::setY(float y)
    {
        m_position.y = y;
        m_vertices[0].m_pos.y = y + m_size.y / 2.f;
        m_vertices[1].m_pos.y = y - m_size.y / 2.f;
        m_vertices[2].m_pos.y = y - m_size.y / 2.f;
        m_vertices[3].m_pos.y = y + m_size.y / 2.f;
        m_transformedVertices[0].m_pos.y = m_vertices[0].m_pos.y;
        m_transformedVertices[1].m_pos.y = m_vertices[1].m_pos.y;
        m_transformedVertices[2].m_pos.y = m_vertices[2].m_pos.y;
        m_transformedVertices[3].m_pos.y = m_vertices[3].m_pos.y;
    }

    float Sprite::getX()
    {
        return m_position.x;
    }

    float Sprite::getY()
    {
        return m_position.y;
    }

    void Sprite::init(TextureRegion& region, int x, int y, int width, int height)
    {
        m_size = glm::abs(glm::vec2(width, height));
        m_origin = glm::vec2(m_size.x / 2.f, m_size.y / 2.f);
        setRegion(m_region.getU(), m_region.getV(), m_region.getU2(), m_region.getV2());

        float w = (float) width / 2.f;
        float h = (float) height / 2.f;
        m_vertices[0].m_pos.x = -w;
        m_vertices[0].m_pos.y = h;
        m_vertices[1].m_pos.x = -w;
        m_vertices[1].m_pos.y = -h;
        m_vertices[2].m_pos.x = w;
        m_vertices[2].m_pos.y = -h;
        m_vertices[3].m_pos.x = w;
        m_vertices[3].m_pos.y = h;

        // Vertex Nor,Bin,Tan
        glm::vec3 n(0.f, 1.f, 0.f);
        glm::vec3 t(1.f, 0.f, 0.f);
        glm::vec3 b(0.f, 0.f, 1.f);
        for (unsigned int i = 0; i < 4; ++i)
        {
            m_vertices[i].m_nor = n;
            m_vertices[i].m_tan = t;
            m_vertices[i].m_bin = b;

            m_transformedVertices[i] = m_vertices[i];
        }
    }

    void Sprite::set(const Sprite& other)
    {
        m_region.setRegion(other.m_region);
        m_size = other.m_size;
        m_origin = other.m_origin;
        m_color = other.m_color;

        for (unsigned int i = 0; i < m_vertices.size(); ++i)
        {
            m_vertices[i].m_pos = other.m_vertices[i].m_pos;
            m_vertices[i].m_uv = other.m_vertices[i].m_uv;
            m_vertices[i].m_col = other.m_vertices[i].m_col;
            m_vertices[i].m_nor = other.m_vertices[i].m_nor;
            m_vertices[i].m_bin = other.m_vertices[i].m_bin;
            m_vertices[i].m_tan = other.m_vertices[i].m_tan;
        }
    }

    void Sprite::setSize(float width, float height)
    {
        m_size.x = width;
        m_size.y = height;

        float w = width / 2.f;
        float h = height / 2.f;

        m_vertices[0].m_pos.x = -w;
        m_vertices[0].m_pos.y = h;
        m_vertices[1].m_pos.x = -w;
        m_vertices[1].m_pos.y = -h;
        m_vertices[2].m_pos.x = w;
        m_vertices[2].m_pos.y = -h;
        m_vertices[3].m_pos.x = w;
        m_vertices[3].m_pos.y = h;

        m_transformedVertices[0].m_pos = glm::vec3(m_position, 0.f) + m_vertices[0].m_pos;
        m_transformedVertices[1].m_pos = glm::vec3(m_position, 0.f) + m_vertices[1].m_pos;
        m_transformedVertices[2].m_pos = glm::vec3(m_position, 0.f) + m_vertices[2].m_pos;
        m_transformedVertices[3].m_pos = glm::vec3(m_position, 0.f) + m_vertices[3].m_pos;
    }

    /*
    void Sprite::setSize(const glm::vec2& size)
    {
        setSize(size.x, size.y);
    }
    */

    const glm::vec2& Sprite::getSize()
    {
        return m_size;
    }

    float Sprite::getWidth()
    {
        return m_size.x;
    }

    float Sprite::getHeight()
    {
        return m_size.y;
    }

    void Sprite::setRotationX(float degrees)
    {
        if (degrees != m_rotationX)
        {
            m_vertices[0].m_pos = glm::rotateX(m_vertices[0].m_pos, -m_rotationX);
            m_vertices[1].m_pos = glm::rotateX(m_vertices[1].m_pos, -m_rotationX);
            m_vertices[2].m_pos = glm::rotateX(m_vertices[2].m_pos, -m_rotationX);
            m_vertices[3].m_pos = glm::rotateX(m_vertices[3].m_pos, -m_rotationX);

            float rad = glm::radians(degrees);
            m_vertices[0].m_pos = glm::rotateX(m_vertices[0].m_pos, rad);
            m_vertices[1].m_pos = glm::rotateX(m_vertices[1].m_pos, rad);
            m_vertices[2].m_pos = glm::rotateX(m_vertices[2].m_pos, rad);
            m_vertices[3].m_pos = glm::rotateX(m_vertices[3].m_pos, rad);

            m_rotationX = rad;

            m_transformedVertices[0].m_pos = m_vertices[0].m_pos;
            m_transformedVertices[1].m_pos = m_vertices[1].m_pos;
            m_transformedVertices[2].m_pos = m_vertices[2].m_pos;
            m_transformedVertices[3].m_pos = m_vertices[3].m_pos;
        }
    }

    void Sprite::setAlpha(float a)
    {
        m_color.a = a;
        m_vertices[0].m_col.a = a;
        m_vertices[1].m_col.a = a;
        m_vertices[2].m_col.a = a;
        m_vertices[3].m_col.a = a;
    }

    float Sprite::getAlpha()
    {
        return m_color.a;
    }

    void Sprite::setColor(float rgba)
    {
        m_color = glm::vec4(rgba);
        m_vertices[0].m_col = m_color;
        m_vertices[1].m_col = m_color;
        m_vertices[2].m_col = m_color;
        m_vertices[3].m_col = m_color;
    }

    void Sprite::setColorRGB(float r, float g, float b)
    {
        m_color.r = r;
        m_color.g = g;
        m_color.b = b;
        m_vertices[0].m_col = m_color;
        m_vertices[1].m_col = m_color;
        m_vertices[2].m_col = m_color;
        m_vertices[3].m_col = m_color;
        m_transformedVertices[0].m_col = m_color;
        m_transformedVertices[1].m_col = m_color;
        m_transformedVertices[2].m_col = m_color;
        m_transformedVertices[3].m_col = m_color;
    }

    void Sprite::setColorRGBA(float r, float g, float b, float a)
    {
        m_color = glm::vec4(r, g, b, a);
        m_vertices[0].m_col = m_color;
        m_vertices[1].m_col = m_color;
        m_vertices[2].m_col = m_color;
        m_vertices[3].m_col = m_color;
        m_transformedVertices[0].m_col = m_color;
        m_transformedVertices[1].m_col = m_color;
        m_transformedVertices[2].m_col = m_color;
        m_transformedVertices[3].m_col = m_color;
    }

    float Sprite::getColorR()
    {
        return m_color.r;
    }

    float Sprite::getColorG()
    {
        return m_color.g;
    }

    float Sprite::getColorB()
    {
        return m_color.b;
    }

    glm::vec3 Sprite::getColorRGB()
    {
        return glm::vec3(m_color.r, m_color.g, m_color.b);
    }

    const glm::vec4& Sprite::getColorRGBA()
    {
        return m_color;
    }


    void Sprite::setOrigin(float x, float y)
    {
        m_origin = glm::vec2(x, y);
    }

    const glm::vec2& Sprite::getOrigin()
    {
        return m_origin;
    }

    void Sprite::setOriginX(float x)
    {
        m_origin.x = x;
    }

    float Sprite::getOriginX()
    {
        return m_origin.x;
    }

    void Sprite::setOriginY(float y)
    {
        m_origin.y = y;
    }

    float Sprite::getOriginY()
    {
        return m_origin.y;
    }


    void Sprite::rotate90(bool clockwise)
    {
        if (clockwise)
        {
            glm::vec2 tmp = m_vertices[0].m_uv;
            m_vertices[0].m_uv = m_vertices[3].m_uv;
            m_vertices[3].m_uv = m_vertices[2].m_uv;
            m_vertices[2].m_uv = m_vertices[1].m_uv;
            m_vertices[1].m_uv = m_vertices[0].m_uv;
        }
        else
        {
            glm::vec2 tmp = m_vertices[0].m_uv;
            m_vertices[0].m_uv = m_vertices[1].m_uv;
            m_vertices[1].m_uv = m_vertices[2].m_uv;
            m_vertices[2].m_uv = m_vertices[3].m_uv;
            m_vertices[3].m_uv = m_vertices[0].m_uv;
        }
    }

    const std::vector<Vertex>& Sprite::getVertices()
    {
        return m_vertices;
    }

    const std::vector<Vertex>& Sprite::getTransformedVertices()
    {
        return m_transformedVertices;
    }

    void Sprite::applyTransform(Transform& transform, bool round)
    {
        glm::mat4 t = transform.getWorldTransform();
        for (unsigned int i = 0; i < 4; ++i)
        {
            glm::vec4 p = glm::vec4(m_vertices[i].m_pos, 1.f);
            p = t * p;
            m_transformedVertices[i].m_pos = round ? glm::round(glm::vec3(p)) : glm::vec3(p);
        }
    }


    void Sprite::setRegion(float u, float v, float u2, float v2)
    {
        // m_region.setRegion(u, v, u2, v2);
        m_vertices[0].m_uv.x = u;
        m_vertices[0].m_uv.y = v;
        m_vertices[1].m_uv.x = u;
        m_vertices[1].m_uv.y = v2;
        m_vertices[2].m_uv.x = u2;
        m_vertices[2].m_uv.y = v2;
        m_vertices[3].m_uv.x = u2;
        m_vertices[3].m_uv.y = v;

        m_transformedVertices[0].m_uv.x = u;
        m_transformedVertices[0].m_uv.y = v;
        m_transformedVertices[1].m_uv.x = u;
        m_transformedVertices[1].m_uv.y = v2;
        m_transformedVertices[2].m_uv.x = u2;
        m_transformedVertices[2].m_uv.y = v2;
        m_transformedVertices[3].m_uv.x = u2;
        m_transformedVertices[3].m_uv.y = v;
    }

    void Sprite::setU(float u)
    {
        m_region.setU(u);
        m_vertices[0].m_uv.x = u;
        m_vertices[1].m_uv.x = u;
    }

    void Sprite::setV(float v)
    {
        m_region.setV(v);
        m_vertices[0].m_uv.y = v;
        m_vertices[3].m_uv.y = v;
    }

    void Sprite::setU2(float u2)
    {
        m_region.setU2(u2);
        m_vertices[2].m_uv.x = u2;
        m_vertices[3].m_uv.x = u2;
    }

    void Sprite::setV2(float v2)
    {
        m_region.setV2(v2);
        m_vertices[1].m_uv.y = v2;
        m_vertices[2].m_uv.y = v2;
    }

    void Sprite::setFlip(bool x, bool y)
    {
        bool doX = false, doY = false;
        if (m_region.isFlipX() != x) doX = true;
        if (m_region.isFlipY() != y) doY = true;
        flip(doX, doY);
    }

    void Sprite::flip(bool x, bool y)
    {
        m_region.flip(x, y);
        if (x)
        {
            float tmp = m_vertices[0].m_uv.x;
            m_vertices[0].m_uv.x = m_vertices[2].m_uv.x;
            m_vertices[2].m_uv.x = tmp;
            tmp = m_vertices[1].m_uv.x;
            m_vertices[1].m_uv.x = m_vertices[3].m_uv.x;
            m_vertices[3].m_uv.x = tmp;
        }
        if (y)
        {
            float tmp = m_vertices[0].m_uv.y;
            m_vertices[0].m_uv.y = m_vertices[2].m_uv.y;
            m_vertices[2].m_uv.y = tmp;
            tmp = m_vertices[1].m_uv.y;
            m_vertices[1].m_uv.y = m_vertices[3].m_uv.y;
            m_vertices[3].m_uv.y = tmp;
        }
    }

    void Sprite::scroll(float xAmount, float yAmount)
    {
        if (xAmount != 0.f)
        {
            float u = glm::mod(m_vertices[0].m_uv.x + xAmount, 1.0f);
            float u2 = u + m_size.x / m_region.getTexture().getWidth();
            m_region.setU(u);
            m_region.setU2(u2);
            m_vertices[0].m_uv.x = u;
            m_vertices[1].m_uv.x = u;
            m_vertices[2].m_uv.x = u2;
            m_vertices[3].m_uv.x = u2;
        }
        if (yAmount != 0.f)
        {
            float v = glm::mod(m_vertices[1].m_uv.y + yAmount, 1.0f);
            float v2 = v + m_size.y / m_region.getTexture().getHeight();
            m_region.setV(v);
            m_region.setV2(v2);
            m_vertices[0].m_uv.y = v2;
            m_vertices[1].m_uv.y = v;
            m_vertices[2].m_uv.y = v;
            m_vertices[3].m_uv.y = v2;
        }
    }

    void Sprite::setSourceRectangle(int x, int y, int width, int height)
    {
        // m_region.setRegion(x, y, width, height);
        // setRegion(m_region.getU(), m_region.getV(), m_region.getU2(), m_region.getV2());
        
        int _x = x + m_region.getRegionX();
        int _y = y + m_region.getRegionY();
        float u = (float) _x / (float) m_region.getTexture().getWidth();
        float v = (float) _y / (float) m_region.getTexture().getHeight();
        float u2 = (float) (_x + width) / (float) m_region.getTexture().getWidth();
        float v2 = (float) (_y + height) / (float) m_region.getTexture().getHeight();
        setRegion(u, v, u2, v2);
    }

    Texture& Sprite::getTexture()
    {
        return m_region.getTexture();
    }
}