#pragma once

#include <Component.h>
#include <glm/glm.hpp>

namespace Tremor
{
    enum PizzaShadowType
    {
        Solid = 1,
        Illuminated = 2,
        Occluded = 3
    };

    class PizzaLight : public Tecs::Component
    {
    public:
        PizzaLight()
            : m_position(0.f, 0.f),
            m_color(1.f, 1.f, 1.f),
            m_range(256.f),
            m_intensity(1.f),
            m_shadowType(PizzaShadowType::Solid)
        {

        }

        ~PizzaLight()
        {

        }

        const glm::vec2& getPosition()
        {
            return m_position;
        }

        void setPosition(float x, float y)
        {
            m_position.x = x;
            m_position.y = y;
        }

        const glm::vec3& getColor()
        {
            return m_color;
        }

        void setColor(float r, float g, float b)
        {
            m_color = glm::vec3(r, g, b);
        }

        float getRange()
        {
            return m_range;
        }

        void setRange(float range)
        {
            m_range = range;
        }

        float getIntensity()
        {
            return m_intensity;
        }

        void setIntensity(float intensity)
        {
            m_intensity = glm::clamp(intensity, 0.01f, 3.f);
        }

        PizzaShadowType getShadowType()
        {
            return m_shadowType;
        }

        void setShadowType(PizzaShadowType type)
        {
            m_shadowType = type;
        }
    protected:
        glm::vec3 m_color;
        glm::vec2 m_position;
        float m_range;
        float m_intensity;
        PizzaShadowType m_shadowType;
    };
}