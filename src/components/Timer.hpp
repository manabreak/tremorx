#pragma once
#include <Component.h>

namespace Tremor
{
    /* A simple timer component. Can be used either as a countdown timer as well.
       The timer automatically works as an increasing or decreasing timer, depending
       on the initial and target values.

       The timer keeps running regardless of the target being met or not. The time
       since the target was met can be retrieved as well.
    */
    class Timer : public Tecs::Component
    {
    public:
        // Creates an empty timer that's "done"
        Timer()
            : m_done(true),
            m_initial(0.f),
            m_target(0.f),
            m_current(0.f),
            m_down(false)
        {

        }

        // Creates a timer with an initial time value and a target time
        Timer(float initial, float target)
            : m_initial(initial),
            m_target(target),
            m_current(initial),
            m_done(false),
            m_down(initial > target)
        {

        }

        ~Timer()
        {

        }

        // Advance the timer by 'delta' and marks the timer done if the target is met.
        void advance(float delta)
        {
            if (m_down)
            {
                m_current -= delta;
                if (m_current <= m_target)
                {
                    m_done = true;
                }
            }
            else
            {
                m_current += delta;
                if (m_current >= m_target)
                {
                    m_done = true;
                }
            }
        }

        // De-advances the timer by 'delta' and marks the timer undone if the target is un-met.
        void stepBack(float delta)
        {
            if (m_down)
            {
                m_current += delta;
                if (m_current > m_target)
                {
                    m_done = false;
                }
            }
            else
            {
                m_current -= delta;
                if (m_current < m_target)
                {
                    m_done = false;
                }
            }
        }

        // Gets the current timer value
        float getTimer()
        {
            return m_current;
        }

        // Gets the target value
        float getTarget()
        {
            return m_target;
        }

        // Gets the initial timer value
        float getInitial()
        {
            return m_initial;
        }

        // Resets the timer to its initial values
        void reset()
        {
            m_done = false;
            m_current = m_initial;
        }

        // Returns true if the target is met, false otherwise
        bool isDone()
        {
            return m_done;
        }

        // Returns the time since the target was met
        float getTimeSinceCompletion()
        {
            if (m_down)
            {
                return m_target - m_current;
            }
            else
            {
                return m_current - m_target;
            }
        }
    private:
        bool m_down;
        bool m_done;
        float m_target;
        float m_current;
        float m_initial;
    };
}