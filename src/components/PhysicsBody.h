#pragma once
#include <Component.h>
#include <glm/glm.hpp>
#include <box2d/Box2D.h>

namespace Tremor
{
    class Physics;

    struct CollisionListener;

    class PhysicsBody : public Tecs::Component
    {
    public:
        PhysicsBody();
        ~PhysicsBody();

        PhysicsBody(const PhysicsBody&) = delete;

        void addBoxCollider(float width, float height);
        void addOffsetBoxCollider(float x, float y, float width, float height);
        void addBoxSensor(float width, float height);
        void addOffsetBoxSensor(float x, float y, float width, float height);

        void addCircleCollider(float radius);
        void addOffsetCircleCollider(float x, float y, float radius);
        void addCircleSensor(float radius);
        void addOffsetCircleSensor(float x, float y, float radius);

        bool isDynamic();
        bool isStatic();
        bool isKinematic();

        void setDynamic();
        void setStatic();
        void setKinematic();

        const glm::vec2& getPosition();
        const glm::vec2& getVelocity();

        void setPosition(float x, float y);
        void setVelocity(float x, float y);

        float getX();
        float getY();

        b2Body* getb2Body();
        
        void create(Physics& physics);
        void destroy(Physics& physics);

        void setCollisionListener(CollisionListener* listener);
        CollisionListener* getCollisionListener();
    private:
        void createBox(float x, float y, float width, float height, bool sensor);
        void createCircle(float x, float y, float radius, bool sensor);
        b2BodyType m_type;
        b2Body* m_body;

        b2BodyDef m_bodyDef;
        std::vector<b2FixtureDef> m_fixtureDefs;
        std::vector<std::unique_ptr<b2Shape>> m_shapes;

        float m_createPosX;
        float m_createPosY;
        float m_createVelX;
        float m_createVelY;

        CollisionListener* m_listener;
    };

    struct CollisionListener
    {
        virtual void onCollisionBegin(PhysicsBody& other) {}
        virtual void onCollision(PhysicsBody& other) {}
        virtual void onCollisionEnd(PhysicsBody& other) {}
    };
}