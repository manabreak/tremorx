#pragma once
#include "stdafx.h"
#include <Component.h>

namespace Tremor
{
    class LuaScript : public Tecs::Component
    {
    public:
        LuaScript(const std::string& script)
            : m_script(script),
            m_createCalled(false)
        {
            m_L = luaL_newstate();
            luaL_openlibs(m_L);
            luaL_dostring(m_L, getScript());
        }

        ~LuaScript()
        {

        }

        lua_State* getLuaState()
        {
            return m_L;
        }

        const char* getScript()
        {
            return m_script.c_str();
        }

        bool isCreateCalled()
        {
            return m_createCalled;
        }

        void setCreateCalled(bool createCalled)
        {
            m_createCalled = createCalled;
        }
    private:
        lua_State* m_L;
        bool m_createCalled;
        std::string m_script;
    };
}