#include "stdafx.h"
#include "PhysicsBody.h"
#include "systems/Physics.h"

namespace Tremor
{
    PhysicsBody::PhysicsBody()
        : m_type(b2BodyType::b2_staticBody),
        m_body(nullptr),
        m_createPosX(0.f),
        m_createPosY(0.f),
        m_createVelX(0.f),
        m_createVelY(0.f),
        m_listener(nullptr)
    {
        
    }

    PhysicsBody::~PhysicsBody()
    {

    }

    void PhysicsBody::createBox(float x, float y, float width, float height, bool sensor)
    {
        b2PolygonShape* shape = new b2PolygonShape();
        shape->SetAsBox(width / 2.f, height / 2.f, b2Vec2(x, y), 0.f);
        m_shapes.emplace_back(shape);

        m_fixtureDefs.emplace_back();
        m_fixtureDefs.back().shape = shape;
        m_fixtureDefs.back().isSensor = sensor;
    }

    void PhysicsBody::addBoxCollider(float width, float height)
    {
        createBox(0.f, 0.f, width, height, false);
    }

    void PhysicsBody::addBoxSensor(float width, float height)
    {
        createBox(0.f, 0.f, width, height, true);
    }

    void PhysicsBody::addOffsetBoxCollider(float x, float y, float width, float height)
    {
        createBox(x, y, width, height, false);
    }

    void PhysicsBody::addOffsetBoxSensor(float x, float y, float width, float height)
    {
        createBox(x, y, width, height, true);
    }

    void PhysicsBody::createCircle(float x, float y, float radius, bool sensor)
    {
        b2CircleShape* shape = new b2CircleShape();
        shape->m_radius = radius;
        shape->m_p = b2Vec2(x, y);

        m_fixtureDefs.emplace_back();
        m_fixtureDefs.back().shape = shape;
        m_fixtureDefs.back().isSensor = sensor;
    }

    void PhysicsBody::addCircleCollider(float radius)
    {
        createCircle(0.f, 0.f, radius, false);
    }

    void PhysicsBody::addOffsetCircleCollider(float x, float y, float radius)
    {
        createCircle(x, y, radius, false);
    }

    void PhysicsBody::addCircleSensor(float radius)
    {
        createCircle(0.f, 0.f, radius, true);
    }

    void PhysicsBody::addOffsetCircleSensor(float x, float y, float radius)
    {
        createCircle(x, y, radius, true);
    }

    bool PhysicsBody::isDynamic()
    {
        return m_type == b2_dynamicBody;
    }

    bool PhysicsBody::isStatic()
    {
        return m_type == b2_staticBody;
    }

    bool PhysicsBody::isKinematic()
    {
        return m_type == b2_kinematicBody;
    }

    void PhysicsBody::setDynamic()
    {
        m_type = b2_dynamicBody;
    }

    void PhysicsBody::setStatic()
    {
        m_type = b2_staticBody;
    }

    void PhysicsBody::setKinematic()
    {
        m_type = b2_kinematicBody;
    }

    const glm::vec2& PhysicsBody::getPosition()
    {
        if (m_body != nullptr)
        {
            b2Vec2 p = m_body->GetPosition();
            return glm::vec2(p.x, p.y);
        }
        return glm::vec2(m_createPosX, m_createPosY);
    }

    const glm::vec2& PhysicsBody::getVelocity()
    {
        if (m_body != nullptr)
        {
            b2Vec2 v = m_body->GetLinearVelocity();
            return glm::vec2(v.x, v.y);
        }
        return glm::vec2(m_createVelX, m_createVelY);
    }

    void PhysicsBody::setPosition(float x, float y)
    {
        if (m_body != nullptr)
        {
            float a = m_body->GetAngle();
            m_body->SetTransform(b2Vec2(x, y), a);
        }
        else
        {
            m_createPosX = x;
            m_createPosY = y;
        }
    }

    void PhysicsBody::setVelocity(float x, float y)
    {
        if (m_body != nullptr)
        {
            m_body->SetLinearVelocity(b2Vec2(x, y));
        }
        else
        {
            m_createVelX = x;
            m_createVelY = y;
        }
    }

    float PhysicsBody::getX()
    {
        if (m_body != nullptr)
        {
            return m_body->GetPosition().x;
        }
        return m_createPosX;
    }

    float PhysicsBody::getY()
    {
        if (m_body != nullptr)
        {
            return m_body->GetPosition().y;
        }
        return m_createPosY;
    }

    b2Body* PhysicsBody::getb2Body()
    {
        return m_body;
    }

    void PhysicsBody::create(Physics& physics)
    {
        m_body = physics.getb2World().CreateBody(&m_bodyDef);
        m_body->SetType(m_type);
        
        m_body->SetTransform(b2Vec2(m_createPosX, m_createPosY), 0.f);
        m_body->SetLinearVelocity(b2Vec2(m_createVelX, m_createVelY));

        m_body->SetUserData(this);

        for (unsigned int i = 0; i < m_fixtureDefs.size(); ++i)
        {
            b2FixtureDef& def = m_fixtureDefs[i];
            m_body->CreateFixture(&def);
        }

        m_createPosX = 0.f;
        m_createPosY = 0.f;
        m_createVelX = 0.f;
        m_createVelY = 0.f;
    }

    void PhysicsBody::destroy(Physics& physics)
    {
        physics.getb2World().DestroyBody(m_body);
        m_body = nullptr;
    }

    void PhysicsBody::setCollisionListener(CollisionListener* listener)
    {
        m_listener = listener;
    }

    CollisionListener* PhysicsBody::getCollisionListener()
    {
        return m_listener;
    }
}