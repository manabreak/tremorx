#pragma once

#include <Component.h>
#include "3d/Mesh.h"
#include "2d/Texture.h"
#include "3d/Model.h"

namespace  Tremor
{
    // Component wrapper for models
    class ModelComponent : public Tecs::Component
    {
    public:
        ModelComponent(Model& model)
            : m_model(&model),
            m_ownsModel(false)
        {

        }

        ModelComponent(Mesh& mesh)
            : m_model(new Model(mesh)),
            m_ownsModel(true)
        {

        }

        ModelComponent(Mesh& mesh, Texture& colormap)
            : m_model(new Model(mesh, colormap)),
            m_ownsModel(true)
        {

        }

        ModelComponent(Mesh& mesh, Texture& colormap, Texture& normalmap)
            : m_model(new Model(mesh, colormap, normalmap)),
            m_ownsModel(true)
        {

        }

        ~ModelComponent()
        {
            if (m_ownsModel)
            {
                delete m_model;
            }
        }

        Model& getModel()
        {
            return *m_model;
        }

        Mesh& getMesh()
        {
            return m_model->getMesh();
        }

        Texture& getColorMap()
        {
            return m_model->getColorMap();
        }

        Texture& getNormalMap()
        {
            return m_model->getNormalMap();
        }

        bool hasNormalMap()
        {
            return m_model->hasNormalMap();
        }
    private:
        Model* m_model;
        bool m_ownsModel;
    };
}