#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <memory>
#include "../2d/Texture.h"
#include "maps/TileSet.h"
#include "../3d/Mesh.h"
#include "2d/BitmapFont.h"
#include "2d/TextureAtlas.h"

namespace Tremor
{
    class TiledMap;
    class Model;

    class AssetManager
    {
    public:
        static Texture& loadTexture(const std::string& filename);
        static TextureAtlas& loadTextureAtlas(const std::string& filename);
        static ShaderProgram& loadShader(const std::string& vertexSrc, const std::string& fragmentSrc);
        static TiledMap& loadTiledMap(const std::string& filename);
        static BitmapFont& loadBitmapFont(const std::string& filename);
        static Mesh& loadMesh(const std::string& name, const std::vector<Vertex>& vertices, const std::vector<unsigned short>& indices);
        static Model& loadOBJ(const std::string& filename);

        static void storeTiledMap(TiledMap* map);
        // TODO Add unloading functions
    private:
        static std::vector<std::unique_ptr<Texture>> m_textures;
        static std::vector<std::unique_ptr<TextureAtlas>> m_textureAtlases;
        static std::vector<std::unique_ptr<ShaderProgram>> m_shaders;
        static std::vector<std::unique_ptr<TiledMap>> m_tiledmaps;
        static std::vector<std::unique_ptr<BitmapFont>> m_bitmapfonts;
        static std::unordered_map<std::string, std::unique_ptr<Mesh>> m_meshes;
        static std::unordered_map<std::string, std::unique_ptr<Model>> m_models;
    };
}