#include "stdafx.h"
#include "TiledMapLoader.h"
#include <rapidjson/document.h>
#include "utils/StringUtils.h"

using namespace rapidjson;

namespace Tremor
{
    TiledMap* TiledMapLoader::loadFromFile(const std::string& filename)
    {
        Document d;
        d.Parse(StringUtils::readFile(filename).c_str());
        assertTilemap(d);

        int mapWidth = d["width"].GetInt();
        int mapHeight = d["height"].GetInt();
        int version = d["version"].GetInt();
        std::string orientation = d["orientation"].GetString();
        int tileWidth = d["tilewidth"].GetInt();
        int tileHeight = d["tileheight"].GetInt();

        std::vector<MapLayer> mapLayers;
        loadLayers(d["layers"], mapLayers);

        std::vector<TileSet> tilesets;
        loadTilesets(d["tilesets"], tilesets);

        TiledMap* map = new TiledMap(filename, mapWidth, mapHeight, version, orientation, tileWidth, tileHeight);
        map->addLayers(mapLayers);
        map->addTilesets(tilesets);
        return map;
    }

    void TiledMapLoader::loadLayers(const Value& layers, std::vector<MapLayer>& mapLayers)
    {
        for (SizeType i = 0; i < layers.Size(); ++i)
        {
            const Value& layer = layers[i];
            assertLayer(layer);
            mapLayers.emplace_back(layer);
        }
    }

    void TiledMapLoader::loadTilesets(const Value& tilesets, std::vector<TileSet>& vecTilesets)
    {
        for (SizeType i = 0; i < tilesets.Size(); ++i)
        {
            const Value& tileset = tilesets[i];
            assertTileset(tileset);
            vecTilesets.emplace_back(tileset);
        }
    }

    void TiledMapLoader::assertTilemap(Document& d)
    {
        assert(d.IsObject());
        assert(d.HasMember("width") && d["width"].IsNumber());
        assert(d.HasMember("height") && d["height"].IsNumber());
        assert(d.HasMember("tilewidth") && d["tilewidth"].IsNumber());
        assert(d.HasMember("tileheight") && d["tileheight"].IsNumber());
        assert(d.HasMember("orientation") && d["orientation"].IsString());
        assert(d.HasMember("layers") && d["layers"].IsArray());
    }

    void TiledMapLoader::assertLayer(const Value& layer)
    {
        assert(layer.HasMember("name"));
        assert(layer.HasMember("visible"));
        assert(layer.HasMember("width"));
        assert(layer.HasMember("height"));
        assert(layer.HasMember("opacity"));
        assert(layer.HasMember("x"));
        assert(layer.HasMember("y"));
        assert(layer.HasMember("type"));
        assert(layer.HasMember("data") || layer.HasMember("objects"));
    }

    void TiledMapLoader::assertTileset(const Value& tileset)
    {

    }
}