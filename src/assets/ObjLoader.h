#pragma once

#include <string>

namespace Tremor
{
    class Model;

    class ObjLoader
    {
    public:
        static Model* loadModel(const std::string& file);
    };
}