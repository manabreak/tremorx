#include "stdafx.h"
#include "AssetManager.h"
#include "utils/Pooler.hpp"
#include "TiledMapLoader.h"
#include "3d/Model.h"
#include "ObjLoader.h"

namespace Tremor
{
    std::vector<std::unique_ptr<Texture>> AssetManager::m_textures;
    std::vector<std::unique_ptr<TextureAtlas>> AssetManager::m_textureAtlases;
    std::vector<std::unique_ptr<ShaderProgram>> AssetManager::m_shaders;
    std::vector<std::unique_ptr<TiledMap>> AssetManager::m_tiledmaps;
    std::vector<std::unique_ptr<BitmapFont>> AssetManager::m_bitmapfonts;
    std::unordered_map<std::string, std::unique_ptr<Mesh>> AssetManager::m_meshes;
    std::unordered_map<std::string, std::unique_ptr<Model>> AssetManager::m_models;

    Texture& AssetManager::loadTexture(const std::string& filename)
    {
        std::string name = filename;
        if (name.compare(0, 3, "../") == 0)
        {
            name = name.substr(3);
        }

        // Check if already have the texture
        for (unsigned int i = 0; i < AssetManager::m_textures.size(); ++i)
        {
            // if (std::strcmp(AssetManager::m_textures[i]->getFilename().c_str(), filename.c_str()))
            if (AssetManager::m_textures[i]->getFilename() == name)
            {
                return *AssetManager::m_textures[i];
            }
        }

        // Load
        Texture* texture = new Texture(name);
        m_textures.push_back(std::unique_ptr<Texture>(texture));
        return *AssetManager::m_textures.back();
    }

    TextureAtlas& AssetManager::loadTextureAtlas(const std::string& filename)
    {
        for (unsigned int i = 0; i < AssetManager::m_textureAtlases.size(); ++i)
        {
            if (AssetManager::m_textureAtlases[i]->getFilename() == filename)
            {
                return *AssetManager::m_textureAtlases[i];
            }
        }

        TextureAtlas* atlas = new TextureAtlas(filename);
        m_textureAtlases.push_back(std::unique_ptr<TextureAtlas>(atlas));
        return *AssetManager::m_textureAtlases.back();
    }

    ShaderProgram& AssetManager::loadShader(const std::string& vertexSrc, const std::string& fragmentSrc)
    {
        ShaderProgram* shader = new ShaderProgram(vertexSrc, fragmentSrc);
        m_shaders.push_back(std::unique_ptr<ShaderProgram>(shader));
        return *AssetManager::m_shaders.back();
    }

    TiledMap& AssetManager::loadTiledMap(const std::string& filename)
    {
        for (unsigned int i = 0; i < m_tiledmaps.size(); ++i)
        {
            if (m_tiledmaps[i]->getFilename() == filename)
            {
                return *m_tiledmaps[i];
            }
        }
        TiledMap* tiledMap = TiledMapLoader::loadFromFile(filename);
        m_tiledmaps.push_back(std::unique_ptr<TiledMap>(tiledMap));
        return *tiledMap;
    }

    void AssetManager::storeTiledMap(TiledMap* map)
    {
        // Check that the map is not stored yet
        for (unsigned int i = 0; i < m_tiledmaps.size(); ++i)
        {
            if (m_tiledmaps[i]->getFilename() == map->getFilename())
            {
                return;
            }
        }

        m_tiledmaps.push_back(std::unique_ptr<TiledMap>(map));
    }

    BitmapFont& AssetManager::loadBitmapFont(const std::string& filename)
    {
        for (unsigned int i = 0; i < m_bitmapfonts.size(); ++i)
        {
            if (m_bitmapfonts[i]->getFilename() == filename)
            {
                return *m_bitmapfonts[i];
            }
        }

        BitmapFont* font = new BitmapFont(filename);
        m_bitmapfonts.push_back(std::unique_ptr<BitmapFont>(font));
        return *font;
    }

    Mesh& AssetManager::loadMesh(const std::string& name, const std::vector<Vertex>& vertices, const std::vector<unsigned short>& indices)
    {
        if (m_meshes.find(name) != m_meshes.end())
        {
            return *m_meshes[name];
        }

        m_meshes[name] = std::unique_ptr<Mesh>(new Mesh(vertices, indices));
        return *m_meshes[name];
    }

    Model& AssetManager::loadOBJ(const std::string& filename)
    {
        if (m_models.find(filename) != m_models.end())
        {
            return *m_models[filename];
        }

        m_models[filename] = std::unique_ptr<Model>(ObjLoader::loadModel(filename));
        return *m_models[filename];
    }
}