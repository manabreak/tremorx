#include "stdafx.h"
#include "ObjLoader.h"
#include "3d/Model.h"
#include <fstream>
#include <sstream>

namespace Tremor
{
    Model* ObjLoader::loadModel(const std::string& file)
    {
        std::vector<glm::vec3> vPositions;
        std::vector<glm::vec2> vUVs;
        std::vector<glm::vec3> vNormals;
        std::vector<unsigned short> faces;
        std::vector<unsigned short> faceUVs;
        std::vector<unsigned short> faceNormals;
        std::string objName;
        std::string textureFile;

        std::ifstream stream(file);
        std::string line;
        std::string read;
        std::size_t i;

        while (!stream.eof())
        {
            std::stringstream lineStream;
            std::getline(stream, line);
            lineStream << line;
            lineStream >> read;

            if (read == "") continue;

            // Material library
            else if (read == "mtllib")
            {
                std::string mtllibFile;
                lineStream >> mtllibFile;
                std::string mtllibPath = "assets/";
                mtllibPath.append(mtllibFile);
                std::ifstream matstream(mtllibPath);
                std::string mread;
                std::string mline;
                std::string materialName;
                while (!matstream.eof())
                {
                    std::stringstream matLineStream;
                    std::getline(matstream, mline);
                    matLineStream << mline;
                    matLineStream >> mread;
                    if (mread == "") continue;
                    else if (mread == "newmtl")
                    {
                        matLineStream >> materialName;
                    }
                    else if (mread == "map_Kd")
                    {
                        matLineStream >> textureFile;
                    }
                }

            }
            // Objects
            else if (read == "o")
            {
                lineStream >> objName;
            }
            // Vertex positions
            else if (read == "v")
            {
                std::string svx, svy, svz;
                float vx, vy, vz;
                lineStream >> svx;
                lineStream >> svy;
                lineStream >> svz;
                std::stringstream conv;
                conv << svx;
                conv >> vx;
                conv.clear();
                conv << svy;
                conv >> vy;
                conv.clear();
                conv << svz;
                conv >> vz;
                // vx = glm::round(vx);
                vy = vy / glm::cos(glm::pi<float>() / 6.f);
                // vz = glm::round(vz);
                vPositions.emplace_back(vx, vy, vz);        
            }
            // Vertex UVs
            else if (read == "vt")
            {
                float uv0, uv1;
                lineStream >> uv0;
                lineStream >> uv1;

                vUVs.emplace_back(uv0, 1.f - uv1);
            }
            // Vertex normals
            else if (read == "vn")
            {
                float vx, vy, vz;
                lineStream >> vx;
                lineStream >> vy;
                lineStream >> vz;
                vNormals.emplace_back(vx, vy, vz);
            }
            // Vertex indices
            else if (read == "f")
            {
                unsigned short index, uv, normal;
                for (unsigned int i = 0; i < 3; ++i)
                {
                    // Positions
                    std::string fn;
                    lineStream >> fn;
                    std::stringstream cPos;
                    size_t j = fn.find("/");
                    std::string strIndex = fn.substr(0, j);
                    cPos << strIndex;
                    cPos >> index;
                    faces.push_back(index);

                    // UVs
                    std::stringstream cUV;
                    std::string strUV = fn.substr(fn.find("/") + 1);
                    strUV = strUV.substr(0, fn.find("/"));
                    cUV << strUV;
                    cUV >> uv;
                    faceUVs.push_back(uv);

                    // Normals
                    std::stringstream cNor;
                    j = fn.rfind("/");
                    std::string strNorIndex = fn.substr(j + 1);
                    cNor << strNorIndex;
                    cNor >> normal;
                    faceNormals.push_back(normal);
                }
            }

            read = "";
        }

        // Create the mesh
        std::vector<Vertex> vertices;
        std::vector<unsigned short> indices;
        for (unsigned int i = 0; i < faces.size(); ++i)
        {
            unsigned short index = faces[i] - 1;
            unsigned short uv = faceUVs[i] - 1;
            unsigned short n = faceNormals[i] - 1;

            vertices.emplace_back(vPositions[index], vUVs[uv], vNormals[n]);
        }

        for (unsigned int i = 0; i < vertices.size(); i += 3)
        {
            indices.push_back(i);
            indices.push_back(i + 1);
            indices.push_back(i + 2);
        }

        Mesh& mesh = AssetManager::loadMesh(objName, vertices, indices);
        Texture& texture = AssetManager::loadTexture("assets/" + textureFile);

        return new Model(mesh, texture);
    }
}