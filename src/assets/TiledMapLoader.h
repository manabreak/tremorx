#pragma once

#include "maps/TiledMap.h"
#include <string>

namespace Tremor
{
    class TiledMapLoader
    {
    public:
        static TiledMap* loadFromFile(const std::string& filename);
    private:
        static void assertTilemap(Document& d);
        static void assertLayer(const Value& layer);
        static void assertTileset(const Value& tileset);
        static void loadLayers(const Value& layers, std::vector<MapLayer>& mapLayers);
        static void loadTilesets(const Value& tilesets, std::vector<TileSet>& vecTilesets);
    };
}