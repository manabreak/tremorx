#include "stdafx.h"
#include "TiledMap.h"

namespace Tremor
{
    TiledMap::TiledMap(const std::string& filename, int width, int height, int version, const std::string& orientation, int tileWidth, int tileHeight)
        : m_filename(filename),
        m_width(width),
        m_height(height),
        m_version(version),
        m_orientation(orientation),
        m_tilewidth(tileWidth),
        m_tileheight(tileHeight)
    {

    }

    TiledMap::~TiledMap()
    {
    }

    int TiledMap::getLayerCount()
    {
        return m_layers.size();
    }

    MapLayer& TiledMap::getLayer(int index)
    {
        return m_layers[index];
    }

    const std::string& TiledMap::getFilename()
    {
        return m_filename;
    }

    void TiledMap::addLayers(const std::vector<MapLayer>& layers)
    {
        for (unsigned int i = 0; i < layers.size(); ++i)
        {
            m_layers.push_back(layers[i]);
        }
    }

    void TiledMap::addLayer(MapLayer layer)
    {
        m_layers.push_back(layer);
    }

    void TiledMap::addTilesets(const std::vector<TileSet>& tilesets)
    {
        for (unsigned int i = 0; i < tilesets.size(); ++i)
        {
            m_tilesets.push_back(tilesets[i]);
        }
    }

    void TiledMap::addTileset(TileSet tileset)
    {
        m_tilesets.push_back(tileset);
    }

    std::vector<MapLayer>& TiledMap::getLayers()
    {
        return m_layers;
    }

    std::vector<TileSet>& TiledMap::getTilesets()
    {
        return m_tilesets;
    }

    int TiledMap::getTileWidth()
    {
        return m_tilewidth;
    }

    int TiledMap::getTileHeight()
    {
        return m_tileheight;
    }

    int TiledMap::getVersion()
    {
        return m_version;
    }

    int TiledMap::getWidth()
    {
        return m_width;
    }

    int TiledMap::getHeight()
    {
        return m_height;
    }

    const std::string& TiledMap::getOrientation()
    {
        return m_orientation;
    }

    TileSet& TiledMap::getTileset(int gid)
    {
        int size = m_tilesets.size();
        for (int i = 0; i < size; ++i)
        {
            int firstGID = m_tilesets[i].getFirstGID();
            if (gid >= firstGID)
            {
                if (i == size - 1)
                {
                    return m_tilesets[i];
                }
                else
                {
                    int nextGID = m_tilesets[i + 1].getFirstGID();
                    if (gid < nextGID) return m_tilesets[i];
                }
            }
        }
        return m_tilesets.back();
    }
}