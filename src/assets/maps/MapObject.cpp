#include "stdafx.h"

#include "MapObject.h"

using namespace rapidjson;

namespace Tremor
{
    MapObject::MapObject(const std::string& name, const std::string& type, int x, int y, int width, int height)
        : m_name(name),
        m_type(type),
        m_x(x),
        m_y(y),
        m_width(width),
        m_height(height),
        m_visible(true)
    {

    }

    MapObject::MapObject(const Value& object)
        : m_name(object["name"].GetString()),
        m_type(object["type"].GetString()),
        m_x(object["x"].GetInt()),
        m_y(object["y"].GetInt()),
        m_width(object["width"].GetInt()),
        m_height(object["height"].GetInt()),
        m_visible(object["visible"].GetBool())
    {
        if (object.HasMember("properties"))
        {
            m_properties = MapProperties(object["properties"]);
        }
    }

    MapObject::~MapObject()
    {

    }

    int MapObject::getX() const
    {
        return m_x;
    }

    int MapObject::getY() const
    {
        return m_y;
    }

    int MapObject::getWidth() const
    {
        return m_width;
    }

    int MapObject::getHeight() const
    {
        return m_height;
    }

    const std::string& MapObject::getName() const
    {
        return m_name;
    }

    const std::string& MapObject::getType() const
    {
        return m_type;
    }

    bool MapObject::isVisible() const
    {
        return m_visible;
    }

    MapProperties& MapObject::getProperties()
    {
        return m_properties;
    }
}