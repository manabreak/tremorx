#include "stdafx.h"
#include "MapLayer.h"

using namespace rapidjson;

namespace Tremor
{
    MapLayer::MapLayer()
        : m_name(""),
        m_type("tilelayer"),
        m_width(0),
        m_height(0),
        m_x(0),
        m_y(0),
        m_opacity(0.f),
        m_visible(true)
    {

    }

    MapLayer::MapLayer(const std::string& name, int x, int y, int width, int height, MapLayerType type)
        : m_name(name),
        m_x(x),
        m_y(y),
        m_width(width),
        m_height(height),
        m_visible(true),
        m_opacity(1.f),
        m_type(type == TileLayer ? "tilelayer" : "objectgroup")
    {
        if (type == TileLayer)
        {
            unsigned int size = width * height;
            for (unsigned int i = 0; i < size; ++i)
            {
                m_data.push_back(0);
            }
        }
    }

    MapLayer::MapLayer(const Value& layer)
        : m_name(layer["name"].GetString()),
        m_visible(layer["visible"].GetBool()),
        m_width(layer["width"].GetInt()),
        m_height(layer["height"].GetInt()),
        m_opacity((float) layer["opacity"].GetDouble()),
        m_type(layer["type"].GetString()),
        m_x(layer["x"].GetInt()),
        m_y(layer["y"].GetInt())
    {
        if (isTileLayer())
        {
            const Value& data = layer["data"];
            for (SizeType d = 0; d < data.Size(); ++d)
            {
                m_data.push_back(data[d].GetInt());
            }
        }
        else
        {
            const Value& objects = layer["objects"];
            for (SizeType o = 0; o < objects.Size(); ++o)
            {
                m_objects.emplace_back(objects[o]);
            }
        }

        if (layer.HasMember("properties"))
        {
            m_properties = MapProperties(layer["properties"]);
        }
    }

    MapLayer::~MapLayer()
    {

    }

    int MapLayer::getDataCount()
    {
        return m_data.size();
    }

    int MapLayer::getObjectCount()
    {
        return m_objects.size();
    }

    int MapLayer::getDataAt(int index)
    {
        return m_data[index];
    }

    MapObject& MapLayer::getMapObject(int index)
    {
        return m_objects[index];
    }

    bool MapLayer::isVisible() const
    {
        return m_visible;
    }

    bool MapLayer::isTileLayer() const
    {
        // return std::strcmp(m_type.c_str(), "tilelayer");
        return m_type == "tilelayer";
    }

    int MapLayer::getWidth() const
    {
        return m_width;
    }

    int MapLayer::getHeight() const
    {
        return m_height;
    }

    int MapLayer::getX() const
    {
        return m_x;
    }

    int MapLayer::getY() const
    {
        return m_y;
    }

    const std::vector<int>& MapLayer::getData() const
    {
        return m_data;
    }

    const std::vector<MapObject>& MapLayer::getObjects() const
    {
        return m_objects;
    }

    const std::string& MapLayer::getName() const
    {
        return m_name;
    }

    const std::string& MapLayer::getType() const
    {
        return m_type;
    }

    MapProperties& MapLayer::getProperties()
    {
        return m_properties;
    }

    void MapLayer::setData(unsigned int x, unsigned int y, int value)
    {
        m_data[y * m_width + x] = value;
    }

    void MapLayer::addMapObject(MapObject object)
    {
        m_objects.push_back(object);
    }
}