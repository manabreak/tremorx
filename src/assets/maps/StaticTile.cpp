#include "stdafx.h"
#include "StaticTile.h"

namespace Tremor
{
    StaticTile::StaticTile(Texture& texture, int x, int y, int width, int height, int id)
        : m_region(TextureRegion(texture, x, y, width, height)),
        m_id(id),
        m_offsetX(0.f),
        m_offsetY(0.f)
    {

    }

    StaticTile::~StaticTile()
    {
    }

    int StaticTile::getID()
    {
        return m_id;
    }

    void StaticTile::setID(int id)
    {
        m_id = id;
    }

    TextureRegion& StaticTile::getTextureRegion()
    {
        return m_region;
    }

    float StaticTile::getOffsetX()
    {
        return m_offsetX;
    }

    float StaticTile::getOffsetY()
    {
        return m_offsetY;
    }

    void StaticTile::setOffsetX(float offsetX)
    {
        m_offsetX = offsetX;
    }

    void StaticTile::setOffsetY(float offsetY)
    {
        m_offsetY = offsetY;
    }

    void StaticTile::setOffset(float offsetX, float offsetY)
    {
        m_offsetX = offsetX;
        m_offsetY = offsetY;
    }
}