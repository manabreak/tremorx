#pragma once

#include <map>
#include <string>
#include <rapidjson/document.h>

using namespace rapidjson;

namespace Tremor
{
    class MapProperties
    {
    public:
        MapProperties()
        {

        }

        MapProperties(const Value& props)
        {
            for (Value::ConstMemberIterator itr = props.MemberBegin(); itr != props.MemberEnd(); ++itr)
            {
                if (itr->value.IsString())
                {
                    // Integer
                    try
                    {
                        int val = std::stoi(itr->value.GetString());
                        pushInt(itr->name.GetString(), val);
                        continue;
                    }
                    catch (std::invalid_argument e) {}

                    // Float
                    try
                    {
                        float val = std::stof(itr->value.GetString());
                        pushFloat(itr->name.GetString(), val);
                        continue;
                    }
                    catch (std::invalid_argument e) {}

                    // Boolean
                    if (itr->value.GetString() == "true")
                    {
                        pushBool(itr->name.GetString(), true);
                    }
                    else if (itr->value.GetString() == "false")
                    {
                        pushBool(itr->name.GetString(), false);
                    }
                    // String
                    else
                    {
                        pushStr(itr->name.GetString(), itr->value.GetString());
                    }
                }
                else if (itr->value.IsInt()) pushInt(itr->name.GetString(), itr->value.GetInt());
                else if (itr->value.IsDouble()) pushFloat(itr->name.GetString(), (float) itr->value.GetDouble());
                else if (itr->value.IsBool()) pushBool(itr->name.GetString(), itr->value.GetBool());
            }
        }

        float getFloat(const std::string& key) { return floatProps[key]; }
        int getInt(const std::string& key) { return intProps[key]; }
        const std::string& getString(const std::string& key) { return strProps[key]; }
        bool getBool(const std::string& key) { return boolProps[key]; }

        void pushBool(const std::string& key, bool value) { boolProps[key] = value; }
        void pushInt(const std::string& key, int value) { intProps[key] = value; }
        void pushFloat(const std::string& key, float value) { floatProps[key] = value; }
        void pushStr(const std::string& key, const std::string& value) { strProps[key] = value; }

        bool hasFloat(const std::string& name)
        {
            std::map<std::string, float>::iterator it = floatProps.find(name);
            if (it != floatProps.end()) return true;
            return false;
        }

        bool hasProperty(const std::string& name)
        {
            if (intProps.find(name) != intProps.end()) return true;
            if (strProps.find(name) != strProps.end()) return true;
            if (floatProps.find(name) != floatProps.end()) return true;
            if (boolProps.find(name) != boolProps.end()) return true;
            return false;
        }
    private:
        std::map<std::string, int> intProps;
        std::map<std::string, std::string> strProps;
        std::map<std::string, float> floatProps;
        std::map<std::string, bool> boolProps;
    };
}