#pragma once

#include "2d/TextureRegion.h"

namespace Tremor
{
    class StaticTile
    {
    public:
        StaticTile(Texture& texture, int x, int y, int width, int height, int id);
        ~StaticTile();

        int getID();
        void setID(int id);
        TextureRegion& getTextureRegion();
        float getOffsetX();
        float getOffsetY();
        void setOffsetX(float offsetX);
        void setOffsetY(float offsetY);
        void setOffset(float offsetX, float offsetY);
    private:
        int m_id;
        TextureRegion m_region;
        float m_offsetX;
        float m_offsetY;
    };
}