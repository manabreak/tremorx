#include "stdafx.h"
#include "TileSet.h"
#include "../AssetManager.h"

using namespace rapidjson;

namespace Tremor
{
    TileSet::TileSet(unsigned int firstGid, const std::string& imagefile, unsigned int imgWidth, unsigned int imgHeight, unsigned int margin, const std::string& name, unsigned int spacing, unsigned int tileWidth, unsigned int tileHeight)
        : m_firstgid(firstGid),
        m_image(imagefile),
        m_imagewidth(imgWidth),
        m_imageheight(imgHeight),
        m_margin(margin),
        m_name(name),
        m_spacing(spacing),
        m_tilewidth(tileWidth),
        m_tileheight(tileHeight),
        m_texture(AssetManager::loadTexture(imagefile))
    {
        createTiles();
    }

    TileSet::TileSet(const Value& tileset)
        : m_firstgid(tileset["firstgid"].GetUint()),
        m_image(tileset["image"].GetString()),
        m_imagewidth(tileset["imagewidth"].GetUint()),
        m_imageheight(tileset["imageheight"].GetUint()),
        m_margin(tileset["margin"].GetUint()),
        m_name(tileset["name"].GetString()),
        m_spacing(tileset["spacing"].GetUint()),
        m_tilewidth(tileset["tilewidth"].GetUint()),
        m_tileheight(tileset["tileheight"].GetUint()),
        m_texture(AssetManager::loadTexture(tileset["image"].GetString()))
    {
        if (tileset.HasMember("properties"))
        {
            const Value& props = tileset["properties"];
            for (Value::ConstMemberIterator itr = props.MemberBegin(); itr != props.MemberEnd(); ++itr)
            {
                int id = itr->name.GetInt();
                m_tileprops.insert(std::pair<int, MapProperties>(itr->name.GetInt(), MapProperties(itr->value)));
            }
        }

        createTiles();
    }

    void TileSet::createTiles()
    {
        unsigned int id = m_firstgid;
        unsigned int stopWidth = m_texture.getWidth() - m_tilewidth;
        unsigned int stopHeight = m_texture.getHeight() - m_tileheight;
        for (int y = m_margin; y <= stopHeight; y += m_tileheight + m_spacing)
        {
            for (int x = m_margin; x <= stopWidth; x += m_tilewidth + m_spacing)
            {
                m_tiles.emplace_back(m_texture, x, y, m_tilewidth, m_tileheight, id++);
            }
        }
    }

    TileSet::~TileSet()
    {
        
    }

    Texture& TileSet::getTexture()
    {
        return m_texture;
    }

    StaticTile& TileSet::getTile(unsigned int i)
    {
        return m_tiles[i - m_firstgid];
    }

    unsigned int TileSet::getFirstGID()
    {
        return m_firstgid;
    }

    void TileSet::addTileProperties(unsigned int id, MapProperties props)
    {
        m_tileprops[id] = props;
    }

    MapProperties& TileSet::getTileProperties(unsigned int id)
    {
        return m_tileprops[id];
    }
}