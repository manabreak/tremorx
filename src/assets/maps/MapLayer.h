#pragma once

#include "MapObject.h"
#include "StaticTile.h"
#include "MapProperties.h"
#include <string>
#include <vector>
#include <rapidjson/document.h>

namespace Tremor
{
    enum MapLayerType
    {
        TileLayer,
        ObjectGroup
    };

    class MapLayer
    {
    public:
        MapLayer();
        MapLayer(const std::string& name, int x, int y, int width, int height, MapLayerType type);
        MapLayer(const rapidjson::Value& layer);
        ~MapLayer();
        bool isVisible() const;
        bool isTileLayer() const;
        int getWidth() const;
        int getHeight() const;
        int getX() const;
        int getY() const;
        const std::vector<int>& getData() const;
        const std::vector<MapObject>& getObjects() const;
        const std::string& getName() const;
        const std::string& getType() const;
        MapProperties& getProperties();

        int getDataCount();
        int getObjectCount();

        int getDataAt(int index);
        MapObject& getMapObject(int index);

        void setData(unsigned int x, unsigned int y, int value);
        void addMapObject(MapObject object);
    private:
        std::string m_name;
        std::string m_type;
        int m_width;
        int m_height;
        int m_x;
        int m_y;
        float m_opacity;
        bool m_visible;
        std::vector<int> m_data;
        std::vector<MapObject> m_objects;
        MapProperties m_properties;
    };
}