#pragma once

#include <string>
#include <rapidjson/document.h>
#include "MapProperties.h"

namespace Tremor
{
    class MapObject
    {
    public:
        MapObject(const std::string& name, const std::string& type, int x, int y, int width, int height);
        MapObject(const rapidjson::Value& object);
        ~MapObject();
        int getX() const;
        int getY() const;
        int getWidth() const;
        int getHeight() const;
        const std::string& getName() const;
        const std::string& getType() const;
        bool isVisible() const;
        MapProperties& getProperties();
    private:
        int m_x;
        int m_y;
        int m_width;
        int m_height;
        std::string m_name;
        std::string m_type;
        bool m_visible;
        MapProperties m_properties;
    };
}