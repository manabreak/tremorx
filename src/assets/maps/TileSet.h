#pragma once

#include <string>
#include <vector>
#include "MapProperties.h"
#include "StaticTile.h"
#include <rapidjson/document.h>
#include "2d/Texture.h"

namespace Tremor
{
    class TileSet
    {
    public:
        TileSet(unsigned int firstGid, const std::string& imagefile, unsigned int imgWidth, unsigned int imgHeight, unsigned int margin, const std::string& name, unsigned int spacing, unsigned int tileWidth, unsigned int tileHeight);
        TileSet(const rapidjson::Value& tileset);
        ~TileSet();
        StaticTile& getTile(unsigned int i);
        unsigned int getFirstGID();

        Texture& getTexture();
        void addTileProperties(unsigned int id, MapProperties props);
        MapProperties& getTileProperties(unsigned int id);
    private:
        void createTiles();
        std::vector<StaticTile> m_tiles;
        unsigned int m_firstgid;
        std::string m_image;
        unsigned int m_imagewidth;
        unsigned int m_imageheight;
        Texture& m_texture;
        unsigned int m_margin;
        std::string m_name;
        unsigned int m_spacing;
        unsigned int m_tilewidth;
        unsigned int m_tileheight;
        std::map<unsigned int, MapProperties> m_tileprops;
    };
}