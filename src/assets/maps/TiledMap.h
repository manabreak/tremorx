#pragma once

#include "MapLayer.h"
#include <vector>
#include <rapidjson/document.h>
#include "TileSet.h"

namespace Tremor
{

    class TiledMap
    {
    public:
        TiledMap(const std::string& filename, int width, int height, int version, const std::string& orientation, int tileWidth, int tileHeight);
        ~TiledMap();

        const std::string& getFilename();
        std::vector<MapLayer>& getLayers();
        std::vector<TileSet>& getTilesets();
        int getTileWidth();
        int getTileHeight();
        int getVersion();
        int getWidth();
        int getHeight();
        const std::string& getOrientation();
        TileSet& getTileset(int gid);

        int getLayerCount();
        MapLayer& getLayer(int index);

        void addLayers(const std::vector<MapLayer>& layers);
        void addLayer(MapLayer layer);

        void addTilesets(const std::vector<TileSet>& tilesets);
        void addTileset(TileSet tileset);
    private:
        int m_tilewidth;
        int m_tileheight;
        int m_version;
        int m_width;
        int m_height;
        std::string m_orientation;
        std::vector<MapLayer> m_layers;
        std::vector<TileSet> m_tilesets;
        std::string m_filename;
    };
}