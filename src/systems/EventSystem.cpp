#include "stdafx.h"
#include "EventSystem.h"
#include "components/Event.h"

namespace Tremor
{
    EventSystem::EventSystem()
        : Tecs::System(Tecs::Aspect::createAspectForAll<Event>())
    {

    }
}