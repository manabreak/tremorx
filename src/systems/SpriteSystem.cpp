#include "stdafx.h"
#include "SpriteSystem.h"
#include "Components/Transform.h"
#include "components/Sprite.h"
#include "core/Tremor.h"
#include "assets/AssetManager.h"
#include <World.h>

namespace Tremor
{
    SpriteSystem::SpriteSystem()
        : Tecs::System(Tecs::Aspect::createAspectForAll<Transform, Sprite>()),
        m_enabled(true)
    {

    }

    SpriteSystem::SpriteSystem(DeferredRenderer& deferredRenderer)
        : Tecs::System(Tecs::Aspect::createAspectForAll<Transform, Sprite>()),
        m_enabled(true),
        m_spriteBatch(deferredRenderer)
    {

    }

    SpriteSystem::SpriteSystem(ForwardRenderer& forwardRenderer)
        : Tecs::System(Tecs::Aspect::createAspectForAll<Transform, Sprite>()),
        m_enabled(true),
        m_spriteBatch(forwardRenderer)
    {
        
    }

    SpriteSystem::~SpriteSystem()
    {

    }

    bool SpriteSystem::shouldProcess()
    {
        return m_enabled;
    }

    void SpriteSystem::begin()
    {
        m_spriteBatch.begin();
    }

    void SpriteSystem::processEntities(const std::vector<unsigned int>& entities, float delta)
    {
        for (int i = 0; i < entities.size(); ++i)
        {
            Transform& t = m_world->getComponentManager().getComponent<Transform>(entities[i]);
            Sprite& s = m_world->getComponentManager().getComponent<Sprite>(entities[i]);
            Texture* texture = &s.getTexture();

            s.applyTransform(t, false);
            m_spriteBatch.draw(s);
        }
    }

    void SpriteSystem::end()
    {
        m_spriteBatch.end();
    }
}
