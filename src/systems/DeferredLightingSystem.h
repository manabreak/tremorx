#pragma once

#include <System.h>
#include "rendering/DeferredRenderer.h"

namespace Tremor
{
    class DeferredLightingSystem : public Tecs::System
    {
    public:
        DeferredLightingSystem(DeferredRenderer& renderer);
        ~DeferredLightingSystem();
        virtual void begin();
        virtual void end();
        virtual bool shouldProcess() { return true; }
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);

        void setAmbientLight(float r, float g, float b);
    private:
        DeferredRenderer& m_renderer;
    };
}