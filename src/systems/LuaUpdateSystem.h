#pragma once
#include <vector>
#include <System.h>

extern "C"
{
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}
#include <LuaBridge/LuaBridge.h>

namespace Tremor
{
    class LuaUpdateScript;

    // Simple system that calls an 'update' function on Lua scripts.
    // Useful for AI scripts etc.
    class LuaUpdateSystem : public Tecs::System
    {
    public:
        LuaUpdateSystem(lua_State* L);

        virtual bool shouldProcess() { return true; }
        virtual void begin() {}
        virtual void end() {}

        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);

        virtual void inserted(unsigned int entity);
        virtual void removed(unsigned int entity);
    private:
        lua_State* m_L;
        std::vector<unsigned int> m_entitiesWithScripts;
        std::vector<LuaUpdateScript*> m_scripts;
    };
}