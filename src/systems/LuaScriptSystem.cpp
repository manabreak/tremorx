#include "stdafx.h"
#include "LuaScriptSystem.h"
#include "components/LuaScript.h"
#include "utils/LuaExposer.h"
#include <World.h>

using namespace luabridge;

namespace Tremor
{
    lua_State* LuaScriptSystem::L;

    LuaScriptSystem::LuaScriptSystem()
        : Tecs::System(Tecs::Aspect::createAspectForAll<LuaScript>())
    {
        L = luaL_newstate();
        luaL_openlibs(L);
        LuaExposer exposer(*this);
    }

    LuaScriptSystem::~LuaScriptSystem()
    {

    }

    lua_State* LuaScriptSystem::getL()
    {
        return L;
    }

    void LuaScriptSystem::executeScript(const char* script)
    {
        luaL_dostring(L, script);
    }

    void LuaScriptSystem::processEntities(const std::vector<unsigned int>& entities, float delta)
    {
        for (unsigned int i = 0; i < entities.size(); ++i)
        {
            

            // Tecs::Entity& entity = m_world->getEntity(entities[i]);

            // LuaScript& script = entity.getComponent<LuaScript>();
            LuaScript& script = m_world->getComponentManager().getComponent<LuaScript>(entities[i]);
            // lua_State* L = script.getLuaState();

            luaL_dostring(L, script.getScript());
        }
    }
}