#pragma once

#include <System.h>
#include "3d/Mesh.h"
#include "2d/Texture.h"
#include "rendering/DeferredRenderer.h"
#include "rendering/ForwardRenderer.h"
#include "rendering/SpriteBatch.h"

namespace Tremor
{
    class SpriteSystem : public Tecs::System
    {
    public:
        SpriteSystem();
        SpriteSystem(DeferredRenderer& deferredRenderer);
        SpriteSystem(ForwardRenderer& forwardRenderer);
        ~SpriteSystem();

    protected:
        virtual void begin();
        virtual void end();
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);
        virtual bool shouldProcess();
    private:
        bool m_enabled;
        SpriteBatch m_spriteBatch;
    };
}