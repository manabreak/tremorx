#pragma once
#include <System.h>
#include <LuaBridge/LuaBridge.h>
extern "C"
{
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

namespace Tremor
{
    class LuaScriptSystem : public Tecs::System
    {
    public:
        LuaScriptSystem();
        ~LuaScriptSystem();
        virtual void begin() {}
        virtual void end() {}
        virtual bool shouldProcess() { return true; }
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);

        static lua_State* getL();
        static void executeScript(const char* script);
    private:
        static lua_State* L;
    };
}