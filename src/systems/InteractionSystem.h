#pragma once

#include <System.h>

namespace Tremor
{
    class InteractionSystem : public Tecs::System
    {
    public:
        InteractionSystem();

        virtual bool shouldProcess() { return true; }
        virtual void begin();
        virtual void end();
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);
    };
}