#include "stdafx.h"
#include "ModelSystem.h"
#include "components/Transform.h"
#include "components/ModelComponent.h"
#include <World.h>

namespace Tremor
{
    ModelSystem::ModelSystem(DeferredRenderer& renderer)
        : Tecs::System(Tecs::Aspect::createAspectForAll<Transform, ModelComponent>()),
        m_renderer(renderer)
    {
        setPassive(true);
    }

    ModelSystem::~ModelSystem()
    {

    }

    void ModelSystem::begin()
    {

    }

    void ModelSystem::end()
    {

    }

    void ModelSystem::processEntities(const std::vector<unsigned int>& entities, float delta)
    {
        for (unsigned int i = 0; i < entities.size(); ++i)
        {
            Transform& transform = m_world->getComponentManager().getComponent<Transform>(entities[i]);
            ModelComponent& mc = m_world->getComponentManager().getComponent<ModelComponent>(entities[i]);

            if (mc.hasNormalMap())
            {
                m_renderer.render(transform.getWorldTransform(), mc.getMesh(), mc.getColorMap(), mc.getNormalMap());
            }
            else
            {
                m_renderer.render(transform.getWorldTransform(), mc.getMesh(), mc.getColorMap());
            }
        }
    }

    void ModelSystem::renderForShadows(ShaderProgram& shader)
    {
        int worldLoc = shader.getUniformLocation("u_world");

        const std::vector<unsigned int>& actives = getActives();
        for (unsigned int i = 0; i < actives.size(); ++i)
        {
            Transform& transform = m_world->getComponentManager().getComponent<Transform>(actives[i]);
            ModelComponent& mc = m_world->getComponentManager().getComponent<ModelComponent>(actives[i]);

            shader.setUniform(worldLoc, transform.getWorldTransform());
            mc.getMesh().render();
        }
    }
}