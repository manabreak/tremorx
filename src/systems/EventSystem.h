#pragma once
#include <System.h>

namespace Tremor
{
    class EventSystem : public Tecs::System
    {
    public:
        EventSystem();
        ~EventSystem();

        virtual void begin() {}
        virtual void end() {}
        virtual bool shouldProcess() { return true; }
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);
    };
}