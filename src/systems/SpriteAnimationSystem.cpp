#include "stdafx.h"
#include "SpriteAnimationSystem.h"
#include "components/Sprite.h"
#include "components/SpriteAnimation.h"
#include <World.h>

namespace Tremor
{
    SpriteAnimationSystem::SpriteAnimationSystem()
        : Tecs::System(Tecs::Aspect::createAspectForAll<Sprite, SpriteAnimation>())
    {

    }

    SpriteAnimationSystem::~SpriteAnimationSystem()
    {

    }

    void SpriteAnimationSystem::begin()
    {

    }

    void SpriteAnimationSystem::end()
    {

    }

    void SpriteAnimationSystem::processEntities(const std::vector<unsigned int>& entities, float delta)
    {
        for (unsigned int i = 0; i < entities.size(); ++i)
        {
            // Tecs::Entity& e = m_world->getEntity(entities[i]);
            // Sprite& sprite = e.getComponent<Sprite>();
            Sprite& sprite = m_world->getComponentManager().getComponent<Sprite>(entities[i]);
            SpriteAnimation& animation = m_world->getComponentManager().getComponent<SpriteAnimation>(entities[i]);

            if (animation.update(delta))
            {
                SpriteAnimation::Take& currentTake = animation.getTakes()[animation.getCurrentTake()];
                int frame = animation.getCurrentFrame();

                int width = currentTake.getFrameWidth();
                int height = currentTake.getFrameHeight();
                // int row = currentTake.getStartRow();
                // int col = currentTake.getStartColumn();
                int startX = currentTake.getStartX();
                int startY = currentTake.getStartY();

                /*
                int x = width * col;
                int y = height * row;
                int dx = frame * width;
                int dy = frame * height;

                sprite.setSourceRectangle(x + dx, y, width, height);
                */

                int x = startX + frame * width;
                int y = startY;
                sprite.setSourceRectangle(x, y, width, height);
            }
        }
    }
}