#pragma once
#include <System.h>
#include <vector>
#include <box2d/Box2D.h>

namespace Tremor
{
    class PhysicsBody;

    struct Collision
    {
    public:
        Collision(PhysicsBody& bodyA, PhysicsBody& bodyB)
            : m_bodyA(bodyA),
            m_bodyB(bodyB)
        {

        }

        PhysicsBody& m_bodyA;
        PhysicsBody& m_bodyB;
    };

    class Physics : public Tecs::System, public b2ContactListener
    {
    public:
        Physics();
        ~Physics();

        virtual void begin();
        virtual void end();
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);
        virtual bool shouldProcess() { return true; }

        virtual void inserted(unsigned int entity);
        virtual void removed(unsigned int entity);

        b2World& getb2World();
        void setGravity(float x, float y);

        virtual void BeginContact(b2Contact* contact);
        virtual void EndContact(b2Contact* contact);

        PhysicsBody* getBodyAt(const glm::vec3& point);
    private:
        b2World m_b2world;
        std::vector<Collision> m_contacts;
    };
}