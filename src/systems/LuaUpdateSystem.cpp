#include "stdafx.h"
#include "LuaUpdateSystem.h"
#include "components/LuaUpdateScript.h"
#include <World.h>

namespace Tremor
{
    LuaUpdateSystem::LuaUpdateSystem(lua_State* L)
        : Tecs::System(Tecs::Aspect::createAspectForAll<LuaUpdateScript>()),
        m_L(L)
    {

    }

    void LuaUpdateSystem::processEntities(const std::vector<unsigned int>& entities, float delta)
    {
        /*
        for (unsigned int i = 0; i < m_scripts.size(); ++i)
        {
            m_scripts[i]->update(delta);
        }
        */
        for (unsigned int i = 0; i < entities.size(); ++i)
        {
            LuaUpdateScript& script = m_world->getComponentManager().getComponent<LuaUpdateScript>(entities[i]);
            script.update(delta);
        }
    }

    void LuaUpdateSystem::inserted(unsigned int entity)
    {
        LuaUpdateScript& script = m_world->getComponentManager().getComponent<LuaUpdateScript>(entity);
        m_scripts.push_back(&script);
        m_entitiesWithScripts.push_back(entity);
    }

    void LuaUpdateSystem::removed(unsigned int entity)
    {
        for (unsigned int i = 0; i < m_entitiesWithScripts.size(); ++i)
        {
            if (m_entitiesWithScripts[i] == entity)
            {
                std::swap(m_entitiesWithScripts.begin() + i, m_entitiesWithScripts.end());
                m_entitiesWithScripts.pop_back();
                std::swap(m_scripts.begin() + i, m_scripts.end());
                m_scripts.pop_back();
            }
        }
    }
}