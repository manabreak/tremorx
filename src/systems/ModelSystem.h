#pragma once

#include <System.h>
#include "rendering/DeferredRenderer.h"
#include "rendering/ShadowCasterSystem.h"

namespace Tremor
{
    class ModelSystem : public Tecs::System, public ShadowCasterSystem
    {
    public:
        ModelSystem(DeferredRenderer& renderer);
        ~ModelSystem();
        virtual void begin();
        virtual void end();
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);
        virtual bool shouldProcess() { return true; }

        virtual void renderForShadows(ShaderProgram& shader);
    private:
        DeferredRenderer& m_renderer;
    };
}