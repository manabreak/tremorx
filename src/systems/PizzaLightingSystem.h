#pragma once

#include <System.h>
#include <box2d/Box2D.h>
#include "Physics.h"
#include "components/PizzaHull.h"

namespace Tremor
{
    class GBuffer;
    class Camera;

    // System that creates 2D lighting using "pizza" geometries
    class PizzaLightingSystem : public Tecs::System
    {
    public:
        PizzaLightingSystem(GBuffer& gbuffer, int width, int height);

        ~PizzaLightingSystem();

        void setCamera(Camera& camera);

        virtual void begin();
        virtual void end();
        virtual bool shouldProcess() { return true; }
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);

        void addHull(PizzaHull* hull);

        void renderStencil();
        void combine(Texture& base);
    private:
        void init();
        Texture m_fboTexture;
        Texture m_stencilTexture;
        GLuint m_fbo;
        Camera* m_camera;
        GBuffer& m_gbuffer;

        int m_width;
        int m_height;

        std::vector<PizzaHull*> m_hulls;

        std::unique_ptr<Mesh> m_unitQuad;

        ShaderProgram& m_lightShader; // Renders the light
        ShaderProgram& m_shadowShader; // Renders the shadows
        ShaderProgram& m_hullShader; // Renders the hulls
        ShaderProgram& m_combineShader;

        ShaderProgram& m_stencilOutputShader;

        static ShaderProgram& createLightShader();
        static ShaderProgram& createShadowShader();
        static ShaderProgram& createHullShader();

        static ShaderProgram& createCombineShader();

        static ShaderProgram& createStencilOutputShader();

        Texture& m_lightMap;

        std::unique_ptr<Mesh> m_quadMesh;
    };
}