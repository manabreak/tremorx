#pragma once

#include <System.h>

namespace Tremor
{
    class SpriteAnimationSystem : public Tecs::System
    {
    public:
        SpriteAnimationSystem();
        ~SpriteAnimationSystem();
        virtual void begin();
        virtual void end();
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta);
        virtual bool shouldProcess() { return true; }
    private:

    };
}