#include "stdafx.h"
#include "Physics.h"
#include "components/Transform.h"
#include "components/PhysicsBody.h"
#include "World.h"


namespace Tremor
{
    Physics::Physics()
        : Tecs::System(Tecs::Aspect::createAspectForAll<PhysicsBody>()),
        m_b2world(b2Vec2(0.f, 0.f))
    {
        m_b2world.SetContactListener(this);
    }

    Physics::~Physics()
    {

    }

    void Physics::begin()
    {

    }

    void Physics::end()
    {

    }

    void Physics::processEntities(const std::vector<unsigned int>& entities, float delta)
    {
        m_b2world.Step(delta, 5, 5);

        for (unsigned int i = 0; i < m_contacts.size(); ++i)
        {
            PhysicsBody& body1 = m_contacts[i].m_bodyA;
            PhysicsBody& body2 = m_contacts[i].m_bodyB;

            if (body1.getCollisionListener() != nullptr) body1.getCollisionListener()->onCollision(body2);
            if (body2.getCollisionListener() != nullptr) body2.getCollisionListener()->onCollision(body1);
        }

        for (unsigned int i = 0; i < entities.size(); ++i)
        {
            if (m_world->getComponentManager().hasComponent<Transform>(entities[i]))
            {
                PhysicsBody& body = m_world->getComponentManager().getComponent<PhysicsBody>(entities[i]);
                Transform& t = m_world->getComponentManager().getComponent<Transform>(entities[i]);
                t.setPositionXYZ(body.getX(), t.getPosition().y, body.getY());
            }
        }
    }

    PhysicsBody* Physics::getBodyAt(const glm::vec3& point)
    {
        for (unsigned int i = 0; i < getActives().size(); ++i)
        {
            unsigned int e = getActives()[i];
            PhysicsBody& body = m_world->getComponentManager().getComponent<PhysicsBody>(e);
            b2Body* bBody = body.getb2Body();
            b2Fixture* f = bBody->GetFixtureList();
            while (f != nullptr)
            {
                const b2AABB& aabb = f->GetAABB(0);
                const b2Vec2& lower = aabb.lowerBound;
                const b2Vec2& upper = aabb.upperBound;
                if (point.x >= lower.x && point.x <= upper.x &&
                    point.z >= lower.y && point.z <= upper.y) return &body;
                f = f->GetNext();
            }
        }
        return nullptr;
    }

    void Physics::inserted(unsigned int entity)
    {
        PhysicsBody& body = m_world->getComponentManager().getComponent<PhysicsBody>(entity);
        body.create(*this);
    }

    void Physics::removed(unsigned int entity)
    {
        PhysicsBody& body = m_world->getComponentManager().getComponent<PhysicsBody>(entity);
        body.destroy(*this);
    }

    b2World& Physics::getb2World()
    {
        return m_b2world;
    }

    void Physics::setGravity(float x, float y)
    {
        m_b2world.SetGravity(b2Vec2(x, y));
    }

    void Physics::BeginContact(b2Contact* contact)
    {
        PhysicsBody* body1 = static_cast<PhysicsBody*>(contact->GetFixtureA()->GetBody()->GetUserData());
        PhysicsBody* body2 = static_cast<PhysicsBody*>(contact->GetFixtureB()->GetBody()->GetUserData());
        CollisionListener* listener1 = body1->getCollisionListener();
        CollisionListener* listener2 = body2->getCollisionListener();

        if (listener1 != nullptr) listener1->onCollisionBegin(*body2);
        if (listener2 != nullptr) listener2->onCollisionBegin(*body1);

        m_contacts.emplace_back(*body1, *body2);
    }

    void Physics::EndContact(b2Contact* contact)
    {
        PhysicsBody* body1 = static_cast<PhysicsBody*>(contact->GetFixtureA()->GetBody()->GetUserData());
        PhysicsBody* body2 = static_cast<PhysicsBody*>(contact->GetFixtureB()->GetBody()->GetUserData());
        CollisionListener* listener1 = body1->getCollisionListener();
        CollisionListener* listener2 = body2->getCollisionListener();

        if (listener1 != nullptr) listener1->onCollisionEnd(*body2);
        if (listener2 != nullptr) listener2->onCollisionEnd(*body1);

        for (unsigned int i = 0; i < m_contacts.size(); ++i)
        {
            if (&m_contacts[i].m_bodyA == body1 && &m_contacts[i].m_bodyB == body2)
            {
                std::swap(m_contacts.begin() + i, m_contacts.end());
                m_contacts.pop_back();
            }
        }
    }
}