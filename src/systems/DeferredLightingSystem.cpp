#include "stdafx.h"
#include "DeferredLightingSystem.h"
#include "lights/PointLight.h"
#include "lights/SpotLight.h"
#include <World.h>
#include "components/Transform.h"

namespace Tremor
{
    DeferredLightingSystem::DeferredLightingSystem(DeferredRenderer& renderer)
        : Tecs::System(Tecs::Aspect::createAspectForOne<Light>()),
        m_renderer(renderer)
    {
        setPassive(true);
    }

    DeferredLightingSystem::~DeferredLightingSystem()
    {

    }

    void DeferredLightingSystem::setAmbientLight(float r, float g, float b)
    {
        m_renderer.setAmbientLight(r, g, b);
    }

    void DeferredLightingSystem::begin()
    {
        m_renderer.beginLights();
    }

    void DeferredLightingSystem::end()
    {
        m_renderer.endLights();
    }

    void DeferredLightingSystem::processEntities(const std::vector<unsigned int>& entities, float delta)
    {
        Tecs::ComponentManager& cm = m_world->getComponentManager();

        for (unsigned int i = 0; i < entities.size(); ++i)
        {
            if (cm.hasComponent<PointLight>(entities[i]))
            {
                PointLight& l = cm.getComponent<PointLight>(entities[i]);
                if (cm.hasComponent<Transform>(entities[i]))
                {
                    Transform& t = cm.getComponent<Transform>(entities[i]);
                    glm::vec3 p = l.getPosition();
                    glm::vec3 np = p + t.getWorldPosition();
                    l.setFinalPosition(np.x, np.y, np.z);
                    m_renderer.renderLight(l);
                }
                else m_renderer.renderLight(l);
            }
            if (cm.hasComponent<SpotLight>(entities[i]))
            {
                SpotLight& l = cm.getComponent<SpotLight>(entities[i]);
                if (cm.hasComponent<Transform>(entities[i]))
                {
                    Transform& t = cm.getComponent<Transform>(entities[i]);
                    glm::vec3 p = l.getPosition();
                    glm::vec3 np = p + t.getWorldPosition();
                    l.setFinalPosition(np.x, np.y, np.z);
                    m_renderer.renderLight(l);
                }
                else m_renderer.renderLight(l);
            }
        }
    }
}