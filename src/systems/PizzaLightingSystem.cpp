#include "stdafx.h"
#include "PizzaLightingSystem.h"
#include "components/PizzaLight.h"
#include "rendering/Camera.h"
#include <World.h>
#include "utils/MeshFactory.h"
#include "components/Transform.h"
#include "core/Tremor.h"
#include "rendering/GBuffer.h"

namespace Tremor
{
    PizzaLightingSystem::PizzaLightingSystem(GBuffer& gbuffer, int width, int height)
        : Tecs::System(Tecs::Aspect::createAspectForAll<PizzaLight>()),
        m_fboTexture(width, height, TextureFormat::RGB888),
        m_stencilTexture(width, height, TextureFormat::STENCIL),
        m_camera(nullptr),
        m_gbuffer(gbuffer),
        m_width(width),
        m_height(height),
        m_lightShader(createLightShader()),
        m_shadowShader(createShadowShader()),
        m_hullShader(createHullShader()),
        m_combineShader(createCombineShader()),
        m_stencilOutputShader(createStencilOutputShader()),
        m_lightMap(AssetManager::loadTexture("assets/lightmap.png")),
        m_quadMesh(MeshFactory::createQuadXY(2.f, 2.f))
    {
        init();
    }

    void PizzaLightingSystem::init()
    {
        
        glGenFramebuffers(1, &m_fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
        
        // glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, m_stencilTexture.getHandle(), 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fboTexture.getHandle(), 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_gbuffer.getDepthResult().getHandle(), 0);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);

        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to initialize Box2DLightingSystem\n");
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        

        // Create the "unit quad"
        m_unitQuad = std::unique_ptr<Mesh>(MeshFactory::createQuadXZ(1.f, 1.f));
    }

    PizzaLightingSystem::~PizzaLightingSystem()
    {

    }

    void PizzaLightingSystem::setCamera(Camera& camera)
    {
        m_camera = &camera;
        
    }

    void PizzaLightingSystem::begin()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

        glClearColor(1.f, 1.f, 1.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        glViewport(0, 0, m_width, m_height);
    }

    void PizzaLightingSystem::end()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void PizzaLightingSystem::renderStencil()
    {
        glClearColor(0.f, 0.f, 0.f, 0.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glViewport(0, 0, Tremor::graphics->getWidth(), Tremor::graphics->getHeight());
        m_stencilOutputShader.begin();
        m_fboTexture.bind(GL_TEXTURE0);
        glDisable(GL_BLEND);
        m_stencilOutputShader.setUniform("u_base", 0);
        m_quadMesh->render();
        m_stencilOutputShader.end();
    }

    void PizzaLightingSystem::combine(Texture& base)
    {
        
        glClearColor(0.f, 0.f, 0.f, 0.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glViewport(0, 0, Tremor::graphics->getWidth(), Tremor::graphics->getHeight());
        m_combineShader.begin();
        base.bind(GL_TEXTURE0);
        m_fboTexture.bind(GL_TEXTURE1);
        glDisable(GL_BLEND);
        m_combineShader.setUniform("u_base", 0);
        m_combineShader.setUniform("u_overlay", 1);
        m_quadMesh->render();
        m_combineShader.end();
        
    }

    void PizzaLightingSystem::addHull(PizzaHull* hull)
    {
        m_hulls.push_back(hull);
    }

    void PizzaLightingSystem::processEntities(const std::vector<unsigned int>& entities, float delta)
    {
        

        for (unsigned int i = 0; i < entities.size(); ++i)
        {
            if (m_world->getComponentManager().hasComponent<Transform>(entities[i]))
            {
                Transform& t = m_world->getComponentManager().getComponent<Transform>(entities[i]);
                PizzaLight& light = m_world->getComponentManager().getComponent<PizzaLight>(entities[i]);

                glm::vec2 lightPosition = glm::vec2(t.getX(), t.getZ()) + light.getPosition();

                float r = light.getRange();

                glDepthFunc(GL_LESS);                

                m_shadowShader.begin();
                m_shadowShader.setUniform("u_projection", m_camera->getProjection());
                m_shadowShader.setUniform("u_view", m_camera->getView());

                // Render shadows
                for (unsigned int j = 0; j < m_hulls.size(); ++j)
                {
                    if (m_hulls[j] == nullptr || &m_hulls[j]->getMesh() == nullptr) continue;

                    const glm::vec2& pos = m_hulls[j]->getPosition();
                    glm::mat4 world = glm::translate(glm::vec3(pos.x, 0.f, pos.y));
                    m_shadowShader.setUniform("u_world", world);
                    m_shadowShader.setUniform("u_lightPosition", lightPosition);
                    m_hulls[j]->getMesh().render();
                }

                m_shadowShader.end();

                /*
                glStencilFunc(GL_ALWAYS, 1, 0xFF);
                glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
                */
                // Render hulls
                
                glDisable(GL_DEPTH_TEST);
                glDepthFunc(GL_LEQUAL);

                m_hullShader.begin();
                m_hullShader.setUniform("u_projection", m_camera->getProjection());
                m_hullShader.setUniform("u_view", m_camera->getView());

                for (unsigned int j = 0; j < m_hulls.size(); ++j)
                {
                    const glm::vec2& pos = m_hulls[j]->getPosition();
                    glm::mat4 world = glm::translate(glm::vec3(pos.x, 0.f, pos.y));
                    m_hullShader.setUniform("u_world", world);
                    m_hulls[j]->getMesh().render();
                }
                m_hullShader.end();
                glEnable(GL_DEPTH_TEST);


                /*
                glStencilFunc(GL_NEVER, 1, 0xFF);
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
                glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
                */
            
            
                // Render light
                /*
                m_lightShader.begin();
                m_lightShader.setUniform("u_projection", m_camera->getProjection());
                m_lightShader.setUniform("u_view", m_camera->getView());
                m_lightShader.setUniform("u_lightMap", 0);

                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                glm::mat4 world = t.getWorldTransform() * glm::scale(glm::vec3(r, r, r));
                m_lightShader.setUniform("u_world", world);
                m_unitQuad->render();
                
                m_lightShader.end();

                glDisable(GL_STENCIL_TEST);
                */
            }

            
            
        }
    }

    ShaderProgram& PizzaLightingSystem::createLightShader()
    {
        std::string v =
            "#version 330\n"

            "layout(location = 0) in vec3 in_position;\n"
            "layout(location = 1) in vec2 in_uv;\n"

            "uniform mat4 u_world;\n"
            "uniform mat4 u_view;\n"
            "uniform mat4 u_projection;\n"

            "out vec2 uv;\n"

            "void main() {\n"
            "    gl_Position = u_projection * u_view * u_world * vec4(in_position, 1.0);\n"
            "    uv = in_uv;\n"
            "}\n"
            ;

        std::string f =
            "#version 330\n"

            "in vec2 uv;\n"

            "layout(location = 0) out vec4 out_color;\n"

            "uniform vec3 u_lightPosition;\n"
            "uniform sampler2D u_lightMap;\n"

            "void main() {\n"
            "    out_color = texture2D(u_lightMap, uv);\n"
            "    // out_color = vec4(0.5, 0.5, 1, 0.1);\n"
            "    "
            "}\n"
            ;

        return AssetManager::loadShader(v, f);
    }

    ShaderProgram& PizzaLightingSystem::createShadowShader()
    {
        std::string v =
            "#version 330\n"

            "layout(location = 0) in vec3 in_position;\n"
            "layout(location = 3) in vec3 in_nor;\n"

            "uniform mat4 u_world;\n"
            "uniform mat4 u_view;\n"
            "uniform mat4 u_projection;\n"
            "uniform vec2 u_lightPosition;\n"

            "void main() {\n"
            "    vec4 vPos = u_world * vec4(in_position, 1.0);\n"
            "    vec2 xzPos = vec2(vPos.x, vPos.z);\n"
            "    vec2 dir = normalize(xzPos - u_lightPosition);\n"
            "    if(dot(dir, vec2(in_nor.x, in_nor.z)) < 0) {\n"
            "        vPos.x += dir.x * 10000;\n"
            "        vPos.z += dir.y * 10000;\n"
            "    }\n"

            "    gl_Position = u_projection * u_view * vPos;\n"
            "}\n"
            ;

        std::string f =
            "#version 330\n"

            "layout(location = 0) out vec4 out_color;\n"

            "void main() {\n"
            "    out_color = vec4(0.0, 0.0, 0.0, 1);\n"
            "}\n"
            ;

        return AssetManager::loadShader(v, f);
    }

    ShaderProgram& PizzaLightingSystem::createHullShader()
    {
        std::string v =
            "#version 330\n"

            "layout(location = 0) in vec3 in_position;\n"

            "uniform mat4 u_projection;\n"
            "uniform mat4 u_view;\n"
            "uniform mat4 u_world;\n"

            "void main() {\n"
            "    gl_Position = u_projection * u_view * u_world * vec4(in_position + vec3(0, 0.01, 0), 1.0);\n"
            "}\n"
            ;

        std::string f =
            "#version 330\n"

            "layout(location = 0) out vec3 out_color;\n"

            "void main() {\n"
            "    out_color = vec3(1.0);\n"
            "}\n"
            ;

        return AssetManager::loadShader(v, f);
    }

    ShaderProgram& PizzaLightingSystem::createCombineShader()
    {
        std::string v =
            "#version 330\n"

            "layout(location = 0) in vec3 in_position;\n"
            "layout(location = 1) in vec2 in_uv;\n"

            "out vec2 uv;\n"

            "void main() {\n"
            "    gl_Position = vec4(in_position, 1.0);\n"
            "    uv = in_uv;\n"
            "}\n"
            ;

        std::string f =
            "#version 330\n"

            "in vec2 uv;\n"

            "layout(location = 0) out vec3 out_color;\n"

            "uniform sampler2D u_base;\n"
            "uniform sampler2D u_overlay;\n"

            "void main() {\n"
            "    vec4 base = texture2D(u_base, uv);\n"
            "    vec4 overlay = texture2D(u_overlay, uv);\n"
            "    out_color = base.xyz * overlay.xyz;\n"
            "}\n"
            ;

        return AssetManager::loadShader(v, f);
    }

    ShaderProgram& PizzaLightingSystem::createStencilOutputShader()
    {
        std::string v =
            "#version 330\n"

            "layout(location = 0) in vec3 in_position;\n"
            "layout(location = 1) in vec2 in_uv;\n"

            "out vec2 uv;\n"

            "void main() {\n"
            "    gl_Position = vec4(in_position, 1.0);\n"
            "    uv = in_uv;\n"
            "}\n"
            ;

        std::string f =
            "#version 330\n"

            "in vec2 uv;\n"

            "layout(location = 0) out vec3 out_color;\n"

            "uniform sampler2D u_base;\n"

            "void main() {\n"
            "    out_color = texture2D(u_base, uv).xyz;\n"
            "}\n"
            ;

        return AssetManager::loadShader(v, f);
    }
}