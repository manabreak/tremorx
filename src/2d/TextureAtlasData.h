#pragma once

#include <string>
#include <vector>
#include "Enums2D.h"
#include "Texture.h"
#include "AtlasPage.h"
#include "TextureAtlasDataRegion.h"

namespace Tremor
{
    class TextureAtlasData
    {
    public:
        TextureAtlasData(const std::string& packFile, const std::string& imagesDir, bool flip);
        ~TextureAtlasData();
        const std::vector<AtlasPage>& getPages();
        AtlasPage& getPage(unsigned int index);

        const std::vector<TextureAtlasDataRegion>& getRegions();
        TextureAtlasDataRegion& getRegion(unsigned int index);

        unsigned int getPageCount();
        unsigned int getRegionCount();
    private:
        std::vector<AtlasPage> m_pages;
        std::vector<TextureAtlasDataRegion> m_regions;
        std::vector<std::string> m_tuple;
        int readTuple(std::ifstream& ifs);
        std::string readValue(std::ifstream& ifs);
    };
}