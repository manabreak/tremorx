#pragma once

#include <string>
#include "Enums2D.h"

#ifdef __TREMOR_ANDROID__
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#else
// #include "GL\glew.h"
#include <glew/include/GL/glew.h>
#endif

namespace Tremor
{
    class Texture
    {
    public:
        Texture();
        Texture(std::string filename);
        Texture(int width, int height, TextureFormat format);
        ~Texture();
        Texture(const Texture&) = delete;

        void bind() const;
        void bind(int slot) const;
        int getWidth() const;
        int getHeight() const;
        void setFilter(TextureFilter minFilter, TextureFilter magFilter);
        void setWrap(TextureWrap uWrap, TextureWrap vWrap);
        GLuint getHandle();

        bool operator==(const Texture& other) const;
        bool operator!=(const Texture& other) const;

        const std::string& getFilename();
    protected:
        TextureFilter m_minFilter;
        TextureFilter m_magFilter;
        TextureWrap m_uWrap;
        TextureWrap m_vWrap;
        unsigned int m_width;
        unsigned int m_height;
    private:
        GLuint m_target;
        GLuint m_handle;
        std::string m_filename;
    };
}