#pragma once

#ifdef __TREMOR_ANDROID__
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#else
#include <glew/include/GL/glew.h>
#endif

namespace Tremor
{
    enum TextureFilter
    {
        Nearest = GL_NEAREST,
        Linear = GL_LINEAR
    };

    enum TextureWrap
    {
        Repeat = GL_REPEAT,
        ClampToEdge = GL_CLAMP_TO_EDGE
    };

    enum TextureFormat
    {
        RGBA8888 = GL_RGBA,
        RGB888 = GL_RGB,
#ifdef __TREMOR_WIN32__
        DEPTH = GL_DEPTH_COMPONENT,
        STENCIL = GL_DEPTH24_STENCIL8
#elif defined __TREMOR_ANDROID__
        DEPTH = GL_DEPTH_COMPONENT
#endif
    };
}