#include "stdafx.h"
#include "TextureAtlas.h"
#include "TextureAtlasData.h"
#include "components/AtlasSprite.h"
#include <World.h>
#include <Entity.h>

namespace Tremor
{
    TextureAtlas::TextureAtlas(const std::string& packFile)
        : m_filename(packFile)
    {
        TextureAtlasData data = TextureAtlasData(packFile, "./", false);
        load(data);
    }

    TextureAtlas::~TextureAtlas()
    {

    }

    const std::string& TextureAtlas::getFilename()
    {
        return m_filename;
    }

    void TextureAtlas::load(TextureAtlasData& data)
    {
        std::map<AtlasPage*, Texture*> pageToTexture;

        for (unsigned int i = 0; i < data.getPageCount(); ++i)
        {
            AtlasPage& page = data.getPage(i);
            Texture& texture = page.getTexture();

            texture.setFilter(page.getMinFilter(), page.getMagFilter());
            texture.setWrap(page.getWrapU(), page.getWrapV());

            pageToTexture[&page] = &texture;
        }

        for (unsigned int i = 0; i < data.getRegionCount(); ++i)
        {
            TextureAtlasDataRegion& region = data.getRegion(i);
            m_regions.emplace_back(region.getName(), *pageToTexture[&region.getPage()], region.getX(), region.getY(), region.getWidth(), region.getHeight());
        }
    }

    AtlasRegion& TextureAtlas::addRegion(const std::string& name, Texture& texture, int x, int y, int width, int height)
    {
        m_regions.emplace_back(name, texture, x, y, width, height);
        return *m_regions.end();
    }

    AtlasRegion& TextureAtlas::addRegion(const std::string& name, TextureRegion& region)
    {
        m_regions.emplace_back(name, region.getTexture(), region.getRegionX(), region.getRegionY(), region.getRegionWidth(), region.getRegionHeight());
        return *m_regions.end();
    }

    std::vector<AtlasRegion>& TextureAtlas::getRegions()
    {
        return m_regions;
    }

    AtlasRegion& TextureAtlas::findRegion(const std::string& name)
    {
        for (unsigned int i = 0; i < m_regions.size(); ++i)
        {
            if (m_regions[i].getName() == name) return m_regions[i];
        }
    }

    Sprite& TextureAtlas::createSprite(Tecs::Entity& entity, const std::string& name)
    {
        for (unsigned int i = 0; i < m_regions.size(); ++i)
        {
            if (m_regions[i].getName() == name)
            {
                return entity.addComponent<Sprite>(m_regions[i]);
            }
        }
    }
}