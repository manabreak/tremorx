#include "stdafx.h"
#include "Texture.h"
#include "../utils/lodepng.h"
#include "utils/FileUtils.h"

namespace Tremor
{

    Texture::Texture()
        : m_filename("")
    {

    }

    Texture::Texture(std::string filename)
        : m_target(GL_TEXTURE_2D),
        m_filename(filename),
        m_minFilter(TextureFilter::Nearest),
        m_magFilter(TextureFilter::Nearest),
        m_uWrap(TextureWrap::ClampToEdge),
        m_vWrap(TextureWrap::ClampToEdge)
    {
        std::vector<unsigned char> image;
        std::string filepath = filename;
        if (!FileUtils::fileExists(filepath))
        {
            std::cout << "File " << filepath << " does not exist!" << std::endl;
            filepath = std::string("./assets/").append(filename);
            if (!FileUtils::fileExists(filepath))
            {
                std::cout << "File " << filepath << " does not exist!" << std::endl;
            }
        }

        unsigned error = lodepng::decode(image, m_width, m_height, filepath .c_str());
        if (error != 0)
        {
            assert(error == 0);
        }

        glGenTextures(1, &m_handle);
        glBindTexture(m_target, m_handle);
        glTexParameteri(m_target, GL_TEXTURE_MIN_FILTER, m_minFilter);
        glTexParameteri(m_target, GL_TEXTURE_MAG_FILTER, m_magFilter);
        glTexParameteri(m_target, GL_TEXTURE_WRAP_S, m_uWrap);
        glTexParameteri(m_target, GL_TEXTURE_WRAP_T, m_vWrap);
        glTexImage2D(m_target, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

        image.clear();
    }

    Texture::Texture(int width, int height, TextureFormat format)
        : m_target(GL_TEXTURE_2D),
        m_minFilter(TextureFilter::Nearest),
        m_magFilter(TextureFilter::Nearest),
        m_uWrap(TextureWrap::ClampToEdge),
        m_vWrap(TextureWrap::ClampToEdge),
        m_width(width),
        m_height(height),
        m_filename("")
    {
        glGenTextures(1, &m_handle);
        bind();
        glTexParameteri(m_target, GL_TEXTURE_MIN_FILTER, m_minFilter);
        glTexParameteri(m_target, GL_TEXTURE_MAG_FILTER, m_magFilter);
        glTexParameteri(m_target, GL_TEXTURE_WRAP_S, m_uWrap);
        glTexParameteri(m_target, GL_TEXTURE_WRAP_T, m_vWrap);

        switch (format)
        {
        case TextureFormat::DEPTH:
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
            glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
            glTexImage2D(m_target, 0, format, m_width, m_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
            break;
        case TextureFormat::STENCIL:
            glTexImage2D(m_target, 0, format, m_width, m_height, 0, GL_DEPTH24_STENCIL8, GL_UNSIGNED_BYTE, 0);
            break;
        default:
            glTexImage2D(m_target, 0, format, m_width, m_height, 0, format, GL_UNSIGNED_BYTE, 0);
            break;
        }
        // glTexImage2D(m_target, 0, format, m_width, m_height, 0, format == DEPTH ? GL_DEPTH_COMPONENT : format, format == DEPTH ? GL_FLOAT : GL_UNSIGNED_BYTE, 0);
    }

    void Texture::bind() const
    {
        glBindTexture(m_target, m_handle);
    }

    void Texture::bind(int slot) const
    {
        glActiveTexture(slot);
        glBindTexture(m_target, m_handle);
    }

    Texture::~Texture()
    {
        std::cout << "Texture dtor(" << m_filename << ")" << std::endl;
        glDeleteTextures(1, &m_handle);
    }

    int Texture::getWidth() const
    {
        return m_width;
    }

    int Texture::getHeight() const 
    {
        return m_height;
    }

    void Texture::setFilter(TextureFilter minFilter, TextureFilter magFilter)
    {
        if (minFilter != m_minFilter || magFilter != m_magFilter)
        {
            bind();
            m_minFilter = minFilter;
            m_magFilter = magFilter;
            glTexParameteri(m_target, GL_TEXTURE_MIN_FILTER, m_minFilter);
            glTexParameteri(m_target, GL_TEXTURE_MAG_FILTER, m_magFilter);
        }
    }

    void Texture::setWrap(TextureWrap uWrap, TextureWrap vWrap)
    {
        if (m_uWrap != uWrap || m_vWrap != vWrap)
        {
            bind();
            m_uWrap = uWrap;
            m_vWrap = vWrap;
            glTexParameteri(m_target, GL_TEXTURE_WRAP_S, m_uWrap);
            glTexParameteri(m_target, GL_TEXTURE_WRAP_T, m_vWrap);
        }
    }

    GLuint Texture::getHandle()
    {
        return m_handle;
    }

    bool Texture::operator==(const Texture& other) const
    {
        return m_handle == other.m_handle;
    }

    bool Texture::operator!=(const Texture& other) const
    {
        return m_handle != other.m_handle;
    }

    const std::string& Texture::getFilename()
    {
        return m_filename;
    }
}