#include "stdafx.h"
#include "AtlasRegion.h"

namespace Tremor
{
    AtlasRegion::AtlasRegion(const std::string& name, Texture& texture, int x, int y, int width, int height)
        : TextureRegion(texture, x, y, width, height),
        m_name(name),
        m_originalWidth(width),
        m_originalHeight(height),
        m_packedWidth(width),
        m_packedHeight(height),
        m_offsetX(0.f),
        m_offsetY(0.f),
        m_rotate(false)
    {

    }

    AtlasRegion::~AtlasRegion()
    {

    }

    void AtlasRegion::flip(bool x, bool y)
    {
        TextureRegion::flip(x, y);
        if (x)
        {
            m_offsetX = m_originalWidth - m_offsetX - getRotatedPackedWidth();
        }
        if (y)
        {
            m_offsetY = m_originalHeight - m_offsetY - getRotatedPackedHeight();
        }
    }

    float AtlasRegion::getRotatedPackedWidth()
    {
        return (float) (m_rotate ? m_packedHeight : m_packedWidth);
    }

    float AtlasRegion::getRotatedPackedHeight()
    {
        return (float) (m_rotate ? m_packedWidth : m_packedHeight);
    }

    const std::string& AtlasRegion::getName()
    {
        return m_name;
    }

    int AtlasRegion::getOriginalWidth()
    {
        return m_originalWidth;
    }

    int AtlasRegion::getOriginalHeight()
    {
        return m_originalHeight;
    }

    int AtlasRegion::getPackedWidth()
    {
        return m_packedWidth;
    }

    int AtlasRegion::getPackedHeight()
    {
        return m_packedHeight;
    }

    bool AtlasRegion::shouldRotate()
    {
        return m_rotate;
    }

    float AtlasRegion::getOffsetX()
    {
        return m_offsetX;
    }

    float AtlasRegion::getOffsetY()
    {
        return m_offsetY;
    }

    void AtlasRegion::setOffsetX(float x)
    {
        m_offsetX = x;
    }

    void AtlasRegion::setOffsetY(float y)
    {
        m_offsetY = y;
    }
}