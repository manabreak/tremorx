#pragma once

#include "TextureRegion.h"

namespace Tremor
{
    class AtlasRegion : public TextureRegion
    {
    public:
        AtlasRegion(const std::string& name, Texture& texture, int x, int y, int width, int height);
        ~AtlasRegion();
        void flip(bool x, bool y);
        float getRotatedPackedWidth();
        float getRotatedPackedHeight();

        const std::string& getName();
        int getOriginalWidth();
        int getOriginalHeight();
        int getPackedWidth();
        int getPackedHeight();
        bool shouldRotate();

        float getOffsetX();
        float getOffsetY();

        void setOffsetX(float x);
        void setOffsetY(float y);
    private:
        std::string m_name;
        float m_offsetX;
        float m_offsetY;
        int m_packedWidth;
        int m_packedHeight;
        int m_originalWidth;
        int m_originalHeight;
        bool m_rotate;
    };

}