#pragma once

#include <string>
#include <vector>
#include "3d/Vertex.h"

namespace Tremor
{
    class BitmapFont;
    class SpriteBatch;

    class Text2D
    {
    public:
        Text2D(BitmapFont& font);
        Text2D(BitmapFont& font, const std::string& text);

        void setText(const std::string& text);
        const std::string& getText();

        void setFont(BitmapFont& font);
        BitmapFont& getFont();

        int getLength();

        float getWidth();
        float getHeight();

        float getRotation();
        void setRotation(float rotation);

        void setVisible(bool visible);
        bool isVisible();

        void setRGB(float r, float g, float b);
        void setRGBA(float r, float g, float b, float a);
        void setAlpha(float a);

        float getR();
        float getG();
        float getB();
        float getAlpha();

        void setScale(float scale);
        void setScaleX(float x);
        void setScaleY(float y);

        float getScaleX();
        float getScaleY();

        void setPosition(float x, float y);
        void setX(float x);
        void setY(float y);
        float getX();
        float getY();

        bool isUniformScale();

        const std::vector<Vertex>& getVertices();
        const std::vector<unsigned short>& getIndices();

        void refresh();
        void refreshScale(const glm::vec2& oldScale);

        void render(SpriteBatch& batch);
    private:
        BitmapFont& m_font;
        std::string m_text;

        bool m_visible;
        float m_rotation;

        glm::vec2 m_scale;
        glm::vec2 m_position;
        glm::vec4 m_color;
        glm::vec2 m_size;

        std::vector<Vertex> m_vertices;
        std::vector<unsigned short> m_indices;
    };
}