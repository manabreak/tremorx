#pragma once

#ifdef __TREMOR_WIN32__
#include <glew/include/GL/glew.h>
#endif

#include <string>
#include <map>
#include <vector>
// #include "../opengl/TRTexture.h"
#include "Texture.h"
#include "../3d/Vertex.h"

#ifndef MAKE_RGBA
#define MAKE_RGBA(r,g,b,a)  (r | (g << 8) | (b << 16) | (a << 24))
#endif

#ifndef GET_BLUE
#define GET_BLUE(rgba) (( (rgba)>>16 ) & 0xff )
#endif

#ifndef GET_GREEN
#define GET_GREEN(rgba) (( (rgba)>>8 ) & 0xff )
#endif

#ifndef GET_RED
#define GET_RED(rgba) ( rgba & 0xff )
#endif

#ifndef GET_ALPHA
#define GET_ALPHA(rgba) (( (rgba)>>24 ) & 0xff)
#endif

namespace Tremor
{
    class BitmapFont
    {
    private:
        struct txdata
        {
            float m_x, m_y;
            float m_tx, m_ty;
            GLubyte m_r, m_g, m_b, m_a;
            txdata(float x, float y, float tx, float ty, GLubyte r, GLubyte g, GLubyte b, GLubyte a)
            {
                m_x = x;
                m_y = y;
                m_tx = tx;
                m_ty = ty;
                m_r = r;
                m_g = g;
                m_b = b;
                m_a = a;
            }
        };

        class KearningInfo
        {
        public:
            short m_first;
            short m_second;
            short m_amount;

            KearningInfo() : m_first(0), m_second(0), m_amount(0)  { }
        };

        class CharDescriptor
        {

        public:
            short m_x, m_y;
            short m_width;
            short m_height;
            short m_xOffset;
            short m_yOffset;
            short m_xAdvance;
            short m_page;

            CharDescriptor() : m_x(0), m_y(0), m_width(0), m_height(0), m_xOffset(0), m_yOffset(0),
                m_xAdvance(0), m_page(0)
            { }
        };

    public:
        BitmapFont(const std::string& fontFile);
        BitmapFont(const BitmapFont&) = delete;
        ~BitmapFont() {}

        void setColor(int r, int g, int b, int a) { m_fcolor = MAKE_RGBA(r, g, b, a); }
        void setBlend(int b) { m_fblend = b; }
        void setScale(float scale){ m_fscale = scale; }
        float getHeight(){ return m_lineHeight * m_fscale; }
        void useKerning(bool b) { m_useKern = b; }
        void print(float, float, const char *, ...);
        void printCenter(float, const char *);
        float getStringWidth(const std::string& text);

        const std::string& getName() { return m_name; }
        const std::string& getFilename() { return m_fontfilename; }

        std::vector<Vertex> getVerticesFor(const std::string& text, int x, int y);
        void getVerticesFor(const std::string& text, float x, float y, std::vector<Vertex>& vertices, std::vector<unsigned short>& indices);
        void appendVerticesFor(const std::string& text, int x, int y, std::vector<Vertex>& vertices);

        Texture& getTexture() { return *m_texture; }
    private:
        bool parseFont(const std::string& fontfile);

        std::string m_fontfilename;
        std::string m_name;
        short m_lineHeight;
        short m_base;
        short m_width;
        short m_height;
        short m_pages;
        short m_outline;
        short m_kernCount;
        bool m_useKern;
        std::map<int, BitmapFont::CharDescriptor> m_chars;
        std::vector<BitmapFont::KearningInfo> m_kearn;
        int m_fcolor;
        GLuint m_ftexid;
        float m_fscale;
        int m_fblend;
        void BitmapFont::ReplaceExtension(std::string& str, std::string rep);
        char* BitmapFont::replace_str(char* str, char* orig, char* rep);
        void renderString(int len);
        int getKerningPair(int first, int second);
        std::vector<txdata> m_txlist;

        Texture* m_texture;
    };
}