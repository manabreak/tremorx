#include "stdafx.h"

#include "BitmapFont.h"
#include <fstream>
#include <sstream>
#include "../assets/AssetManager.h"

namespace Tremor
{
    BitmapFont::BitmapFont(const std::string& fontFile)
        : m_fontfilename(fontFile)
    {
        setColor(255, 255, 255, 255);
        m_kernCount = 0;
        m_ftexid = -1;
        m_fblend = 0;
        m_fscale = 1.f;
        m_useKern = true;

        parseFont(fontFile);

        m_name = fontFile.substr(0, fontFile.rfind('.'));
        std::string textureFileName = m_name;
        textureFileName.append("_0.png");

        m_texture = &AssetManager::loadTexture(textureFileName);
        m_texture->setFilter(TextureFilter::Linear, TextureFilter::Linear);
    }

    bool BitmapFont::parseFont(const std::string& fontfile)
    {
        std::ifstream stream(fontfile);
        std::string line;
        std::string read;
        std::string key;
        std::string value;
        std::size_t i;
        int first, second, amount;

        KearningInfo k;
        CharDescriptor c;

        while (!stream.eof())
        {
            std::stringstream lineStream;
            std::getline(stream, line);
            lineStream << line;

            lineStream >> read;
            if (read == "common")
            {
                while (!lineStream.eof())
                {
                    std::stringstream converter;
                    lineStream >> read;
                    i = read.find('=');
                    key = read.substr(0, i);
                    value = read.substr(i + 1);

                    converter << value;
                    if (key == "lineHeight")
                    {
                        converter >> m_lineHeight;
                    }
                    else if (key == "base")
                    {
                        converter >> m_base;
                    }
                    else if (key == "scaleW")
                    {
                        converter >> m_width;
                    }
                    else if (key == "scaleH")
                    {
                        converter >> m_height;
                    }
                    else if (key == "pages")
                    {
                        converter >> m_pages;
                    }
                    else if (key == "outline")
                    {
                        converter >> m_outline;
                    }
                }
            }
            else if (read == "char")
            {
                int charID = 0;

                while (!lineStream.eof())
                {
                    std::stringstream converter;
                    lineStream >> read;
                    i = read.find('=');
                    key = read.substr(0, i);
                    value = read.substr(i + 1);

                    converter << value;
                    if (key == "id")
                    {
                        converter >> charID;
                    }
                    else if (key == "x")
                    {
                        converter >> c.m_x;
                    }
                    else if (key == "y")
                    {
                        converter >> c.m_y;
                    }
                    else if (key == "width")
                    {
                        converter >> c.m_width;
                    }
                    else if (key == "height")
                    {
                        converter >> c.m_height;
                    }
                    else if (key == "xoffset")
                    {
                        converter >> c.m_xOffset;
                    }
                    else if (key == "yoffset")
                    {
                        converter >> c.m_yOffset;
                    }
                    else if (key == "xadvance")
                    {
                        converter >> c.m_xAdvance;
                    }
                    else if (key == "page")
                    {
                        converter >> c.m_page;
                    }
                }

                m_chars.insert(std::map<int, CharDescriptor>::value_type(charID, c));
            }
            else if (read == "kernings")
            {
                while (!lineStream.eof())
                {
                    std::stringstream converter;
                    lineStream >> read;
                    i = read.find('=');
                    key = read.substr(0, i);
                    value = read.substr(i + 1);

                    //assign the correct value
                    converter << value;
                    if (key == "count")
                    {
                        converter >> m_kernCount;
                    }
                }
            }
            else if (read == "kerning")
            {
                while (!lineStream.eof())
                {
                    std::stringstream converter;
                    lineStream >> read;
                    i = read.find('=');
                    key = read.substr(0, i);
                    value = read.substr(i + 1);

                    //assign the correct value
                    converter << value;
                    if (key == "first")
                    {
                        converter >> k.m_first; converter >> first;
                    }

                    else if (key == "second")
                    {
                        converter >> k.m_second; converter >> second;
                    }

                    else if (key == "amount")
                    {
                        converter >> k.m_amount; converter >> amount;
                    }
                }

                m_kearn.push_back(k);
            }
        }

        stream.close();
        return true;
    }

    int BitmapFont::getKerningPair(int first, int second)
    {
        if (m_kernCount && m_useKern)
        {
            for (int j = 0; j < m_kernCount; j++)
            {
                if (m_kearn[j].m_first == first && m_kearn[j].m_second == second)
                {
                    return m_kearn[j].m_amount;
                }
            }
        }
        return 0;
    }

    float BitmapFont::getStringWidth(const std::string& text)
    {
        float total = 0.f;
        CharDescriptor* f;
        for (unsigned int i = 0; i < text.size(); ++i)
        {
            f = &m_chars[text[i]];
            total += f->m_xAdvance;
        }
        return total * m_fscale;
    }

    std::vector<Vertex> BitmapFont::getVerticesFor(const std::string& text, int x, int y)
    {
        std::vector<Vertex> verts;
        int xStart = x;

        float invWidth = 1.f / m_width;
        float invHeight = 1.f / m_height;

        for (unsigned int i = 0; i < text.size(); ++i)
        {
            CharDescriptor c = m_chars[text[i]];

            // Vertex positions
            Vertex v0 = Vertex((float) xStart, (float) y + (float) c.m_height, 0.f);
            Vertex v1 = Vertex((float) xStart, (float) y, 0.f);
            Vertex v2 = Vertex((float) xStart + c.m_width, (float) y, 0.f);
            Vertex v3 = Vertex((float) xStart + c.m_width, (float) y + (float) c.m_height, 0.f);

            // UVs
            v0.m_uv.x = invWidth * (float) c.m_x;
            v0.m_uv.y = invHeight * (float) c.m_y;
            v1.m_uv.x = invWidth * (float) c.m_x;
            v1.m_uv.y = invHeight * (float) c.m_y + invHeight * (float) c.m_height;
            v2.m_uv.x = invWidth * (float) c.m_x + invWidth * (float) c.m_width;
            v2.m_uv.y = invHeight * (float) c.m_y + invHeight * (float) c.m_height;
            v3.m_uv.x = invWidth * (float) c.m_x + invWidth * (float) c.m_width;
            v3.m_uv.y = invHeight * (float) c.m_y;

            // Normals
            v0.m_nor = glm::vec3(0.f, 0.f, 1.f);
            v1.m_nor = glm::vec3(0.f, 0.f, 1.f);
            v2.m_nor = glm::vec3(0.f, 0.f, 1.f);
            v3.m_nor = glm::vec3(0.f, 0.f, 1.f);

            verts.push_back(v0);
            verts.push_back(v1);
            verts.push_back(v2);
            verts.push_back(v3);

            xStart += c.m_xAdvance;
        }
        return verts;
    }

    void BitmapFont::getVerticesFor(const std::string& text, float x, float y, std::vector<Vertex>& vertices, std::vector<unsigned short>& indices)
    {
        float xStart = x;

        float invWidth = 1.f / m_width;
        float invHeight = 1.f / m_height;

        std::vector<Vertex> tmpVerts;
        std::vector<unsigned short> tmpIndices;

        for (unsigned int i = 0; i < text.size(); ++i)
        {
            CharDescriptor c = m_chars[text[i]];

            // Vertex positions
            Vertex v0 = Vertex(xStart, y + c.m_height, 0.f);
            Vertex v1 = Vertex(xStart, y, 0.f);
            Vertex v2 = Vertex(xStart + c.m_width, y, 0.f);
            Vertex v3 = Vertex(xStart + c.m_width, y + c.m_height, 0.f);

            // UVs
            v0.m_uv.x = invWidth * c.m_x;
            v0.m_uv.y = invHeight * c.m_y;
            v1.m_uv.x = invWidth * c.m_x;
            v1.m_uv.y = invHeight * c.m_y + invHeight * c.m_height;
            v2.m_uv.x = invWidth * c.m_x + invWidth * c.m_width;
            v2.m_uv.y = invHeight * c.m_y + invHeight * c.m_height;
            v3.m_uv.x = invWidth * c.m_x + invWidth * c.m_width;
            v3.m_uv.y = invHeight * c.m_y;

            unsigned short vCount = tmpVerts.size();
            tmpIndices.push_back(vCount);
            tmpIndices.push_back(vCount + 1);
            tmpIndices.push_back(vCount + 2);
            tmpIndices.push_back(vCount + 2);
            tmpIndices.push_back(vCount + 3);
            tmpIndices.push_back(vCount);

            tmpVerts.push_back(v0);
            tmpVerts.push_back(v1);
            tmpVerts.push_back(v2);
            tmpVerts.push_back(v3);

            xStart += c.m_xAdvance;
        }

        vertices.swap(tmpVerts);
        indices.swap(tmpIndices);
    }

    void BitmapFont::appendVerticesFor(const std::string& text, int x, int y, std::vector<Vertex>& vertices)
    {
        int xStart = x;

        float invWidth = 1.f / m_width;
        float invHeight = 1.f / m_height;

        for (unsigned int i = 0; i < text.size(); ++i)
        {
            CharDescriptor c = m_chars[text[i]];

            // Vertex positions
            Vertex v0 = Vertex((float) xStart, (float) y + (float) c.m_height, 0.f);
            Vertex v1 = Vertex((float) xStart, (float) y, 0.f);
            Vertex v2 = Vertex((float) xStart + (float) c.m_width, (float) y, 0.f);
            Vertex v3 = Vertex((float) xStart + (float) c.m_width, (float) y + (float) c.m_height, 0.f);

            // UVs
            v0.m_uv.x = invWidth * (float) c.m_x;
            v0.m_uv.y = invHeight * (float) c.m_y;
            v1.m_uv.x = invWidth * (float) c.m_x;
            v1.m_uv.y = invHeight * (float) c.m_y + invHeight * (float) c.m_height;
            v2.m_uv.x = invWidth * (float) c.m_x + invWidth * (float) c.m_width;
            v2.m_uv.y = invHeight * (float) c.m_y + invHeight * (float) c.m_height;
            v3.m_uv.x = invWidth * (float) c.m_x + invWidth * (float) c.m_width;
            v3.m_uv.y = invHeight * (float) c.m_y;

            vertices.push_back(v0);
            vertices.push_back(v1);
            vertices.push_back(v2);
            vertices.push_back(v3);

            xStart += c.m_xAdvance;
        }
    }
}