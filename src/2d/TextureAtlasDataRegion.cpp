#include "stdafx.h"
#include "TextureAtlasDataRegion.h"

namespace Tremor
{
    TextureAtlasDataRegion::TextureAtlasDataRegion(AtlasPage& page,
                                                   int index,
                                                   int left,
                                                   int top,
                                                   int width,
                                                   int height,
                                                   const std::string& name,
                                                   bool rotate,
                                                   bool flip,
                                                   int originalWidth,
                                                   int originalHeight,
                                                   float offsetX,
                                                   float offsetY)
        : m_page(page),
        m_name(name),
        m_index(index),
        m_offsetX(offsetX),
        m_offsetY(offsetY),
        m_originalWidth(originalWidth),
        m_originalHeight(originalHeight),
        m_rotate(rotate),
        m_left(left),
        m_top(top),
        m_width(width),
        m_height(height),
        m_flip(flip),
        m_splits(nullptr),
        m_pads(nullptr)
    {

    }

    TextureAtlasDataRegion::~TextureAtlasDataRegion()
    {
        if (m_splits != nullptr)
        {
            delete m_splits;
            m_splits = nullptr;
        }
        if (m_pads != nullptr)
        {
            delete m_pads;
            m_pads = nullptr;
        }
    }

    AtlasPage& TextureAtlasDataRegion::getPage()
    {
        return m_page;
    }

    int TextureAtlasDataRegion::getIndex()
    {
        return m_index;
    }

    const std::string& TextureAtlasDataRegion::getName()
    {
        return m_name;
    }

    float TextureAtlasDataRegion::getOffsetX()
    {
        return m_offsetX;
    }

    float TextureAtlasDataRegion::getOffsetY()
    {
        return m_offsetY;
    }

    int TextureAtlasDataRegion::getOriginalWidth()
    {
        return m_originalWidth;
    }

    int TextureAtlasDataRegion::getOriginalHeight()
    {
        return m_originalHeight;
    }

    bool TextureAtlasDataRegion::shoulRotate()
    {
        return m_rotate;
    }

    int TextureAtlasDataRegion::getX()
    {
        return m_left;
    }

    int TextureAtlasDataRegion::getY()
    {
        return m_top;
    }

    int TextureAtlasDataRegion::getWidth()
    {
        return m_width;
    }

    int TextureAtlasDataRegion::getHeight()
    {
        return m_height;
    }

    bool TextureAtlasDataRegion::shouldFlip()
    {
        return m_flip;
    }
}