#pragma once

#include <vector>
#include "Texture.h"

namespace Tremor
{
    class TextureRegion
    {
    public:
        TextureRegion(Texture& texture);
        TextureRegion(Texture& texture, int width, int height);
        TextureRegion(Texture& texture, int x, int y, int width, int height);
        TextureRegion(Texture& texture, float u, float v, float u2, float v2);
        TextureRegion(const TextureRegion& other);
        TextureRegion(const TextureRegion& other, int x, int y, int width, int height);



        void setRegion(Texture& texture);
        void setRegion(int x, int y, int width, int height);
        void setRegion(float u, float v, float u2, float v2);
        void setRegion(const TextureRegion& region);
        void setRegion(const TextureRegion& region, int x, int y, int width, int height);

        Texture& getTexture();
        void setTexture(Texture& texture);
        int getRegionWidth() const;
        int getRegionHeight() const;
        int getRegionX() const;
        int getRegionY() const;
        void setRegionWidth(int width);
        void setRegionHeight(int height);
        void setRegionX(int x);
        void setRegionY(int y);
        float getU() const;
        float getV() const;
        float getU2() const;
        float getV2() const;
        void setU(float u);
        void setV(float v);
        void setU2(float u2);
        void setV2(float v2);
        void flip(bool x, bool y);
        bool isFlipX() const;
        bool isFlipY() const;
        void scroll(float xAmount, float yAmount);
        // std::vector<std::vector<TRTextureRegion*>> split(int tileWidth, int tileHeight);
        // static std::vector<std::vector<TRTextureRegion*>> split(TRTexture* texture, int tileWidth, int tileHeight);
    protected:
        Texture& m_texture;
        int m_regionWidth;
        int m_regionHeight;
        float m_u;
        float m_v;
        float m_u2;
        float m_v2;
    };
}