#pragma once

#include "AtlasPage.h"

namespace Tremor
{
    class TextureAtlasDataRegion
    {
    public:


        TextureAtlasDataRegion(AtlasPage& page,
                               int index, 
                               int left, 
                               int top, 
                               int width, 
                               int height, 
                               const std::string& name, 
                               bool rotate, 
                               bool flip,
                               int originalWidth,
                               int originalHeight,
                               float offsetX,
                               float offsetY);
        ~TextureAtlasDataRegion();

        AtlasPage& getPage();
        int getIndex();
        const std::string& getName();
        float getOffsetX();
        float getOffsetY();
        int getOriginalWidth();
        int getOriginalHeight();
        bool shoulRotate();
        int getX();
        int getY();
        int getWidth();
        int getHeight();
        bool shouldFlip();
    private:
        AtlasPage& m_page;
        int m_index;
        std::string m_name;
        float m_offsetX;
        float m_offsetY;
        int m_originalWidth;
        int m_originalHeight;
        bool m_rotate;
        int m_left;
        int m_top;
        int m_width;
        int m_height;
        bool m_flip;
        int* m_splits;
        int* m_pads;
    };
}