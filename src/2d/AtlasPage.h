#pragma once

#include <string>
#include "Enums2D.h"

namespace Tremor
{
    class Texture;

    class AtlasPage
    {
    public:
        AtlasPage(const std::string& filename, float width, float height, bool useMipMaps, TextureFilter minFilter, TextureFilter magFilter, TextureWrap uWrap, TextureWrap vWrap);

        const std::string& getFilename();
        Texture& getTexture();
        float getWidth();
        float getHeight();
        bool usesMipMaps();
        TextureFilter getMinFilter();
        TextureFilter getMagFilter();
        TextureWrap getWrapU();
        TextureWrap getWrapV();
    private:
        std::string m_texturefile;
        Texture& m_texture;
        float m_width;
        float m_height;
        bool m_useMipMaps;
        TextureFilter m_minFilter;
        TextureFilter m_magFilter;
        TextureWrap m_uWrap;
        TextureWrap m_vWrap;
    };
}