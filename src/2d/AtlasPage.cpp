#include "stdafx.h"
#include "AtlasPage.h"
#include "Texture.h"
#include "assets/AssetManager.h"

namespace Tremor
{
    AtlasPage::AtlasPage(const std::string& filename, float width, float height, bool useMipMaps, TextureFilter minFilter, TextureFilter magFilter, TextureWrap uWrap, TextureWrap vWrap)
        : m_texturefile(filename),
        m_texture(AssetManager::loadTexture(filename)),
        m_width(width),
        m_height(height),
        m_useMipMaps(useMipMaps),
        m_minFilter(minFilter),
        m_magFilter(magFilter),
        m_uWrap(uWrap),
        m_vWrap(vWrap)
    {

    }

    const std::string& AtlasPage::getFilename()
    {
        return m_texturefile;
    }

    Texture& AtlasPage::getTexture()
    {
        return m_texture;
    }

    float AtlasPage::getWidth()
    {
        return m_width;
    }

    float AtlasPage::getHeight()
    {
        return m_height;
    }

    bool AtlasPage::usesMipMaps()
    {
        return m_useMipMaps;
    }

    TextureFilter AtlasPage::getMinFilter()
    {
        return m_minFilter;
    }

    TextureFilter AtlasPage::getMagFilter()
    {
        return m_magFilter;
    }

    TextureWrap AtlasPage::getWrapU()
    {
        return m_uWrap;
    }

    TextureWrap AtlasPage::getWrapV()
    {
        return m_vWrap;
    }
}