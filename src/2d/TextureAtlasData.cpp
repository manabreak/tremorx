#include "stdafx.h"
#include "TextureAtlasData.h"
#include <fstream>
#include <sstream>
#include "utils/StringUtils.h"

namespace Tremor
{
    TextureAtlasData::TextureAtlasData(const std::string& packFile, const std::string& imagesDir, bool flip)
    {
        m_tuple.reserve(4);
        m_tuple.resize(4);

        // Get pack file's directory
        std::string packFileDir = packFile.substr(0, packFile.find_last_of('/') + 1);

        std::ifstream infile(packFile);
        std::string line;
        int lineNumber = 1;
        AtlasPage* lastPage = nullptr;
        while (std::getline(infile, line))
        {
            StringUtils::trim(line);

            if (line.size() == 0) continue;
            if (lastPage == nullptr)
            {
                std::string imageFile = packFileDir + std::string(line);
                float width = 0.f, height = 0.f;
                if (readTuple(infile) == 2)
                {
                    std::istringstream(m_tuple[0]) >> width;
                    std::istringstream(m_tuple[1]) >> height;
                    readTuple(infile);
                }

                int format;
                if (m_tuple[0] == "RGBA8888") format = TextureFormat::RGBA8888;

                readTuple(infile);
                TextureFilter min = TextureFilter::Nearest, mag = TextureFilter::Nearest;
                if (m_tuple[0] == "Nearest") min = TextureFilter::Nearest;
                else if (m_tuple[0] == "Linear") min = TextureFilter::Linear;
                if (m_tuple[1] == "Nearest") mag = TextureFilter::Nearest;
                else if (m_tuple[1] == "Linear") mag = TextureFilter::Linear;

                std::string direction = readValue(infile);
                TextureWrap repeatX = ClampToEdge;
                TextureWrap repeatY = ClampToEdge;
                if (direction == "x") repeatX = Repeat;
                else if (direction == "y") repeatY = Repeat;
                else if (direction == "xy") repeatX = repeatY = Repeat;

                // pageImage = new Page(imageFile, width, height, false, min, mag, repeatX, repeatY);
                // m_pages.push_back(pageImage);
                m_pages.emplace_back(imageFile, width, height, false, min, mag, repeatX, repeatY);
                lastPage = &m_pages[m_pages.size() - 1];
            }
            else
            {
                std::string rotate = readValue(infile);

                readTuple(infile);
                int left, top;
                std::istringstream(m_tuple[0]) >> left;
                std::istringstream(m_tuple[1]) >> top;

                readTuple(infile);
                int width, height;
                std::istringstream(m_tuple[0]) >> width;
                std::istringstream(m_tuple[1]) >> height;

                if (readTuple(infile) == 4)
                {
                    // TODO splits & pads
                    if (readTuple(infile) == 4)
                    {
                        readTuple(infile);
                    }
                }

                int originalWidth, originalHeight;
                std::istringstream(m_tuple[0]) >> originalWidth;
                std::istringstream(m_tuple[1]) >> originalHeight;

                readTuple(infile);
                float offsetX, offsetY;
                std::istringstream(m_tuple[0]) >> offsetX;
                std::istringstream(m_tuple[1]) >> offsetY;

                std::string indexString = readValue(infile);
                int index;
                std::istringstream(indexString) >> index;

                m_regions.emplace_back(*lastPage, index, left, top, width, height, line, rotate == "true" ? true : false, flip, originalWidth, originalHeight, offsetX, offsetY);
            }
        }
    }

    TextureAtlasData::~TextureAtlasData()
    {

    }

    std::string TextureAtlasData::readValue(std::ifstream& ifs)
    {
        std::string line;
        std::getline(ifs, line);
        int colon = line.find_first_of(':');
        assert(colon != std::string::npos);
        return StringUtils::trim(line.substr(colon + 1));
    }

    int TextureAtlasData::readTuple(std::ifstream& ifs)
    {
        std::string line;
        std::getline(ifs, line);
        int colon = line.find_first_of(':');
        assert(colon != std::string::npos);
        int i = 0, lastMatch = colon + 1;
        for (; i < 3; ++i)
        {
            int comma = line.find_first_of(',', lastMatch);
            if (comma == std::string::npos) break;
            m_tuple[i] = StringUtils::trim(line.substr(lastMatch, comma));
            lastMatch = comma + 1;
        }
        m_tuple[i] = StringUtils::trim(line.substr(lastMatch));
        return i + 1;
    }

    const std::vector<AtlasPage>& TextureAtlasData::getPages()
    {
        return m_pages;
    }

    AtlasPage& TextureAtlasData::getPage(unsigned int index)
    {
        return m_pages[index];
    }

    const std::vector<TextureAtlasDataRegion>& TextureAtlasData::getRegions()
    {
        return m_regions;
    }

    TextureAtlasDataRegion& TextureAtlasData::getRegion(unsigned int index)
    {
        return m_regions[index];
    }

    unsigned int TextureAtlasData::getPageCount()
    {
        return m_pages.size();
    }

    unsigned int TextureAtlasData::getRegionCount()
    {
        return m_regions.size();
    }
}