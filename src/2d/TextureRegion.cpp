#include "stdafx.h"
#include "TextureRegion.h"

namespace Tremor
{

    TextureRegion::TextureRegion(Texture& texture)
        : m_texture(texture)
    {
        setRegion(0, 0, texture.getWidth(), texture.getHeight());
    }

    TextureRegion::TextureRegion(Texture& texture, int width, int height)
        : m_texture(texture)
    {
        setRegion(0, 0, width, height);
    }

    TextureRegion::TextureRegion(Texture& texture, int x, int y, int width, int height)
        : m_texture(texture)
    {
        setRegion(x, y, width, height);
    }

    TextureRegion::TextureRegion(Texture& texture, float u, float v, float u2, float v2)
        : m_texture(texture)
    {
        setRegion(u, v, u2, v2);
    }

    TextureRegion::TextureRegion(const TextureRegion& other)
        : m_texture(other.m_texture),
        m_regionWidth(other.m_regionWidth),
        m_regionHeight(other.m_regionHeight),
        m_u(other.m_u),
        m_v(other.m_v),
        m_u2(other.m_u2),
        m_v2(other.m_v2)
    {

    }

    TextureRegion::TextureRegion(const TextureRegion& other, int x, int y, int width, int height)
        : m_texture(other.m_texture)
    {
        setRegion(other, x, y, width, height);
    }

    void TextureRegion::setRegion(Texture& texture)
    {
        m_texture = texture;
        setRegion(0, 0, m_texture.getWidth(), m_texture.getHeight());
    }

    void TextureRegion::setRegion(int x, int y, int width, int height)
    {
        float invTexWidth = 1.f / m_texture.getWidth();
        float invTexHeight = 1.f / m_texture.getHeight();
        setRegion(x * invTexWidth, y * invTexHeight, (x + width) * invTexWidth, (y + height) * invTexHeight);
    }

    void TextureRegion::setRegion(float u, float v, float u2, float v2)
    {
        int texWidth = m_texture.getWidth();
        int texHeight = m_texture.getHeight();
        m_regionWidth = (int) glm::round(glm::abs(u2 - u) * texWidth);
        m_regionHeight = (int) glm::round(glm::abs(v2 - v) * texHeight);

        if (m_regionWidth == 1 && m_regionHeight == 1)
        {
            float adjustX = 0.25f / texWidth;
            u += adjustX;
            u2 += adjustX;
            float adjustY = 0.25f / texHeight;
            v += adjustY;
            v2 += adjustY;
        }

        m_u = u;
        m_v = v;
        m_u2 = u2;
        m_v2 = v2;
    }

    void TextureRegion::setRegion(const TextureRegion& region)
    {
        m_texture = region.m_texture;
        setRegion(region.m_u, region.m_v, region.m_u2, region.m_v2);
    }

    void TextureRegion::setRegion(const TextureRegion& region, int x, int y, int width, int height)
    {
        m_texture = region.m_texture;
        setRegion(region.getRegionX() + x, region.getRegionY() + y, width, height);
    }

    Texture& TextureRegion::getTexture()
    {
        return m_texture;
    }

    void TextureRegion::setTexture(Texture& texture)
    {
        m_texture = texture;
    }

    int TextureRegion::getRegionWidth() const
    {
        return m_regionWidth;
    }

    int TextureRegion::getRegionHeight() const
    {
        return m_regionHeight;
    }

    int TextureRegion::getRegionX() const
    {
        return (int) glm::round(m_u * m_texture.getWidth());
    }

    int TextureRegion::getRegionY() const
    {
        return (int) glm::round(m_v * m_texture.getHeight());
    }

    void TextureRegion::setRegionWidth(int width)
    {
        setU2(m_u + width / (float) m_texture.getWidth());
    }

    void TextureRegion::setRegionHeight(int height)
    {
        setV2(m_v + height / (float) m_texture.getHeight());
    }

    void TextureRegion::setRegionX(int x)
    {
        setU(x / (float) m_texture.getWidth());
    }

    void TextureRegion::setRegionY(int y)
    {
        setV(y / (float) m_texture.getHeight());
    }

    float TextureRegion::getU() const
    {
        return m_u;
    }

    float TextureRegion::getV() const
    {
        return m_v;
    }

    float TextureRegion::getU2() const
    {
        return m_u2;
    }

    float TextureRegion::getV2() const
    {
        return m_v2;
    }

    void TextureRegion::setU(float u)
    {
        m_u = u;
        m_regionWidth = (int) glm::round(glm::abs(m_u2 - m_u) * m_texture.getWidth());
    }

    void TextureRegion::setV(float v)
    {
        m_v = v;
        m_regionHeight = (int) glm::round(glm::abs(m_v2 - m_v) * m_texture.getHeight());
    }

    void TextureRegion::setU2(float u2)
    {
        m_u2 = u2;
        m_regionWidth = (int) glm::round(glm::abs(m_u2 - m_u) * m_texture.getWidth());
    }

    void TextureRegion::setV2(float v2)
    {
        m_v2 = v2;
        m_regionHeight = (int) glm::round(glm::abs(m_v2 - m_v) * m_texture.getHeight());
    }

    void TextureRegion::flip(bool x, bool y)
    {
        if (x)
        {
            float temp = m_u;
            m_u = m_u2;
            m_u2 = temp;
        }
        if (y)
        {
            float temp = m_v;
            m_v = m_v2;
            m_v2 = temp;
        }
    }

    bool TextureRegion::isFlipX() const
    {
        return m_u > m_u2;
    }

    bool TextureRegion::isFlipY() const
    {
        return m_v > m_v2;
    }

    void TextureRegion::scroll(float xAmount, float yAmount)
    {
        if (xAmount != 0.f)
        {
            float width = (m_u2 - m_u) * m_texture.getWidth();
            m_u = glm::mod(m_u + xAmount, 1.f);
            m_u2 = m_u + width / m_texture.getWidth();
        }
        if (yAmount != 0.f)
        {
            float height = (m_v2 - m_v) * m_texture.getHeight();
            m_v = glm::mod(m_v + yAmount, 1.f);
            m_v2 = m_v + height / m_texture.getHeight();
        }
    }

    /*
    std::vector<std::vector<TextureRegion*>> TextureRegion::split(int tileWidth, int tileHeight)
    {

    }

    std::vector<std::vector<TextureRegion*>> TextureRegion::split(Texture* texture, int tileWidth, int tileHeight)
    {

    }
    */
}