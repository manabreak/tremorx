#include "stdafx.h"
#include "Text2D.h"
#include "BitmapFont.h"
#include "rendering/SpriteBatch.h"

namespace Tremor
{
    Text2D::Text2D(BitmapFont& font)
        : m_font(font),
        m_text(""),
        m_scale(1.f, 1.f),
        m_visible(true),
        m_rotation(0.f),
        m_color(1.f, 1.f, 1.f, 1.f)
    {

    }

    Text2D::Text2D(BitmapFont& font, const std::string& text)
        : m_font(font),
        m_text(text),
        m_scale(1.f, 1.f),
        m_visible(true),
        m_rotation(0.f),
        m_color(1.f, 1.f, 1.f, 1.f)
    {
        refresh();
    }

    void Text2D::refresh()
    {
        m_font.getVerticesFor(m_text, m_position.x, m_position.y, m_vertices, m_indices);
        
    }

    void Text2D::refreshScale(const glm::vec2& oldScale)
    {
        for (unsigned int i = 0; i < m_vertices.size(); ++i)
        {
            Vertex& v = m_vertices[i];
            glm::vec3& p = v.m_pos;
            p.x = ((p.x - m_position.x) / oldScale.x) * m_scale.x + m_position.x;
            p.y = ((p.y - m_position.y) / oldScale.y) * m_scale.y + m_position.y;
        }
    }

    void Text2D::setText(const std::string& text)
    {
        if (m_text != text)
        {
            m_text = text;
            refresh();
            refreshScale(glm::vec2(1.f, 1.f));
        }
    }

    const std::string& Text2D::getText()
    {
        return m_text;
    }

    void Text2D::setFont(BitmapFont& font)
    {
        if (&font != &m_font)
        {
            m_font = font;
            refresh();
        }
    }

    BitmapFont& Text2D::getFont()
    {
        return m_font;
    }

    int Text2D::getLength()
    {
        return m_text.length();
    }

    float Text2D::getWidth()
    {
        return m_size.x;
    }

    float Text2D::getHeight()
    {
        return m_size.y;
    }

    float Text2D::getRotation()
    {
        return m_rotation;
    }

    void Text2D::setRotation(float rotation)
    {
        if (m_rotation != rotation)
        {
            m_rotation = rotation;
        }
    }

    void Text2D::setVisible(bool visible)
    {
        m_visible = visible;
    }

    bool Text2D::isVisible()
    {
        return m_visible;
    }

    void Text2D::setRGB(float r, float g, float b)
    {
        m_color.r = r;
        m_color.g = g;
        m_color.b = b;
        for (unsigned int i = 0; i < m_vertices.size(); ++i)
        {
            m_vertices[i].m_col = m_color;
        }
    }

    void Text2D::setRGBA(float r, float g, float b, float a)
    {
        m_color.r = r;
        m_color.g = g;
        m_color.b = b;
        m_color.a = a;
        for (unsigned int i = 0; i < m_vertices.size(); ++i)
        {
            m_vertices[i].m_col = m_color;
        }
    }

    void Text2D::setAlpha(float a)
    {
        m_color.a = a;
        for (unsigned int i = 0; i < m_vertices.size(); ++i)
        {
            m_vertices[i].m_col = m_color;
        }
    }

    float Text2D::getR()
    {
        return m_color.r;
    }

    float Text2D::getG()
    {
        return m_color.g;
    }

    float Text2D::getB()
    {
        return m_color.b;
    }

    float Text2D::getAlpha()
    {
        return m_color.a;
    }

    void Text2D::setScale(float scale)
    {
        if (scale != m_scale.x || scale != m_scale.y)
        {
            glm::vec2 oldScale = m_scale;
            m_scale.x = scale;
            m_scale.y = scale;
            refreshScale(oldScale);
        }
    }

    void Text2D::setScaleX(float x)
    {
        if (x != m_scale.x)
        {
            glm::vec2 oldScale = m_scale;
            m_scale.x = x;
            refreshScale(oldScale);
        }
    }

    void Text2D::setScaleY(float y)
    {
        if (y != m_scale.y)
        {
            glm::vec2 oldScale = m_scale;
            m_scale.y = y;
            refreshScale(oldScale);
        }
    }

    float Text2D::getScaleX()
    {
        return m_scale.x;
    }

    float Text2D::getScaleY()
    {
        return m_scale.y;
    }

    bool Text2D::isUniformScale()
    {
        return m_scale.x == m_scale.y;
    }

    const std::vector<Vertex>& Text2D::getVertices()
    {
        return m_vertices;
    }

    const std::vector<unsigned short>& Text2D::getIndices()
    {
        return m_indices;
    }

    void Text2D::setPosition(float x, float y)
    {
        if (m_position.x != x || m_position.y != y)
        {
            float oldX = m_position.x;
            float oldY = m_position.y;
            m_position.x = x;
            m_position.y = y;
            for (unsigned int i = 0; i < m_vertices.size(); ++i)
            {
                glm::vec3& p = m_vertices[i].m_pos;
                p.x = (p.x - oldX) + x;
                p.y = (p.y - oldY) + y;
            }
        }
    }

    void Text2D::setX(float x)
    {
        if (m_position.x != x)
        {
            float oldX = m_position.x;
            m_position.x = x;
            for (unsigned int i = 0; i < m_vertices.size(); ++i)
            {
                // m_vertices[i].m_pos.x = x;
                glm::vec3& p = m_vertices[i].m_pos;
                p.x = (p.x - oldX) + x;
            }
        }
    }

    void Text2D::setY(float y)
    {
        if (m_position.y != y)
        {
            float oldY = m_position.y;
            m_position.y = y;
            for (unsigned int i = 0; i < m_vertices.size(); ++i)
            {
                // m_vertices[i].m_pos.y = y;
                glm::vec3& p = m_vertices[i].m_pos;
                p.y = (p.y - oldY) + y;
            }
        }
    }

    float Text2D::getX()
    {
        return m_position.x;
    }

    float Text2D::getY()
    {
        return m_position.y;
    }

    void Text2D::render(SpriteBatch& batch)
    {
        batch.drawText(*this);
    }
}