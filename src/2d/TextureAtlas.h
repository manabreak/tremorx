#pragma once

#include <vector>
#include <unordered_set>
#include "AtlasRegion.h"
#include "components/Sprite.h"

namespace Tremor
{
    class TextureAtlasData;

    class TextureAtlas
    {
    public:
        

        TextureAtlas(const std::string& packfile);
        ~TextureAtlas();
        AtlasRegion& addRegion(const std::string& name, Texture& texture, int x, int y, int width, int height);
        AtlasRegion& addRegion(const std::string& name, TextureRegion& region);
        
        std::vector<AtlasRegion>& getRegions();
        AtlasRegion& findRegion(const std::string& name);
        Sprite& createSprite(Tecs::Entity& entity, const std::string& name);

        const std::string& getFilename();
    private:
        std::string m_filename;
        void load(TextureAtlasData& data);
        std::vector<AtlasRegion> m_regions;
    };

}